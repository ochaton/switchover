#!/usr/bin/env tarantool
local fio = require 'fio'
local cluster_name = fio.basename(debug.getinfo(1, "S").source:sub(2)):match('switchover_([^.]+)')
_G.arg = { [-1] = arg[-1], [0] = arg[0], '--config', '/etc/switchover/autofailover.yaml', 'watch', 'start', cluster_name }
dofile('/usr/bin/switchover')
