std = "tarantool"
codes = true
max_line_length = 200
files = { "switchover/", "switchover.lua" }
exclude_files = {
	"api/",
	".rocks",
	"test",
	"tools",
	"switchover/_net_url.lua",
	"tmp",
	"lua_modules",
	".luarocks",
	"switchover/*_test.lua",
}

files["switchover/top.lua"] = {
	max_line_length = 300,
	ignore = {"211"},
}
