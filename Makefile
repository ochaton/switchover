build:
	tarantoolctl rocks install --tree $(DESTDIR) --only-deps switchover-scm-1.rockspec

install:
	tarantoolctl rocks make --tree $(DESTDIR)/usr switchover-scm-1.rockspec
	install -dm 0755 $(DESTDIR)/usr/bin
	install -m 0755 switchover.lua $(DESTDIR)/usr/bin/switchover
	install -dm 0755 $(DESTDIR)/etc/switchover
	install -pm 0644 switchover.yaml $(DESTDIR)/etc/switchover/config.yaml.example

.PHONY: switchover

run-%:
	make -C test $*
	docker ps

stop:
	make -C test stop-etcd
	make -C test stop

stop-%:
	make -C test $@

build-test-docker:
	docker build -t $$SWITCHOVER_TEST_IMAGE_TAG -f Dockerfile.test .

switchover%:
	docker build -t switchover -f Dockerfile.test .
	docker run --name $@ -v $$(pwd):/source -h $@ --rm --net tt_net -it switchover /bin/sh

test-etcd-parsing: run-etcd
	docker run -e LUACOV_ENABLE=true --rm --network tt_net --name switchover_test -v $$(pwd):/source \
		$$SWITCHOVER_TEST_IMAGE_TAG \
		/source/test_in_docker.sh

test-single: clear run-etcd run-single
	docker run -e LUACOV_ENABLE=true --rm --network tt_net \
		-v /var/run/docker.sock:/var/run/docker.sock \
		--name switchover -v $$(pwd):/source $$SWITCHOVER_TEST_IMAGE_TAG \
		/source/test_suite.sh "single";

test-vshard: clear run-etcd run-vshard
	docker run -e LUACOV_ENABLE=true --rm --network tt_net \
		-v /var/run/docker.sock:/var/run/docker.sock \
		--name switchover -v $$(pwd):/source $$SWITCHOVER_TEST_IMAGE_TAG \
		/source/test_suite.sh "vshard";

test-release: clear run-etcd run-single run-vshard run-cluster run-proxy
	docker run -e LUACOV_ENABLE=true --rm --network tt_net \
		-v /var/run/docker.sock:/var/run/docker.sock \
		--name switchover -v $$(pwd):/source $$SWITCHOVER_TEST_IMAGE_TAG \
		/source/test_suite.sh "release";

coverage-report:
	docker run -e CI_COMMIT_REF_NAME=$$CI_COMMIT_REF_NAME -e CI_COMMIT_SHA=$$CI_COMMIT_SHA \
		-e COVERALLS_TOKEN=$$COVERALLS_TOKEN \
		--rm --name switchover -v $$(pwd):/source $$SWITCHOVER_TEST_IMAGE_TAG \
		/bin/sh -c 'git checkout -B "$$CI_COMMIT_REF_NAME" "$$CI_COMMIT_SHA" && luacov-console /source && luacov-console -s && luacov-coveralls -v -t $$COVERALLS_TOKEN'

clear:
	make stop-vshard || true
	make stop-single || true
	make stop-cluster || true
	make stop-proxy || true
	make stop-etcd || true
