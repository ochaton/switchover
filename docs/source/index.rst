.. switchover documentation master file, created by
   sphinx-quickstart on Sat Sep 25 23:36:33 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to switchover's documentation!
======================================

Switchover – это тулза для консистентного переключения мастера в шарде для `tarantool <https://tarantool.io>`_ ≥ 1.10.4.

По своей сути, представляет из себя многофайловый Lua-скрипт, исполняемый tarantool на отдельном хосте.

Предоставляет максимум функциональности при использовании вместе с ETCD и сконфигурированными инстансами при помощи `moonlibs/config <https://github.com/moonlibs/config>`_ (начиная с версии v4).

На данный момент поддерживаются топологии ``etcd.cluster.master`` и ``etcd.instance.read_only``.

.. toctree::
   :maxdepth: 2
   :caption: Contents

   installation
   usage
