Promote - внештатное продвижение узла в мастеры
===============================================

Promote - метод ``switchover`` позволяющий во время аварии продвинуть реплику до мастера.

Включает минимум проверок, полагается на то, что "Вы знаете, что делаете!". Актуализирует состояние ETCD.

Работает только, если мастер в репликасете не найден!

.. code-block:: bash

  Usage: switchover 2.10 promote [-h] [--max-lag <max_lag>]
        [--no-reload] [--no-etcd-lock] [--no-fence-master]
        [--valid-status <valid_status>] [--allow-vshard]
        [--no-reload-routers] <instance>

  Promote fails when master exists in replicaset. Supports only tarantool >= 1.10.0
  Examples:
          switchover promote mycluster/mycluster_002@mycluster_002_01 - Makes mycluster_002_01 master of mycluster_002
          switchover promote mysingle@mysingle_001                    - Makes mysingle_001 master of mysingle
          switchover promote --no-etcd tnt1:3301                      - Makes tnt1:3301 master of it's replicaset

  Arguments:

    instance                                Choose instance to become master

  Options:

    -h, --help                              Show this help message and exit.
    --max-lag <max_lag>                     Maximum allowed Replication lag of the future master (in seconds)
    --no-reload                             Disables execution of package.reload on candidate and old master (default: enabled)
    --no-etcd-lock                          Disables ETCD mutexes and discovery
    --no-fence-master                       Does not force master to RO before promote candidate
    --valid-status <valid_status>           List of extra statuses (except running) valid for candidate (ex: orphan,loading)
    --allow-vshard                          Allows promote for vshard cluster (Use for your own risk)
    --no-reload-routers                     Disable routers reload if were discovered via etcd.cluster.vshard


В качестве аргумента принимает **что-то** из чего можно достать информацию об инстансе.
При использовании ETCD (рекомендуемый вариант) требует указание полного имени инстанса.

Полное имя задается как <application-name>/<shard-name>@<instance-name>.
Либо доступна сокращенная версия <application-name>@<instance-name>.

.. code-block:: bash

  $ switchover promote myapp@instance_001_02
  $ switchover promote myapp/shard_001@instance_001_02


Алгоритм promote
****************

**promote требует отсутствие работающего мастера в репликасете**

promote можно использовать в случае потери мастера (отказ узла, оборудования, сетевой связности).

Рекомендуется использовать, когда важнее восстановить доступность сервиса, нежели гарантированную согласованность всех данных.

Сам алгоритм еще проще, чем **switch**:

#. **promote** получает полную информацию о репликасете (состояние репликации, лаг репликации, расхождение vclock и тп)
#. **promote** проверяет отсутствие мастера в репликасете.
#. проверяет легитимность ``candidate`` для становления лидером:
    #. присутствие в vshard кластере (можно разрешить ``--allow-vshard``),
    #. наличие RAFT (отключить проверку нельзя),
    #. проверяет статус инстанса (разрешен только ``running``, можно поменять с ``--valid-status``, но не рекомендуется),
#. **promote** берет mutex в ETCD в шарде (можно отключить ``--no-etcd-lock``, но не рекомендуется),
#. (крит) выполняет перевод текущего мастера в RO (``box.cfg{read_only=true}``) если это возможно
#. (крит) выполняет перевод кандидата в RW (``box.cfg{read_only=false}``)
#. снимает ETCD mutex
#. вызывает ``package.reload`` на кандидате (можно отключить с ``--no-reload``)
#. вызывает ``package.reload`` на старом мастере (можно отключить с ``--no-reload``) если это возможно
#. вызывает ``package.reload`` на роутерах, если они корректно сконфигурированы в ETCD и доступны (можно отключить с ``--no-reload-routers``)
#. вызывает финальный discovery для отображения нового состояния шарда.

На проведение действий в критической секции может быть задан таймаут (``--timeout``), по-умолчанию равен ``2*max_lag`` (2 секунды).

Обычно, проведение переключения критической секции выполняется за 10мс.

В случае фатальных проблем во время выполнения критической секции, кластер может остаться в Read-Only, получение 2х мастеров без внешнего вмешательства невозможно.

.. code-block:: bash

  $ switchover promote vshard@storage_001_02

  storage_001_01:3301: Invalid argument
  storage_001_01:3301: Invalid argument
  2022-03-10T10:45:11.871 +3035.3ms Took to long to discovery all instances. Continuing with instances which were discovered (@discovery.lua:118:44-136)
  2022-03-10T10:45:11.871 +3035.4ms Discovered 1/2 nodes from storage_001_01:3301,storage_001_02:3301 in 3.035s (@discovery.lua:129:44-136)
  2022-03-10T10:45:11.871 +3035.7ms Candidate id:2 storage_001_02 dc:no role:replica status:running uptime:10h14m2s ups:0 dws:0 q:81.25%/0.25GB v:[2942804] can be next leader (@_switch_promote_common.lua:85:60-88)
  2022-03-10T10:45:11.872 +3036.0ms Taking mutex key: vshard/storage_001_switchover (@promote.lua:90:37-179)
  2022-03-10T10:45:11.883 +3047.8ms Candidate 2/storage_001_02:3301 was promoted. Performing discovery (@promote.lua:123:37-179)
  2022-03-10T10:45:11.895 +3059.2ms Changing master in ETCD: storage_001_01 -> storage_001_02 (@heal.lua:45:17-60)
  2022-03-10T10:45:11.906 +3070.4ms ETCD response: {"action":"compareAndSwap","node":{"createdIndex":26,"modifiedIndex":63,"key":"\/apps\/vshard\/clusters\/storage_001\/master","value":"storage_001_02"},"prevNode":{"createdIndex":26,"modifiedIndex":26,"key":"\/apps\/vshard\/clusters\/storage_001\/master","value":"storage_001_01"}} (@heal.lua:52:17-60)
  2022-03-10T10:45:11.906 +3070.5ms Calling package.reload on id:2 storage_001_02 dc:no role:master  status:running uptime:10h14m2s ups:0 dws:0 q:81.25%/0.25GB v:[2942804] (@_tarantool.lua:550:545-556)
  2022-03-10T10:45:13.205 +4369.4ms Found routers in ETCD: router_002,router_003,router_001 (@promote.lua:155:37-179)
  2022-03-10T10:45:13.205 +4369.6ms Got shards 3: router_002,router_003,router_001 (@discovery.lua:191:187-247)
  2022-03-10T10:45:13.215 +4379.7ms Discovered 1/1 nodes from router_003:3301 in 4.414s (@discovery.lua:129:44-136)
  2022-03-10T10:45:13.216 +4380.3ms Discovered 1/1 nodes from router_001:3301 in 4.414s (@discovery.lua:129:44-136)
  2022-03-10T10:45:13.220 +4384.1ms Discovered 1/1 nodes from router_002:3301 in 4.415s (@discovery.lua:129:44-136)
  2022-03-10T10:45:13.220 +4384.7ms Calling package.reload on router router_002 (@promote.lua:167:37-179)
  2022-03-10T10:45:13.220 +4384.9ms Calling package.reload on id:1 router_002 dc:no role:master  status:running uptime:10h14m3s ups:0 dws:0 q:12.50%/0.25GB v:[4] (@_tarantool.lua:550:545-556)
  2022-03-10T10:45:14.822 +5986.1ms Calling package.reload on router router_003 (@promote.lua:167:37-179)
  2022-03-10T10:45:14.822 +5986.2ms Calling package.reload on id:1 router_003 dc:no role:master  status:running uptime:10h14m5s ups:0 dws:0 q:12.50%/0.25GB v:[4] (@_tarantool.lua:550:545-556)
  2022-03-10T10:45:16.453 +7617.2ms Calling package.reload on router router_001 (@promote.lua:167:37-179)
  2022-03-10T10:45:16.453 +7617.3ms Calling package.reload on id:1 router_001 dc:no role:master  status:running uptime:10h14m7s ups:0 dws:0 q:12.50%/0.25GB v:[4] (@_tarantool.lua:550:545-556)
  storage_001_01:3301: Invalid argument
  storage_001_01:3301: Invalid argument
  2022-03-10T10:45:21.091 +12255.1ms Took to long to discovery all instances. Continuing with instances which were discovered (@discovery.lua:118:44-136)
  2022-03-10T10:45:21.091 +12255.3ms Discovered 1/2 nodes from storage_001_01:3301,storage_001_02:3301 in 12.255s (@discovery.lua:129:44-136)

  storage_001_01 etcd:replica storage_001_01:3301 - not connected
  storage_001_02 etcd:master  id:2 storage_001_02 dc:no role:master  status:running uptime:10h14m11s ups:0 dws:0 q:81.25%/0.25GB v:[2942804,394] self


