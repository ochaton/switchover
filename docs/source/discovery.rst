Discovery
=========

Discovery - это основной метод ``switchover``. Выдает информацию о репликасете.

.. code-block:: bash

	Usage: switchover 2.1.0 discovery [-h] [-g] [-l] <endpoints|shard>

	Some of the examples:
        switchover discovery cluster_name/shard_name     - discovers nodes inside single shard inside cluster (requires ETCD)
        switchover discovery cluster_name@instance_name  - discovers nodes inside single shard inside cluster (requires ETCD)
        switchover discovery mysingle                    - discovers nodes from the single replicaset (not cluster, required ETCD)
        switchover discovery tnt1:3301,tnt2:3301         - performs upstream-first search of the replicas (requires full-mesh, no ETCD)


	Arguments:
   		endpoints|shard         host:port to tarantool or name of replicaset

	Options:
		-h, --help              Show this help message and exit.
		-g, --show-graph        Prints topology to the console in dot format
		-l, --link-graph        Build url to online visualization


В качестве аргументов принимает **что-то** из чего можно достать информацию о шарда.

Discovery с указанием ip:port через ``,``
*****************************************

Самый базовый вариант, выдающий минимум информации о репликасете.

Необходимо перечислить список URI до инстансов, разделенных через ``,`` ОДНОГО репликасата.

.. code-block:: bash

	$ switchover discovery 172.18.0.8:3301,172.18.0.7:3301

	2022-01-23T17:54:45.847 +77.3ms @discovery.lua:103> Discovered 3/2 nodes from 172.18.0.8:3301,172.18.0.7:3301 in 0.077s
	id:1 172.18.0.8:3301 role:replica status:running uptime:1h30m5s ups:2 dws:2 q:50.00%/1.00GB v:[3707168,2495749,952087] ok/3:follow/0.000s
	id:2 single_003:3301 role:replica status:running uptime:1h30m5s ups:2 dws:2 q:50.00%/1.00GB v:[3707168,2495749,952087] ok/3:follow/0.000s
	id:3 172.18.0.7:3301 role:master  status:running uptime:1h30m5s ups:2 dws:2 q:50.00%/1.00GB v:[3707168,2495749,952087] self

В таком варианте ``switchover`` пишет, что он нашел 3 инстанса из 2 указанных. После распечатал информацию о каждом инстансе, отсортировав по ``box.info.id``.

``id:1 172.18.0.8:3301 role:replica status:running uptime:1h22m45s ups:2 dws:2 q:50.00%/1.00GB v:[3707168,2495749,952087] ok/3:follow/0.000s``

``id:1`` говорит, что ``box.info.id`` у этого инстанса равна 1.

``endpoint``, то по какому адресу ``switchover`` подключался к инстансу.

``role:replica`` указывает на значение ``box.info.ro``.

``status:running`` указывает на значение ``box.info.status``.

``uptime:1h22m45s`` конвертирует значение ``box.info.uptime`` к часам и минутам.

``ups:2`` говорит, что этот инстанс имеет 2 активных upstream (коннектов, с которых Тарантул скачивает данные по репликации).

``dws:2`` говорит, что этот инстанс имеет 2 активных реплики (downstreams) - коннектов, которым Тарантул отдает свои данные по репликации.

``q:50.00%/1.00GB`` показывает информацию о свободном месте на арене для ``memtx_memory``. В данном случае, это 50% выданной арены (1GB) уже аллоцировано.

``v:[3707168,2495749,952087]`` это ``box.info.vclock`` версия данных в инстансе.

``ok/3:follow/0.000s`` показывает информацию о репликации c нодой 3. Также показывается статус репликации и лаг данных.


Discovery с указанием имени в ETCD
**********************************

Для использования такого формата discovery, необходимо изначально сложить информацию о репликасете/кластере репликасетов/прокси в ETCD.

Для полноценной работы, необходимо использовать `moonlibs/config <https://github.com/moonlibs/config>`_.

Ниже несколько примеров конфигов репликасета/кластера/прокси

Конфиг ETCD репликасета
***********************

.. code-block:: yaml

    apps:
      single:
        common:
          box:
            log_level: 5
        clusters:
          single:
            master: single_001
            replicaset_uuid: '5713e7b4-29d2-4b1c-b6ff-1f2f8bae0d28'
        instances:
          single_001:
            cluster: single
            box:
              instance_uuid: '1a154d2c-2aad-4785-b7fe-ae5ad41fc4b6'
              listen: single_001:3301
          single_002:
            cluster: single
            box:
              instance_uuid: '20ebffd7-f40e-4c26-8263-a2382f2edafe'
              listen: single_002:3301
          single_003:
            cluster: single
            box:
              listen: single_003:3301
              instance_uuid: 'ce343d92-3640-4146-8655-abb7d3501be7'

Конфиг ETCD кластера
********************

.. code-block:: yaml

    apps:
      cluster:
        common:
          box:
            log_level: 5
            replication_connect_quorum: 2
        clusters:
          shard_001:
            master: shard_001_01
            replicaset_uuid: "77181c35-ae29-5af8-af2f-3f92dfb502b5"
          shard_002:
            master: shard_002_01
            replicaset_uuid: "58c87276-4f42-55a5-b217-e3bd4c538186"
        instances:
          shard_001_01:
            cluster: shard_001
            box:
              instance_uuid: "fb6c25c7-1d52-5287-a556-2d02add10a3f"
              listen: shard_001_01:3301
          shard_001_02:
            cluster: shard_001
            box:
              instance_uuid: "44db828d-93ed-594f-9c88-a62ed0de8430"
              listen: shard_001_02:3301
          shard_002_01:
            cluster: shard_002
            box:
              instance_uuid: "f840bc35-a97f-5252-9144-ed44560747d7"
              listen: shard_002_01:3301
          shard_002_02:
            cluster: shard_002
            box:
              instance_uuid: "af217aa9-f49b-5c74-86f5-934b3d39c413"
              listen: shard_002_02:3301

Конфиг прокси
*************

.. code-block:: yaml

    apps:
      proxy:
        common:
          box:
            log_level: 5
            replication_connect_quorum: 2
        instances:
          proxy_001:
            box:
              instance_uuid: '1a154d2c-2aad-4785-b7fe-ae5ad41fc4b6'
              listen: proxy_001:3301
          proxy_002:
            box:
              instance_uuid: '20ebffd7-f40e-4c26-8263-a2382f2edafe'
              listen: proxy_002:3301
          proxy_003:
            box:
              listen: proxy_003:3301
              instance_uuid: 'ce343d92-3640-4146-8655-abb7d3501be7'

Конфиг switchover для использования ETCD
****************************************

.. code-block:: yaml

    etcd:
      prefix: /apps
      timeout: 3
      endpoints:
        - http://etcd0:2379
        - http://etcd1:2379
        - http://etcd2:2379
      # http_params:
      #   headers:
      #     User-Agent: Switchover
      # login: ''
	  # password: ''

В ``etcd.endpoints`` мы указываем списком эндпоинты до ETCD нод, не забывая ``http://``.

В etcd.login, etcd.password можно указать креды для доступа. Будет использоваться Basic Auth.

``etcd.prefix`` -- это root для switchover. Именно в этом префиксе он будет искать приложения на Тарантуле.

Как видно из примеров ETCD конфигов выше, мы зарегистрировали 3 приложения ``/apps/single``, ``/apps/cluster`` и ``/apps/proxy``.

Использование Discovery с ETCD
******************************

Если инстансы Тарантула зарегистрированы в ETCD, то мы можем перейти к более простому discovery.

.. code-block:: bash

	$ switchover discovery single/single

	single_001 etcd:replica id:1 single_001:3301 role:replica status:running uptime:1h59m25s ups:2 dws:2 q:50.00%/1.00GB v:[3707168,2495749,952087] ok/single_002:follow/0.000s
	single_002 etcd:master  id:3 single_002:3301 role:master  status:running uptime:1h59m25s ups:2 dws:2 q:50.00%/1.00GB v:[3707168,2495749,952087] self
	single_003 etcd:replica id:2 single_003:3301 role:replica status:running uptime:1h59m25s ups:2 dws:2 q:50.00%/1.00GB v:[3707168,2495749,952087] ok/single_002:follow/0.000s

Мы можем указывать имя до шарда, информацию о котором мы хотим получить в формате ``<application-name>/<shard-name>``.

В данном случае имя приложения совпадает с именем шарда.

Для кластерных приложений это всегда не так:

.. code-block:: bash

	$ switchover discovery cluster/shard_001

	shard_001_01 etcd:master  id:2 shard_001_01:3301 role:master  status:running uptime:1h59m37s ups:1 dws:1 q:100.00%/0.25GB v:[2685565,1223699] self
	shard_001_02 etcd:replica id:1 shard_001_02:3301 role:replica status:running uptime:1h59m37s ups:1 dws:1 q:100.00%/0.25GB v:[2685565,1223699] ok/shard_001_01:follow/0.000s

Здесь ``cluster`` -- это имя приложения (первый уровень в ``/apps``), а ``shard_001`` -- это имя шарда (в ``/apps/cluster/clusters/shard_001``).

Для удобства работы с не-кластерными приложениями, вторую часть можно опускать:

.. code-block:: bash

	$ switchover discovery single

	single_001 etcd:replica id:1 single_001:3301 role:replica status:running uptime:1h59m25s ups:2 dws:2 q:50.00%/1.00GB v:[3707168,2495749,952087] ok/single_002:follow/0.000s
	single_002 etcd:master  id:3 single_002:3301 role:master  status:running uptime:1h59m25s ups:2 dws:2 q:50.00%/1.00GB v:[3707168,2495749,952087] self
	single_003 etcd:replica id:2 single_003:3301 role:replica status:running uptime:1h59m25s ups:2 dws:2 q:50.00%/1.00GB v:[3707168,2495749,952087] ok/single_002:follow/0.000s

Но такое не сработает, если имя шарда в одно-шардовом приложении отличается от имени приложения.

Аналогично, switchover выдаст ошибку, если не указать имя шарда для кластерного приложения

.. code-block:: bash

	$ switchover discovery cluster

	2022-01-23T18:30:02.103 +36.7ms @_error.lua:14> PANIC: cluster.clusters does not contain shard "cluster"

Также, можно указать путь до инстанса внутри шарда, а не имя шарда, так как в Тарантуле один инстанс может находиться строго в одном репликасете.

Полный путь до инстанса задается так ``<application-name>/<shard-name>@<instance-name>``, но для удобства поддержана сокращенная форма ``<application-name>@<instance-name>``

.. code-block:: bash

	$ switchover discovery cluster@shard_001_01

	2022-01-23T18:32:49.482 +49.6ms @discovery.lua:103> Discovered 2/2 nodes from shard_001_01:3301,shard_001_02:3301 in 0.049s
	shard_001_01 etcd:master  id:2 shard_001_01:3301 role:master  status:running uptime:2h6m16s ups:1 dws:1 q:100.00%/0.25GB v:[2685565,1223699] self
	shard_001_02 etcd:replica id:1 shard_001_02:3301 role:replica status:running uptime:2h6m17s u

Несмотря на то, что в discovery передали путь до инстанса, команда все равно работает для репликасета. Имя шарда было выведено из имени инстанса на основании ETCD.

Discovery в случае недоступности инстанса
*****************************************

Если получилось дотянуться хотя бы до одного инстанса, discovery будет совершен.

.. code-block:: bash

	$ switchover discovery single
	2022-01-23T18:39:20.216 +3028.1ms @discovery.lua:92> Took to long to discovery all instances. Continuing with instances which were discovered
	2022-01-23T18:39:20.216 +3028.4ms @discovery.lua:103> Discovered 2/3 nodes from single_001:3301,single_002:3301,single_003:3301 in 3.028s

	single_001 etcd:replica id:1 single_001:3301 role:replica status:running uptime:2h14m41s ups:1 dws:1 q:50.00%/1.00GB v:[3707168,2495749,952087] ok/single_002:follow/0.000s
	single_002 etcd:master  id:3 single_002:3301 role:master  status:running uptime:2h14m41s ups:1 dws:1 q:50.00%/1.00GB v:[3707168,2495749,952087] self
	single_003 etcd:replica single_003:3301 - not connected

В данном случае, discovery смог подключиться к 2/3 инстансов и выдать по ним информацию.

Процесс Discovery длился значительно дольше обычного, потому что switchover дожидался истечения ``discovery_timeout``.

Таймаут можно указывать в ключах запуска ``-d`` или в конфиге ``discovery_timeout``. Значение по-умолчанию 3 секунды.

Если стартануть реплику обратно, можно увидеть, как она поднимается

.. code-block:: bash

	single_001 etcd:replica id:1 single_001:3301 role:replica status:running uptime:2h17m ups:1 dws:1 q:50.00%/1.00GB v:[3707168,2495749,952087] ok/single_002:follow/0.000s
	single_002 etcd:master  id:3 single_002:3301 role:master  status:running uptime:2h17m ups:1 dws:1 q:50.00%/1.00GB v:[3707168,2495749,952087] self
	single_003 etcd:replica id:2 single_003:3301 role:replica status:loading uptime:4s ups:0 dws:0 q:87.50%/0.25GB v:[3707168,2495749,952087] fail/single_002:connected/2.595s err:unknown

Статус ``loading`` свидетельствует о процессе поднятия из снапшота и xlog'ов. Также видно, что при старте на реплику дали арены 250МБ, в то время как у остальных 1ГБ и половина забита. Скорее всего реплика не поднимется

Discovery в случае недоступности ноды ETCD
******************************************

Полноценно работает, пока в ETCD кластере есть лидер, за который голосует хотя бы половина+1 доступных нод.
switchover это проверяет посредством вызова метода ``/v2/members/leader``. Если отключить 2 ноды во время перевыборов, можно достичь такой ситуации:

.. code-block:: bash

	$ switchover discovery single

	LuajitError: ./switchover/_etcd.lua:304: No ETCD leader found (etcd is a mess?)
	fatal error, exiting the event loop

При включении verbose режима работы switchover'a можно посмотреть что отвечает ETCD:

.. code-block:: bash

	$ switchover -v discovery single

	2022-01-23T18:47:29.904 +16.8ms @_etcd.lua:75> GET http://etcd0:2379/v2/members => 200:Ok (0.0047)
	2022-01-23T18:47:29.914 +26.9ms @_etcd.lua:75> GET http://etcd1:2379/v2/members => 595:Couldn't resolve host name (0.0099)
	2022-01-23T18:47:29.921 +34.7ms @_etcd.lua:75> GET http://etcd2:2379/v2/members => 595:Couldn't resolve host name (0.0076)
	2022-01-23T18:47:29.924 +37.0ms @_etcd.lua:75> GET http://etcd0:2379/v2/members/leader => 503:Ok (0.0019)
	2022-01-23T18:47:29.924 +37.1ms @_etcd.lua:75> http

Видно, что 2 узла из 3х не доступны, оставщийся узел не может дать информацию о лидерстве, так как находится в состоянии перевыборов.
В этом случае нужно чинить ETCD.

Discovery format
****************

В discovery можно передать поля, которые мы хотим увидеть, если вывод по-умолчанию не устраивает.

Указание формата глобально на остальные команды.

.. code-block:: bash

	$ switchover --discovery-format '{{endpoint}}' discovery single

	single_001 etcd:replica single_001:3301 ok/single_002:follow/0.000s
	single_002 etcd:master  single_002:3301 self
	single_003 etcd:replica single_003:3301 ok/single_002:follow/0.000s

Поля имени инстанса, роли в etcd и статуса репликации не перебиваемы. Но серединную информацию можно менять.

При подключении к инстансу Тарантула, ``switchover`` скачивает информацию из ``box.info``, ``box.slab``, ``box.stat``, ``box.cfg``.

.. list-table:: Функции в Discovery format
  :header-rows: 1

  * - Команда в Tarantool
    - Паттерн в switchover
  * - box.info.*
    - {{info.*}}
  * - box.cfg.*
    - {{cfg.*}}
  * - box.stat.*
    - {{stat.*}}
  * - box.slab.info.*
    - {{slab.*}}
  * - box.stat.net.*
    - {{stat.net.*}}

Дополнительные поля:

* ``vclock`` - выдает ``box.info.vclock`` в компактном виде
* ``n_upstreams`` - выдает кол-во подключенных апстримов (из box.info.replication.*.upstream)
* ``n_downstreams`` - выдает кол-во подключенных даунстримов (из box.info.replication.*.downstream)
* ``uptime`` - выдает человеко-читаемый ``box.info.uptime``
* ``endpoint`` - выдает маскированный URI до Тарантула полученный из консоли или из ETCD.

Формат можно указать в /etc/switchover/config.yaml или в своем конфиге в поле ``discovery_format``.

Пример:

.. code-block:: yaml

  discovery_format: >-
    {{info.id}}/{{endpoint}}
    role:{{info.ro and (cfg.read_only and 'replica' or 'replica/master') or 'master '}}
    status:{{info.status}}
    uptime:{{uptime}}
    v:{{info.vclock}}
    ups:{{n_upstreams}}
    dws:{{n_downstreams}}
    quota:{{("%s/%.2fGB"):format(slab.quota_used_ratio, slab.quota_size/2^30)}}
    {{stat.INSERT.rps > 0 and "I:"..stat.INSERT.rps or ''}}
    {{stat.REPLACE.rps > 0 and "R:"..stat.REPLACE.rps or ''}}
    {{stat.UPDATE.rps > 0 and "U:"..stat.UPDATE.rps or ''}}
    {{stat.DELETE.rps > 0 and "D:"..stat.DELETE.rps or ''}}

Подстановка переменных и вычисления проводятся внутри ``switchover`` и никогда не передаются на сторону инстансов Тарантула.

Dot view
********

В основном discovery удобно использовать для проверки состояния репликации, ролей инстансов и согласованности с конфигом в ETCD.

Discovery может выдавать информацию о репликации в формате dot.

.. code-block:: bash

  switchover discovery -g single

  digraph G {
        node[shape="circle"]
        "single_003_3301" [label="2/replica: {'1':3707168,'2':2495749,'3':952087}\nsingle_003:3301"]
        "single_001_3301" [label="1/replica: {'1':3707168,'2':2495749,'3':952087}\nsingle_001:3301"]
        "single_002_3301" [label="3/master: {'1':3707168,'2':2495749,'3':952087}\nsingle_002:3301"]
        "single_001_3301" -> "single_003_3301" [style=solid,label="0.000s"]
        "single_002_3301" -> "single_003_3301" [style=solid,label="0.000s"]
        "single_003_3301" -> "single_001_3301" [style=solid,label="0.000s"]
        "single_002_3301" -> "single_001_3301" [style=solid,label="0.000s"]
        "single_001_3301" -> "single_002_3301" [style=solid,label="0.000s"]
        "single_003_3301" -> "single_002_3301" [style=solid,label="0.000s"]
  }

Также switchover может сгенерировать ссылку для рисовалки dot:

.. code-block:: bash

  switchover discovery -gl single

  https://dreampuf.github.io/GraphvizOnline/#digraph%20G%20%7B%0A%09node%5Bshape%3D%22circle%22%5D%0A%09%22single_003_3301%22%20%5Blabel%3D%222%2Freplica%3A%20%7B%271%27%3A3707168%2C%272%27%3A2495749%2C%273%27%3A952087%7D%5Cnsingle_003%3A3301%22%5D%20%0A%09%22single_001_3301%22%20%5Blabel%3D%221%2Freplica%3A%20%7B%271%27%3A3707168%2C%272%27%3A2495749%2C%273%27%3A952087%7D%5Cnsingle_001%3A3301%22%5D%20%0A%09%22single_002_3301%22%20%5Blabel%3D%223%2Fmaster%3A%20%7B%271%27%3A3707168%2C%272%27%3A2495749%2C%273%27%3A952087%7D%5Cnsingle_002%3A3301%22%5D%20%0A%09%22single_001_3301%22%20-%3E%20%22single_003_3301%22%20%5Bstyle%3Dsolid%2Clabel%3D%220.000s%22%5D%20%0A%09%22single_002_3301%22%20-%3E%20%22single_003_3301%22%20%5Bstyle%3Dsolid%2Clabel%3D%220.000s%22%5D%20%0A%09%22single_003_3301%22%20-%3E%20%22single_001_3301%22%20%5Bstyle%3Dsolid%2Clabel%3D%220.000s%22%5D%20%0A%09%22single_002_3301%22%20-%3E%20%22single_001_3301%22%20%5Bstyle%3Dsolid%2Clabel%3D%220.000s%22%5D%20%0A%09%22single_001_3301%22%20-%3E%20%22single_002_3301%22%20%5Bstyle%3Dsolid%2Clabel%3D%220.000s%22%5D%20%0A%09%22single_003_3301%22%20-%3E%20%22single_002_3301%22%20%5Bstyle%3Dsolid%2Clabel%3D%220.000s%22%5D%20%0A%7D

По `ссылке <https://dreampuf.github.io/GraphvizOnline/#digraph%20G%20%7B%0A%09node%5Bshape%3D%22circle%22%5D%0A%09%22single_003_3301%22%20%5Blabel%3D%222%2Freplica%3A%20%7B%271%27%3A3707168%2C%272%27%3A2495749%2C%273%27%3A952087%7D%5Cnsingle_003%3A3301%22%5D%20%0A%09%22single_001_3301%22%20%5Blabel%3D%221%2Freplica%3A%20%7B%271%27%3A3707168%2C%272%27%3A2495749%2C%273%27%3A952087%7D%5Cnsingle_001%3A3301%22%5D%20%0A%09%22single_002_3301%22%20%5Blabel%3D%223%2Fmaster%3A%20%7B%271%27%3A3707168%2C%272%27%3A2495749%2C%273%27%3A952087%7D%5Cnsingle_002%3A3301%22%5D%20%0A%09%22single_001_3301%22%20-%3E%20%22single_003_3301%22%20%5Bstyle%3Dsolid%2Clabel%3D%220.000s%22%5D%20%0A%09%22single_002_3301%22%20-%3E%20%22single_003_3301%22%20%5Bstyle%3Dsolid%2Clabel%3D%220.000s%22%5D%20%0A%09%22single_003_3301%22%20-%3E%20%22single_001_3301%22%20%5Bstyle%3Dsolid%2Clabel%3D%220.000s%22%5D%20%0A%09%22single_002_3301%22%20-%3E%20%22single_001_3301%22%20%5Bstyle%3Dsolid%2Clabel%3D%220.000s%22%5D%20%0A%09%22single_001_3301%22%20-%3E%20%22single_002_3301%22%20%5Bstyle%3Dsolid%2Clabel%3D%220.000s%22%5D%20%0A%09%22single_003_3301%22%20-%3E%20%22single_002_3301%22%20%5Bstyle%3Dsolid%2Clabel%3D%220.000s%22%5D%20%0A%7D>`_ можно перейти и оттуда скачать картинку.