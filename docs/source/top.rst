Top - atop для Тарантул кластера
===============================================

top - метод ``switchover`` позволяющий в реал-тайме собирать информацию о произвольном кластере на Tarantool.

Качественнее всего работает, при использовании ETCD (для полного сбора информации о кластере), но с версии 2.6.0 может работать и с произвольным репликасетом по ip:port, выполняя процесс Discovery.

.. code-block:: bash

	Usage: switchover top [-h] [--ero] [-i <interval>]
		<cluster_name|shard_name>

	Some of the examples:
			switchover top cluster_name                - shows top of cluster (requires ETCD)
			switchover top cluster_name/shard_name     - shows top of shard (requires ETCD)
			switchover top cluster_name:routers        - shows top of routers of cluster (requires ETCD)


	Arguments:
	cluster_name|shard_name                                  Name of the cluster (application)

	Options:
	-h, --help                                               Show this help message and exit.
	--ero                                                    Allows ReadOnly requests from ETCD if ETCD leader is not discovered
			-i <interval>,                                   Refresh interval in seconds (must be ≥ 0.1)
	--interval <interval>


.. code-block:: bash

  $ switchover top myapp # включает top для всего кластера
  $ switchover top myapp/shard_001 # включает top для одного шарда кластера


Top - главный экран
*******************


