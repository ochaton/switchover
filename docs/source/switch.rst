Switch - штатное ручное переключение мастера
============================================

Switch - метод ради которого реализован ``switchover``. Выполняет безопасное переключение мастера в шарде.

.. code-block:: bash

  Usage: switchover 2.10 switch [-h] [--max-lag <max_lag>]
        [--valid-status <valid_status>] [--no-etcd-lock]
        [--timeout <timeout>] [--no-reload] [--allow-vshard]
        [--no-reload-routers] [--no-check-downstreams] <instance>

  Switch fails when no master found in replicaset. Supports only tarantool >= 1.10.0
  Examples:
          switchover switch mycluster/mycluster_002@mycluster_002_01 - Makes mycluster_002_01 master of mycluster_002
          switchover switch mysingle@mysingle_001                    - Makes mysingle_001 master of mysingle
          switchover switch --no-etcd tnt1:3301                      - Makes tnt1:3301 master of it's replicaset

  Arguments:

    instance                                 Instance to become master

  Options:

    -h, --help                               Show this help message and exit.
    --max-lag <max_lag>                      Maximum allowed Replication lag of the future master (in seconds)
    --valid-status <valid_status>            List of extra statuses (except running) valid for candidate (ex: orphan,loading)
    --no-etcd-lock                           Disables ETCD mutexes and discovery
    --timeout <timeout>,                     Configures timeout of the switch (default: 2*max_lag)
            -t <timeout>

    --no-reload                              Disables execution of package.reload on candidate and old master (default: enabled)
    --allow-vshard                           Allows switch for vshard cluster (Use for your own risk)
    --no-reload-routers                      Disable routers reload if were discovered via etcd.cluster.vshard
    --no-check-downstreams                   Disable having live downstreams for candidate


В качестве аргумента принимает **что-то** из чего можно достать информацию об инстансе.
При использовании ETCD (рекомендуемый вариант) требует указание полного имени инстанса.

Полное имя задается как <application-name>/<shard-name>@<instance-name>.
Либо доступна сокращенная версия <application-name>@<instance-name>.

.. code-block:: bash

  $ switchover switch myapp@instance_001_02
  $ switchover switch myapp/shard_001@instance_001_02

Алгоритм switch
***************

**switch требует наличие живого работающего мастера в репликасете**

switch нужно использовать при штатных работах с кластером, для переноса роли мастера на другую ноду с этого сервера.

Это может пригодиться для:

* обновления версии Tarantool,
* обновления програмного кода в Tarantool,
* обновления нединамических полей в конфигах,
* ребута сервера,
* выключение сервера

для минимизации downtime.

**switch** требует указания только реплики-кандидата (будущего мастера), текущий мастер вычисляется автоматически.

**switch** вытаскивает информацию из ETCD и выполняет **discovery** для получения максимальной информации о работе реликасета (шарда).

**switch** работает только в рамках одного репликасета (шарда), массового переключения мастеров кластера на данный момент не поддержано.

Сам алгоритм достаточно простой:

#. **switch** получает полную информацию о репликасете (состояние репликации, лаг репликации, расхождение vclock и тп)
#. проверяет легитимность ``candidate`` для становления лидером:
    #. присутствие в vshard кластере (можно разрешить ``--allow-vshard``),
    #. наличие RAFT (отключить проверку нельзя),
    #. наличие живых upstreams и downstreams (можно отключить ``--no-check-downstreams``, ``--no-check-upstreams``),
    #. проверяет статус инстанса (разрешен только ``running``, можно поменять с ``--valid-status``, но не рекомендуется),
    #. установленный репликационный канал до действующего мастера (отключить проверку нельзя)
    #. замеряется lag репликации между мастером (можно задать ``--max-lag``)
#. **switch** берет mutex в ETCD в шарде (можно отключить ``--no-etcd-lock``, но не рекомендуется),
#. (крит) выполняет перевод текущего мастера в RO (``box.cfg{read_only=true}``)
#. (крит) дожидается на мастере достижения кандидатом необходимого vclock
#. (крит) дожидается на кандидате достижения vclock мастера
#. (крит) выполняет перевод кандидата в RW (``box.cfg{read_only=false}``)
#. снимает ETCD mutex
#. вызывает ``package.reload`` на кандидате (можно отключить с ``--no-reload``)
#. вызывает ``package.reload`` на старом мастере (можно отключить с ``--no-reload``)
#. вызывает ``package.reload`` на роутерах, если они корректно сконфигурированы в ETCD и доступны (можно отключить с ``--no-reload-routers``)
#. вызывает финальный discovery для отображения нового состояния шарда.

На проведение действий в критической секции может быть задан таймаут (``--timeout``), по-умолчанию равен ``2*max_lag`` (2 секунды).

Обычно, проведение переключения критической секции выполняется за 10мс.

В случае фатальных проблем во время выполнения критической секции, кластер может оказаться в Read-Only, получение 2х мастеров без внешнего вмештельства невозможно.

.. code-block:: bash

  $ switchover switch single@single_001

  2022-03-09T15:45:58.707 +37.4ms Discovered 3/3 nodes from single_001:3301,single_002:3301,single_003:3301 in 0.037s (@discovery.lua:129:44-136)
  2022-03-09T15:45:58.707 +37.6ms Candidate id:1 single_001 dc:no role:replica status:running uptime:3h44m40s ups:2 dws:2 q:100.00%/1.00GB v:[4154659,7881900,2600300] can be next leader (@_switch_promote_common.lua:85:60-88)
  2022-03-09T15:45:58.707 +37.9ms Candidate id:1 single_001 dc:no role:replica status:running uptime:3h44m40s ups:2 dws:2 q:100.00%/1.00GB v:[4154659,7881900,2600300] can be next leader (current: 2/single_003:3301). Running replication check (safe) (@switch.lua:447:412-542)
  2022-03-09T15:45:58.711 +41.5ms wait_clock(7881900, 2) on 1 succeed {"1":4154659,"2":7881900,"3":2600300} => 0.0000s (@switch.lua:42:23-50)
  2022-03-09T15:45:58.711 +41.6ms Replication 2/single_003:3301 -> 1/single_001:3301 is good. Lag=0.0001s (@switch.lua:456:412-542)
  2022-03-09T15:45:58.711 +41.6ms Taking mutex key: single/switchover (@switch.lua:387:361-410)
  2022-03-09T15:45:58.718 +48.8ms Running switch for: 2/ce343d92-3640-4146-8655-abb7d3501be7 (single_003:3301 vclock:{"1":4154659,"2":7881900,"3":2600300}) -> 1/1a154d2c-2aad-4785-b7fe-ae5ad41fc4b6 (single_001:3301 vclock: {"1":4154659,"2":7881900,"3":2600300}) (@switch.lua:117:107-244)
  2022-03-09T15:45:58.720 +51.0ms Master is in RO: id:2 single_003 dc:no role:replica/master status:running uptime:3h44m40s ups:2 dws:2 q:100.00%/1.00GB v:[4154659,7881900,2600300] took -3.9988s (@switch.lua:182:107-244)
  2022-03-09T15:45:58.723 +53.6ms Candidate is in RW state took: 0.0000s (@switch.lua:216:107-244)
  2022-03-09T15:45:58.730 +61.3ms Switch 2/single_003:3301 -> 1/single_001:3301 was successfully done (@switch.lua:491:412-542)
  2022-03-09T15:45:58.737 +68.2ms Changing master in ETCD: single_003 -> single_001 (@heal.lua:45:17-60)
  2022-03-09T15:45:58.743 +73.8ms ETCD response: {"action":"compareAndSwap","node":{"createdIndex":9,"modifiedIndex":61,"key":"\/apps\/single\/clusters\/single\/master","value":"single_001"},"prevNode":{"createdIndex":9,"modifiedIndex":58,"key":"\/apps\/single\/clusters\/single\/master","value":"single_003"}} (@heal.lua:52:17-60)
  2022-03-09T15:45:58.743 +73.9ms Perfoming package.reload (@switch.lua:509:412-542)
  2022-03-09T15:45:58.743 +74.1ms Calling package.reload on id:1 single_001 dc:no role:master  status:running uptime:3h44m40s ups:2 dws:2 q:100.00%/1.00GB v:[4154659,7881900,2600300] (@_tarantool.lua:550:545-556)
  2022-03-09T15:45:59.800 +1131.3ms Calling package.reload on id:2 single_003 dc:no role:replica status:running uptime:3h44m41s ups:2 dws:2 q:100.00%/1.00GB v:[4154659,7881900,2600300] (@_tarantool.lua:550:545-556)
  2022-03-09T15:46:00.870 +2200.9ms Discovered 3/3 nodes from single_001:3301,single_002:3301,single_003:3301 in 2.200s (@discovery.lua:129:44-136)
  single_001 etcd:master  id:1 single_001 dc:no role:master  status:running uptime:3h44m42s ups:2 dws:2 q:100.00%/1.00GB v:[4154659,7881900,2600300] self
  single_002 etcd:replica id:3 single_002 dc:no role:replica status:running uptime:3h44m42s ups:2 dws:2 q:100.00%/1.00GB v:[4154659,7881900,2600300] ok/single_001:follow/0.000s
  single_003 etcd:replica id:2 single_003 dc:no role:replica status:running uptime:3h44m42s ups:2 dws:2 q:100.00%/1.00GB v:[4154659,7881900,2600300] ok/single_001:follow/0.000s

