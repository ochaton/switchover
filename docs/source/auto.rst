Auto - автоматический выбор мастера во время аварии
===================================================

Auto - метод ``switchover`` позволяющий во время аварии выбрать наилучшую реплику-кандидата и провести ее до мастера.

Является автоматизированным вариантом promote.

Работает только, если мастер в репликасете не найден!

.. code-block:: bash

  Usage: switchover 2.10 auto [-h] [-t <timeout>] [--no-orphan-fix]
        [--no-reload-routers] <shard>

  Evaluates heuristics and promote replica to master if it is necessary

  Arguments:

    shard                                                    Name of the shard

  Options:

    -h, --help                                Show this help message and exit.

            -t <timeout>,                     Autofailover timeout (ETCD lock TTL)
    --timeout <timeout>

    --no-orphan-fix                           Does not change replication_connect_quorum
    --no-reload-routers                       Disable routers reload if were discovered via etcd.cluster.vshard

В качестве аргумента принимает имя шарда.
Использование ETCD обязательно.

Полное имя задается как <application-name>/<shard-name>.

.. code-block:: bash

  $ switchover auto myapp/shard_001


Алгоритм auto
****************

**auto требует отсутствие работающего мастера в репликасете**

**auto** можно использовать в случае потери мастера (отказ узла, оборудования, сетевой связности).

Отличие алгоритма **auto** только в наличии определения "лучшего кандидата на роль мастера":

#. **auto** получает полную информацию о репликасете (состояние репликации, лаг репликации, расхождение vclock и тп)
#. **auto** проверяет отсутствие мастера в репликасете.
#. **auto** выбирает наилучшего кандидата
    #. вычисляется теоретический максимальный vclock в репликасете
    #. для каждого узла вычисляется теоретический максимально достижимый vclock
    #. из всех реплик выбирается узел с наибольшим количеством активных downstreams.
#. **auto** берет mutex в ETCD в шарде
#. (крит) изменяет информацию о мастере в ETCD (это отличается от поведения switch и promote).
#. (крит) выполняет перевод текущего мастера в RO (``box.cfg{read_only=true}``) если это возможно. это необходимо, потому что причина, по которой мастер оказался в RO непонятна.
#. (крит) запускает ожидание данных vclock на кандидате (технология ``wait_lsn``).
#. (крит) выполняет перевод кандидата в RW (``box.cfg{read_only=false}``)
#. снимает ETCD mutex
#. вызывает ``package.reload`` на кандидате
#. вызывает ``package.reload`` на старом мастере если это возможно
#. вызывает ``package.reload`` на роутерах, если они корректно сконфигурированы в ETCD и доступны (можно отключить с ``--no-reload-routers``)

На проведение действий в критической секции может быть задан таймаут (``--timeout``), по-умолчанию равен (5 секунд).

Обычно, проведение переключения критической секции выполняется за 3с.

В случае фатальных проблем во время выполнения критической секции, кластер может остаться в Read-Only, получение 2х мастеров без внешнего вмешательства невозможно.

.. code-block:: bash

  $ switchover auto vshard/storage_001

  storage_001_01:3301: Invalid argument
  storage_001_01:3301: Invalid argument
  2022-03-10T11:00:44.512 +2997.3ms [vshard/storage_001] Discovered 1/2 nodes from storage_001_01:3301,storage_001_02:3301 in 2.997s (@discovery.lua:129:44-136)
  2022-03-10T11:00:44.512 +2997.7ms [vshard/storage_001] 0 replicas replicate data from ETCD Master. Searching for best candidate to vclock [2943398,70219] (@auto.lua:291:225-414)
  2022-03-10T11:00:44.512 +2997.8ms [vshard/storage_001] Replication Connect Quorum is not set in ETCD: def:2 (@auto.lua:301:225-414)
  2022-03-10T11:00:44.512 +2997.9ms [vshard/storage_001] storage_001_02:3301 => dv:[0,0] ds:0 (@auto.lua:312:225-414)
  2022-03-10T11:00:44.512 +2998.0ms [vshard/storage_001] Choosing candidate: id:2 storage_001_02 dc:no role:replica status:running uptime:10h29m36s ups:0 dws:0 q:81.25%/0.25GB v:[2943398,70219] (@auto.lua:340:225-414)
  2022-03-10T11:00:44.513 +2998.2ms [vshard/storage_001] vclock [2943398,70219] is achievable. No operations will be lost (@auto.lua:346:225-414)
  2022-03-10T11:00:44.513 +2998.3ms [vshard/storage_001] Taking mutex key: vshard/storage_001_switchover (@auto.lua:351:225-414)
  2022-03-10T11:00:44.529 +3015.0ms [vshard/storage_001] Changing etcd master to storage_001_02:3301 (@auto.lua:87:62-223)
  2022-03-10T11:00:44.530 +3015.2ms [vshard/storage_001] Changing master in ETCD: storage_001_01 -> storage_001_02 (@heal.lua:45:17-60)
  2022-03-10T11:00:44.538 +3024.0ms [vshard/storage_001] ETCD response: {"action":"compareAndSwap","node":{"createdIndex":26,"modifiedIndex":69,"key":"\/apps\/vshard\/clusters\/storage_001\/master","value":"storage_001_02"},"prevNode":{"createdIndex":26,"modifiedIndex":67,"key":"\/apps\/vshard\/clusters\/storage_001\/master","value":"storage_001_01"}} (@heal.lua:52:17-60)
  2022-03-10T11:00:44.539 +3024.1ms [vshard/storage_001] Will fix replication_connect_quorum: 2 -> 1 (@auto.lua:135:62-223)
  2022-03-10T11:00:44.544 +3029.4ms [vshard/storage_001] Calling package.reload on id:2 storage_001_02 dc:no role:master  status:running uptime:10h29m36s ups:0 dws:0 q:81.25%/0.25GB v:[2943398,70219] (@_tarantool.lua:550:545-556)
  2022-03-10T11:00:45.728 +4213.2ms [vshard/storage_001] Candidate 2/storage_001_02:3301 was auto promoted (@auto.lua:390:225-414)
  2022-03-10T11:00:45.728 +4213.3ms [vshard/storage_001] Found routers in ETCD: router_002,router_003,router_001 (@auto.lua:394:225-414)
  2022-03-10T11:00:45.728 +4213.4ms [vshard/storage_001] Got shards 3: router_002,router_003,router_001 (@discovery.lua:191:187-247)
  2022-03-10T11:00:45.740 +4225.3ms [vshard/storage_001] Discovered 1/1 nodes from router_003:3301 in 4.224s (@discovery.lua:129:44-136)
  2022-03-10T11:00:45.743 +4228.1ms [vshard/storage_001] Discovered 1/1 nodes from router_001:3301 in 4.227s (@discovery.lua:129:44-136)
  2022-03-10T11:00:45.743 +4228.7ms [vshard/storage_001] Discovered 1/1 nodes from router_002:3301 in 4.227s (@discovery.lua:129:44-136)
  2022-03-10T11:00:45.744 +4229.5ms [vshard/storage_001] Calling package.reload on router router_002 (@auto.lua:406:225-414)
  2022-03-10T11:00:45.744 +4229.6ms [vshard/storage_001] Calling package.reload on id:1 router_002 dc:no role:master  status:running uptime:10h29m37s ups:0 dws:0 q:12.50%/0.25GB v:[4] (@_tarantool.lua:550:545-556)
  2022-03-10T11:00:47.362 +5847.4ms [vshard/storage_001] Calling package.reload on router router_003 (@auto.lua:406:225-414)
  2022-03-10T11:00:47.362 +5847.5ms [vshard/storage_001] Calling package.reload on id:1 router_003 dc:no role:master  status:running uptime:10h29m39s ups:0 dws:0 q:12.50%/0.25GB v:[4] (@_tarantool.lua:550:545-556)
  2022-03-10T11:00:48.992 +7477.2ms [vshard/storage_001] Calling package.reload on router router_001 (@auto.lua:406:225-414)
  2022-03-10T11:00:48.992 +7477.3ms [vshard/storage_001] Calling package.reload on id:1 router_001 dc:no role:master  status:running uptime:10h29m41s ups:0 dws:0 q:12.50%/0.25GB v:[4] (@_tarantool.lua:550:545-556)
