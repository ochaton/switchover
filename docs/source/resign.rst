Resign - ручное снятие полномочий с мастера
===========================================

Resign - метод ``switchover`` позволяющий штатно переключить мастеров кластера на доступные реплики

Является автоматизированным вариантом switch.

Удобно использовать для подготовки сервера к выключению.

.. code-block:: bash

  Usage: switchover resign [-h] [--selector <selector>]
        [--ignore-not-connected] [-i <interval>] <cluster-name>

  Example:
          switchover resign --selector 'fqdn=userdb.i' userdb
          switchover resign --selector 'ipv4=10.0.0.1' userdb

  Arguments:
    cluster-name                                             Name of the cluster

  Options:
    -h, --help                                               Show this help message and exit.
    --selector <selector>,                                   Selector rule
            -s <selector>
    --ignore-not-connected                                   Ignores not connected instances
            -i <interval>,                                   Sleeps interval between subsequent executions
    --interval <interval>


В качестве аргумента принимает имя шарда. Использование ETCD обязательно.

.. code-block:: bash

  $ switchover resign --selector 'fqdn in (srv1.q, srv2.q)' myapp


Алгоритм resign
****************

**resign требует задания --selector**

Синтаксис ``--selector`` взят из одноименной опции ``--selector`` ``kubectl``.

Ключ ``--selector`` используется для задания условия, по которому будут выбраны инстансы для переключения. ``switchover`` самостоятельно добавляет условие на лидерство.

Также, ``--selector`` задает условие на реплики, которые не должны быть выбраны в качестве кандидатов. Это сделано осознанно для того, чтобы исключить возможность переключения лидерства на сервера с текущими мастерами.

Поддержанные операторы
**********************

``!`` - унарное НЕ, например ``--selector '!is_ro'`` (инстансы, являющиеся мастерами)

``=`` - равенство, например ``--selector 'ipv4=10.0.0.1'`` (инстансы, которые слушают на ``ipv4`` адресе ``10.0.0.1``)

``!=`` - неравенство, например ``--selector 'status!=running'`` (инстансы, у которых ``box.info.status != "running"``)

``in`` - вхождение, например ``--selector 'fqdn in (srv1.q, srv2.q)'`` (инстансы, которые находятся на заданных fqdn)

``notin`` - исключение, например ``--selector 'election_mode notin (candidate, voter)'`` (инстансы с RAFT, использующие недефолтные настройки)

``,`` - логический AND, например ``--selector 'is_connected,!is_router,!is_ro,etcd_role!=master'`` (подключенные мастера, которые в ETCD записаны не как мастера)

Доступные переменные
********************

``shard_name`` - имя репликасета, взятое из ETCD

``etcd_master_name`` - имя мастера в шарде (проставляется для всех инстансов)

``name`` - имя инстанса, взятое из ETCD

``ipv4`` - ipv4 адрес инстанса

``fqdn`` - fqdn инстанса

``endpoint`` - полный URI до инстанса

``etcd_role`` - (master, replica, router, candidate, voter, manual) роль, указанная в ETCD для инстанса

``etcd_is_master`` - (true/false) указан ли данный инстанс в etcd как мастер

``is_connected`` - (true/false) удалось ли установить подключение к инстансу

``is_router`` - (true/false) является ли инстанс роутером

``has_enabled_raft`` (true/false) включен ли RAFT в шарде данного инстанса

``status`` - (loading, running, orphan) box.info.status инстанса

``id`` - box.info.id инстанса

``is_ro`` - (true/false) box.info.ro инстанса

``is_cfg_ro`` - (true/false) box.cfg.read_only инстанса

``election_mode`` - (candidate, voter, off, manual) box.cfg.election_mode инстанса

``version`` - box.info.version инстанса (полная версия тарантула)

``election.*`` - box.info.election инстанса (доступна вложенность)

``etcd_config`` - скаченный конфиг ETCD инстанса

``slab_info`` - box.slab.info() инстанса

``stat`` - box.stat() и box.stat.net() инстанса

``bucket_count`` - количество бакетов для vshard-кластера на инстансе

``role`` - (M, R, M/R, R/M) текущая роль инстанса в репликасете. M/R следует читать как "является мастером, но должен быть репликой". R/M следует читать как "является репликой, но должен быть мастером"


.. code-block:: bash

  $ swr resign -s 'fqdn in (shard_001_01)' cluster

  2023-04-20T19:01:12.246 +95.5ms 103/swr Got shards 2: shard_001,shard_002 (@discovery.lua:215:211-284)
  2023-04-20T19:01:12.273 +122.9ms 112/shard_002 Discovered 2/2 nodes from shard_002_01:3301,shard_002_02:3301 in 0.121s (@discovery.lua:139:46-153)
  2023-04-20T19:01:12.274 +123.7ms 110/shard_001 Discovered 2/2 nodes from shard_001_01:3301,shard_001_02:3301 in 0.123s (@discovery.lua:139:46-153)
  2023-04-20T19:01:12.276 +125.6ms 103/swr ignoring instances {shard_001_01} (@resign.lua:57:43-150)
  2023-04-20T19:01:12.280 +130.2ms 103/swr Planning to switch cluster/shard_001@shard_001_01 ([773858,13460]) -> cluster/shard_001@shard_001_02 ([773858,13460], lag = 0.00s) (@resign.lua:101:43-150)
  Are you ready? [yes/no] yes
  2023-04-20T19:01:13.541 +1390.4ms 103/swr Discovered 2/2 nodes from shard_001_01:3301,shard_001_02:3301 in 1.389s (@discovery.lua:139:46-153)
  2023-04-20T19:01:13.541 +1390.6ms 103/swr Candidate id:2 shard_001_02 dc:no role:replica status:running uptime:17m18s ups:1 dws:1 q:31.25%/0.25GB v:[774869,13460] can be next leader (@_switch_promote_common.lua:99:73-102)
  2023-04-20T19:01:13.541 +1390.8ms 103/swr RAFT is ok (@switch.lua:450:419-552)
  2023-04-20T19:01:13.544 +1394.0ms 103/swr wait_clock(774878, 1) on 2 succeed {"1":774878,"2":13460} => 0.0000s (@switch.lua:43:24-51)
  2023-04-20T19:01:13.544 +1394.1ms 103/swr Replication 1/shard_001_01:3301 -> 2/shard_001_02:3301 is good. Lag=0.0001s (@switch.lua:459:419-552)
  2023-04-20T19:01:13.544 +1394.2ms 103/swr Taking mutex key: cluster/clusters/shard_001/switchover (@switch.lua:392:366-417)
  2023-04-20T19:01:13.549 +1398.6ms 103/swr Running switch for: 1/fb6c25c7-1d52-5287-a556-2d02add10a3f (shard_001_01:3301 vclock:{"1":774878,"2":13460}) -> 2/44db828d-93ed-594f-9c88-a62ed0de8430 (shard_001_02:3301 vclock: {"1":774878,"2":13460}) (@switch.lua:120:110-247)
  2023-04-20T19:01:13.550 +1399.5ms 103/swr Master is in RO: id:1 shard_001_01 dc:no role:replica/master status:running uptime:17m19s ups:1 dws:1 q:31.25%/0.25GB v:[774885,13460] took -1.9996s (@switch.lua:185:110-247)
  2023-04-20T19:01:13.551 +1401.1ms 103/swr Candidate is in RW state took: 0.0000s (@switch.lua:219:110-247)
  2023-04-20T19:01:13.551 +1401.3ms 103/swr Switch 1/shard_001_01:3301 -> 2/shard_001_02:3301 was successfully done (@switch.lua:495:419-552)
  2023-04-20T19:01:13.561 +1410.4ms 103/swr Changing master in ETCD: shard_001_01 -> shard_001_02 (@heal.lua:46:18-61)
  2023-04-20T19:01:13.565 +1414.8ms 103/swr ETCD response: {"action":"compareAndSwap","node":{"createdIndex":54,"modifiedIndex":77,"key":"\/apps\/cluster\/clusters\/shard_001\/master","value":"shard_001_02"},"prevNode":{"createdIndex":54,"modifiedIndex":74,"key":"\/apps\/cluster\/clusters\/shard_001\/master","value":"shard_001_01"}} (@heal.lua:53:18-61)
  2023-04-20T19:01:13.570 +1419.9ms 103/swr mutex:release: {"action":"compareAndDelete","node":{"createdIndex":76,"key":"\/apps\/cluster\/clusters\/shard_001\/switchover","modifiedIndex":78},"prevNode":{"createdIndex":76,"value":"switchover:77181c35-ae29-5af8-af2f-3f92dfb502b5:fb6c25c7-1d52-5287-a556-2d02add10a3f:44db828d-93ed-594f-9c88-a62ed0de8430","expiration":"2023-04-20T19:01:23.545366644Z","ttl":10,"key":"\/apps\/cluster\/clusters\/shard_001\/switchover","modifiedIndex":76}} (@_mutex.lua:73:66-74)
  2023-04-20T19:01:13.570 +1420.0ms 103/swr Perfoming package.reload (@switch.lua:518:419-552)
  2023-04-20T19:01:13.570 +1420.1ms 103/swr Calling package.reload on id:2 shard_001_02 dc:no role:master  status:running uptime:17m18s ups:1 dws:1 q:31.25%/0.25GB v:[774885,13460] (@_tarantool.lua:728:723-734)
  2023-04-20T19:01:14.642 +2492.2ms 103/swr Calling package.reload on id:1 shard_001_01 dc:no role:replica status:running uptime:17m20s ups:1 dws:1 q:31.25%/0.25GB v:[774885,14270] (@_tarantool.lua:728:723-734)
  2023-04-20T19:01:18.035 +5885.2ms 103/swr Discovered 2/2 nodes from shard_001_01:3301,shard_001_02:3301 in 5.884s (@discovery.lua:139:46-153)

  cluster/shard_001 [etcd master: shard_001_02, real masters: shard_001_02]
          shard_001_01    shard_001_01:3301 (shard_001_01)        v:2.8.2-0-gfc96d10f5    etcd:replica    role:replica    status:running  uptime:17m23s   ↓:1 ↑:1 ok/shard_001_02:follow/0.000s
          shard_001_02    shard_001_02:3301 (shard_001_02)        v:2.8.2-0-gfc96d10f5    etcd:master     role:master     status:running  uptime:17m23s   ↓:1 ↑:1 self

  2023-04-20T19:01:18.162 +6012.1ms 103/swr Got shards 2: shard_001,shard_002 (@discovery.lua:215:211-284)
  2023-04-20T19:01:18.196 +6046.2ms 161/shard_001 Discovered 2/2 nodes from shard_001_01:3301,shard_001_02:3301 in 6.045s (@discovery.lua:139:46-153)
  2023-04-20T19:01:18.202 +6052.3ms 163/shard_002 Discovered 2/2 nodes from shard_002_01:3301,shard_002_02:3301 in 6.052s (@discovery.lua:139:46-153)

  shard_001 [etcd master: shard_001_02, real masters: shard_001_02]
          shard_001_02    shard_001_02:3301 (shard_001_02)        v:2.8.2-0-gfc96d10f5    etcd:master     role:master     status:running  uptime:17m23s   ↓:1 ↑:1 self

  shard_002 [etcd master: shard_002_01, real masters: shard_002_01]
          shard_002_01    shard_002_01:3301 (shard_002_01)        v:2.8.2-0-gfc96d10f5    etcd:master     role:master     status:running  uptime:17m26s   ↓:1 ↑:1 self
