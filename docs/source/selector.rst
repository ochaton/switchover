Selector - Механизм фильтрации инстансов кластера
=================================================

selector (флаг ``--selector`` или ``-s``) - указание правил для фильтрации инстансов для проведения операции.

Семантика подхода ``selector`` была скопирована с `kubectl selector <https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/>`_.

На момент switchover 2.7.0 ``--selector`` поддержан для следующих команд

* ``top`` - Atop для кластера
* ``status`` - Вывод текущей карты кластера и его алертов
* ``resign`` - Декомиссия инстансов (переключение роли мастера С указанных групп инстансов)
* ``package-reload`` - Горячий релоад кода и конфигурации
* ``restart-replication`` - Горячий перезапуск репликации
* ``eval`` - Выполнение произвольного кода

В отличии от механизма ``labels`` в ``kubectl`` переменные предопределены самим свичовером и на данный момент не могут быть расширены.

Список доступных переменных

.. list-table:: Список переменных в ``--selector``
  :header-rows: 1

  * - Имя переменной
    - Возможные значения
    - Комментарий
  * - ``bucket_count``
    - "no", 0, 3000
    - Количество бакетов ``vshard`` в инстансе (длина спейса ``box.space._bucket``)
  * - ``election_mode``
    - "off", "candidate", "voter", "manual"
    - Настройка ``box.cfg.election_mode``
  * - ``endpoint``
    - "srv1.q:3301", "172.20.1.5:3301"
    - iproto адрес инстанса без указания пользователя (то как к нему подключился ``switchover``).
  * - ``etcd_is_master``
    - true, false
    - Является ли инстанс мастером в ETCD (при настройке через moonlibs/config).
  * - ``etcd_master_name``
    - ``userdb_001_01``
    - Имя инстанса, которое указано как мастер в репликасете для опрошенного инстанса
  * - ``etcd_role``
    - ``master``, ``repica``, ``router``
    - Роль инстанса по информации из ETCD.
  * - ``fqdn``
    - ``srv1.q``
    - FQDN инстанса (если прошел обратный резолв listen-ip в домен)
  * - ``ipv4``
    - ``172.20.0.5``
    - IPv4 адрес инстанса
  * - ``is_cfg_ro``
    - ``true``, ``false``
    - Указан ли инстанс как ReadOnly в конфиге ``box.cfg`` (в действительности)
  * - ``is_connected``
    - ``true``, ``false``
    - Удалось ли switchover подключиться к инстансу
  * - ``is_ro``
    - ``true``, ``false``
    - Находится ли сейчас инстанс в действительности в ReadOnly (значение ``box.info.ro``)
  * - ``is_router``
    - ``true``, ``false``
    - Является ли инстанс роутером (принадлежит топологии ``etcd.instance.single``, либо является ``vshard-router``)
  * - ``lsn``
    - 0, 92336
    - Значение ``box.info.lsn`` инстанса
  * - ``master_upstream``
    - "self", "ok/shard_001_01:follow/0.000s"
    - Состояние репликации до инстанса указанного как мастер в топологии ETCD
  * - ``n_downstreams``
    - 0, 1, 2, ...
    - Количество живых реплик, которые успешно забирают данные с этого инстанса
  * - ``n_fibers``
    - 1, 2, ...
    - Количество файберов запущенных на этом инстансе
  * - ``n_upstreams``
    - 0, 1, 2, ...
    - Количество живых мастеров, с которых этот инстанс успешно забирает данных
  * - ``name``
    - ``userdb_001_01``
    - Имя инстанса, указанное в топологии (ETCD)
  * - ``quota_size``
    - 268435456, ...
    - Примененный размер арены memtx на инстансе
  * - ``quota_used_ratio``
    - 37.5
    - Процентное соотношение уже аллоцированной арены memtx на инстансе
  * - ``replication_from_master``
    - true, false
    - Флаг наличия работающей репликации данных с инстанса указанного как мастер в ETCD (для мастера всегда true)
  * - ``role``
    - "R", "M", "M/R", "R/M"
    - Фактическая роль инстанса (высчитывается из ``box.info.ro``. Для ``true`` ``"R"``, для ``false`` ``"M"``)
  * - ``shard_name``
    - "userdb_001"
    - Имя репликасета из ETCD, которому принадлежит инстанс
  * - ``vshard_storage_status``
    - 0, 1, 2, 3, 4
    - Уровень зелености ``vshard.storage``, чем больше, тем хуже

Selector -- как использовать
****************************

Выполнить декомиссию всех инстансов (переключить всех мастеров) с указанного сервера

.. code-block:: bash

	$ switchover resign --selector 'fqdn=srv1.q' userdb

Обновить конфигурацию реплик на указанном сервере

.. code-block:: bash

	$ switchover pr --cluster --selector 'fqdn=srv1.q,is_ro' userdb

Починить репликацию до мастера у всех инстансов, у которых сломалась репликация до мастера

.. code-block:: bash

	$ switchover rr --cluster --selector '!replication_from_master' userdb

Вывести только те инстансы, у которых есть алерты на ``vshard.storage``

.. code-block:: bash

	$ switchover status --selector 'vshard_storage_status!=0' userdb

Вывести инстансы, которые по конфигурации должны быть RW, но в действительности RO

.. code-block:: bash

	$ switchover status --selector 'is_ro,!is_cfg_ro' userdb

Вывести инстансы, которые по ETCD должны быть мастерами, но в действительности RO

.. code-block:: bash

	$ switchover status --selector 'is_ro,etcd_role=master' userdb

Вывести инстансы, которые по ETCD не должны быть мастерами, но в действительности являются

.. code-block:: bash

	$ switchover status --selector '!is_ro,etcd_role!=master' userdb
