Switchover installation & configuration
=======================================
Инсталлировать switchover можно из gitlab repository: https://gitlab.com/ochaton/switchover/-/packages

Необходимо иметь предустанновленный Тарантул, который можно поставить отсюда: https://www.tarantool.io/ru/download/os-installation/

Конфигурация
************
switchover конфигурируется из параметров командной строки и из отдельного yaml файла, по умолчанию располагающемся в `/etc/switchover/config.yaml`.
Флаги и опции командной строки имеют больший приоритет.

.. list-table:: Опции конфигурации
  :header-rows: 1

  * - Описание
    - Опция командной строки
    - Опция файла конфига
    - Команды
    - Значение по-умолчанию
    - Версия switchover
  * - Задает таймаут для подключения ко всем инстансам шарда
    - ``-d``, ``--discovery-timeout``
    - ``discovery_timeout``
    - all, кроме etcd
    - 3 секунды
    - ≥ 1.0.0
  * - Задает список эндпоинтов etcd
    - ``-e``, ``--etcd``
    - ``etcd.endpoints``
    - all
    - None
    - ≥ 1.0.0
  * - Задает root внутри etcd
    - ``-p``, ``--prefix``
    - ``etcd.prefix``
    - all
    - ``/``
    - ≥ 1.0.0
  * - Выключает логи
    - ``-q``, ``--quiet``
    - ``quiet``
    - all
    - false
    - ≥ 2.0.0
  * - Включает логи трассировки
    - ``-v``, ``--verbose``
    - ``verbose``
    - all
    - false
    - ≥ 1.0.0
  * - Раскрашивает логи
    - ``-C``, ``--color``
    - ``color``
    - all
    - false
    - ≥ 2.0.0
  * - Выключает раскрашивание логов (имеет больший приоритет над `--color``)
    - ``--no-color``
    - ``no_colr``
    - all
    - false
    - ≥ 2.5.0
  * - Задает максимально допустимый replication lag для переключения
    - ``--max-lag``
    - ``max_lag``
    - promote, switch
    - 1 секунда
    - ≥ 1.0.0
  * - Отключает автоматический package.reload
    - ``--no-reload``
    - ``no_reload``
    - promote, switch
    - false
    - ≥ 2.0.0
  * - Отключает использование etcd (по умолчанию, etcd включен)
    - ``--no-etcd``
    - ``no_etcd``
    - promote, switch
    - false
    - ≥ 2.0.0
  * - Отключает master fencing
    - ``--no-fence-master``
    - ``no_fence_master``
    - promote
    - false
    - ≥ 2.0.0
  * - Задает таймаут на switch
    - ``-t``, ``--timeout``
    - ``switch_timeout``
    - switch
    - 2*max_lag
    - ≥ 2.0.0
  * - Конфигурирует настройки графита для автофейловера
    - none
    - autofalolover.graphite
    - watch
    - `{ host: "ipaddr / host", port: 2003, prefix: "" }``
    - ≥ 2.6.0

Пример файла конфигурации
*************************

.. code-block:: yaml

    ---
    etcd:
      prefix: /apps # no / at the end
      timeout: 3
      endpoints:
        - http://etcd0:2379
        - http://etcd1:2379
        - http://etcd2:2379
    max_lag: 3
    discovery_timeout: 3
    switch_timeout: 5


Первый запуск
*************

.. code-block:: bash

  Usage: switchover _VERSION_ [-h] [--completion {bash,zsh,fish}] [-C]
        [-c <config>] [-w <work_dir>] [--no-config] [-e <etcd>]
        [-p <prefix>] [-q] [-v] [-d <discovery_timeout>]
        [--cartridge <cartridge>] [-f <format>] <command> ...

  Tarantool master <-> replica switchover

  Options:

   -h, --help                                     Show this help message and exit.
   --completion {bash,zsh,fish}                   Output a shell completion script for the specified shell.
   -C, --color                                    Enables colored logs
         -c <config>,                             Path to config (default: /etc/switchover/config.yaml)
   --config <config>
           -w <work_dir>,                         Path to workdir where switchover will keep its files (default: .)
   --work-dir <work_dir>
   --no-config                                    Does not load config even if it exists
       -e <etcd>,                                 Address to ETCD endpoint
   --etcd <etcd>
         -p <prefix>,                             Prefix to configuration of clusters in ETCD
   --prefix <prefix>
   -q, --quiet                                    Disables logs
   -v, --verbose                                  Verbosity level
                    -d <discovery_timeout>,       Discovery timeout (in seconds)
   --discovery_timeout <discovery_timeout>
   --cartridge <cartridge>                        URI to any cartridge instance
         -f <format>,                             Discovery format
   --format <format>

  Commands:

   help                                      Show help for commands.
   version                                   Prints switchover version
   discovery, d                              Discovers all members of the replicaset
   status, s                                 Shows status of cluster
   promote                                   Promotes given instance to master
   switch, sw                                Switches current master to given instance
   auto                                      Runs auto failover for single shard
   watch                                     Watch of healthiness of cluster
   heal                                      Heals ETCD /cluster/master
   restart-replication, rr                   Restarts replication on choosen instance
   eval                                      Evals given command on instance or all instances
   package-reload, pr                        Reload replication on given instance
   connect, c                                Opens tarantool console to given instance
   etcd, e                                   Interface to operate with ETCD

  Home: https://gitlab.com/ochaton/switchover
