local M = {}
local log = require 'log'
local json = require 'json'
local e = require 'switchover._error'
local clock = require 'clock'
local fiber = require 'fiber'

local function iskv(tbl)
	local prev
	for k in pairs(tbl) do
		if type(k) ~= 'number' then
			return true
		elseif prev and prev+1 ~= k then
			return true
		end
		prev = k
	end
	return false
end

local function prettify(tbl)
	if type(tbl) ~= 'table' then return tbl end
	if iskv(tbl) then
		setmetatable(tbl, {__serialize='mapping'})
	else
		setmetatable(tbl, {__serialize='array'})
	end
	for k, v in pairs(tbl) do
		tbl[k] = prettify(v)
	end
	return tbl
end

local outputs = {
	json = require 'json',
	yaml = require 'yaml',
	plain = {
		encode = function(tbl)
			for k, v in ipairs(tbl) do
				if type(v) == 'table' then
					v = json.encode(v)
				end
				tbl[k] = tostring(v)
			end
			return table.concat(tbl, " ")
		end
	}
}

function M.run(args)
	assert(args.command == "eval")

	do
		if not args.shell:match('^%s*return') and not args.shell:match("=") and not args.shell:match('do') then
			args.shell = 'return '..args.shell
		end
		local ok, err = loadstring(args.shell)
		if not ok then
			e.panic("Compilation of command %q failed: %s", args.shell, err)
		end
	end

	if args.all then
		args.shard = true
		args.cluster = nil
		args.all = nil
	end

	local order = {}
	if args.cluster or args.shard then
		local cluster = require 'switchover.core.resolve_and_discovery'.cluster_or_shard(args)

		local shards = require 'switchover.discovery'.cluster({
			cluster = cluster,
			discovery_timeout = args.discovery_timeout,
		})

		if args.cluster and not args.yes then
			io.stdout:setvbuf("no")
			io.stdout:write("You are going to execute command on all instances of the cluster. Are you sure? [yes/no] ")
			if io.stdin:read("*line") ~= "yes" then
				log.error("Discarding command execution")
				return 1
			end
		end

		for _, sh in pairs(shards) do
			local repl = sh.replicaset
			for _, inst in ipairs(repl.replica_list) do
				if cluster:instance_by_uuid(inst:uuid()) then
					table.insert(order, inst)
				end
			end
		end

		table.sort(order, function(a, b)
			local aro = a:ro() and 1 or 0
			local bro = b:ro() and 1 or 0
			return aro > bro -- read_only must come first
		end)
	else
		local candidate = require 'switchover.discovery'.candidate {
			instance = args.instance,
			discovery_timeout = args.discovery_timeout,
		}

		order = { candidate }
	end

	local retcode = 0

	local first = true
	for _, inst in ipairs(order) do
		if not args.dry_run then
			if first then
				first = false
			else
				fiber.sleep(args.eval_interval or 0)
			end
			print(("Executing on %s"):format(inst:name()))
			local start = clock.time()
			local ret = {pcall(inst.conn.eval, inst.conn, args.shell)}
			local elapsed = clock.time()-start
			local ok = table.remove(ret, 1)
			if not ok then
				local err = table.remove(ret, 1)
				if args.no_fail then
					log.error("%s: %q failed: %s", inst.endpoint, args.shell, err)
					retcode = 1
				else
					e.panic("Eval of %q failed on %s with %s", args.shell, inst:name(), err)
				end
			else
				print(outputs[args.output].encode(prettify(ret)))
			end
			log.info("Executed in %.3fs", elapsed)
		else
			log.info("dry run eval %q on %s/%s", args.shell, inst:name(), inst:role())
		end
	end

	return retcode
end

return M
