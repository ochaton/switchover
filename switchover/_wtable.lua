---@class WTable2
---@field padding string
---@field align_cell string
---@field height number
---@field width number
---@field data string[][]
---@field rows any[]
---@field border string
---@field left_border string
---@field right_border string
local M = {}
M.__index = M

function M:__tostring()
	return ("wtab[h:%sxw:%s]"):format(self.height, self.width)
end

local term = require 'switchover._plterm'

function M:setData(data)
	self.data = data
end

local function center(sz, len)
	if sz > len then
		local lpad = math.floor((sz-len)/2)
		local rpad = sz-len-lpad
		sz = (' '):rep(lpad) .. '%s' .. (' '):rep(rpad)
	else
		sz = '%s'
	end
	return sz
end

local function left(sz)
	return '%-'..sz..'s'
end

local function right(sz)
	return '%'..sz..'s'
end

local function draw(line, szs, padd, align)
	padd = padd or " "
	local txt = {}
	for i = 1, #szs do
		local sz
		local al = line[i]._align or align
		if line[i].draw then
			txt[i] = line[i]:draw(szs[i], al)
		else
			local word = tostring(line[i])
			if al == 'right' then
				sz = right(szs[i])
			elseif al == 'left' then
				sz = left(szs[i])
			elseif al == 'center' then
				sz = center(szs[i], #word)
			end
			txt[i] = sz:format(word)
		end
	end
	return table.concat(txt, padd)
end

function M:draw()
	local data = self.data
	self.rows = table.new(#data + 1, 0)

	local szs = {}
	for _, line in ipairs(data) do
		for col = 1, #line do
			szs[col] = math.max(szs[col] or 0, line[col]:len())
		end
	end
	local left_border = self.left_border or self.border or ""
	local right_border = self.right_border or self.border or ""
	for row_id, line in ipairs(data) do
		self.rows[row_id] = left_border .. draw(line, szs, self.padding, self.align_cell)..right_border
	end

	for _, row in ipairs(self.rows) do
		row = row:sub(1, self.width)
		print(row)
	end
end

function M.init()
	term.setrawmode()
	local rws, cls = term.getscrlc()
	term.setsanemode()

	return setmetatable({
		width = cls,
		height = rws,
		padding = " ",
		align_cell = 'center',
	}, M)
end

---@class Cell
---@field text string
---@field _color string
local Cell = {}
Cell.__index = Cell

function Cell:len()
	return #self.text
end

function Cell:__tostring()
	return self._color .. self.text .. term.colorf(0)
end

function Cell:draw(size, align)
	if align == 'left' then
		return self._color .. left(size):format(self.text) .. term.colorf(0)
	elseif align == 'right' then
		return self._color .. right(size):format(self.text) .. term.colorf(0)
	elseif align == 'center' then
		local len = #self.text
		return self._color .. center(size, len):format(self.text) .. term.colorf(0)
	end
end

function Cell.init(text)
	return setmetatable({ text = text, _color = term.colorf(0) }, Cell)
end

function Cell:setColor(foreground, background, attributes)
	self._color = term.colorf(foreground, background, attributes)
	return self
end

function Cell:setAlign(align)
	self._align = align
	return self
end

function M.newcell(text)
	return Cell.init(text)
end


return M
