local M = {}
local log = require 'log'
local uri = require 'uri'
local console = require 'console'
local fun = require 'fun'

local ps1_format = [==[[{{date}} {{time}}] ({{cmd_t}}) {{remote.instance_name}} role:{{remote.role}} etcd:{{remote.etcd_role}} {{remote.alerts}} Arena:{{p(remote.slab.arena_used/remote.slab.quota_size)}} LjMEM:{{hb(remote.mem.lua)}}]==].."\n" -- luacheck:ignore

function M.run(args)
	assert(args.command == "connect")

	local candidate, repl, _, etcd_shard = require 'switchover.discovery'.candidate {
		instance = args.instance,
		discovery_timeout = args.discovery_timeout,
	}

	local on_fetch

	if etcd_shard then
		on_fetch = function(res)
			local info = require 'switchover.core.resolve_and_discovery'.refresh {
				shards = { [etcd_shard.name] = { shard_name = etcd_shard.name, shard = etcd_shard, replicaset = repl } },
			}

			local ms = fun.iter(info.alerts)
				:reduce(function(r, al) r[al.level] = (r[al.level] or 0)+1 return r end, {0,0,0,0})

			res.alerts = ""
			for i = 1, 4 do
				if ms[i] > 0 then
					res.alerts = res.alerts..({'CRIT','HIGH','MID','LOW'})[i]..(':%d '):format(ms[i])
				end
			end
			if #res.alerts > 0 then
				res.alerts = ' Alerts:'..res.alerts
			end

			res.etcd_role = etcd_shard:instance_by_uuid(candidate:uuid()).role
			res.role = candidate:role()
			res.instance_name = candidate:name()

			res.info = candidate.cached_info
			res.mem  = candidate.cached_info.memory
			res.stat = candidate.stat_info
			res.net  = candidate.stat_info.net
			res.slab = candidate.slab_info
			return res
		end
	else
		on_fetch = function(res)
			res.role = candidate:role()
			res.instance_name = candidate:name()
			res.info = candidate.cached_info
			res.mem  = candidate.cached_info.memory
			res.stat = candidate.stat_info
			res.net  = candidate.stat_info.net
			res.slab = candidate.slab_info
			return res
		end
	end

	local endpoint = candidate.endpoint
	local euri = uri.parse(endpoint)

	if args.user and not euri.login then
		euri.login = args.user
	end
	if args.pass then
		if not euri.login then euri.login = 'admin' end
		euri.password = args.pass
	end

	local destination = ('%s:%s@%s:%s'):format(
		(euri.login or 'guest'),
		(euri.password or ''),
		euri.host,
		euri.service or '3301'
	)
	log.info("Connecting to %s", destination:gsub(":.+@", ":****@"))

	console.on_start(function(self)
		if args.use_ps1 then
			local ps1 = require 'switchover._ps1'
			ps1.on_remote_fetch = on_fetch
			ps1.remote_stat = on_fetch({})
			ps1.format = ps1_format
		end
        local status, reason
        status, reason = pcall(function()
            console.connect(destination, {
                connect_timeout = args.discovery_timeout
            })
        end)
        if not status then
            log.info("Connection failed: %s", reason)
            self.running = false
        end
	end)
	console.on_client_disconnect(function()
		log.info("Disconnected from %s", candidate.masked_endpoint)
		os.exit(0)
	end)
	console.start()
	return 0
end


return M