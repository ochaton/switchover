local M = {}

local function copy(t)
	local r = {}
	for k in pairs(t) do
		r[k] = t[k]
	end
	return r
end

---@class CheckError
---@field path string
---@field path_r string[]
---@field message string
local E = {}
E.__index = E

---@param err CheckError
function E.new(err)
	local self = setmetatable(err, E)
	self.path_r = {}
	for chunk in self.path:gmatch("([^.]+)") do
		table.insert(self.path_r, chunk)
	end
	if self.path_r[1] == 'arg' then
		table.remove(self.path_r, 1)
	end
	return self
end

function E:__tostring()
	return ("'%s' %s"):format((self.path:gsub("^arg%.?", "")):gsub("^$", "/"), self.message)
end

---@class CheckContext
---@field path string
---@field path_r string[]
---@field errors CheckError[]
---@field value any
---@field init any
local context = {}
context.__index = context

---get key
---@param depth? number
---@return string?
function context:key(depth)
	depth = math.min(tonumber(depth) or 1, 1)
	return self.path_r[depth]
end

---comment
---@param str string
---@param ... unknown
function context:push_error(str, ...)
	table.insert(self.errors, E.new { path = self.path, message = str:format(...), path_r = copy(self.path_r) })
end

function context:push_field_error(field, str, ...)
	local path
	if field:match('^[a-zA-Z_][0-9A-Za-z_]*$') then
		path = ('%s.%s'):format(self.path, field)
	else
		path = ('%s[%q]'):format(self.path, field)
	end
	table.insert(self.errors, E.new { path = path, message = str:format(...), path_r = copy(self.path_r) })
end

---@class Checker
---@field check fun(Checker, ctx: CheckContext, value: any): boolean
local C = {}
C.__index = C

function C:verify_safe(data)
	local ctx = setmetatable({ path = 'arg', path_r = {}, init = data, errors = {}, value = data }, context)
	local ok = self:check(ctx, data)
	ctx.ok = ok
	if not ok then
		return nil, ctx
	end
	return ctx.value or data, ctx
end

function C:verify(data)
	local ret, ctx = self:verify_safe(data)
	if not ctx.ok then
		local msg = {}
		for i, e in ipairs(ctx.errors) do
			msg[i] = tostring(e)
		end
		error("Validation failed:\n\t" .. table.concat(msg, "\n\t"), 0)
	end
	return ret
end

---@class ObjectParams
---@field required string[]

---@class ObjectChecker: Checker
---@field required string[]
---@field _fields table<string, Checker>
---@field _constaints table<string, (fun(table, CheckContext):boolean)>
---@field matchers table<string, Checker>
---@field match_order string[]
---@field allow_undefined boolean
local Object = setmetatable({}, C)
Object.__index = Object

---@param fields table<string, Checker>
---@return ObjectChecker
function Object:fields(fields)
	self._fields = fields
	return self
end

---@param matchers table<string, Checker>
---@return ObjectChecker
function Object:match(matchers)
	self.matchers = matchers
	self.match_order = {}
	for match in pairs(matchers) do
		table.insert(self.match_order, match)
	end
	table.sort(self.match_order, function(a, b) return #a < #b end)
	return self
end

---@param funcs table<string, (fun(table):boolean)>
---@return ObjectChecker
function Object:must(funcs)
	self._constaints = funcs
	return self
end

---comment
---@param ctx CheckContext
---@param value any
---@return boolean
function Object:check(ctx, value)
	if type(value) ~= 'table' then
		ctx:push_error("required to be 'table' got %s", type(value))
		return false
	end

	local has_error = false
	for _, field in ipairs(self.required) do
		if type(value[field]) == 'nil' then
			has_error = true
			ctx:push_field_error(field, 'field is required but not given')
		end
	end

	local validate_with = {}
	for key in pairs(value) do
		local by_name = self._fields[key]

		if by_name then
			validate_with[key] = by_name
			goto continue
		end

		for _, match in ipairs(self.match_order) do
			if key:match(match) then
				validate_with[key] = self.matchers[match]
				break
			end
		end

		if not validate_with[key] and not self.allow_undefined then
			has_error = true
			ctx:push_field_error(key, 'unexpected field given')
		end

		::continue::
	end

	local path = ctx.path
	local path_r = copy(ctx.path_r)
	for key, checker in pairs(validate_with) do
		ctx.path_r = copy(path_r)
		table.insert(ctx.path_r, 1, key)

		if key:match("^[a-zA-Z_][a-zA-Z0-9_]*$") then
			ctx.path = path .. '.' .. key
		else
			ctx.path = path .. "['" .. key .. "']"
		end

		-- save ctx.value
		local ctx_value = ctx.value or value
		ctx.value = nil

		local ok = checker:check(ctx, ctx_value[key])
		if ok and ctx.value then
			value[key] = ctx.value
		end
		if not ok then
			has_error = true
		end
		-- we do not need to push errors, checker do it itself

		-- restore ctx.value
		ctx.value = ctx_value
	end
	-- restore path
	ctx.path = path
	ctx.path_r = path_r

	for name, func in pairs(self._constaints) do
		if not func(value, ctx) then
			has_error = true
			ctx:push_error("violates '%s'", name)
		end
	end

	return not has_error
end

---comment
---@param opt ObjectParams
---@return ObjectChecker
function M.object(opt)
	local self = {
		required = {},
		matchers = {},
		match_order = {},
		_fields = {},
		allow_undefined = true,
		_constaints = {},
	}
	for k in pairs(opt or {}) do
		self[k] = opt[k]
	end
	return setmetatable(self, Object)
end

---@class LuaType: Checker
---@field type "string"|"number"|"boolean"|"nil"|"function"
---@field cast? fun(any): any caster
local LuaType = setmetatable({}, C)
LuaType.__index = LuaType

---comment
---@param ctx CheckContext
---@param value any
---@return boolean
function LuaType:check(ctx, value)
	local lua_type = type(value)
	if lua_type == self.type then
		return true
	end
	if not self.cast then
		ctx:push_error("required to be '%s' but given '%s'", self.type, lua_type)
		return false
	end

	value = self.cast(value)
	if type(value) == self.type then
		ctx.value = value
		return true
	end

	ctx:push_error("required to be '%s' but given '%s' (cast '%s')",
		self.type, lua_type, type(value))
	return false
end

local casters = {
	string = tostring,
	number = tonumber,
	boolean = function(v)
		if v == 'false' then return false end
		if v == 'true' then return true end
		return nil
	end,
}

---@param luatype "string"|"number"|"boolean"
---@param opt string?
---@return LuaType
local function build_luatype(luatype, opt)
	local v = setmetatable({ type = luatype }, LuaType)
	opt = opt or ''
	if opt:match('%+cast') then
		v.cast = casters[luatype]
	end
	return v
end

---@param opt? string
---@return LuaType
function M.string(opt)
	return build_luatype('string', opt)
end

---@param opt? string
---@return LuaType
function M.number(opt)
	return build_luatype('number', opt)
end

---@param opt? string
---@return LuaType
function M.boolean(opt)
	return build_luatype('boolean', opt)
end

---@class EnumChecker: Checker
---@field values table<any, boolean>
local EnumChecker = setmetatable({}, C)
EnumChecker.__index = EnumChecker

---comment
---@param ctx CheckContext
---@param value any
---@return boolean
function EnumChecker:check(ctx, value)
	for val in pairs(self.values) do
		if val == value then
			return true
		end
	end

	ctx:push_error("value '%s' not defined in enum", value)
	return false
end

---@param values any[]
function M.enum(values)
	local v = setmetatable({ values = {} }, EnumChecker)
	for _, value in pairs(values) do
		v.values[value] = true
	end
	return v
end

---@class OneOfChecker: Checker
---@field choices Checker[]
local OneOf = setmetatable({}, C)
OneOf.__index = OneOf

---comment
---@param ctx CheckContext
---@param value any
---@return boolean success
function OneOf:check(ctx, value)
	for _, choice in ipairs(self.choices) do
		local child = setmetatable({
			path = ctx.path,
			path_r = copy(ctx.path_r),
			errors = {},
			init = ctx.init,
			value = ctx.value,
		}, context)
		if choice:check(child, value) then
			return true
		end
	end
	ctx:push_error("does not pass any of OneOf checkers")
	return false
end

---comment
---@param choices Checker[]
function M.oneof(choices)
	local v = setmetatable({}, OneOf)
	v.choices = choices
	return v
end

return M
