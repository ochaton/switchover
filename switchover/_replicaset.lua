---
---@alias InstanceUUID string
---@class SwitchoverReplicaset
---Container of Tarantool instances connected via native replication.
---@field public uuid string UUID of Replicaset
---@field public replica_list Tarantool[] list of tarantools
---@field public replicas table<InstanceUUID, Tarantool> kv of tarantools
local Replicaset = {}
Replicaset.__index = Replicaset

local log = require 'log'
local fun = require 'fun'
local json = require 'json'
local Cluster = require 'switchover._cluster'


---
---Constructor of Replicaset
---@param tnts Tarantool[] list of tarantools
---@return SwitchoverReplicaset
function Replicaset:new(tnts)
	local replicaset = setmetatable({ replicas = {}, replica_list = {} }, self)
	for _, tnt in ipairs(tnts) do
		if not replicaset.uuid then
			replicaset.uuid = tnt:info().cluster.uuid
		end
		replicaset:add_replica(tnt)
	end
	return replicaset
end

function Replicaset:check_cluster_uuid(tnt)
	if tnt:info().cluster.uuid ~= self.uuid then
		error(("Cluster uuid missmatch for %s: expected %s got %s"):format(
			tnt, self.uuid, tnt:info().cluster.uuid
		), 3)
	end
end

---
---@param t Tarantool replica
---Checks duplicates via Replicaset.replicas. Checks cluster_uuid
function Replicaset:add_replica(t)
	self:check_cluster_uuid(t)

	if not self.replicas[t:uuid()] then
		-- t.replicaset = self
		self.replicas[ t:uuid() ] = t
		table.insert(self.replica_list, t)
		log.verbose("Replica %s/%s was registered (id: %s, vclock: %s, role: %-7s)",
			t:uuid(), self.uuid, t:id(), json.encode(t:vclock()), t:role())
	else
		log.warn("[Replicaset %s] Replica %s already exists", self.uuid, t:uuid())
	end
end

---@return Tarantool[] # returns list of masters (calls Tarantool:role)
function Replicaset:masters()
	return fun.iter(self.replica_list)
		:grep(function(t) return t:role() == 'master' end)
		:totable()
end

---Fails if Replicaset has not single master
---@param nofail boolean|nil nofails if Replicaset has more than 1 master (default: nil)
---@return Tarantool returns master if Replicaset has it
function Replicaset:master(nofail)
	local masters = self:masters()
	if not nofail then
		assert(#masters <= 1, "Too many masters in replicaset")
	end
	return masters[1]
end

---@param instance Tarantool replica to rate
---@return number score of given replica
function Replicaset:rate(instance)
	local score = 0

	if self:has_enabled_raft() then
		if instance:raft().election_mode == 'candidate' then
			-- If node is candidate then it definately can be the leader
			score = score + 500
		else
			if instance:cfg().read_only then
				-- Having RAFT would be difficult to promote RO node to the leader
				score = score - 100
			end
		end
	end

	local masters = self:masters()
	if #masters > 0 then
		-- choose the one with the biggest signature
		table.sort(masters, function(a, b) return a:signature() > b:signature() end)
		local master = masters[1]

		if instance:replicates_from(master:uuid()) then
			score = score + 100
		end
		if master:replicates_from(instance:uuid()) then
			score = score + 100
		end
	end

	score = score + 100*#instance:followed_downstreams()
	score = score + 100*#instance:followed_upstreams()

	return score
end

local etonode = function(e)
	return (e:gsub("[^%w%d]", "_"))
end

function Replicaset:graph()
	local graphviz = {
		'digraph G {',
			'\tnode[shape="circle"]',
	}

	for _, r in pairs(self.replicas) do
		table.insert(graphviz,
			("\t"..[[%q [label="%s/%s: %s\n%s"] ]]):format(
				etonode(r.endpoint), r:id(), r:role(), (json.encode(r:vclock()):gsub([["]], "'")),
				r.endpoint
		))
	end

	for _, r in pairs(self.replicas) do
		for _, u in pairs(r:info().replication) do
			local up = self.replicas[u.uuid]
			if u.upstream and up then
				table.insert(graphviz,
					("\t"..[[%q -> %q [style=%s,label="%.3fs"] ]]):format(
						etonode(up.endpoint), etonode(r.endpoint),
						u.upstream.status == "follow" and "solid" or "dotted",
						u.upstream.lag or 0
					)
				)
			end
		end
	end

	table.insert(graphviz, '}')
	return table.concat(graphviz, "\n")
end

function Replicaset:score(leader)
	-- how many instances successfully replicates data from leader
	local downs = fun.iter(leader:followed_downstreams()):map(function(down)
		return self.replicas[ down.uuid ]
	end):grep(function(down)
		return down:replicates_from(leader:uuid())
	end):totable()

	-- how many instances pushes their data to leader
	local ups = fun.iter(leader:followed_upstreams()):map(function(up)
		return self.replicas[ up.uuid ]
	end):grep(function(up)
		return up:replicated_by(leader:uuid())
	end):totable()

	return ups, downs
end

function Replicaset:has_enabled_raft()
	for _, tt in ipairs(self.replica_list) do
		local em = tt:cfg().election_mode
		if em and em ~= "off" then
			return true
		end
	end
	return false
end

function Replicaset:has_cartridge()
	for _, tt in ipairs(self.replica_list) do
		if tt.has_cartridge then
			return true
		end
	end
	return false
end

function Replicaset:cartridge_resolve()
	assert(self:has_cartridge(), "replicaset must have cartridge")
	local inst = fun.iter(self.replica_list):grep(function(tt) return tt.has_cartridge end):nth(1)
	local user_pass = inst.endpoint:match("^([^@]+)")

	local cluster = inst.conn:eval[[
		local cartridge = require 'cartridge'
		local cluster = { clusters = {}, instances = {} }

		local servers = cartridge.admin_get_servers()
		for _, s in ipairs(servers) do
			cluster.instances[s.alias] = {
				box = { listen = s.uri, instance_uuid = s.uuid },
				cluster = s.replicaset.alias
			}
			cluster.clusters[s.replicaset.alias] = {
				master = s.replicaset.master.alias,
				replicaset_uuid = s.replicaset.uuid,
			}
		end
		return cluster
	]]

	for _, s in pairs(cluster.instances) do
		s.box.listen = user_pass .. '@' .. s.box.listen
	end
	if cluster then
		return Cluster:new {
			tree = cluster,
			policy = 'etcd.cluster.master',
			name = 'cartridge',
		}
	end
end

function Replicaset:has_vshard()
	for _, tt in ipairs(self.replica_list) do
		if tt.has_vshard then
			return true
		end
	end
	return false
end

function Replicaset:vshard_topology()
	assert(self:has_vshard(), "replicaset must have vshard")
	local inst = fun.iter(self.replica_list):grep(function(tt) return tt.has_vshard end):nth(1)

	local cluster = inst.conn:eval[[
		local rs = (vshard.storage.internal.replicasets or vshard.router.internal.static_router.replicasets)
		if not rs then return false, "replicasets not found" end
		local cluster = { clusters = {}, instances = {} }
		for r_uuid, r in pairs(rs) do
			cluster.clusters[r_uuid] = {
				master = r.master.name,
				replicaset_uuid = r_uuid,
			}
			for i_uuid, i in pairs(r.replicas) do
				cluster.instances[i.name] = {
					cluster = r_uuid,
					box = {
						instance_uuid = i.uuid,
						listen = i.uri,
					},
				}
			end
		end
		return cluster
	]]
	if cluster then
		return Cluster:new {
			tree = cluster,
			policy = 'etcd.cluster.master',
			name = 'vshard',
		}
	end
end

function Replicaset:auto_topology()
	return Cluster:new {
		tree = {
			clusters = {
				[self:info().uuid] = {
					master = 'instance_'..self:master():id(),
					replicaset_uuid = self:info().uuid,
				},
			},
			instances =
				fun.iter(self.replica_list)
					---@param r Tarantool
					:map(function(r)
						return
							("instance_%s"):format(r:id()),
							{
								cluster = self:info().uuid,
								box = {
									instance_uuid = r:uuid(),
									listen = r.endpoint,
								},
							}
					end)
					:tomap()
			,
		},
		policy = 'etcd.cluster.master',
		name = 'auto',
	}
end


---@param role BoxInfoElectionStateCandidate | BoxInfoElectionStateFollower
---@return Tarantool[]
function Replicaset:election_mode(role)
	return fun.iter(self.replica_list)
		:grep(function(tt)
			return tt:cfg().election_mode == role
		end)
		:totable()
end

function Replicaset:cluster_max_id()
	local cluster_max_id = 0
	for _, r in ipairs(self.replica_list) do
		if r.connected and cluster_max_id < r.cluster_max_id then
			cluster_max_id = r.cluster_max_id
		end
	end

	return cluster_max_id
end

function Replicaset:max_vclock()
	local maxv = fun.range(1, self:cluster_max_id()):zip(fun.zeros()):tomap()

	for _, r in ipairs(self.replica_list) do
		if not r.connected then goto skip end

		for id, lsn in pairs(r:vclock()) do
			if not maxv[id] or maxv[id] < lsn then
				maxv[id] = lsn
			end
		end

		::skip::
	end

	return maxv
end

---@param tt Tarantool
---@return number score
function Replicaset:stability_score(tt)
	assert(tt:cluster_uuid() == self.uuid, "replica does not belong to this cluster")

	-- how much downstreams of tt including tt we may lose to keep connectivity to quorum of nodes

	-- Simple strategy:
	return #tt:followed_downstreams()
end

---Evaluates paths and evaluates max achievable vclock to the instance
---@param tt Tarantool
---@return table<integer,integer> vclock
function Replicaset:max_achievable_vclock(tt)

	local function upstreams(r)
		local us = {}
		for _, u in ipairs(r:followed_upstreams()) do
			table.insert(us, self.replicas[u.uuid])
		end
		return us
	end

	local maxv = {}

	for _, r in ipairs(self.replica_list) do
		local uuid = r:uuid()
		maxv[uuid] = fun.range(1, self:cluster_max_id()):zip(fun.zeros()):tomap()

		-- set already achived vclock
		for id, lsn in pairs(r:vclock()) do
			maxv[uuid][id] = lsn
		end
	end

	local function dfs(r, seen)
		seen = seen or {}
		seen[r] = true

		local r_maxv = maxv[r:uuid()]
		log.verbose("DFS(%s) <= %s", r:uuid(), json.encode(r_maxv))

		local function copy_t(t)
			local nt = {}
			for k, v in pairs(t) do
				nt[k] = v
			end
			return nt
		end

		local us = {}
		for _, u in ipairs(upstreams(r)) do
			if not seen[u] then
				table.insert(us, u)
			end
		end

		for _, u in ipairs(us) do
			local u_maxv = dfs(u, copy_t(seen))
			for id in pairs(u_maxv) do
				if r_maxv[id] < u_maxv[id] then
					r_maxv[id] = u_maxv[id]
				end
			end
		end

		log.verbose("DFS(%s) => %s", r.endpoint, json.encode(r_maxv))
		return r_maxv
	end

	return dfs(tt, {})
end

function Replicaset:refresh()
	for _, r in ipairs(self.replica_list) do
		r:_get{update = true}
	end
end

function Replicaset:destroy()
	log.verbose("Destroying replicaset %s", self.uuid)
	for _, r in pairs(self.replicas) do
		r:destroy()
	end
	self.replicas = {}
	self.replica_list = {}
end

function Replicaset:destroy_background()
	log.verbose("Destroying background fibers in replicaset %s", self.uuid)
	for _, r in pairs(self.replicas) do
		r:destroy_background()
	end
end

---@class SwitchoverReplicasetShadowInfo
---@field uuid string uuid of repicaset
---@field replicas table<string, Tarantool> shadow kv-map of replicas

---Returns shadow repicaset info
---@return SwitchoverReplicasetShadowInfo
function Replicaset:info()
	return {
		uuid = self.uuid,
		replicas = fun.iter(self.replicas)
			:map(function(key, t) return key, t:shadow() end)
			:tomap(),
	}
end

---@param upstream_uuid string
---@return Tarantool[]
function Replicaset:followed_downstreams(upstream_uuid)
	local t = self.replicas[upstream_uuid]
	if not t then return {} end

	---@type Tarantool[]
	local rs = {}
	for _, dws in ipairs(t:followed_downstreams()) do
		local r = self.replicas[dws.uuid]
		if not r then return {} end
		if not r:replicates_from(upstream_uuid) then
			return {}
		end

		table.insert(rs, r)
	end

	return rs
end

return setmetatable(Replicaset, { __call = Replicaset.new })