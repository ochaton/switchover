local e = require 'switchover._error'
local fun = require 'fun'
local log = require 'switchover._log'

local ansicolors = require 'switchover._ansicolors'
local grammar = require 'switchover._selector_grammar'.G
local lpeg = require 'switchover._selector_grammar'.lpeg

local M = {}

---Deep indexes given table
---@param t table
---@param path string
local function deep_index(t, path)
	local pos = t
	for chunk in path:gmatch('([^.]+)') do
		if type(pos) ~= 'table' then
			return nil, false
		end
		pos = pos[chunk]
	end
	return pos, true
end

local AST = {
	S = function(self, tag, _, ...)
		assert(tag == "S")
		local And = {}
		for _, expr in ipairs({...}) do
			log.verbose(expr)
			local operation = expr[1]
			local subtext = expr[2]
			local subtree = expr[3]
			log.verbose("ast:%s(%s, %s)", operation, subtext, subtree)
			table.insert(And, self[operation](self, subtext, subtree))
		end
		local multigrep = function(node)
			for _, cond in ipairs(And) do
				if not cond(node) then
					return false
				end
			end
			return node
		end
		return function(iter)
			return fun.iter(iter):grep(multigrep)
		end
	end,
	Expr = function(self, _, expr)
		local operation = table.remove(expr, 1)
		local subtext = table.remove(expr, 1)
		return self[operation](self, subtext, unpack(expr))
	end,
	Is = function(self, _, id)
		local key = self:Id(unpack(id))

		return function(node)
			local v, is_ok = deep_index(node, key)
			if not is_ok then
				return false
			end
			if v == nil then -- special case for box.NULL
				return false
			end
			return not not v
		end
	end,
	NotIn = function(self, _, id, ...)
		local key = self:Id(unpack(id))
		local notset = {}
		for _, term in ipairs({...}) do
			local val = self:Term(unpack(term))
			notset[val] = false
		end

		return function(node)
			local v, is_ok = deep_index(node, key)
			if not is_ok then
				return false
			end
			return notset[v] ~= false
		end
	end,
	In = function(self, _, id, ...)
		local key = self:Id(unpack(id))
		local set = {}
		for _, term in ipairs({...}) do
			local val = self:Term(unpack(term))
			set[val] = true
		end

		return function(node)
			local v, is_ok = deep_index(node, key)
			if not is_ok then
				return false
			end
			return set[v] == true
		end
	end,
	Id = function(_, tag, text)
		assert(tag == "Id")
		return text
	end,
	Term = function(_, tag, text)
		assert(tag == "Term")
		return text
	end,
	UnNeq = function(self, _, id)
		local key = self:Id(unpack(id))
		return function(node)
			local v, is_ok = deep_index(node, key)
			if not is_ok then
				return false
			end
			return not v
		end
	end,
	Eq = function(self, _, id, term)
		local key = self:Id(unpack(id))
		local value = self:Term(unpack(term))

		return function(node)
			local v, is_ok = deep_index(node, key)
			if not is_ok then
				return false
			end
			return v == value
		end
	end,
	Neq = function(self, _, id, term)
		local key = self:Id(unpack(id))
		local value = self:Term(unpack(term))

		return function(node)
			local v, is_ok = deep_index(node, key)
			if not is_ok then
				return false
			end
			return v ~= value
		end
	end,
}


---Parses string and returns Selector
---@param str string
---@return fun(iter: Iterator)
function M.parse(str)
	-- May I just table.split arguments?
	-- No, I might not :(
	assert(type(str) == 'string', "selector.parse accepts only strings")

	local ast = lpeg.match(grammar, str)
	if type(ast) ~= 'table' then
		e.panic("Selector %q is unparsable", str)
	end

	if #ast[2] ~= #str then
		local endpos = #ast[2]
		e.panic('Selector parsing failed at position %d: "%s%s%s"',
			endpos, str:sub(1, endpos-1),
				ansicolors('%{red}'..str:sub(endpos, endpos)),
			str:sub(endpos+1)
		)
	end

	return AST:S(unpack(ast))
end

return M