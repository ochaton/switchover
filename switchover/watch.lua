local M = {}
local sync = require 'sync'
local fiber = require 'fiber'
local console = require 'console'
local socket = require 'socket'
local errno = require 'errno'
local G = require 'switchover._global'
local e = require 'switchover._error'
local auto_switch = require 'switchover.auto'
local hostname = require 'switchover._hostname'
local heal_safe_switch = require 'switchover.heal'.safe_switch
local build_alerts = require 'switchover.core.resolve_and_discovery'.build_alerts
local background = require 'switchover._background'
local log = require 'switchover._log'
local fio = require 'fio'
local fun = require 'fun'
local clock = require 'clock'
local audit = require 'switchover._audit'
local stats = require 'switchover._stats'

local print_etcd_discovery = require 'switchover.discovery'.print_etcd_discovery

local function n_2_1(n)
	local h = math.floor(n/2)
	if 2*h <= n then
		assert(2*(h+1) > n)
		return h+1
	else
		assert(2*h > n)
		return h
	end
end

assert(n_2_1(0) == 1, "0")
assert(n_2_1(1) == 1, "1")
assert(n_2_1(2) == 2, "2")
assert(n_2_1(3) == 2, "3")
assert(n_2_1(4) == 3, "4")
assert(n_2_1(5) == 3, "5")
assert(n_2_1(6) == 4, "6")
assert(n_2_1(7) == 4, "7")
assert(n_2_1(8) == 5, "8")

---@alias AutofailoverResolution
---| "ok"
---| "is_proxy"
---| "no_master"
---| "not_enough_quorum_to_autopromote"
---| "master_is_not_promotable"
---| "legitimate_master_is_ro"
---| "real_master_missmatch"
---| "vshard_is_broken"


---@class SwitchoverWatchCheckShardArgs
---@field replicaset SwitchoverReplicaset
---@field etcd_shard Cluster
---@field ignore_tarantool_quorum boolean

---Evaluates failover resolution for shard
---@param args SwitchoverWatchCheckShardArgs
---@return AutofailoverResolution
---@return string? msg
local function check_shard(args)
	---@type SwitchoverReplicaset
	local repl = assert(args.replicaset, "replicaset is required")
	---@type Cluster
	local etcd_shard = assert(args.etcd_shard, "etcd_shard is required")

	if etcd_shard:is_proxy() then
		return 'is_proxy'
	end

	if #repl:masters() == 0 then
		local etcd_master_uuid = etcd_shard:master().instance_uuid:lower()
		local legitimate_master = repl.replicas[etcd_master_uuid]

		if not legitimate_master then
			return 'no_master'
		end

		-- If legitimate master is discovered and no other master is up
		-- then we check replica quorum and promote it.

		local ndws = #repl:followed_downstreams(legitimate_master:uuid())

		if not args.ignore_tarantool_quorum and ((ndws+1) < n_2_1(#etcd_shard:instances())) then
			return 'not_enough_quorum_to_autopromote'
		end

		if legitimate_master:cfg().read_only ~= true then
			log.warn("legitimate master %s is RO (reason:%s,cfg.ro:%s,status:%s), but cant be autopromoted",
				legitimate_master:name(), legitimate_master:info().ro_reason or "",
				legitimate_master:cfg().read_only,
				legitimate_master:info().status
			)
			return 'master_is_not_promotable'
		end

		-- read_only==true
		return 'legitimate_master_is_ro', ("no masters discovered, but should be %s"):format(etcd_master_uuid)
	end

	if #repl:masters() > 1 then
		return ('too many masters: %d'):format(#repl:masters())
	end

	local real_master = repl:master()
	local real_master_uuid = real_master:uuid():lower()
	local etcd_master = etcd_shard:master()
	local etcd_master_uuid = etcd_master.instance_uuid:lower()
	local legitimate_master = repl.replicas[etcd_master_uuid]

	-- if legitimate master is connected
	if legitimate_master and legitimate_master.connected
		-- but real master is not legitimate
		and real_master_uuid ~= etcd_master_uuid
		-- double check
		and real_master ~= legitimate_master
		-- and legitimate master is replicated by real one
		and legitimate_master:replicated_by(real_master_uuid)
		-- and real master is replicated by legitimate
		and real_master:replicated_by(legitimate_master:uuid():lower())
		-- and legitimate master is forced to be RO
		and legitimate_master:cfg().read_only == true
		-- and real master is forced to be RW
		and real_master:cfg().read_only == false
	then
		return 'real_master_missmatch', ('real master %s missmatch with etcd_master %s'):format(
			etcd_shard:instance_by_uuid(repl:master():uuid()).name,
			etcd_shard:master_name()
		)
	end

	if legitimate_master and legitimate_master.connected
		and real_master and real_master.connected
		and real_master:uuid() == etcd_master_uuid
		and real_master:vshard_storage_rw_but_ro()
	then
		return 'vshard_is_broken', ('master %s is up but vshard.storage configuration is outdated')
			:format(real_master:name())
	end

	return 'ok'
end

local function discovery_cluster(args)
	local how, cluster = require 'switchover._resolve'({args.watch_cluster}, 'cluster')
	if how ~= 'etcd' then
		e.panic("ETCD was not used")
	end

	G.start_at = clock.time()

	assert(not cluster:is_proxy(), "cannot watch proxies")

	local shards = require 'switchover.discovery'.cluster {
		cluster = cluster,
		discovery_timeout = args.discovery_timeout,
		fetch = {vshard_alerts = true},
	}

	local _, alerts = build_alerts(shards)
	if alerts then
		M.stats.observe_cluster_alerts(alerts)
	end

	local repls = {}

	local shard_names = fun.totable(shards)
	table.sort(shard_names)

	for _, shard_name in ipairs(shard_names) do
		local repl = shards[shard_name].replicaset
		local etcd_shard = shards[shard_name].shard

		table.insert(repls, repl)

		local resolution, audit_msg = check_shard {
			name = shard_name,
			replicaset = repl,
			etcd_shard = etcd_shard,
			ignore_tarantool_quorum = args.ignore_tarantool_quorum,
		}

		M.stats.observe_shard({
			name = shard_name,
			replicaset = repl:info(),
			etcd_shard = etcd_shard,
		})

		if M.audit then
			M.audit:notify({
				etcd_shard = etcd_shard,
				shard_name = shard_name,
				replicaset = repl:info(),
				resolution = resolution,
				message = audit_msg or "no message",
			})
		end

		print_etcd_discovery {
			replicaset = repl,
			etcd_shard = etcd_shard,
		}


	M.pool:send(function()
		log.set_fiber_prefix('['..shard_name..']')
		if resolution == 'no_master' then
			log.warn("Master not discovered in %s should be %s", shard_name, etcd_shard:master_name())

			M.stats.autofailover_resolution_no_master_fire(fiber.time(), { shard_name = shard_name })

			if args.with_auto_switch then
				log.warn("Preparing auto switch for %s", etcd_shard.name)

				local res = G.etcd:get(etcd_shard:switchover_path(), { quorum = true }, { raw = true })
				if res.errorCode == 100 or res.errorCode == 104 then
					local retcode = auto_switch.run {
						command = 'auto',
						no_orphan_fix = args.no_orphan_fix,
						shard = etcd_shard.name,
						discovery_timeout = args.discovery_timeout,
						autofailover_timeout = args.autofailover_timeout,
					}

					if retcode == 0 then
						log.warn("Auto switch finished successfully")
						M.stats.autofailover_resolution_no_master_resolved(fiber.time(), { shard_name = shard_name })
					else
						log.error("Auto switch failed!")
						M.stats.autofailover_resolution_no_master_unresolved(fiber.time(), { shard_name = shard_name })
					end
				else
					log.warn("Someone has already taken mutex. Refusing auto switch")
				end
			else
				log.warn("Watch works without autoswitch")
			end
		elseif resolution == 'real_master_missmatch' then
			log.warn("%s - %s", etcd_shard.name, resolution)
			M.stats.autofailover_resolution_master_missmatch_fire(fiber.time(), { shard_name = shard_name })
			if args.with_auto_heal then
				heal_safe_switch {
					etcd_master = etcd_shard:master(),
					candidate_uuid = repl:master():uuid(),
					etcd_master_name = etcd_shard:master_name(),
					etcd_shard = etcd_shard,
					autofailover_timeout = args.autofailover_timeout,
				}
				M.stats.autofailover_resolution_master_missmatch_resolved(fiber.time(), { shard_name = shard_name })
			else
				log.warn("Watch works without autoheal")
				M.stats.autofailover_resolution_master_missmatch_unresolved(fiber.time(), { shard_name = shard_name })
			end
		elseif resolution == 'legitimate_master_is_ro' then
			log.warn("%s - %s", etcd_shard.name, resolution)
			M.stats.autofailover_resolution_master_is_ro_fire(fiber.time(), { shard_name = shard_name })
			if args.with_auto_promote then
				local retcode = auto_switch.run {
					command = 'auto',
					no_orphan_fix = args.no_orphan_fix,
					shard = etcd_shard.name,
					discovery_timeout = args.discovery_timeout,
					autofailover_timeout = args.autofailover_timeout,
					promote_master_uuid = etcd_shard:master().instance_uuid,
				}

				if retcode == 0 then
					log.warn("Auto promote finished successfully")
					M.stats.autofailover_resolution_master_is_ro_resolved(fiber.time(), { shard_name = shard_name })
				else
					log.error("Auto promote failed!")
					M.stats.autofailover_resolution_master_is_ro_unresolved(fiber.time(), { shard_name = shard_name })
				end
			else
				log.warn("Watch works without autopromote")
				M.stats.autofailover_resolution_master_is_ro_unresolved(fiber.time(), { shard_name = shard_name })
			end
		elseif resolution == 'vshard_is_broken' then
			log.warn("%s - %s (willfix: %s)", etcd_shard.name, resolution, args.with_vshard_storage_fix == true)
			M.stats.autofailover_resolution_vshard_is_broken_fire(fiber.time(), { shard_name = shard_name })
			if args.with_vshard_storage_fix then
				local real_master = repl:master()

				if real_master.can_package_reload then
					real_master:package_reload(args.autofailover_timeout)
					M.stats.autofailover_resolution_vshard_is_broken_resolved(fiber.time(), { shard_name = shard_name })
				else
					log.error("Instance does not support package.reload()")
					M.stats.autofailover_resolution_vshard_is_broken_unresolved(fiber.time(), { shard_name = shard_name })
				end
			else
				log.info("Watch works without vshard_storage_fix (set with_vshard_storage_fix option)")
				M.stats.autofailover_resolution_vshard_is_broken_unresolved(fiber.time(), { shard_name = shard_name })
			end
		elseif resolution == 'ok' then
			log.warn("%s - ok", etcd_shard.name)
		elseif resolution == 'is_proxy' then
			log.warn("%s - is proxy", etcd_shard.name)
		else
			log.error("%s - %s", etcd_shard.name, resolution)
			M.stats.autofailover_resolution_unknown_unresolved(fiber.time(), { shard_name = shard_name })
		end

		if resolution == 'ok' then
			M.stats.autofailover_shard_is_ok(1, { shard_name = shard_name })
		else
			M.stats.autofailover_shard_is_ok(0, { shard_name = shard_name })
		end

		if type(repl) == 'table' and repl.destroy then
			repl:destroy()
		end
	end):on_finish(function(ok, ...)
		if ok then
			log.verbose("finished ok")
		else
			log.error("finished with error: %s", ...)
		end
	end) -- end of pool
	end

	if not M.pool:wait(30) then
		M.pool:terminate()
		M.pool = sync.pool("watch", M.pool_size)
	end

	return repls
end

local function pid_file(args)
	return type(args.autofailover) == 'table'
		and args.autofailover.pid_file
		or ('/var/run/tarantool/switchover_'..args.watch_cluster..'.pid')
end

function M.start(args)
	assert(args.watch_start, "watch_start is required")
	assert(not args.etcd_read_only, "watch_start cannot work if ETCD ReadOnly is allowed")

	log.info("Config file: %s", args.config)

	if args.daemonize then
		log.colors(false)
	end

	local how, cluster = require 'switchover._resolve'({args.watch_cluster}, 'cluster')
	if how ~= 'etcd' then
		e.panic("ETCD was not used")
	end

	if cluster:is_proxy() then
		e.panic("Cannot watch proxies")
	end

	local shards = require 'switchover.discovery'.cluster {
		cluster = cluster,
		discovery_timeout = args.discovery_timeout,
	}

	log.verbose("Destroy replicasets (after first run)")
	local n_shards = 0
	for _, r in pairs(shards) do
		n_shards = n_shards + 1
		r.replicaset:destroy()
	end

	args.autofailover = args.autofailover or {}

	if args.autofailover.graphite then
		local graphite = require 'switchover._graphite'
		M.graphite = graphite:new(args.autofailover.graphite)
	end

	M.stats = stats.new(M.graphite, { host = hostname.host(), cluster = args.watch_cluster })

	local dir = args.autofailover.work_dir or ('/var/lib/tarantool/snaps/switchover_'..cluster.name)
	fio.mktree(dir)

	log.verbose("Calling box.cfg")
	box.cfg{
		wal_mode = 'write',
		checkpoint_interval = args.autofailover.checkpoint_interval or 600,
		background = args.daemonize,
		custom_proc_title = 'switchover@'..cluster.name,
		vinyl_memory = 0,
		vinyl_cache = 0,
		memtx_memory = args.autofailover.memtx_memory or (32*2^20),
		memtx_dir = dir,
		wal_dir   = dir,
		vinyl_dir = dir,
		pid_file  = pid_file(args),
		log       = args.autofailover.log or ('/var/log/tarantool/switchover_' .. cluster.name .. '.log'),
		log_level = args.autofailover.log_level or G.log_level,
	}

	pcall(function()
		local console_sock = fio.abspath(box.cfg.pid_file):gsub("%.pid", ".control")
		console.listen("unix/:"..console_sock)
	end)

	if args.autofailover.audit then
		M.audit = audit.new(args.autofailover.audit)
	end

	if tonumber(args.autofailover.maximum_pool_size) then
		M.pool_size = math.min(tonumber(args.autofailover.maximum_pool_size), n_shards)
	else
		M.pool_size = math.min(16, n_shards)
	end

	M.pool = sync.pool("watch", M.pool_size)

	local job = background {
		name = 'watch/' .. args.watch_cluster,
		restart = 3,
		run_interval = args.watch_check_timeout,
		func = function(job)
			local repls = discovery_cluster(args)
			log.verbose("Destroying connections: cycle:%s", job.cycles)
			for _, repl in ipairs(repls) do
				repl:destroy()
			end

			M.stats.autofailover_last_online(fiber.time())

			collectgarbage('collect')
			collectgarbage('collect')
		end,
		setup = function()
			log.switchover_log(false)
		end,
		on_fail = function(_, err)
			log.error("Watch crushed with: %s", err)
			M.stats.autofailover_last_error(fiber.time())
		end,
		teardown = function()
			log.error("Job was finished. Exiting")
			os.exit(1)
		end,
	}

	rawset(_G, 'job', job)
end

function M.status(args)
	assert(args.watch_status, "watch_status is required")

	local file = fio.abspath(pid_file(args))
	if fio.stat(file) == nil then
		if errno() == errno.ENOENT then
			log.info('%s is stopped (pid file does not exist)', fio.basename(file, "%.pid"))
			return os.exit(1)
		end
		log.error("Can't access pidfile %s: %s", file, errno.strerror())
	end

	local console_sock = fio.pathjoin(fio.dirname(file), (fio.basename(file):gsub("%.pid$", ".control")))

	if fio.stat(console_sock) == nil and errno() == errno.ENOENT then
		log.error("Pid file exists, but the control socket (%s) doesn't", console_sock)
		return os.exit(2)
	end

	local s = socket.tcp_connect('unix/', console_sock)
	if s == nil then
		if errno() ~= errno.EACCES then
			log.warn("Can't access control socket '%s' [%d]: %s", console_sock,
				errno(), errno.strerror())
			return os.exit(2)
		end
		return os.exit(0)
	end

	s:close()
	print(('%s is running (pid: %s)'):format(fio.basename(file, "%.pid"), file))
	os.exit(1)
end

function M.stop(args)
	assert(args.watch_stop, "watch_stop is required")

	local file = fio.abspath(pid_file(args))
	if fio.stat(file) == nil then
		if errno() == errno.ENOENT then
			log.info('%s is stopped (pid file does not exist)', fio.basename(file, "%.pid"))
			return os.exit(1)
		end
		log.error("Can't access pidfile %s: %s", file, errno.strerror())
	end

	local console_sock = fio.pathjoin(fio.dirname(file), (fio.basename(file):gsub("%.pid$", ".control")))

	if fio.stat(console_sock) == nil and errno() == errno.ENOENT then
		log.error("Pid file exists, but the control socket (%s) doesn't", console_sock)
		return os.exit(2)
	end

	local s = socket.tcp_connect('unix/', console_sock)
	if s == nil then
		if errno() ~= errno.EACCES then
			log.warn("Can't access control socket '%s' [%d]: %s", console_sock,
				errno(), errno.strerror())
			return os.exit(2)
		end
		return os.exit(0)
	end

	s:close()
	local pid = tonumber(io.open((console_sock:gsub("%.control$", "%.pid")), 'r'):read("*n"))
	print(("Killing (pid %s): "):format(pid) .. tostring(io.popen("kill -15 "..pid):read("*all")))
	-- print(('%s is running (pid: %s)'):format(fio.basename(file, "%.pid"), file))
	os.exit(0)
end

function M.run(args)
	assert(args.command == "watch", "command missmatch")

	if not G.etcd then
		e.panic("ETCD is required for switchover auto")
	end

	if args.watch_start then
		return M.start(args)
	elseif args.watch_stop then
		return M.stop(args)
	elseif args.watch_status then
		return M.status(args)
	else
		log.error("Unknown command")
		os.exit(1)
	end
end

return M
