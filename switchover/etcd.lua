local M    = {}
local log  = require 'switchover._log'
local fio  = require 'fio'
local fun  = require 'fun'
local json = require 'json'
local yaml = require 'yaml'
local G    = require 'switchover._global'
local e    = require 'switchover._error'

---@type table<string, ObjectChecker>
local parsers = {
	['etcd.cluster.master'] = require 'switchover.core.etcd_cluster_master',
	['etcd.instance.single'] = require 'switchover.core.etcd_instance_single',
}

local formates = {
	json = json,
	yaml = yaml,
}

local function pp(t, fmt, opts)
	fmt = fmt or 'yaml'
	opts = opts or {}
	local formatter = formates[fmt] or yaml
	print(formatter.encode(t))
	if opts.exit == false then return end
	os.exit(0)
end

---comment
---@param tree any
---@param errs CheckError[]
---@param policy string
local function describe_error(tree, errs, policy)
	local es = table.concat(fun.map(tostring, errs):totable(), "\n\t")
	log.error("Validation failed (policy: %s):\n\t%s", policy, es)

	-- Fill shadown tree with error messages
	local shadow = {}
	for _, err in ipairs(errs) do
		local t = shadow
		for i = 1, #err.path_r - 1 do
			local k = err.path_r[i]
			t[k] = t[k] or {}
			t = t[k]
		end
		if #err.path_r > 0 then
			t[err.path_r[#err.path_r]] = err.message
		end
	end

	---Tree minus Tree
	---@param node table
	---@param shadow_node table
	local function dfs(node, shadow_node)
		if type(node) ~= 'table' then return end
		for key, subtree in pairs(node) do
			if not shadow_node[key] and type(subtree) == 'table' then
				node[key] = '...'
			else
				dfs(subtree, shadow_node[key])
			end
		end
	end

	dfs(tree, shadow)
	for _, err in ipairs(errs) do
		local s = tree
		for _, path in ipairs(err.path_r) do
			s = s[path]
		end
		if type(s) == 'table' then
			local msg = err.message
			setmetatable(s, {
				__serialize = function(v)
					local mt = getmetatable(v)
					if mt and mt.__serialize then mt.__serialize = nil end
					setmetatable(v, mt)
					v = yaml.encode(v)
						:gsub("%-%-%-", "")
						:gsub("%.%.%.\n", "")
						:gsub("%s+\n", "\n")
						:gsub("\n[ ]+'\n", "")
						:gsub("\n+", "\n")
					return ('%{red}// ' .. msg .. '%{white}') .. v
				end
			})
		end
	end
	print((log.ansicolors(yaml.encode(tree))
		:gsub("\\n", "\n")
		:gsub("\\ ", " ")
		:gsub("\n[ ]+'\n", "")
		:gsub("\n+", "\n")
		:gsub("'+", "'")
		))

end

local function validate(args)
	assert(args.command_etcd, "must be command_etcd")
	assert(args.etcd_path, "etcd_path must be given")

	local tree = G.etcd:getr(args.etcd_path, { leader = true })

	local parser = parsers[args.etcd_policy]
	if not parser then
		e.panic("Policy not implemented yet")
	end

	local data, ctx = parser:verify_safe(tree)
	if data == nil then
		describe_error(tree, ctx.errors, args.etcd_policy)
		os.exit(1)
	end

	log.info("%s is valid %s", args.etcd_path, args.etcd_policy)
end

---prompts user
---@param question string
---@param valids string[]
---@param default? string
local function prompt(question, valids, default)
	local vals = table.concat(valids, "/")
	local vmap = fun.zip(valids, fun.ones()):tomap()
	if default then
		vals = vals:gsub(default, default:upper())
	end

	if vals ~= '' then
		question = question .. ('[%s] '):format(vals)
	end

	io.stdout:setvbuf("no")

	local r
	repeat
		r = ''
		while r == '' do
			io.stdout:write(question)
			r = io.stdin:read("*l")
			if r == '' and default then r = default end
		end
		if not next(vmap) then break end
	until vmap[r]

	return r
end

---Creates template for application
---@param args table
local function create(args)
	local app_name = assert(args.etcd_create_name, "name must be specified")
	local template = {
		common = {},
		instances = {}
	}

	local cfg = {}

	io.stdout:setvbuf("no")
	-- 0) Cluster or Proxy
	cfg.statefull = prompt("Is application statefull? ", {'y', 'n'}, 'y') == 'y'

	-- 1) Cluster or Single Shard?
	if cfg.statefull then
		cfg.n_shards = tonumber(prompt("How many shards do you need [default: 2]: ", {}, "2"))
	else
		cfg.n_proxies = tonumber(prompt("How many instances do you need [default: 2]: ", {}, "2"))
	end

	-- 2) Replica Factor?
	if cfg.statefull then
		cfg.replica_factor = tonumber(prompt("How many replicas for each shard do you need [default: 2]: ", {}, "2"))
	end

	local mm = {
		[true] = '256m',
		[false] = '32m',
	}
	-- 3) Initial memtx_memory
	while true do
		local memtx_memory = prompt(
			("What memtx_memory do you need for each instance? [default: %s] "):format(mm[cfg.statefull]),
		{}, mm[cfg.statefull])
		local x = memtx_memory:lower()
		if x:match('^[0-9]+[mg]$') then
			local num = tonumber(x:match('[0-9]+'))
			cfg.memtx_memory = num * tonumber((x:sub(-1, -1):gsub('.', {m=2^20, g=2^30})))
			if cfg.memtx_memory >= 32*2^20 then break end
			io.stdout:write("Sorry, memtx_memory can't be less than 32m\n")
		else
			io.stdout:write("Didn't catch that. Please type 100m or 4g\n")
		end
	end

	do
		local q = "How many servers do you need? "
		if cfg.replica_factor then
			q = ("How many servers do you need? [default: %s] "):format(cfg.replica_factor)
		end

		local min = tonumber(cfg.replica_factor) or 1
		while true do -- luacheck:ignore
			::again::
			local n = prompt(q, {}, tostring(min))
			if not n:match('^[0-9]+$') then
				io.stdout:write("Please use only positive numeric value")
				goto again
			end
			if tonumber(n) < min then
				io.stdout:write(("You should have at least %s number of servers\n"):format(min))
				goto again
			end
			cfg.n_servers = tonumber(n)
			break
		end
	end

	-- 4) IP of servers?
	local srvs = {}
	for i = 1, cfg.n_servers do
		while true do -- luacheck: ignore
			::again::
			local srv = prompt(("Specify ip for server %d: "):format(i), {})
			if not srv:match('^[0-9]+%.[0-9]+%.[0-9]+%.[0-9]+$') then
				local is_valid = prompt(("%q is not ipv4. Is it valid? "):format(srv), {'y', 'n'}, 'n') == 'y'
				if not is_valid then goto again end
			end

			if srvs[srv] then
				io.stdout:write(("address %q already used as server %d\n"):format(srv, srvs[srv]))
				goto again
			end

			local need_ping = prompt(
				("Do you want me to ping %q for you? "):format(srv), {'y', 'n'}, 'n') == 'y'

			if not need_ping then
				srvs[srv] = i
				srvs[i] = srv
				break
			end

			local ping_ok = os.execute(("ping -W 1 -c 1 %s"):format(srv)) == 0
			local save
			if not ping_ok then
				save = prompt(
					("%q does not respond to ping still use it? "):format(srv), {'y', 'n'}, 'n') == 'y'
			else
				save = prompt(
					("ping to %q is received. Use it? "):format(srv), {'y', 'n'}, 'n') == 'y'
			end

			if not save then
				goto again
			end

			srvs[srv] = i
			srvs[i] = srv
			break
		end
	end

	cfg.servers = fun.totable(srvs)

	-- 5) Port range of instances?
	do
		while true do -- luacheck: ignore
			::again::
			local num = prompt("Please specify port_base for instance. (ex: 7100 allows 100 instances) ", {})
			if not num:match('^[0-9]+$') or not tonumber(num) then
				io.stdout:write("You response must be numeric\n")
				goto again
			end
			local port = tonumber(num)
			if port < 1024 or port > 32678 then
				io.stdout:write("Please use base in range [1024-32768]\n")
				goto again
			end

			if port % 100 ~= 0 then
				io.stdout:write(("Cannot use port %q as base, it must end with 00\n"):format(port))
				goto again
			end
			cfg.port_base = port
			break
		end
	end
	-- 6) Instance uuid/Replicaset uuid prefixes? or auto?
	local l337 = function(s)
		return (s:lower():gsub('.', {
			a = 'a', b = '8', c = 'c', d = 'd', e = 'e', f = 'f',
			g = '6', h = '4', i = '9', j = '7', k = '0', l = '1',
			m = '44', n = '11', o = '0', p = '17', q = '9', r = '12',
			s = '5', t = '7', u = '4', v = '5', w = '22', x = '8', y = '7', z = '5',
		}))
	end

	local uuid_pref = ("%8s"):format(l337(app_name):sub(1, 8)):gsub(" ", '0')
	local uuid_template = ('%s-%04d-%04d-0000-000000000000')

	local function rcq(n)
		if n <= 2 then return n end
		return math.floor(n/2)+1
	end

	template.common.box = {
		replication_connect_quorum = cfg .statefull and rcq(cfg.replica_factor) or nil,
		memtx_memory = cfg.memtx_memory,
		log_level = 5,
	}

	if cfg.statefull then
		template.clusters = {}
		if cfg.n_shards == 1 then
			template.clusters[app_name] = {
				master = app_name .. '_01',
				replicaset_uuid = uuid_template:format(uuid_pref, 1, 0),
			}
			for i = 1, cfg.replica_factor do
				local inst_name = app_name .. ('_%02d'):format(i)
				local listen_port = cfg.port_base + 10*0 + i
				template.instances[inst_name] = {
					cluster = app_name,
					box = {
						instance_uuid = uuid_template:format(uuid_pref, 0, i),
						listen = cfg.servers[i]..':'..listen_port,
					}
				}
			end
		else
			for n = 1, cfg.n_shards do
				local shard_name = app_name .. ('_%03d'):format(n)
				template.clusters[shard_name] = {
					master = shard_name .. '_01',
					replicaset_uuid = uuid_template:format(uuid_pref, n, 0),
				}
				for i = 1, cfg.replica_factor do
					local inst_name = shard_name .. ('_%02d'):format(i)
					local listen_port = cfg.port_base + 10*n + i
					template.instances[inst_name] = {
						cluster = shard_name,
						box = {
							instance_uuid = uuid_template:format(uuid_pref, n, i),
							listen = cfg.servers[i]..':'..listen_port,
						}
					}
				end
			end
		end
	else
		template.clusters = nil
		template.instances = {}

		for i = 1, cfg.n_proxies do
			local inst_name = app_name .. ('_%03d'):format(i)
			local listen_port = cfg.port_base + i
			local listen_addr = cfg.servers[i%#cfg.servers+1] .. ':'..listen_port
			template.instances[inst_name] = {
				box = {
					instance_uuid = uuid_template:format(uuid_pref, 0, i),
					listen = listen_addr,
				},
			}
		end
	end

	pp({ [app_name] =  template }, args.etcd_output_format, {exit=false})

	if prompt("Save configuration to file? ", {'y', 'n'}, 'y') == 'y' then
		local fname = ("%s.etcd.yaml"):format(app_name)
		local fh = assert(io.open(fname, "w"))
		assert(fh:write(yaml.encode({ [app_name] = template })))
		assert(fh:close())
		print("Saved to file "..fname)
		os.exit(0)
	end
	os.exit(0)
end

--- ETCD API
function M.run(args)
	assert(args.command_etcd, "must be command_etcd")
	if not G.etcd and not args.etcd_create then
		e.panic("ETCD is required for command etcd")
	end

	if args.etcd_list then
		local listing
		if args.recursive then
			listing = G.etcd:lsr(args.etcd_path, {}, { leader = true })
		else
			listing = G.etcd:ls(args.etcd_path, {}, { leader = true })
		end
		table.sort(listing)
		pp(listing, args.etcd_output_format)
	elseif args.etcd_get then
		if args.recursive then
			pp(G.etcd:getr(args.etcd_path, {}, { leader = true }), args.etcd_output_format)
		else
			pp(G.etcd:get(args.etcd_path, {}, { leader = true }), args.etcd_output_format)
		end
	elseif args.etcd_set then
		pp(G.etcd:set(args.etcd_path, args.etcd_value, {}, { leader = true }), args.etcd_output_format)
	elseif args.etcd_load then
		local data = args.etcd_input:read("*all")

		if args.etcd_load_input_format == "json" then
			data = json.decode(data)
		else
			data = yaml.decode(data)
		end

		do
			local etcd_data = G.etcd:getr(args.etcd_path, {}, { raw = true, leader = true })
			local fname = fio.pathjoin(G.workdir,
				G.etcd.last_headers["x-etcd-index"] .. "_" .. os.date("%Y%m%dT%H%M%S") ..
				"_etcd_dump_" .. args.etcd_path:gsub("/", "_"))
			local f = assert(io.open(fname, "wx"))
			if etcd_data.node then
				assert(f:write(yaml.encode(G.etcd:unpack(etcd_data.node, G.etcd:_path(args.etcd_path)))))
				assert(f:close())
			end
			log.verbose("ETCD %s dumped => %s", args.etcd_path, fname)
		end

		pp(G.etcd:fill(args.etcd_path, data, {
			leader = true,
			dry_run = args.etcd_load_dry_run,
			sync_delete = args.etcd_load_delete,
			no_change = args.etcd_load_append_only,
		}), args.etcd_output_format)
	elseif args.etcd_rm then
		pp(G.etcd:rm(args.etcd_path, { recursive = args.etcd_recursive == true }, { leader = true }), args.etcd_output_format)
	elseif args.etcd_rmdir then
		pp(G.etcd:rmdir(args.etcd_path, { leader = true }), args.etcd_output_format)
	elseif args.etcd_wait then
		pp(G.etcd:wait(args.etcd_path,
			{ recursive = args.etcd_recursive == true, waitIndex = args.etcd_index },
			{ timeout = args.timeout }
		), args.etcd_output_format)
	elseif args.etcd_validate then
		return validate(args)
	elseif args.etcd_create then
		return create(args)
	elseif args.etcd_version then
		pp(G.etcd:version())
	elseif args.etcd_health then
		local health = G.etcd:health()
		local is_healthy = true
		for _, v in pairs(health) do
			if v ~= 'healthy' then
				is_healthy = false
			end
		end
		pp(health, 'yaml', {exit=false})
		if is_healthy then
			os.exit(0)
		else
			log.warn("ETCD is degraded")
			os.exit(1)
		end
	else
		e.panic("Command is Not Implemented Yet")
	end
end

return M
