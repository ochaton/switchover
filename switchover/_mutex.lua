local G = require 'switchover._global'
local log = require 'log'
local json = require 'json'
local fiber = require 'fiber'


local Mutex = {}
Mutex.__index = Mutex

function Mutex:new(path)
	assert(G.etcd, "etcd required to create Mutex")
	return setmetatable({ path = assert(path, "mutex: path is requred") }, self)
end

function Mutex:atomic(opts, func, ...)
	local key = assert(opts.key, "Mutex: key is required")
	local ttl = assert(opts.ttl, "Mutex: ttl is required")
	self.key = key

	local res = G.etcd:set(self.path, key, { prevExist = false, ttl = ttl, quorum = true }, { leader = true })
	log.verbose("set %s => %s (ttl: %d): %s", self.path, key, ttl, json.encode(res))

	if res.action ~= 'create' then
		-- data is safe, so first arg is true
		return true, ("Mutex wasn't acquired: %s:%s"):format(res.cause, res.message)
	end

	self._leasing_f = fiber.create(function()
		fiber.yield()
		repeat
			fiber.sleep(ttl / 2)
			fiber.testcancel()

			if self._leasing_f ~= fiber.self() then break end
			local cas = G.etcd:set(self.path, key,
				{ prevValue = key, ttl = ttl, quorum = true },
				{ leader = true })
			log.warn('CaS leasing: %s', json.encode(cas))
		until self._leasing_f ~= fiber.self()

		log.verbose("End CaS {%s => %s} leasing fiber", self.path, key)
	end)

	local r = { pcall(func, ...) }
	-- safely remove _leasing fiber. It will be removed after ttl/2 timeout
	-- do not call fiber.cancel()!
	self._leasing_f = nil

	if table.remove(r, 1) then
		local ok, err = unpack(r)
		if ok then
			if opts.release_on_success then
				self.released = true
				local var = G.etcd:rm(self.path, { prevValue = key, quorum = true }, {leader = true})
				log.info("mutex:autorelease: %s", json.encode(var))
			end
		else
			log.verbose("mutex:atomic failed %s:%s", tostring(ok), tostring(err))
		end
		return ok, err
	else
		return false, r[1]
	end
end

function Mutex:release()
	if self.released then
		log.warn("duplicate mutex release")
		return
	end
	self.released = true
	local var = G.etcd:rm(self.path, { prevValue = self.key }, {leader = true, quorum = true})
	log.info("mutex:release: %s", json.encode(var))
end

return setmetatable(Mutex, { __call = Mutex.new })
