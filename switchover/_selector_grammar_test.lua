local t = require 'luatest'
local g = t.group('selector_grammar')

local sg = require 'switchover._selector_grammar'
local grammar = sg.grammar
local G = sg.G
local lpeg = sg.lpeg

local log = require 'log'

function g.test_neq()
	local sentences = {
		{
			text = 'cfg.read_only ~= true',
			ast = {
				"Neq", "cfg.read_only ~= true",
				{ "Id", "cfg.read_only" },
				{ "Term", "true" }
			}
		},
		{
			text = 'cfg.election_mode != candidate',
			ast = {
				"Neq", 'cfg.election_mode != candidate',
				{ "Id", 'cfg.election_mode' },
				{ "Term", 'candidate' }
			}
		},
	}
	local Neq = table.deepcopy(grammar)
	Neq[1] = "Neq"

	Neq = lpeg.P(Neq)

	for _, test in ipairs(sentences) do
		local capture = lpeg.match(Neq, test.text)
		log.info(capture)
		t.assert_items_equals(capture, test.ast, "Neq - " .. test.text)
	end
end

function g.test_sentences()
	local sentences = {
		{
			text = 'server',
			ast = {
				'S', 'server',
				{
					"Expr", "server",
					{ "Is", "server",
						{ "Id", "server" },
					},
				}
			}
		},
		{
			text = '!info.ro',
			ast = {
				'S', '!info.ro',
				{ "Expr", '!info.ro',
					{ "UnNeq", '!info.ro',
						{ "Id", "info.ro" }
					}
				}
			},
		},
		{
			text = '!cfg.read_only',
			ast = {
				'S', '!cfg.read_only',
				{ "Expr", '!cfg.read_only',
					{ "UnNeq", '!cfg.read_only',
						{ "Id", 'cfg.read_only' },
					}
				},
			},
		},
		{
			text = 'cfg.election_mode ~= candidate',
			ast = {
				'S', 'cfg.election_mode ~= candidate',
				{ "Expr", 'cfg.election_mode ~= candidate',
					{ 'Neq', 'cfg.election_mode ~= candidate',
						{ 'Id', 'cfg.election_mode' },
						{ 'Term', 'candidate' },
					}
				},
			},
		},
		{
			text = 'server in (srv1.q, srv2.q),etcd.role==master',
			ast = {
				'S', 'server in (srv1.q, srv2.q),etcd.role==master',
				{ "Expr", 'server in (srv1.q, srv2.q)',
					{ "In", 'server in (srv1.q, srv2.q)',
						{ "Id", 'server' },
						{ 'Term', 'srv1.q' },
						{ 'Term', 'srv2.q' },
					}
				},
				{ "Expr", 'etcd.role==master',
					{ 'Eq', 'etcd.role==master',
						{ 'Id', 'etcd.role' },
						{ 'Term', 'master' },
					},
				},
			}
		},
		{
			text = 'port=3301',
			ast = {'S', 'port=3301',
				{ "Expr", 'port=3301',
					{ "Eq", 'port=3301',
						{"Id", 'port'},
						{"Term", '3301'},
					}
				}
			},
		},
		{
			text = 'cfg.election_mode notin (candidate, voter), !info.ro',
			ast = {
				'S', 'cfg.election_mode notin (candidate, voter), !info.ro',
				{ "Expr", 'cfg.election_mode notin (candidate, voter)',
					{
						"NotIn", 'cfg.election_mode notin (candidate, voter)',
						{ "Id", 'cfg.election_mode' },
						{ "Term", 'candidate' },
						{ "Term", 'voter' },
					}
				},
				{ "Expr", '!info.ro',
					{ "UnNeq", '!info.ro',
						{ "Id", 'info.ro' }
					},
				}
			}
		}
	}

	for _, test in ipairs(sentences) do
		local capture = lpeg.match(G, test.text)
		log.info(capture)
		t.assert_items_equals(capture, test.ast, 'sentence - '..test.text)
	end

end

