local log = require "log"
local ffi = require "ffi"
local fiber = require "fiber"

local function pack(...)
    return {n = select("#", ...), ...}
end

---Simple way to create background fibers
---@class background
---@field public name string name of job
---@field public func function body of fiber loop
---@field public wait any possible values `ro` or `rw` or nil
---@field public restart boolean|number timeout of restart fiber on fail (default: false)
---@field public once boolean=false runs fiber loop only once
---@field public run_interval number default=1s timeout of each successfull executions of fiber loop
---@field public args any[] constant arguments of `func`
local M = {}
M.registry = setmetatable({}, {__mode='kv'})

local methods = {}
methods.__index = methods

function methods:__tostring()
    if fiber.status(self.fiber) ~= 'dead' then
        return ("b:%s/%s"):format(self.name, tostring(self.fiber))
    end
	return ("b:%s/%s"):format(self.name, 'dead')
end

local id = 0
function methods._id()
    id = id + 1
    return id
end

---Callback is called when execution of fiber-loop is failed
---@param err any
function methods:on_fail(err)
    log.verbose("Background %s failed with %s", self.name, err)
end

function methods:_safe_call(func, ...)
    self.last_error = nil
    local r = pack(xpcall(func, debug.traceback, ...))
    if not r[1] then
        self.last_error = r[2]
        self:on_fail(r[2])
    end
    return r
end

function methods:stats()
    return {
        name = self.name,
        fiber = self.fiber,
        created_at = self.created_at,
        cycles = self.cycles,
        restarts = self.restarts,
        looped_at = self.looped_at,
        reload_generation = self.reload_generation,
    }
end

---Callback setup is called right before entering fiber loop
---Each restart also calls setup callback
function methods:setup()
    -- nothing to do
    log.verbose("setup %s/%s/%s/cat:%s/lat:%s/r:%s",
	self.name, tostring(self.fiber), tostring(self.cycles), tostring(self.created_at), tostring(self.looped_at), tostring(self.restarts))
end

---Callback teardown is called only once right before extermination of the fiber
function methods:teardown()
    -- nothing to do
    log.info("teardown %s", self.name)
end

---Callback run_while is called many times to check whether fiber loop should be finished or not
---Default callback checks package.reload and fiber cancellation
function methods:run_while()
    local gen = self.reload_generation
    if gen then
        ---@diagnostic disable-next-line: undefined-field
        return gen == package.reload.count -- luacheck: ignore
    end
    if self.stop then
        return false
    end
    if self.wait then
        if self.wait == 'rw' then
            assert(not box.info.ro, "node must be RW")
        elseif self.wait == 'ro' then
            assert(box.info.ro, "node must be RO")
        end
    end
    return true
end

function methods:is_stopped()
    if self.stop then
        return true
    end
    local gen = self.reload_generation
    ---@diagnostic disable-next-line: undefined-field
    if gen and gen < package.reload.count then -- luacheck: ignore
        return true
    end
    return false
end

---Method shutdown is called to gracefully or forcely cancel job
---First call sets background.stop flag which is checked inside background.run_while callback
---Second call sends fiber.cancel
function methods:shutdown(force)
    self.sleep_cond:broadcast()
    log.verbose(":shutdown for %s", self)
    if  M.registry[self.name] == self then
        M.registry[self.name] = nil
    end
    if not self.stop then
        self.stop = true
        if not force then return end
    end
    -- double shutdown attempts to cancel fiber
    if self.fiber then
        if self.fiber:status() ~= "dead" then
            local ok, err = pcall(self.fiber.cancel, self.fiber)
            if ok then
                self.fiber = nil
            else
                log.error("fiber.cancel for %s failed with: %s", self.name, err)
            end
        else
            self.fiber = nil
            collectgarbage('collect')
        end
    end
end

---background:notify is a proper way to bring back fiber from sleep
function methods:notify()
    self.sleep_cond:broadcast()
end

---Spawns new background fiber
---@param _ any
---@param opts background
---@return background
function M.new(_, opts)
    assert(type(opts) == "table", "opts required to be table")
    local self = setmetatable(opts, methods)

    self.name = self.name or ("background/" .. self:_id())
    if M.registry[self.name] then
        error(("job %s already exists %s"):format(self.name, M.registry[self.name]), 2)
    end
    assert(self.func, "func is required for background")

    M.registry[self.name] = self
    self.run_interval = self.run_interval or 1
    self.args = self.args or {}
    self.sleep_cond = fiber.cond()

    self.fiber = fiber.create(self.fiber_f, self)
    return self
end

function M.stop_all_fibers()
    for name, job in pairs(M.registry) do
        log.verbose("stop_all_fibers: stopping job %s", name)
        job:shutdown()
        fiber.yield()
        job:shutdown()
        fiber.yield()
    end
end

local function loop_f(self)
    local r = self:_safe_call(self.setup, self)
    if not r[1] then
        return "stop", "setup failed"
    end

    self.setuped_at = fiber.time()
    self.cycles = 0

    if self.wait then
        if self.wait == 'rw' and box.info.ro then
            log.warn("Entering wait loop. Need to be run on RW but node is RO")
        elseif self.wait == 'ro' and not box.info.ro then
            log.warn("Entering wait loop. Need to be run on RO but node is RW")
        end
    end

    while self.wait and not self:is_stopped() do
        local wait_for, wait_with
        if self.wait == "rw" then
            wait_for = not box.info.ro
            wait_with = box.ctl and box.ctl.wait_rw or fiber.sleep
        elseif self.wait == "ro" then
            wait_for = box.info.ro
            wait_with = box.ctl and box.ctl.wait_ro or fiber.sleep
        else
            break
        end

        if wait_for then
            break
        end

        pcall(wait_with, 0.01)
        fiber.testcancel()
    end

    -- Enter loop:
    self.looped_at = fiber.time()
    while not self:is_stopped() do
        if not self:run_while() then
            return "stop", "run_while returned false"
        end
        self.cycles = self.cycles + 1
        local res = self:_safe_call(self.func, self, unpack(self.args or {}))
        if not res[1] then
            return "cont", "executor failed"
        end
        if not self:run_while() then
            return "stop", "run_while returned false"
        end
        local sleep = tonumber(res[2]) or self.run_interval
        self.sleep_cond:wait(sleep)
        fiber.testcancel()
        if self.once then break end
    end
end

function methods:fiber_f()
    fiber.name(self.name, {truncate = true})
    log.verbose("Starting background job: wait:%s restart:%s run_interval:%s once:%s eternal:%s",
        self.wait, self.restart, tostring(self.run_interval), tostring(self.once), tostring(self.eternal))

    ---@diagnostic disable-next-line: undefined-field
    if package.reload and not self.eternal then -- luacheck: ignore
        ---@diagnostic disable-next-line: undefined-field
        self.reload_generation = package.reload.count -- luacheck: ignore
    end

    self.restarts = 0
    self.created_at = fiber.time()

    while not self:is_stopped() do
        local _, err, reason = pcall(loop_f, self)

        if err == "stop" then
            log.verbose("Shut down background because of %s", reason)
            break
        elseif err == "cont" then
            log.verbose("Background failed because: %s", reason)
        elseif ffi.istype("struct error", err) then
            ---@cast err +BoxErrorObject
            if err.type == "FiberIsCancelled" then
                if not self.stop then
                    log.warn("Fiber was cancelled. Finishing")
                end
                break
            end
        elseif err then
            log.verbose("Caught unknown error: %s", err)
        else
            log.verbose("Leaving background loop")
        end

        if not self.restart or self:is_stopped() then
            break
        end

        self.sleep_cond:wait(tonumber(self.restart) or 1)
	if not pcall(fiber.testcancel) then
            M.registry[self.name] = nil
            return
	end
        log.info("Restarting background job")
        self.restarts = self.restarts + 1
    end

    M.registry[self.name] = nil
    return self:_safe_call(self.teardown, self)
end

return setmetatable(M, {__call = M.new})
--[[
	Usage:

	job = background {
		name    = 'xqueue/monitoring',
		wait    = 'rw',
		restart = 1, -- | true, false (default: false)
		run_interval = 1, -- run interval
		on_fail = function(err)
			log.error(err)
		end,
		setup = function(job)
			job.gen = package.reload.count
		end,
		run_while = function(job)
			return job.gen == package.reload.count and not box.info.ro
		end,
		func = function(...)

		end,
		args = {...},
	}
]]
