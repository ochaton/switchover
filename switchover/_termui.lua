local term = require 'switchover._plterm'
local fiber = require 'fiber'
local json  = require 'json'.new()
json.cfg{
	-- encode_invalid_as_nil = true,
	encode_deep_as_nil = true,
	encode_use_tostring = true,
	encode_max_depth = 4,
	-- encode_invalid_numbers = true
}
local fun = require 'fun'

---@class ui
local ui = {}

---@class window
---@field keyboard keyboard
---@field loop ui
---@field tab number
local window = {}
window.__index = window

---@class Widget
---@field ui ui
---@field error fun(Widget, string)
---@field window window
---@field _height number
---@field _width number
local Widget = {
	parent = function(self)
		return getmetatable(self).__index
	end,
	on_init = function(self)
		self.x = self.x or 1
		self.y = self.y or 1
		if self.window then
			self:on_resize(self.window)
		end
	end,
	on_resize = function(self, wnd)
		self.window = wnd:sub(self.x, self.y, self.w, self.h)
		self._height = wnd.height
		self._width = wnd.width
	end,
	update = function() end,
	draw = function() end,
	stylef = function(self, text, ...)
		local style
		if select('#', ...) > 0 then
			style = term.colorf(...)
		else
			style = self.default_style or term.colorf(0)
		end
		return style .. text .. term.colorf(0)
	end,
	align = function(self, text, align)
		if align == 'left' then
			return text
		elseif align == 'center' then
			local padding = (" "):rep(math.floor((self._width - #text) / 2))
			return padding .. text
		elseif align == 'right' then
			local padding = (" "):rep(self._width - #text)
			return padding .. text
		end
	end,
	clone = function(self)
		local meta = getmetatable(self)
		local r = setmetatable({}, meta)
		for key, value in pairs(self) do
			r[key] = value
		end
		return r
	end,
}
Widget.__index = Widget

---@class WTable:Widget
---@field data string[][]
---@field rows any[]
---@field current_row number
---@field top_row number
local WTable = setmetatable({}, Widget)
WTable.__index = WTable

function WTable:on_init()
	Widget.on_init(self)

	self.window.keyboard:on({ 'kup','kdown','kpgup', 'kpgdn', 'khome', 'kend' }, function(key)
		if key == 'kup' then
			self:scroll(-1)
		elseif key == 'kdown' then
			self:scroll(1)
		elseif key == 'khome' then
			self.current_row = 1
		elseif key == 'kend' then
			self.current_row = #self.rows
		elseif key == 'kpgdn' then
			self.top_row = math.max(1, math.min(self.top_row + self._height, #self.rows - self._height))
			self:scroll(self._height)
		elseif key == 'kpgup' then
			self.top_row = math.max(self.top_row - self._height, 1)
			self:scroll(-self._height)
		end
	end)

	self.padding = " "
	self.align_cell = 'left'
	self.rows = {}
	self.styles = {}
	self.current_row = 1
	self.top_row = 1
	self.pin_rows = 0
end

---@param amount number
function WTable:scroll(amount)
	if #self.rows < self.current_row + amount then -- check max overflow
		self.current_row = #self.rows
	elseif self.current_row + amount < 1 then -- check min overflow
		self.current_row = 1
	else
		self.current_row = self.current_row + amount
	end
end

local function draw(line, szs, padd, align)
	padd = padd or " "
	local txt = {}
	for i = 1, #szs do
		if align == 'right' then
			txt[i] = ("%"..szs[i].."s"):format(line[i])
		else
			txt[i] = ("%-"..szs[i].."s"):format(line[i])
		end
	end
	return table.concat(txt, padd)
end

function WTable:redraw()
	local data = self.data
	self.rows = table.new(#data + 1, 0)

	local szs = {}
	for _, line in ipairs(data) do
		for col = 1, #line do
			szs[col] = math.max(szs[col] or 0, #tostring(line[col]))
		end
	end
	for row_id, line in ipairs(data) do
		self.rows[row_id] = draw(line, szs, self.padding, self.align_cell)
	end
	self.ui:redraw()
end

function WTable:draw()
	Widget.draw(self)

	-- h = [pin]+[cur-top+1]
	local height = self._height-self.pin_rows-1
	if self.current_row - self.top_row + 1 >= height then
		-- cr + 1 - h >= top
		self.top_row = self.current_row - height + 2
	elseif self.current_row < self.top_row then
		self.top_row = self.current_row
	end

	local row_ids = {}
	if self.top_row > 1 then
		for row_id = 1, self.pin_rows do
			table.insert(row_ids, row_id)
		end
	end

	for row_id = self.top_row, self.top_row + (self._height - self.pin_rows) do
		if row_id > #self.rows then break end
		table.insert(row_ids, row_id)
	end

	self.tick = (self.tick or 0)%9+1

	local line = 0
	for _, row_id in ipairs(row_ids) do
		local row = tostring(self.rows[row_id])
		line = line + 1
		self.window:clear(1, line)

		local style = self.styles[ row_id ] or self.styles.default or term.colorf(0)
		local default = term.colorf(0)

		row = row:sub(1, self._width)
		self.window:print(style .. row .. (" "):rep(self._width-#row) .. default, 1, line, {notrunc = true})
		-- self:error(("draw(%s) line:%s row:%s"):format(self.tick, line, row_id))
	end

	--- Clear window
	local nclear = 0
	for y = line+1, self._height do
		nclear = nclear + 1
		self.window:clear(1, y)
	end

	self:error(("draw {c:%s t:%s n:%s w:%s h:%s cl:%s}"):format(
		self.current_row, self.top_row, #self.rows, self._width, self._height, nclear
	))
end

---comment
---@param w any
---@param h any
function window:init(w, h)
	assert(self ~= window)
	assert(self.loop ~= nil)
	assert(self.tab ~= nil)
	self.width = w
	self.height = h
	self.x0 = 0
	self.y0 = 0
end

---comment
---@param w any
---@param h any
function window:resize(w, h)
	if self.width ~= w or self.height ~= h then
		self.width = w
		self.height = h
		if self.loop.active_window.tab ~= self.tab then return end
		term.golc(1, 1)
		term.reset()
	end
end

---comment
---@param x any
---@param y any
function window:clear(x, y)
	if self.loop.active_window.tab ~= self.tab then return end
	term.golc(y+self.y0, x+self.x0)
	term.cleareol()
end

---comment
---@param x0 number
---@param y0 number
---@param w number
---@param h number
---@return window
function window:sub(x0, y0, w, h)
	x0 = x0 or 0
	y0 = y0 or 0
	w = w or self.width
	h = h or self.height

	if h <= 0 then
		h = self.height + h
	end
	if w <= 0 then
		w = self.width + w
	end

	return setmetatable({
		height = math.min(h, self.height - y0),
		width = math.min(w, self.width - x0),
		x0 = x0+self.x0, y0 = y0+self.y0,
		loop = self.loop,
		tab  = self.tab,
	}, {__index = self})
end

---Prints line to screen
---@param text string
---@param x number?
---@param y number?
---@param opts { notrunc: boolean? }?
function window:print(text, x, y, opts)
	opts = opts or {}
	if x < 0 then
		x = self.width + x
	end
	if y < 0 then
		y = self.height + y
	end

	if y > self.height or x > self.width then return end

	if x+#text > self.width and not opts.notrunc then
		text = text:sub(1, self.width - x)
	end

	if self.loop.active_window.tab ~= self.tab then return end
	term.golc(y+self.y0, x+self.x0)
	term.outf(tostring(text))
end

---comment
---@param x number
---@param y number
function window:set_cursor(x, y)
	if self.loop.active_window.tab ~= self.tab then return end
	self.loop.active_window.cursor = {x+self.x0, y+self.y0}
end

---@class keyboard
---@field _keys table<string,boolean>
---@field _cbs table<string,fun(key:string)>
local Keyboard = {}
Keyboard.__index = Keyboard

---unpress
function Keyboard:unpress()
	table.clear(self._keys)
end

---press key
---@param key string
function Keyboard:press(key)
	if self._cb then
		self._cb(key)
		return true
	end
	self._keys[key] = true
	local cb = self._cbs[key] or self._cbs[key:lower()]
	if cb then
		cb(key)
		return true
	end
end

---focuses keyboard to cb
---@param cb fun(key:string)
function Keyboard:focus(cb)
	term.disable_escape = true
	self._cb = cb
end

function Keyboard:unfocus()
	term.disable_escape = false
	self._cb = nil
end

---register callback on key
---@param keys string[]
---@param cb fun(key:string)
function Keyboard:on(keys, cb)
	if type(keys) == 'table' then
		for _, key in pairs(keys) do
			self._cbs[key] = cb
		end
	end
end

---unregister callback on key
---@param cb fun(key:string)
function Keyboard:off(cb)
	for key, func in pairs(self._cbs) do
		if func == cb then
			self._cbs[key] = nil
		end
	end
end

function ui:init(opts)
	opts = opts or {}
	self.debug = not not opts.debug
	opts.tabs = math.max(1, opts.tabs or 1)

	self.keyboard = setmetatable({ _cbs = {}, _keys = {} }, Keyboard)
	self.widgets = {}
	self.windows = fun.range(1, opts.tabs):map(function(tab) return self:new_window(tab) end):totable()
	self.active_window = self.windows[1]

	self.fps = 1
	self.main_loop_cond = fiber.cond()
end

function ui:press(key)
	return self.active_window.keyboard:press(key) or self.keyboard:press(key)
end

function ui:unpress()
	self.keyboard:unpress()
	for _, wnd in ipairs(self.windows) do
		wnd.keyboard:unpress()
	end
end

function ui:add(widget, tabno)
	assert(widget.update)
	assert(widget.draw)
	assert(widget ~= self)

	widget.error = function(_, msg)
		self:error(msg)
	end

	local wnd
	if tabno and self.windows[tabno] then
		wnd = self.windows[tabno]
	else
		wnd = self.active_window
	end

	widget.window = wnd
	widget.ui = self
	self.widgets[widget] = true
end

function ui:del(widget)
	widget.window = nil
	self.widgets[widget] = nil
end

---@param x number?
---@param y number?
---@param w number?
---@param h number?
---@return WTable
function ui:new_table(x, y, w, h)
	local wt = setmetatable({ x = x, y = y, w = w, h = h }, WTable)
	self:add(wt)
	return wt
end

---@param x number?
---@param y number?
---@param w number?
---@param h number?
---@return Widget
function ui:new_widget(x, y, w, h)
	local wd = setmetatable({ x = x, y = y, w = w, h = h }, Widget)
	self:add(wd)
	return wd
end

function ui:exit(exitcode)
	exitcode = exitcode or 0
	self.running = false
	self.exitcode = exitcode
	self.main_loop_cond:broadcast()
end

function ui:new_window(tab)
	local kbd = setmetatable({ _cbs = {}, _keys = {} }, Keyboard)
	return setmetatable({ loop = self, tab = tab, keyboard = kbd }, window)
end

function ui:tabNext()
	local nextTab = self.active_window.tab % #self.windows + 1
	self.active_window = self.windows[nextTab]
end

function ui:tabSet(tabno)
	if not tabno then return end
	if not self.windows[tabno] then return end
	self.active_window = self.windows[tabno]
	term.reset()
	self:redraw()
end

function ui:redraw()
	self.main_loop_cond:broadcast()
end

function ui:run()
	if self.debug then
		self._error_fh = assert(io.open('error.log', 'w+'))
	end
	self.error = function(_, msg)
		if not self._error_fh then return end
		if type(msg) ~= 'string' then
			local v, msgs = pcall(json.encode, msg)
			msg = v and msgs or tostring(msg)
		end
		assert(self._error_fh:write(msg.."\n"))
		self._error_fh:flush()
	end

	local ok, err = pcall(function()
		term.setrawmode()
		term.clear()
		term.hide()

		local rows, cols = term.getscrlc()
		self.width = cols
		self.height = rows

		for _, wnd in ipairs(self.windows) do
			wnd:init(self.width, self.height)
		end
		self.active_window = self.windows[1]
		for widget in pairs(self.widgets) do
			if widget.on_init then
				self:error(("widget %s initialized"):format(widget))
				widget:on_init()
			end
		end

		term.golc(1, 1)
		term.cleareol()

		self._stdin_f = fiber.create(function()
			fiber.self():set_joinable(true)
			local input = term.input()
			local once = true
			local iterator = function()
				if not once then
					return input()
				end
				once = false
				local rws, cls = term.getscrlc()
				if rws ~= self.height or cls ~= self.width then
					self.height = rws
					self.width = cls
					for _, wnd in ipairs(self.windows) do
						wnd:resize(cls, rws)
					end

					for widget in pairs(self.widgets) do
						if widget.on_resize then
							pcall(widget.on_resize, widget, self.active_window)
						end
					end
				end
				if not self.running then return nil end
				return input()
			end
			for byte in iterator do
				local key = term.keyname(byte)
				local ok, err = pcall(self.press, self, key)
				if not ok then
					self:error(err)
				end
				self.main_loop_cond:broadcast()
			end
		end)

		self.running = true
		self.loop_time = fiber.time()

		while self.running do
			local dt = fiber.time() - self.loop_time
			self.loop_time = fiber.time()

			self.active_window.cursor = nil

			for widget in pairs(self.widgets) do
				local ok, err = pcall(widget.update, widget, dt)
				if not ok then
					self:error(err)
				end
			end

			for widget in pairs(self.widgets) do
				local ok, err = pcall(widget.draw, widget)
				if not ok then
					self:error(err)
				end
			end

			if self.active_window.cursor then
				local x, y = unpack(self.active_window.cursor)
				self:error(("cursor {%s, %x}"):format(x, y))
				self.active_window:print(" \b", x, y)
				term.show()
			else
				term.hide()
			end

			self:unpress()
			if not self.running then break end
			self.main_loop_cond:wait(1 / self.fps)
		end

		self._stdin_f:cancel()
		self._stdin_f:join()
	end)

	term.reset()
	term.show()
	term.golc(1, 1)
	term.color(0)
	term.setsanemode()

	if not ok then
		print(err)
	end
	if self.exitcode then
		return self.exitcode
	else
		os.exit(1)
	end
end

return ui
