local M = {}
local fun = require 'fun'
local log = require 'log'
local json = require 'json'
local panic = require 'switchover._error'.panic
local e = require 'switchover._error'
local etcd = require 'switchover._global'.etcd
local Mutex = require 'switchover._mutex'

---@class etcdswitchargs
---@field public etcd_master ExtendedInstanceInfo etcd instance info
---@field public candidate_uuid string UUID of the candidate
---@field public etcd_master_name string name of the current master
---@field public etcd_shard Cluster etcd description of the shard

---Performs master switch in ETCD (with CaS and so on)
---@param args etcdSwitchArgs
function M.etcd_switch(args)
	---@type ExtendedInstanceInfo
	local etcd_master = assert(args.etcd_master, "etcd_switch: .etcd_master required")
	---@type string
	local candidate_uuid = assert(args.candidate_uuid, "etcd_switch: .candidate_uuid required")
	---@type string
	local etcd_master_name = assert(args.etcd_master_name, "etcd_switch: .etcd_master_name required")
	---@type Cluster
	local etcd_shard = assert(args.etcd_shard, "etcd_switch: .etcd_shard required")

	if candidate_uuid == etcd_master.box.instance_uuid then
		log.info("%s already registered as master", candidate_uuid)
		return 0
	end

	---@type ExtendedInstanceInfo
	local etcd_candidate = etcd_shard:instance_by_uuid(candidate_uuid)
	if not etcd_candidate then
		log.error("Instance %s wasn't found in etcd (at %s)", candidate_uuid, etcd_shard.name)
		return 1
	end

	if etcd_candidate.cluster ~= etcd_master.cluster then
		log.error("Candidate (%s/cluster:%s) and master (%s/cluster:%s) are from different clusters. Fix ETCD by yourself!",
			etcd_candidate.name, etcd_candidate.cluster, etcd_master_name, etcd_master.cluster)
		return 1
	end

	log.warn("Changing master in ETCD: %s -> %s", etcd_master_name, etcd_candidate.name)

	-- Perform CAS
	local r = etcd:set(etcd_shard.cluster_path.."/master", etcd_candidate.name,
		{ prevValue = etcd_master_name, quorum = true },
		{ leader = true }
	)
	log.info("ETCD response: %s", json.encode(r))

	if r.action == 'compareAndSwap' then
		return 0
	end

	panic("ETCD CaS failed: %s", json.encode(r))
	return 1
end

---@class etcdSwitchArgs
---@field public etcd_master ExtendedInstanceInfo etcd instance info
---@field public candidate_uuid string UUID of the candidate
---@field public etcd_master_name string name of the current master
---@field public etcd_shard Cluster etcd description of the shard
---@field public autofailover_timeout number Mutex timeout

---Performs master switch in ETCD (with CaS under switchover mutex)
---@param args etcdSwitchArgs
function M.safe_switch(args)
	---@type ExtendedInstanceInfo
	local etcd_master = assert(args.etcd_master, "safe_switch: .etcd_master required")
	---@type string
	local candidate_uuid = assert(args.candidate_uuid, "safe_switch: .candidate_uuid required")
	---@type string
	assert(args.etcd_master_name, "safe_switch: .etcd_master_name required")
	---@type Cluster
	local etcd_shard = assert(args.etcd_shard, "safe_switch: .etcd_shard required")

	if candidate_uuid == etcd_master.box.instance_uuid then
		log.info("%s already registered as master", candidate_uuid)
		return 0
	end

	local mutex_key = etcd_shard:switchover_path()
	log.info("heal: Taking mutex key: %s", mutex_key)

	local mutex_value = ('switchover:heal:%s'):format(candidate_uuid)
	local timeout = math.max(args.autofailover_timeout or 0, 2.5)

	local ok, err = Mutex:new(mutex_key)
		:atomic({ key = mutex_value, ttl = 2*timeout }, M.etcd_switch, args)

	if err then
		if ok then
			log.warn("Heal failed but replicaset is consistent. Reason: %s", err)
		else
			log.error("ALERT: Switchover ruined your replicaset. Restore it by yourself. Reason: %s", err)
		end

		return 1
	else
		-- Everything is fine:
		log.info("Heal finished successfully")
		return 0
	end
end


function M.run(args)
	assert(args.command == "heal")
	local how, _
	---@type Cluster
	local etcd_shard

	how, _, etcd_shard = require 'switchover._resolve'({args.shard}, 'shard')
	if how ~= 'etcd' then
		e.panic("ETCD was not used")
	end

	local repl = require 'switchover.discovery'.discovery({
		endpoints = etcd_shard:endpoints(),
		discovery_timeout = args.discovery_timeout,
	})

	local etcd_master, etcd_master_name = etcd_shard:master()
	local master = repl:master()
	if not master then
		local ro_master = repl.replicas[etcd_master.box.instance_uuid]
		log.error("Master in replicaset not found. Should be: %s (%s)", etcd_master_name, ro_master)
		log.warn("Try switchover pr / switchover rr / switchover promote to bring master back")
		return 1
	end

	if etcd_master.box.instance_uuid == master:uuid() then
		log.info("ETCD master %s is actual replicaset master %s (nothing to do)", etcd_master_name, master.endpoint)
		return 0
	end

	-- Check liveness of replication of master
	local ups, downs = repl:score(master)

	-- We need quorum?
	local quorum = math.ceil((#repl.replica_list+1)/2)-1 -- N/2+1 except self
	if #ups < quorum then
		log.error("Master %s has too little upstreams: %s (required >= %s)", master.endpoint,
			table.concat(fun.iter(ups):map(function(r) return r.endpoint end):totable(), ","), quorum)
		return 1
	end
	if #downs < quorum then
		log.error("Master %s has too little downstreams: %s (required >= %s)", master.endpoint,
			table.concat(fun.iter(downs):map(function(r) return r.endpoint end):totable(), ","), quorum)
		return 1
	end

	return M.etcd_switch {
		etcd_master = etcd_master,
		etcd_master_name = etcd_master_name,
		candidate_uuid = master:uuid(),
		etcd_shard = etcd_shard,
	}
end

return M
