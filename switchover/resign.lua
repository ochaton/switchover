local M = {}
local log = require 'switchover._log'
local fun = require 'fun'
local fiber = require 'fiber'
local e = require 'switchover._error'

---@param to_resign SwitchoverAlertsShardInstaceInfo[]
---@param cluster_info table<string,SwitchoverAlertsShardInfo>
---@param ignore_instances SwitchoverAlertsShardInstaceInfo[]
---@return { src: SwitchoverAlertsShardInstaceInfo, dst: SwitchoverAlertsShardInstaceInfo }[]
local function build_switches(to_resign, cluster_info, ignore_instances)
	---@type { src: SwitchoverAlertsShardInstaceInfo, dst: CandidateInfo }[]
	local switches = {}
	---@type table<string, SwitchoverAlertsShardInstaceInfo[]>
	local ignore_in_shard = {}
	for _, inst in ipairs(ignore_instances) do
		ignore_in_shard[inst.etcd_shard.name] = ignore_in_shard[inst.etcd_shard.name] or {}
		table.insert(ignore_in_shard[inst.etcd_shard.name], inst)
	end

	for _, inst in ipairs(to_resign) do
		local sname = inst.etcd_shard.shard_name
		local shard_info = cluster_info[sname]

		if not shard_info then
			e.panic("Shard %s for instance %s is not discovered", sname, inst.name)
		end

		local candidate, err = require 'switchover.core.pick_best_candidate'.pick(inst,
			shard_info, ignore_in_shard[inst.etcd_shard.name])

		if not candidate then
			e.panic(err)
			os.exit(1)
		end

		table.insert(switches, { src = inst, dst = candidate })
	end

	return switches
end

function M.run(args)
	assert(args.command == "resign", "command must be resign")
	if args.selector == nil then
		e.panic("--selector for resign must be specified")
	end

	local discovered_info = require 'switchover.core.resolve_and_discovery'.cluster {
		cluster_or_shard = args.cluster,
		discovery_timeout = args.discovery_timeout,
		selector = args.selector,
		fetch = { metrics = false, fiber_info = false, vshard_alerts = false },
	}

	local instances = discovered_info.instances
	log.info("ignoring instances {%s}",
		table.concat(fun.iter(instances):map(function(inst) return inst.name end):totable(), ',' ))

	if #instances == 0 then
		log.warn("Noone matched selector")
		return 1
	end

	---@type SwitchoverAlertsShardInstaceInfo[]
	local not_connected = fun.iter(instances)
		---@param inst SwitchoverAlertsShardInstaceInfo
		:grep(function(inst)
			return not inst.is_connected
		end)
		:totable()

	if not args.ignore_not_connected and #not_connected > 0 then
		for _, inst in ipairs(not_connected) do
			print(inst:print())
		end
		e.panic("Can't perform resign because failed to connect to %d instances", #not_connected)
		return 1
	end

	---@type SwitchoverAlertsShardInstaceInfo
	local to_resign = fun.iter(instances)
		---@param inst SwitchoverAlertsShardInstaceInfo
		:grep(function(inst)
			return inst.is_connected
				and not inst.is_cfg_ro
				and not inst.is_router
		end)
		:totable()

	if #to_resign == 0 then
		log.warn("No master nor router discovered for selector. Matched instances:")
		for _, inst in ipairs(instances) do
			print(inst:print())
		end
		return 0
	end

	local switches = build_switches(to_resign, discovered_info.cluster_info, instances)
	for _, route in ipairs(switches) do
		log.info("Planning to switch %s@%s (%s) -> %s@%s (%s, lag = %.2fs)",
			route.src.etcd_shard.name, route.src.name, route.src.vclock,
			route.dst.etcd_shard.name, route.dst.name, route.dst.vclock,
			route.dst.real_instance:upstream(route.src.real_instance:uuid()).upstream.lag
		)
	end

	if not args.yes then
		io.stdout:write("Are you ready? [yes/no] ")
		io.stdout:flush()
		if io.stdin:read("*line") ~= "yes" then
			log.error("Discarding switch")
			return 1
		end
	end

	local first = true
	for _, route in ipairs(switches) do
		discovered_info.cluster_info[route.src.etcd_shard.shard_name].repl:destroy_background()
		if first then
			first = false
			fiber.sleep(args.resign_interval or 0)
		end
		require 'switchover.switch'.run {
			command = 'switch',
			max_lag = args.max_lag,
			switch_timeout = args.switch_timeout,
			discovery_timeout = args.discovery_timeout,
			no_check_master_upstream = args.no_check_master_upstream,
			no_check_downstreams = args.no_check_downstreams,
			allow_vshard = args.allow_vshard,
			valid_status = args.valid_status,
			no_reload = args.no_reload,
			no_reload_routers = args.no_reload_routers,
			prefix = args.prefix,
			instance = ('%s@%s'):format(args.cluster, route.dst.etcd_instance.name),
		}
	end

	require 'switchover._background'.stop_all_fibers()

	require 'switchover.status'.run {
		command = 'status',
		selector = 'etcd_is_master',
		discovery_timeout = args.discovery_timeout,
		cluster = args.cluster,
	}

	return 0
end

return M
