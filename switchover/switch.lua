local M = {}

local log = require 'log'
local fun = require 'fun'
local json = require 'json'
local fiber = require 'fiber'
local resolve = require 'switchover._resolve'
local Mutex = require 'switchover._mutex'
local e = require 'switchover._error'

local fail = require 'switchover._switch_promote_common'.fail
local run_package_reload = require 'switchover._switch_promote_common'.run_package_reload
local candidate_prepare = require 'switchover._switch_promote_common'.candidate_prepare

---@class SwitchoverDiscoveryOptions
---@field public src Tarantool current master
---@field public dst Tarantool future master (candidate)
---@field public max_lag number maximum appropriate lag between `src` and `dst`

---@param opts SwitchoverDiscoveryOptions
---@return boolean whether replication is ok
---Remembers vclock on `opts.src` and mesures lag when `opts.dst` achivies it
---If mesured lag is less than `opts.max_lag` then replication check passed.
local function check_replication(opts)
	local src, dst = opts.src, opts.dst
	assert(opts.max_lag, "check_replication: max_lag is required")

	local vclock = src:vclock({ update = true })
	local need_lsn = vclock[src:id()] or 0

	local info, cfg, elapsed = dst.conn:eval([==[
		local lsn, id, timeout = ...
		local f = require 'fiber'
		local deadline = f.time()+timeout
		while (box.info.vclock[id] or 0) < lsn and f.time()<deadline do f.sleep(0.001) end
		return box.info, box.cfg, f.time()-(deadline-timeout)
	]==], { need_lsn, src:id(), opts.max_lag }, { timeout = 2*opts.max_lag })

	dst.cached_info = info
	dst.cached_cfg = cfg

	if need_lsn <= (dst:vclock()[src:id()] or 0) then
		log.warn("wait_clock(%s, %s) on %s succeed %s => %.4fs", need_lsn, src:id(), dst:id(),
			json.encode(dst:vclock()), elapsed)
		return true
	else
		log.error("wait_clock(%s, %s) on %s failed %s => %.4fs", need_lsn, src:id(), dst:id(),
			json.encode(dst:vclock()), elapsed)
		return false
	end
end

---@param candidate Tarantool
---@param repl SwitchoverReplicaset
---Checks if RAFT setup is ok to promote candidate to be next leader of replicaset
---panics if something wrong
local function check_raft(candidate, repl)
	if not repl:has_enabled_raft() then
		-- noone in replicaaset has enabled raft
		return true
	end

	if candidate:raft().mode == "off" then
		local candidates = repl:election_mode('candidate')
		if #candidates > 0 then
			log.error("Node %s can't be elected, because it's election_mode=off and %s has election_mode=candidate",
				candidate, table.concat(fun.map(function(c) return c.endpoint end, candidates):totable(), ",")
			)
			fail(candidate)
		end
		-- if `candidate` has em=off and other nodes are not candidates -- then it can be the leader
		return true
	elseif candidate:raft().mode == "voter" then
		log.error("Node %s can't be elected, because it has election_mode=voter", candidate)
		fail(candidate)
	elseif candidate:raft().mode == "candidate" then
		-- Check quorum?
		local need = candidate:raft().election_quorum
		local have = 0
		for _, mode in ipairs{ "candidate", "voter" } do
			for _, r in ipairs(repl:election_mode(mode)) do
				if candidate:replicates_from(r:uuid()) then
					have = have + 1
				end
			end
		end
		if have < need then
			log.error("Candidate %s will not reach election quorum. Requried: %s but replicaset has: %s",
				candidate, need, have
			)
			fail(candidate)
		end
		return true
	else
		e.panic("Candidate %s has unknown election_mode=%s", candidate, candidate:raft().election_mode)
	end
end

---@param opts SwitchoverSwitchOptions
---@return boolean replicaset_is_not_damaged, string|nil error_message
---Performs src:eval"box.cfg{read_only=true}" dst:eval"box.cfg{read_only=false}" switch.
---returns true - if switch was successfull
---returns true, "message" - if switch failed but replicaset left in consistent state
---returns false, "message" - if switch failed and replicaset is in unknown state (possibly ruined)
local function switch(opts)
	local src, dst = opts.src, opts.dst
	assert(opts.timeout, "switch: timeout is required")
	assert(opts.max_lag, "switch: max_lag is required")

	if opts.timeout < opts.max_lag then
		log.warn("Reducing max_lag to timeout: %s -> %s", opts.max_lag, opts.timeout)
		opts.max_lag = opts.timeout
	end

	log.warn("Running switch for: %s/%s/%s/%s -> %s/%s/%s/%s",
		src:name(), src:id(), src:uuid(), json.encode(src:vclock()),
		dst:name(), dst:id(), dst:uuid(), json.encode(dst:vclock())
	)

	local info, elapsed = src.conn:eval([==[
		local dst, lag, timeout = ...
		if box.info.replication[dst].upstream and lag < box.info.replication[dst].upstream.lag then
			return false, ("upstream %s lag too large. refusing to switch master to RO"):format(lag)
		elseif not box.info.replication[dst].downstream then
			return false, ("downstream %s is not exists in master"):format(dst)
		elseif box.info.replication[dst].downstream.status ~= "follow" then
			return false, ("Downstream %s status = %s (need follow)"):format(dst, box.info.replication[dst].downstream.status)
		elseif lag < (box.info.replication[dst].downstream.lag or 0) then
			return false, ("Downstream lag is too large %s (required <= %s)"):format(box.info.replication[dst].downstream.lag, lag)
		end

		local f = require 'fiber'
		local log = require 'log'
		local started = f.time()
		local deadline = started+timeout

		local ro = box.cfg.read_only
		box.cfg{ read_only = true }
		f.yield()

		log.info("(switchover) cfg.ro:%s info.ro:%s", box.cfg.read_only, box.info.ro)

		if box.cfg.read_only ~= true or not box.info.ro then
			log.warn("switchover: box.cfg{read_only = true} did nothing (possibly RAFT). Refusing switch")
			box.cfg{ read_only = ro } -- returing back
			return false, "box.cfg{read_only=true} does not work. Refusing switch"
		end

		log.warn("switchover: Waiting for replication[%s].downstream.vclock[%s] == %s (box.info.lsn)",
			dst, box.info.id, box.info.lsn)

		while f.time() < deadline do
			if box.info.lsn <= (box.info.replication[dst].downstream.vclock[box.info.id] or 0) then
				break
			end
			f.sleep(math.min(
				deadline - f.time(),
				math.max(0.005, box.info.replication[dst].upstream.lag) )
			)
		end

		if deadline < f.time() then
			log.warn("switchover: (data safe %s) Timed out reached. Rollback the switch (calling box.cfg{ read_only = false }",
				dst)
			box.cfg{ read_only = false }
			return false, "timed out while wait of active transactions"
		end

		log.warn("switchover: Master was switched to RO state successfully")

		return box.info, f.time() - started
	]==], { dst:id(), tonumber(opts.max_lag), tonumber(opts.timeout) }, { timeout = opts.timeout })

	if not info then
		-- Data is consistent, but switchover failed
		local err = elapsed
		return true, err
	end

	src.cached_info = info
	log.warn("Master is in RO: %s/%s took %.4fs", src:name(), json.encode(src:vclock()), elapsed)

	local s = fiber.time()
	info, elapsed = dst.conn:eval([==[
		local lsn, id, timeout = ...
		local log = require 'log'
		local clock = require 'clock'
		local f = require 'fiber' local s = f.time()
		local deadline = s+timeout

		while (box.info.vclock[id] or 0) < lsn and clock.time() < deadline do f.sleep(0.001) end

		if deadline < clock.time() then
			log.warn("switchover: (data safe) Timed out reached. Node wont be promoted to master")
			return false, ("timed out. no switch were done: %.4fs"):format(f.time()-s), box.info
		end

		if lsn > (box.info.vclock[id] or 0) then
			log.error("switchover: (data safe) switchover failed. LSN %s:%s wasnt reached", id, lsn)
			return false, ("lsn was not reached in: %.4fs"):format(f.time()-s), box.info
		end

		log.warn("switchover: (success) LSN on candidate successfully reached. "
			.."Calling box.cfg{ read_only = false } (node will become master)")
		box.cfg{ read_only = false }

		if box.info.ro and box.info.synchro and box.info.synchro.queue and box.info.synchro.queue.owner ~= box.info.id then
			log.warn("switchover: calling box.ctl.promote to seize synchro queue")
			box.ctl.promote()
		end
		return box.info, f.time()-s
	]==], { src:info().lsn or 0, src:id(), opts.max_lag }, { timeout = 2*opts.max_lag })

	if info and info.ro == false then
		dst.cached_info = info
		log.warn("Candidate is in RW state took: %.4fs", fiber.time()-s)
		return true
	end

	-- Switchover failed
	local err = elapsed
	log.error("Waiting vclock on replica failed: %s", err)

	if dst:ro{ update = true } ~= true then
		return false, "Can't rollback the switchover. Candidate is already in RW"
	end

	log.warn("Candidate is still ro: %s. Rollback the switch", dst)

	local rollback_ok, rollback_err = src.conn:eval([[
		local log = require 'log'
		log.warn("switchover: (unsafe) Calling box.cfg{ read_only = false }")
		box.cfg{ read_only = false }
		return box.info
	]], {}, { timeout = 2*opts.max_lag })

	if rollback_ok then
		log.info("Rollback succeed: %s", src)
	else
		log.error("Rollback failed: %s", rollback_err)
	end

	return true, err
end

---@param opts SwitchoverSwitchOptions
---@return boolean replicaset_is_not_damaged, string|nil error_message
---Performs src:eval"box.cfg{read_only=true}" dst:eval"box.cfg{read_only=false}" switch.
---returns true - if switch was successfull
---returns true, "message" - if switch failed but replicaset left in consistent state
---returns false, "message" - if switch failed and replicaset is in unknown state (possibly ruined)
local function raft_switch(opts)
	local src, dst = opts.src, opts.dst
	assert(opts.timeout, "raft_switch: timeout is required")

	--[[
		Rules to switch RAFT leader
		- If everyone is candidate
			-> it is ok to just call box.ctl.promote()
		- If dst is candidate and replicaset has at least synchro.quorum candidates and voters
			-> it is ok to just call box.ctl.promote()
		- If dst is off and noone is candidate
			-> it is ok to just call box.ctl.promote()
	]]

	error("RAFT switch is not implemented yet.\n"
		.."Please call box.ctl.promote() on candidate by yourself")

	log.info("Running RAFT switch for: %s/%s (%s vclock:%s) (s:%s t:%s l:%s) -> %s/%s (%s vclock: %s) (s:%s t:%s l:%s)",
		src:id(), src:uuid(), src.endpoint, json.encode(src:vclock()),
		src:election().state, src:election().term, src:election().leader,

		dst:id(), dst:uuid(), dst.endpoint, json.encode(dst:vclock()),
		dst:election().state, dst:election().term, dst:election().leader
	)

	local info, err_or_elapsed = dst.conn:eval([==[
		local f = require 'fiber' local s = f.time()
		local log = require 'log'
		local json = require 'json'

		log.warn("(switchover) calling box.ctl.promote em:%s ro:%s el:%s v:%s",
			box.cfg.election_mode, box.cfg.read_only, json.encode(box.info.election), json.encode(box.info.vclock))

		box.ctl.promote()
		f.yield()

		local timeout = ...
		local ok, err = pcall(box.ctl.wait_rw, timeout)
		if ok then
			log.warn("(switchover) this node became ro:%s el:%s v:%s", box.info.ro,
				json.encode(box.info.election), json.encode(box.info.vclock))
			return box.info, f.time() - s
		end

		log.error("(switchover) box.ctl.wait_rw failed: %s", tostring(err))
		return false, "box.ctl.wait_rw timed out or cancelled"
	]==], { tonumber(opts.timeout) }, { timeout = opts.timeout })

	if not info then
		local err = err_or_elapsed
		log.error("Candidate failed to become master: %s", err)
	elseif info.ro == false then
		dst.cached_info = info
		local elapsed = err_or_elapsed
		log.info("Candidate is in RW state took: %.4fs", elapsed)
		return true
	else
		log.info("Candidate left in RO state. Start Monitoring RAFT")
	end

	local deadline = fiber.time() + opts.max_lag
	while fiber.time() < deadline and dst:election{update=true}.term ~= src:election{update=true}.term do end

	if dst:election().term ~= src:election().term then
		log.error("Could not catch same term on candidate and master node: candidate.term:%s master.term:%s",
			dst:election().term, src:election().term
		)
		return false, "Could not catch same term"
	end

	if dst:election().leader == dst:id() and src:election().leader == dst:id() then
		log.warn("Candidate has been elected in term:%s and approved by previous leader.", dst:election().term)
		return true
	elseif dst:election().leader ~= src:election().leader then
		log.error("Candidate wasn't elected. Consensus failed. candidate.election.leader:%s master.election.leader:%s",
			dst:election().leader, src:election().leader
		)
		return false, "Consensus wasn't achived"
	else
		log.warn("Consensus achived in term:%s. leader:%s", dst:election().term, src:election().leader)
		return true, ("Elected another leader: %s"):format(src:election().leader)
	end
end

local function cartridge_switch(opts)
	local dst = opts.dst
	assert(opts.timeout, "cartridge_switch: timeout is required")

	log.info("Performing cartridge promote on %s", dst)
	local ok, res = dst:cartridge_promote(opts.timeout)

	if ok == true then
		return true
	end

	if ok == nil then
		return true, res.err
	end
end

---@class SwitchoverSwitchOptions
---@field public use_etcd boolean flag that enforces use of ETCD mutex
---@field public src Tarantool current master
---@field public dst Tarantool future master (candidate)
---@field public timeout number timeout for the switch (in seconds)
---@field public max_lag number maximum appropriate lag between `src` and `dst`
---@field public etcd_shard Cluster information about etcd shard

---@param replicaset SwitchoverReplicaset
---@param args SwitchoverSwitchOptions
---Calls `switch` or `raft_switch` depending on RAFT status of replicaset
---@return boolean replicaset_is_not_damaged, string|boolean? error_message
local function run_switch(replicaset, args)
	assert(args.timeout, "timeout is required")
	assert(args.max_lag, "max_lag is required")
	local master = args.src
	local candidate = args.dst

	local switcher
	if replicaset:has_cartridge() then
		switcher = cartridge_switch
	elseif replicaset:has_enabled_raft() then
		switcher = raft_switch
	else
		switcher = switch
	end

	local switch_args = {
		src = master,
		dst = candidate,
		timeout = args.timeout,
		max_lag = args.max_lag,
	}

	local ok, err
	if args.use_etcd then
		local shard = assert(args.etcd_shard)
		local mutex_key = shard:switchover_path()
		log.info("Taking mutex key: %s", mutex_key)
		local mutex = Mutex:new(mutex_key)
		args.mutex = mutex
		ok, err = mutex:atomic(
			{ -- key
				key = ('switchover:%s:%s:%s'):format(replicaset.uuid, master:uuid(), candidate:uuid()),
				ttl = 2*args.timeout,
				release_on_success = false,
			},
			switcher, -- function
			switch_args
		)
	else
		log.warn("WARN: Doing switch %s -> %s without ETCD lock",
			master.endpoint, candidate.endpoint)

		local r = { pcall(switcher, switch_args) }
		local pcall_ok = table.remove(r, 1)
		if pcall_ok then
			ok, err = unpack(r)
		else
			ok, err = pcall_ok, r[1]
		end
	end
	return ok, err
end

function M.run(args)
	assert(args.command == "switch")
	if not args.switch_timeout then
		args.switch_timeout = 2*args.max_lag
	end

	local candidate, repl, is_etcd, etcd_shard = require 'switchover.discovery'.candidate {
		instance = args.instance,
		discovery_timeout = args.discovery_timeout,
	}

	candidate_prepare(candidate, repl, is_etcd, etcd_shard, args)

	local master = repl:master()
	if not master then
		e.panic("can't perform switch because no master found. Use discovery and promote")
	end

	if candidate == master then
		log.error("Candidate %s already master", candidate.endpoint)
		return 1
	end

	if not candidate:replicates_from(master:uuid()) and not args.no_check_master_upstream then
		log.error("Candidate '%s' does not replicate data from master '%s'", candidate.endpoint, master.endpoint)
		log.warn("Candidate %s", candidate)
		log.warn("Master    %s", master)
		fail(candidate)
	end

	check_raft(candidate, repl)

	if not check_replication {src = master, dst = candidate, max_lag = args.max_lag} then
		log.error("Live replication monitoring failed %s -> %s",
			master.endpoint, candidate.endpoint)
		fail(candidate)
	end

	log.info("Replication %s/%s -> %s/%s is good. Lag=%.4fs",
		master:id(), master.endpoint,
		candidate:id(), candidate.endpoint,
		candidate:info().replication[master:id()].upstream.lag)

	if is_etcd and args.no_etcd_lock then
		return e.panic("--no-etcd-lock is ambiguous when shard discovered from ETCD")
	end

	local use_etcd = is_etcd and not args.no_etcd_lock
	local use_reload = not args.no_reload
	local use_cartridge = false

	local etcd_cluster
	local ctx = {
		src = master, dst = candidate,
		timeout = args.switch_timeout,
		max_lag = args.max_lag,
		use_etcd = use_etcd,
		use_cartridge = use_cartridge,
		etcd_shard = etcd_shard,
	}

	local ok, err = run_switch(repl, ctx)

	if err then
		if ok then
			log.warn("Switchover failed but replicaset is consistent. Reason: %s", err)
		else
			log.error("ALERT: Switchover ruined your replicaset. Restore it by yourself. Reason: %s", err)
		end

		goto final_discovery
	end

	-- Everything is fine:
	log.warn("Switch %s/%s -> %s/%s was successfully done",
		master:id(), master:name(), candidate:id(), candidate:name())

	if use_etcd then
		local _
		_, etcd_cluster, etcd_shard = resolve({ etcd_shard.name }, 'shard')

		local etcd_master, etcd_master_name = etcd_shard:master()

		require 'switchover.heal'.etcd_switch {
			etcd_shard = etcd_shard,
			etcd_master = etcd_master,
			etcd_master_name = etcd_master_name,
			candidate_uuid = candidate:uuid(),
		}
	end

	if ctx.mutex then
		ctx.mutex:release()
		ctx.mutex = nil
	end

	if use_reload then
		log.info("Perfoming package.reload")
		run_package_reload(candidate, { use_etcd = use_etcd })
		run_package_reload(master, { use_etcd = use_etcd })
	end

	if etcd_cluster and not args.no_reload_routers then
		local routers = etcd_cluster:routers()
		if not routers then goto final_discovery end

		log.info("Found routers in ETCD: %s", table.concat(routers:get_shard_names(), ','))

		local proxy = require 'switchover.discovery'.cluster {
			cluster = routers,
			discovery_timeout = args.discovery_timeout,
		}

		for router_name, msg in pairs(proxy) do
			local router = msg.replicaset:master()
			if not router then
				log.warn('Router %s is not discovered', router_name)
			else
				log.info("Calling package.reload on router %s", router_name)
				router:package_reload()
			end
		end
	end

	::final_discovery::
	repl:destroy()
	return require 'switchover.discovery'.run {
		command = 'discovery',
		endpoints = {is_etcd and etcd_shard.name or args.instance},
		discovery_timeout = args.discovery_timeout,
	}
end

return M