local M = {}

---Prints to stdout
---@param fmt string
---@param ... any
function M.out(fmt, ...)
	if select('#', ...) > 0 then
		io.stdout:write(fmt:format(...).."\n")
	else
		io.stdout:write(fmt.."\n")
	end
end

---Prints to stderr
---@param fmt string
---@param ... any
function M.err(fmt, ...)
	if select('#', ...) > 0 then
		io.stderr:write(fmt:format(...).."\n")
	else
		io.stderr:write(fmt.."\n")
	end
end

return M
