local M = {}
local log = require 'log'
local uri = require 'uri'
local fun = require 'fun'
local json = require 'json'
local fiber = require 'fiber'
local clock = require 'clock'
local socket = require 'socket'
local Tarantool = require "switchover._tarantool"
local Replicaset = require "switchover._replicaset"
local G = require 'switchover._global'
local errors = require 'switchover._error'
local resolve = require 'switchover._resolve'
local out = require 'switchover._util'.out

---@type table<string,{ endpoint: string, valid: number }>
local dns_cache = {}

---Enriches endpoint with authorization
---@param e string
---@param timeout number
---@return string
local function enrich_endpoint(e, timeout)
	local endpoint = uri.parse(e)
	if not endpoint.login and G.args.user then
		endpoint.login = G.args.user
	end
	if not endpoint.password and G.args.pass then
		endpoint.password = G.args.pass
		if not endpoint.login then endpoint.login = 'admin' end
	end
	local host = endpoint.host
	if endpoint.ipv4 then
		host = endpoint.ipv4
	elseif endpoint.ipv6 then
		host = endpoint.ipv6
	elseif host then
		if dns_cache[host] and dns_cache[host].valid > fiber.time() then
			host = dns_cache[host].endpoint
		else
			dns_cache[host] = nil
			local s = fiber.time()
			local uris, err = socket.getaddrinfo(host, endpoint.service or '3301', timeout or 0.1, {
				type = 'SOCK_STREAM',
				family = 'AF_INET',
				protocol = 'tcp',
			})
			if err then
				log.warn("resolve(%s) failed: %s", host, err)
			elseif type(uris) ~= 'table' or #uris == 0 then
				log.warn("resolve(%s) resolved to nothing", host)
			else
				dns_cache[host] = { endpoint = uris[1].host, valid = fiber.time()+(tonumber(G.args.dns_timeout) or 60) }
				host = uris[1].host
				log.verbose("resolve(%s) => %s in %.3fs", endpoint.host, host, fiber.time()-s)
			end
		end
	end
	if not (endpoint.password and endpoint.login) then
		return ('%s:%s'):format(host, endpoint.service or '3301')
	end
	local endpoint_str = ('%s:%s@%s:%s'):format(
		endpoint.login or 'guest', endpoint.password or '',
		host, endpoint.service or '3301'
	)
	return endpoint_str
end

---@class FetchTable
---@field public fiber_info boolean enables fiber.info() fetch
---@field public metrics boolean enables metrics.export() fetch

---@class DiscoveryArgs
---@field public need_discovery boolean
---@field public endpoints string[]
---@field public fetch FetchTable
---@field public get_etcd_name fun(string):string|nil

---Runs discovery of the shard
---@param args DiscoveryArgs
---@return SwitchoverReplicaset
local function discovery(args)
	log.verbose("Calling discovery with %s", json.encode(args))

	local tnts = { list = {}, kv = {} }
	local endpoints = args.endpoints

	local discovery_queue = {
		deadline = fiber.time() + (G.args.discovery_timeout or 5),
		chan = fiber.channel(math.max(#endpoints, 3)),
		count = 0,
		seen = {},
		put = function(self, endpoint)
			if self.deadline < fiber.time() then return false end
			if self.seen[endpoint] then return end

			local full_endpoint = enrich_endpoint(endpoint, self.deadline-fiber.time())
			if self.seen[full_endpoint] then return end

			log.verbose("putting %s => %s", endpoint, full_endpoint)
			self.count = self.count + 1
			assert(self.chan:put(full_endpoint))

			self.seen[full_endpoint] = true
		end,
		get = function(self)
			local timeout = self.deadline - fiber.time()
			if timeout < 0 then return false end
			local t = self.chan:get(timeout)
			if t then
				self.count = self.count - 1
				return t
			end
		end,
		close = function(self)
			self.chan:close()
		end,
		deadline_reached = function(self) return self.deadline < fiber.time() end,
	}

	for _, e in ipairs(endpoints) do
		discovery_queue:put(e)
	end

	---@type table<string,Tarantool>
	local connecting = {}
	while true do
		local task = discovery_queue:get()
		if not task then break end

		-- we save Tarantool objects to prevent them from erase (gc issue)
		connecting[task] = Tarantool:new(task, {
			async = true,
			log_format = G.discovery_format,
			fetch = args.fetch,
			need_discovery = args.need_discovery,
			on_connect = function(tnt)
				local uuid = tnt:uuid()
				connecting[task] = nil

				if not tnts.kv[uuid] then
					table.insert(tnts.list, tnt)
					tnts.kv[uuid] = tnt

					local replication = tnt:replication()
					if type(replication) == 'table' then
						for _, r in ipairs(replication) do
							local upstream_uri = uri.parse(r)
							if G.args.user then upstream_uri.login = G.args.user end
							if G.args.pass then upstream_uri.password = G.args.pass end

							local upstream
							if upstream_uri.login or upstream_uri.password then
								upstream_uri.login = upstream_uri.login or 'guest'
								upstream_uri.password = upstream_uri.password or ''
								upstream = ("{login}:{password}@{host}:{service}"):gsub("{(%w+)}", upstream_uri)
							else
								upstream = ("{host}:{service}"):gsub("{(%w+)}", uri.parse(r))
							end

							discovery_queue:put(upstream)
						end
					end
				else
					log.verbose("uuid %s already registered as %s. Closing %s", uuid, tnts.kv[uuid], tnt)
					tnt:destroy()
				end

				if discovery_queue.count == 0 and not next(connecting) then
					discovery_queue:close()
				end
			end,
		})
	end

	if discovery_queue:deadline_reached() and next(connecting) then
		local list = fun.iter(connecting)
			:map(function(endpoint) return ("{host}:{service}"):gsub("{(%w+)}", uri.parse(endpoint)) end)
			:totable()
		log.warn("Took to long to discovery instances (%s). "
			.."Continuing with instances which were discovered", table.concat(list, ","))
	end

	for endpoint, tnt in pairs(connecting) do
		log.verbose("Closing future to not connected endpoint %s", endpoint:gsub(":.+@", ":***@"))
		tnt:destroy()
	end

	local masked = fun.iter(endpoints)
		:map(function(e) return (e:gsub(":.+@", ":***@")) end)
		:totable()

	if not G.args.show_graph and #tnts.list > 0 then
		log.info("Discovered %s/%s nodes from %s in %.3fs",
			#tnts.list, #endpoints, table.concat(masked, ','),
			fiber.time()-G.start_at
		)
	end

	if args.get_etcd_name then
		for _, inst in ipairs(tnts.list) do
			local name = args.get_etcd_name(inst:uuid())
			if name then inst:setname(name) end
		end
	end

	return Replicaset(tnts.list)
end

M.discovery = discovery

---
---@param args table arguments of the discovery candidate
---@return Tarantool,SwitchoverReplicaset,boolean,Cluster
function M.candidate(args)
	local how, _, shard, instance = require 'switchover._resolve'({args.instance}, 'instance')
	local endpoints
	if how == 'etcd' then
		endpoints = shard:endpoints()
	else
		assert(args.instance, "instance is required")
		endpoints = { args.instance }
	end

	local repl = discovery({
		endpoints = endpoints,
		discovery_timeout = args.discovery_timeout,
		get_etcd_name = function(instance_uuid)
			if how ~= 'etcd' then return end
			local inst = shard:instance_by_uuid(instance_uuid)
			if not inst then return end
			return inst.name
		end
	})

	local candidate do
		if how == 'etcd' then
			candidate = repl.replicas[instance.box.instance_uuid]
		else
			candidate = fun.iter(repl.replica_list):grep(function(t) return t.endpoint == instance end):nth(1)
		end
	end

	if not candidate then
		errors.panic("candidate %s wasn't discovered", args.instance)
	end

	return candidate, repl, how == 'etcd', shard
end


---@class DiscoveredShard
---@field public shard_name string name of the Shard
---@field public shard Cluster
---@field public replicaset SwitchoverReplicaset

---@class DiscoveryClusterArgs
---@field public need_discovery boolean
---@field public cluster Cluster|Proxy
---@field public discovery_timeout number
---@field public fetch FetchTable

---
---@param args DiscoveryClusterArgs arguments of the connections
---@return table<string,DiscoveredShard>
function M.cluster(args)
	---@type Cluster
	local cluster = assert(args.cluster, ".cluster must be forwarded via args")
	local shard_names = cluster:get_shard_names()
	log.info("Got shards %d: %s", #shard_names, table.concat(shard_names, ","))

	local retchan = fiber.channel(#shard_names)
	local n_push = 0
	local started = clock.time()

	---
	---@param my_cluster Cluster
	---@param shard_name string
	local function discovery_shard_f(my_cluster, shard_name)
		fiber.name(shard_name)
		n_push = n_push + 1
		local shard = assert(my_cluster:shard(shard_name))
		assert(shard)
		local s = clock.time()

		local repl = discovery {
			endpoints = shard:endpoints(),
			discovery_timeout = args.discovery_timeout,
			fetch = args.fetch,
			need_discovery = args.need_discovery,
			get_etcd_name = function(instance_uuid)
				local inst = shard:instance_by_uuid(instance_uuid)
				if inst then
					return inst.name
				end
			end,
		}

		retchan:put({ shard_name = shard_name, shard = shard, replicaset = repl })
		log.verbose("Shard %s was discovered in %.3fs", shard_name, clock.time()-s)
	end

	for _, shard_name in ipairs(shard_names) do
		fiber.create(discovery_shard_f, cluster, shard_name)
	end

	local routers = cluster.routers and cluster:routers()
	if routers then
		local router_names = routers:get_shard_names()
		log.info("Got routers %d: %s", #router_names, table.concat(router_names, ","))
		for _, router_name in ipairs(routers:get_shard_names()) do
			fiber.create(discovery_shard_f, routers, router_name)
		end
	end

	local deadline = clock.time() + 2*args.discovery_timeout

	---@type table<string,DiscoveredShard>
	local shards = {}
	local nd = 0
	for _ = 1, n_push do
		local time_left = deadline - clock.time()
		if time_left <= 0 then
			break
		end
		local msg = retchan:get(time_left)
		if msg then
			nd = nd + 1
			shards[msg.shard_name] = msg
		end
	end

	if nd < n_push then
		log.warn("Timeout for cluster discovery reached. Discovered: %d/%d", nd, n_push)
	end

	retchan:close()
	log.verbose("discovery.cluster finished in %.2fs", clock.time()-started)
	return shards
end

local function replication(inst, master_uuid, master_name)
	local mu = inst:upstream(master_uuid)
	local replication_status

	if inst:uuid() == master_uuid then
		replication_status = "self"
	elseif not mu then
		replication_status = "not_connected_to_master"
	else
		replication_status = ("%s/%s:%s/%.3fs%s"):format(
			mu.upstream.status == "follow" and "ok" or "fail",
			master_name or mu.id, mu.upstream.status,
			(mu.upstream.status == "follow" or mu.upstream.status == "sync")
				and mu.upstream.lag or mu.upstream.idle,
			mu.upstream.status ~= 'follow' and (" err:"..(mu.upstream.message or "unknown")) or ""
		)
	end
	return replication_status
end
M.replication = replication

local function print_etcd_discovery(args)
	local etcd_shard = assert(args.etcd_shard, "etcd_shard is required")
	local replicaset = assert(args.replicaset, "replicaset is required")

	local _, etcd_master_name = etcd_shard:master()
	local etcd_master_uuid = etcd_shard:instance(etcd_master_name).box.instance_uuid

	for _, etcd_instance in ipairs(etcd_shard:instances()) do
		local real_instance = replicaset.replicas[etcd_instance.instance_uuid]

		if not real_instance then
			out("%s etcd:%-7s %s - not connected",
				etcd_instance.name, etcd_instance.role, etcd_instance.endpoint
			)
		else
			local etcd_synced=''
			if real_instance:role() ~= etcd_instance.role then
				etcd_synced = ' - etcd_not_synced'
				if log.colors() then
					etcd_synced = log.ansicolors('%{red}'..etcd_synced)
				end
			end
			out("%s etcd:%-7s %s "..replication(real_instance, etcd_master_uuid, etcd_master_name)..etcd_synced,
				etcd_instance.name, etcd_instance.role, real_instance:short_status(),
				real_instance:upstream()
			)
		end
	end
end
M.print_etcd_discovery = print_etcd_discovery

local output = {}

function output.plain(etcd_shard, replicaset, is_etcd)
	if is_etcd then
		print_etcd_discovery { etcd_shard = etcd_shard, replicaset = replicaset }
	else
		local rs = fun.iter(replicaset.replica_list):totable()
		table.sort(rs, function(a, b) return a:id() < b:id() end)

		local masters = replicaset:masters()
		if #masters ~= 1 then
			log.warn("Replicaset has %d masters", #masters)
		end

		local master = masters[1]
		if master then
			for _, r in ipairs(rs) do
				out("%s "..replication(r, master:uuid(), ''), r:short_status())
			end
		else
			for _, r in ipairs(rs) do
				out("%s", r:short_status())
			end
		end
	end
end

---comment
---@param etcd_shard Cluster
---@param replicaset SwitchoverReplicaset
---@param is_etcd boolean
---@return table
function output.json(etcd_shard, replicaset, is_etcd)
	---@type Tarantool[]
	local rs = fun.iter(replicaset.replica_list):totable()
	table.sort(rs, function(a, b) return a:id() < b:id() end)

	local result = {}
	if is_etcd then
		for _, etcd_instance in ipairs(etcd_shard:instances()) do
			local real_instance = replicaset.replicas[etcd_instance.instance_uuid]
			local rv = {
				endpoint = etcd_instance.endpoint,
				etcd_shard = etcd_instance,
				instance_name = etcd_instance.name,
				connected = real_instance ~= nil,
			}

			if real_instance then
				for k, v in pairs(real_instance:serialize()) do
					rv[k] = v
				end
			end

			table.insert(result, rv)
		end
	else
		result = rs
	end

	return result
end

function M.run(args)
	assert(args.command == "discovery")

	local endpoints = args.endpoints
	assert(#endpoints > 0, "You must specify at least 1 endpoint for discovery")

	local how, _, shard, _ = resolve(args.endpoints, 'shard')
	if how == 'etcd' then
		if not shard then
			errors.panic("Nothing resolved from %s", args.endpoints[1])
		end
		args.endpoints = shard:endpoints()
	end

	local replicaset = discovery(args)
	if not replicaset then
		error("No nodes discovered", 0)
	end

	if args.show_graph then
		local graphviz = replicaset:graph()
		if args.link_graph then
			print('https://dreampuf.github.io/GraphvizOnline/#'..(graphviz:gsub("([^A-Za-z0-9%_%.%-%~])", function(v)
				return ("%%%02x"):format(v:byte()):upper()
			end)))
		else
			print(graphviz)
		end
		return
	end

	if args.discovery_output_format == "json" then
		out(json.encode(output.json(shard, replicaset, how == "etcd")))
	else
		output.plain(shard, replicaset, how == "etcd")
	end
end

return M
