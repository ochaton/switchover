local M = {}

local log = require 'log'
local e = require 'switchover._error'
local resolve = require 'switchover._resolve'
local Mutex = require 'switchover._mutex'
local run_package_reload = require 'switchover._switch_promote_common'.run_package_reload
local candidate_prepare = require 'switchover._switch_promote_common'.candidate_prepare

local function promote(args)
	local candidate = assert(args.candidate)
	local ro_old_master = assert(args.ro_old_master ~= nil)
	local repl = assert(args.replicaset)
	local candidate_name = assert(args.candidate_name)
	local timeout = assert(args.timeout)

	if ro_old_master then
		local master = repl:master()
		if master then
			log.warn("Attempt to RO old master %s", master)
			master.conn:eval([[
				local log = require 'log'
				local candidate = ...
				log.warn("swithover: fencing RO master before candidate become RW %s", candidate)
				box.cfg{ read_only = true }
			]],
				{("%s %s id:%s"):format(candidate.endpoint, candidate_name, candidate:id())},
				{ timeout = timeout }
			)
			log.warn("Master was switched to RO")
		end
	end

	return candidate:force_promote({allow_vshard = args.allow_vshard})
end

function M.run(args)
	assert(args.command == "promote")

	local candidate, repl, is_etcd, etcd_shard = require 'switchover.discovery'.candidate {
		instance = args.instance,
		discovery_timeout = args.discovery_timeout,
	}

	local candidate_etcd_name do
		if is_etcd then
			candidate_etcd_name = etcd_shard:instance_by_uuid(candidate:uuid()).name
		else
			candidate_etcd_name = candidate.endpoint
		end
	end

	local repl_master = repl:master()
	if repl_master then
		local fail
		for _, r in ipairs(repl.replica_list) do
			if r ~= repl_master then
				if r:replicates_from(repl_master:uuid()) then
					fail = true
					break
				end
			end
		end
		if fail then
			e.panic("can't promote anyone because master exists and some replicas see it. Use `switch`")
		end
	end

	local use_etcd = not args.no_etcd
	local use_reload = not args.no_reload
	local ro_old_master = not args.no_fence_master

	if use_etcd and not is_etcd then
		return e.panic("ETCD is required to perform promote but not used")
	end

	local promote_required = true
	if repl_master == candidate then
		log.warn("Candidate is already RW")
		return 1
	else
		args.no_check_downstreams = true
		args.no_check_upstreams = true
		candidate_prepare(candidate, repl, is_etcd, etcd_shard, args)
	end

	local ok, err, mutex
	if promote_required and use_etcd then
		local mutex_key = etcd_shard:switchover_path()
		log.info("Taking mutex key: %s", mutex_key)
		mutex = Mutex:new(mutex_key)
		ok, err = mutex:atomic(
			{ -- key
				key = ('switchover:%s:%s'):format(repl.uuid, candidate:uuid()),
				ttl = 3*args.max_lag,
				release_on_success = false,
			},
			promote, -- function
			{
				candidate = candidate,
				replicaset = repl,
				candidate_name = candidate_etcd_name,
				ro_old_master = ro_old_master,
				allow_vshard = args.allow_vshard,
				timeout = args.max_lag or 3,
			}
		)
	elseif promote_required then
		ok, err = pcall(candidate.force_promote, candidate)
	end

	local etcd_cluster

	if err then
		if ok then
			log.warn("Promote failed but replicaset is consistent. Reason: %s", err)
		else
			log.error("ALERT: Promote ruined your replicaset. Restore it by yourself. Reason: %s", err)
		end

		goto final_discovery
	end

	log.info("Candidate %s/%s was promoted. Performing discovery",
		candidate:id(), candidate.endpoint)

	if use_etcd then
		local _
		---@type string,Cluster,Cluster
		_, etcd_cluster, etcd_shard = resolve({ etcd_shard.name }, 'shard')

		if etcd_shard:master().name == candidate_etcd_name then
			log.warn("ETCD candidate %s is synced. Nothing to do", candidate_etcd_name)
		else
			local etcd_master, etcd_master_name = etcd_shard:master()

			require 'switchover.heal'.etcd_switch {
				etcd_shard = etcd_shard,
				etcd_master = etcd_master,
				etcd_master_name = etcd_master_name,
				candidate_uuid = candidate:uuid(),
			}
		end
	end

	if mutex then
		mutex:release()
	end

	if use_reload then
		if not run_package_reload(candidate, { use_etcd = use_etcd }) then
			log.error("ERROR: Failed to execute package.reload on new master")
		end
	end

	if etcd_cluster and not args.no_reload_routers then
		local routers = etcd_cluster:routers()
		if not routers then goto final_discovery end

		log.info("Found routers in ETCD: %s", table.concat(routers:get_shard_names(), ','))

		local proxy = require 'switchover.discovery'.cluster {
			cluster = routers,
			discovery_timeout = args.discovery_timeout,
		}

		for router_name, msg in pairs(proxy) do
			local router = msg.replicaset:master()
			if not router then
				log.warn('Router %s is not discovered', router_name)
			else
				log.info("Calling package.reload on router %s", router_name)
				router:package_reload()
			end
		end
	end

	::final_discovery::
	repl:destroy()
	return require 'switchover.discovery'.run {
		command = 'discovery',
		endpoints = {is_etcd and etcd_shard.name or args.instance},
		discovery_timeout = args.discovery_timeout,
	}
end

return M