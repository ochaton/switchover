local M = {}

local log = require 'switchover._log'
local json = require 'json'
local fun = require 'fun'
local clock = require 'clock'
local G = require 'switchover._global'
local e = require 'switchover._error'
local Mutex = require 'switchover._mutex'

local etcd_switch = require 'switchover.heal'.etcd_switch
local switchover_discovery = require 'switchover.discovery'
local switchover_resolve = require 'switchover._resolve'

local function diff_vclock(v1, v2)
	local diffv = {}
	local diff_sign = 0
	local maxid = 0
	for id in pairs(v1) do
		if maxid < id then maxid = id end
	end
	for id in pairs(v2) do
		if maxid < id then maxid = id end
	end
	for id = 1, maxid do
		diffv[id] = math.abs((v1[id] or 0) - (v2[id] or 0))
		diff_sign = diff_sign + diffv[id]
	end
	return diffv, diff_sign
end

---@class CandidateInfo
---@field public replica Tarantool
---@field public n_downstreams number
---@field public score number
---@field public vclock table<number,number>
---@field public dsign number diff signature with master
---@field public dvclock table<number,number> diff vclock with master
---@field public max_achievable_vclock table<number,number>
---@field public mdsign number diff signature with max achievable
---@field public rwf_num number 0 - reload will be ok, 1 - reload will freeze
---@field public failover_priority number failover weight instance with greater failover priority is choosen

---@param repl SwitchoverReplicaset
---@param tt Tarantool
---@param max_vclock table
---@param failover_priority number
---@return CandidateInfo
local function build_candidate_info(repl, tt, max_vclock, failover_priority)
	local diffv, diff_sign = diff_vclock(max_vclock, tt:vclock())
	log.info("%s => dv:%s ds:%s", tt.masked_endpoint, json.encode(diffv), diff_sign)

	local max_achievable_vclock = repl:max_achievable_vclock(tt)
	local _, mdsign = diff_vclock(max_vclock, max_achievable_vclock)

	return {
		replica = tt,
		n_downstreams = #tt:followed_downstreams(),
		score = repl:stability_score(tt),
		vclock = tt:vclock(),
		dsign = diff_sign,
		dvclock = diffv,
		max_achievable_vclock = max_achievable_vclock,
		mdsign = mdsign,
		failover_priority = failover_priority,
		-- reload will freeze (0 - no, 1 - yes)
		rwf_num = tt:reload_will_freeze() and 1 or 0,
	}
end

local function failover(args)
	---@type SwitchoverReplicaset
	local repl = assert(args.replicaset, "replicaset is required")
	---@type CandidateInfo
	local candidate_info = assert(args.candidate_info, "candidate is required")
	---@type Cluster
	local etcd_shard = assert(args.etcd_shard, "shard is required")
	---@type number
	local timeout = assert(args.timeout, "timeout is required")
	---@type boolean
	local no_orphan_fix = args.no_orphan_fix
	---@type Tarantool
	local previous_master = args.previous_master
	---@type number
	local fence_timeout = args.fence_timeout or 3

	local mutex_value = G.etcd:get(args.mutex_key, { quorum = true }, { leader = true })
	local deadline = clock.time()+timeout

	if mutex_value ~= args.mutex_value then
		error("ETCD mutex value changed")
	end

	-- check loading of previous master:
		-- loading => box.cfg{read_only=true}
		-- not loading => rollback ETCD to etcd_master
	if previous_master then
		if previous_master:cfg({update=true}).read_only ~= true then
			local f_elapsed = clock.time()
			log.warn("[%s] Start to fence previous master: %s", etcd_shard.name, previous_master)

			local ok, err = pcall(function()
				previous_master.conn:eval([[ box.cfg{read_only=true} ]], {}, { timeout = fence_timeout })
			end)
			if not ok then
				log.error("[%s] fence %s eval failed: %s", etcd_shard.name, previous_master.endpoint, err)
			else
				log.warn("[%s] fencing %s complete in %.3fs", etcd_shard.name, etcd_shard:master_name(), f_elapsed)
			end
		end

		if previous_master:cfg({update=true}).read_only ~= true then
			log.error("[%s] Failed to fence master: %s", previous_master)
			return true, "Previous master was discovered. auto failed to fence it"
		end
	end

	local candidate = candidate_info.replica
	log.warn("Changing etcd master to %s", candidate.endpoint)

	etcd_switch {
		etcd_shard = etcd_shard,
		etcd_master = etcd_shard:master(),
		etcd_master_name = etcd_shard:master_name(),
		candidate_uuid = candidate:uuid(),
	}

	local optimal_rcq
	local current_rcq = candidate:cfg().replication_connect_quorum or #candidate:replication()

	-- TODO: rework this part. RCQ=1 (because instance already bootstrapped)
	if current_rcq > #repl.replica_list and not no_orphan_fix then
		local n_upstreams = #candidate:followed_upstreams()
		optimal_rcq = #repl.replica_list

		if n_upstreams < optimal_rcq-1 then
			log.warn("Cant fix RCQ because too Too little amount of connected upstreams (got %d, required %d)",
				n_upstreams+1, optimal_rcq -- we count self here
			)
			optimal_rcq = nil
		end
		log.warn("Will fix replication_connect_quorum: %d -> %d", current_rcq, optimal_rcq)
	end

	local target_vclock = candidate_info.max_achievable_vclock
	local new_info, time_or_err = candidate.conn:eval([[
		local target_vclock, timeout, rcq = ...
		local log = require 'log'
		local clock = require 'clock'
		local j = require 'json'
		local f = require 'fiber'
		local s = f.time()
		local deadline = s+timeout

		local function vclock_reached()
			for id, lsn in pairs(target_vclock) do
				if (box.info.vclock[id] or 0) < lsn then
					return false
				end
			end
			return true
		end

		while not vclock_reached() and clock.time() < deadline do f.sleep(0.001) end

		if not vclock_reached() then
			log.error("switchover: (data safe) switchover failed. vclock %s wasnt reached",
				j.encode(target_vclock))
			return false, ("vclock was not reached in: %.4fs"):format(f.time()-s), box.info
		end

		if deadline < clock.time() then
			log.warn("switchover: (data safe) Timed out reached. Node wont be promoted to master")
			return false, ("timed out. no promote were done: %.4fs"):format(f.time()-s), box.info
		end

		log.warn("switchover: (success) vclock on candidate successfully reached. "
			.."Calling box.cfg{ read_only = false } (node will become master)")

		box.cfg{ read_only = false, replication_connect_quorum = rcq }
		return box.info, f.time() - s
	]], {target_vclock, timeout, optimal_rcq}, { timeout = 2*timeout })

	if deadline < clock.time() then
		return false, "Timeout was reached"
	end

	if not new_info then
		return true, time_or_err
	end

	if candidate:ro({update=true}) then
		if candidate:cfg().read_only then
			return true, "Autopromote was finished, but someone change cfg.read_only to true"
		end

		if candidate:status() ~= 'orphan' then
			return true, "Autopromote was finished, but replica stayed in ro: status:"..candidate:status()
		end

		log.warn("Replica is orphan. Cannot change replication_connect_quorum. Trying to restart replication")

		local ok, err = pcall(function()
			candidate.conn:eval([[
				repl = box.cfg.replication
				box.cfg{ replication = {} }
				box.cfg{ replication = repl }
				repl = nil
			]], {}, { timeout = timeout })
		end)

		if not ok then
			return true, ("Restart replication failed: %s"):format(err)
		end

		candidate:_get{update = true}

		if candidate:ro() then
			return true, ("Candidate is still in RO state. Cant do nothing with this")
		end

		log.info("Candidate was promoted successfully to master")
	end

	if not args.no_reload then
		candidate:package_reload()
	end

	return true
end

function M.run(args)
	assert(args.command == "auto", "command must be auto")

	if not G.etcd then
		e.panic("ETCD is required for switchover auto")
	end

	local how, etcd_cluster, shard = switchover_resolve.generic({args.shard}, 'shard')
	if how ~= 'etcd' then
		e.panic("ETCD must be used")
		return
	end
	if shard == nil then
		e.panic("No shard discovered from %s", args.shard)
		return
	end

	assert(shard, "shard must be defined")

	if shard:is_proxy() then
		e.panic("Cannot failover proxy")
	end

	---@type Cluster
	local etcd_shard = shard --[[@as Cluster]]

	log.prefix(("[%s]"):format(shard.name))

	local repl = switchover_discovery.discovery({
		endpoints = shard and shard:endpoints(),
		discovery_timeout = args.discovery_timeout,
	})

	if #repl:masters() > 1 then
		e.panic("Too many masters in replicaset: %d", #repl:masters())
	end

	if repl:master() then
		log.warn("Master is discovered: %s. Nothing to do", repl:master())
		repl:destroy()
		return 0
	end

	local etcd_master = shard:master()
	assert(etcd_master.instance_uuid, "instance_uuid is not set by etcd for etcd_master")

	if not args.no_orphan_fix then
		local rcq, is_set = shard:RCQ()
		if is_set then
			log.warn("Replication connect quorum is set to %d in ETCD. Cannot change it", rcq)
			args.no_orphan_fix = true
		else
			log.warn("Replication Connect Quorum is not set in ETCD: def:%d", rcq)
		end
	end

	if args.promote_master_uuid then
		args.auto_with_promote = nil
	end
	if args.auto_with_promote then
		args.promote_master_uuid = etcd_master.instance_uuid
		args.auto_with_promote = nil
	end

	if args.promote_master_uuid and etcd_master.instance_uuid ~= args.promote_master_uuid then
		log.warn("ETCD master has been changed %s -> %s. Refusing auto promote",
			args.promote_master_uuid, etcd_master.instance_uuid
		)
		repl:destroy()
		return 1
	end

	local previous_master = repl.replicas[etcd_master.instance_uuid]
	if previous_master then
		log.info("ETCD master is connected: %s (status: %s, up: %s)",
			previous_master, previous_master:status(), previous_master:info().uptime)
	elseif args.promote_master_uuid then
		log.error("Required to promote %s leader, but it is down. Refusing auto promote", args.promote_master_uuid)
		repl:destroy()
		return 1
	end

	local candidate
	local n_live_downstreams = 0
	for _, r in ipairs(repl.replica_list) do
		local mu = r:replicates_from(etcd_master.instance_uuid)
		if mu then
			log.warn("Master is replicated by %s -> %s %s:%s %s",
				etcd_master.endpoint, r.endpoint,
				mu.upstream.status,
				tonumber(mu.upstream.lag) and ("%.3fs"):format(tonumber(mu.upstream.lag)) or mu.upstream.lag,
				json.encode((mu.downstream or {}).vclock)
			)
			n_live_downstreams = n_live_downstreams + 1
		end
	end

	-- Check that replication graph is complete with repl.replica_list

	if n_live_downstreams > 0 and not args.promote_master_uuid then
		log.warn("Master has %d live downstreams. Refusing autoswitch", n_live_downstreams)
		repl:destroy()
		return 1
	end

	local max_vclock = repl:max_vclock()
	if not args.promote_master_uuid then
		assert(n_live_downstreams == 0)
		log.warn("0 replicas replicate data from ETCD Master. Searching for best candidate to vclock %s",
			json.encode(max_vclock))
	end

	---Promote order contains list of Candidates (not ETCD master) that are okay to be next Leader.
	---@type CandidateInfo[]
	local promote_order = {}

	for _, r in ipairs(repl.replica_list) do
		local instance_autofailover_role = "candidate"
		local instance_autofailover_priority = 1
		local ei = etcd_shard:instance_by_uuid(r:uuid())
		if ei then
			instance_autofailover_role = ei.autofailover.role
			instance_autofailover_priority = ei.autofailover.priority or 1
		end

		if ({orphan=true,running=true})[r:status()] and r ~= previous_master and instance_autofailover_role ~= "witness" then
			table.insert(promote_order, build_candidate_info(repl, r, max_vclock, instance_autofailover_priority))
		end
	end

	-- if ndws > RF then fastest
	-- otherwise take max_achievable_vclock
	table.sort(promote_order, function(a, b)
		if a.score == b.score then
			if a.mdsign == b.mdsign then
				-- if diff signature is equal use failover priority
				return a.failover_priority > b.failover_priority
			end
			-- choose with lowest diff signature
			return a.mdsign < b.mdsign
		end

		-- if reload will freeze, choose the one where it will not
		-- if possible
		if a.rwf_num ~= b.rwf_num then
			return a.rwf_num < b.rwf_num
		end
		-- choose with most downstreams
		return a.score > b.score
	end)

	candidate = promote_order[1]
	if args.promote_master_uuid then
		---@type CandidateInfo[]
		-- promote_master_uuid must contain etcd_master.uuid
		-- and etcd_master_uuid must be previous_master
		-- previous_master can never be a candidate.
		-- so we check that our previous_master==etcd_master==promote_master_uuid is not in `promote_order`
		local cnds = fun.iter(promote_order)
			---@param cnd CandidateInfo
			:grep(function(cnd) return cnd.replica:uuid() == args.promote_master_uuid end)
			:totable()
		if #cnds ~= 0 then
			log.error("promote_master found in candidate list (but it must not. maybe etcd changed) promote_master_uuid = %s",
				args.promote_master_uuid
			)
			repl:destroy()
			return 1
		end

		local next_candidate = build_candidate_info(repl, previous_master, max_vclock, 1)
		candidate = next_candidate
		previous_master = nil
	end

	if not candidate then
		log.error("No candidate is ready to become master")
		repl:destroy()
		return 1
    end
	---@cast candidate CandidateInfo
	log.warn("Choosing candidate: %s / %s",
		candidate.replica:name(),
		json.encode(candidate.replica:vclock())
	)

	if candidate.mdsign ~= 0 then
		log.error("At least %d operations will be lost during failover. Failoving to vclock %s",
			candidate.mdsign, json.encode(candidate.max_achievable_vclock))
	else
		log.warn("vclock %s is achievable. No operations will be lost",
			json.encode(max_vclock))
	end

	local mutex_key = shard:switchover_path()
	log.info("Taking mutex key: %s", mutex_key)

	local mutex_value = ('switchover:%s:%s'):format(repl.uuid, candidate.replica:uuid())
	local timeout = math.max(args.autofailover_timeout or 0, 2.5)

	local ok, err = Mutex:new(mutex_key)
		:atomic(
			{ -- key
				key = mutex_value,
				ttl = 2*timeout,
				release_on_success = true,
			},
			failover, -- function
			{
				replicaset = repl,
				candidate_info = candidate,
				etcd_shard = shard,
				mutex_key = mutex_key,
				mutex_value = mutex_value,
				timeout = timeout,
				no_orphan_fix = args.no_orphan_fix,
				previous_master = previous_master,
			}
		)

	if err then
		if ok then
			log.warn("Switchover failed but replicaset is consistent. Reason: %s", err)
		else
			log.error("ALERT: Switchover ruined your replicaset. Restore it by yourself. Reason: %s", err)
		end

		switchover_discovery.run {
			command = 'discovery',
			endpoints = shard:endpoints(),
			discovery_timeout = args.discovery_timeout,
		}
		repl:destroy()
		return 1
	else
		-- Everything is fine:
		log.info("Candidate %s/%s was auto promoted", candidate.replica:id(), candidate.replica.endpoint)

		local routers = etcd_cluster and etcd_cluster:routers()
		if etcd_cluster and not args.no_reload_routers and routers then
			log.info("Found routers in ETCD: %s", table.concat(routers:get_shard_names(), ','))

			local proxy = switchover_discovery.cluster {
				cluster = routers,
				discovery_timeout = args.discovery_timeout,
			}

			for router_name, msg in pairs(proxy) do
				local router = msg.replicaset:master()
				if not router then
					log.warn('Router %s is not discovered', router_name)
				elseif router.can_package_reload then
					log.info("Calling package.reload on router %s", router_name)
					router:package_reload()
				else
					log.info("Router is discovered but package.reload() not found: %s", router)
				end
			end
		end

		repl:destroy()
		return 0
	end
end

return M
