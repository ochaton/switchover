local lpeg = require 'lulpeg'

local P, V, Ct, Cc, R, C = lpeg.P, lpeg.V, lpeg.Ct, lpeg.Cc, lpeg.R, lpeg.C

lpeg.locale(lpeg)

-- create a pattern which captures the lua value [id] and the input matching
-- [patt] in a table
local function token(id, patt) return Ct(Cc(id) * C(patt)) end

local G = {"Sentence",
	Sentence = token('S',
		V "Expr" * (V "space" * P ',' * V"space" *V "Expr")^0),

	Neq = token('Neq',
		V"Id" * V"space" * (P '!=' + P '~=') * V "space" * V"term"),

	Eq = token('Eq',
		V"Id" * V"space" * (P '==' + P '=') * V "space" * V"term"),

	Expr = token('Expr',
		token('UnNeq', P'!' * V"Id") +
		V "Eq" +
		V "Neq" +
		V "InExpr" +
		V "NotInExpr" +
		token('Is', V "Id")
	),

	space = lpeg.space^0,

	digit = R"09",
	idsafe = R("az", "AZ", "\127\255") + P '_',
	Id = token('Id',
		V"idsafe" * (V"idsafe" + V"digit" + P '.') ^ 0),

	term = token('Term',
		(lpeg.alnum + P '_' + P'.' + P ':')^1),

	tuple = P '(' * V"space" * (V"term" * (V"space" * P ',' * V"space" * V"term")^0) * V"space" * P ')',

	InExpr = token('In',
		V"Id" * V"space" * P 'in' * V"space" * V"tuple"
	),

	NotInExpr = token('NotIn',
		V"Id" * V"space" * P 'notin' * V"space" * V"tuple"
	),
}

return {
	grammar = G,
	G = P(G),
	lpeg = lpeg,
}