local ffi = require 'ffi'

local function fdef(fn,def)
	if not pcall(function(f) local _ = ffi.C[f] end, fn) then
		ffi.cdef(def)
	end
end

fdef('gethostname',[[ int gethostname(char *name, size_t len); ]])
fdef('strerror',   [[ char *strerror(int errnum); ]])

local size = 256
local buf = ffi.new('char[?]',size)

local M = {}

---Returns full hostname
---@return string
function M.get()
	local ret = ffi.C.gethostname(buf,size)
	if ret == 0 then
		return (string.gsub(ffi.string(buf), "%z+$", ""))
	else
		error("Failed to get hostname: "..ffi.string(ffi.C.strerror(ffi.errno())),2)
	end
end

---Returns short hostname
---@return string
function M.host()
	local ret = ffi.C.gethostname(buf,size)
	if ret == 0 then
		local hostname = string.gsub(ffi.string(buf), "%z+$", "")
		return (hostname:match("^([^.]+)"))
	else
		error("Failed to get hostname: "..ffi.string(ffi.C.strerror(ffi.errno())),2)
	end
end

setmetatable(M,{
	__call = M.get
})

return M