local log = require 'log'
local e = require 'switchover._error'
local v = require 'switchover._semver'
local minimal_supported_version = v'1.10.0'

local M = {}

---@param candidate Tarantool candidate which failed to be leader
function M.fail(candidate)
	error(("Candidate %s cannot be the leader. Run `discovery` to choose another candidate"):format(
		candidate.endpoint), 0)
end

function M.run_package_reload(tarantool, opts)
	opts = opts or {}
	if not tarantool.can_package_reload then
		return true
	end
	if tarantool.has_etcd and not opts.use_etcd then
		log.error("ERROR: Can't run package.reload for %s because it is configured from ETCD",
			tarantool)
		return false
	end
	return tarantool:package_reload()
end

---@param instance Tarantool
---@param allow_vshard boolean default:false
---panics if vshard discovered and not allowed
function M.check_vshard(instance, allow_vshard)
	if allow_vshard then return end
	if instance.has_vshard then
		e.panic("Instance %s is in vshard cluster", instance.endpoint)
	end
end

---@param instance Tarantool
---panics if version is not supported
function M.check_version(instance)
	if instance:version() < minimal_supported_version then
		e.panic("Can't switch because instance %s has unsupported Tarantool version: %s (required at least %s)",
			instance.endpoint, instance:version(), minimal_supported_version)
	end
end

---@param instance Tarantool
---@param valid table<string,boolean> kv of valid tarantool statuses
---panics if validation of `box.info.status` failed
function M.check_status(instance, valid)
	valid = valid or {}
	if instance:status() ~= 'running' and not valid[instance:status()] then
		e.panic("Can't switch because instance %s is in status %s (required running)",
			instance.endpoint, instance:status())
	end
end

---@param instance Tarantool
---@param args table
---panics if node may freeze on reload
function M.check_snapshot(instance, args)
	if args.no_reload then return true end

	if instance:reload_will_freeze() and not args.allow_reload_during_snapshot then
		e.panic("Can't switch because instance is dumping snapshot %s (use --allow-reload-during-snapshot)",
			instance)
	end

	return true
end

---@param args table args to prepare candidate
---@return Tarantool,Replicaset,boolean,Cluster
function M.candidate_prepare(candidate, repl, is_etcd, etcd_shard, args)
	if #candidate:followed_downstreams() == 0 and not args.no_check_downstreams then
		log.error("Candidate '%s' does not have live downstreams", candidate.endpoint)
		log.warn("Candidate %s", candidate)
		M.fail(candidate)
	end
	if #candidate:followed_upstreams() == 0 and not args.no_check_upstreams then
		log.error("Candidate '%s' does not have live upstreams", candidate.endpoint)
		log.warn("Candidate %s", candidate)
		M.fail(candidate)
	end

	if candidate.has_vshard and not args.allow_vshard then
		log.error("Can't do switch because candidate is in vshard cluster")
		M.fail(candidate)
	end
	if candidate:version() < minimal_supported_version then
		log.error("Can't switch because candidate has unsupported Tarantool version: %s (required at least %s)",
			candidate:version(), minimal_supported_version)
		M.fail(candidate)
	end

	M.check_status(candidate, args.valid_status)
	M.check_snapshot(candidate, args)

	-- Check that all nodes may receive data from candidate
	log.info("Candidate %s can be next leader", candidate)

	return candidate, repl, is_etcd, etcd_shard
end

return M