local panic = require 'switchover._error'.panic
local fun = require 'fun'

---@class Proxy
---@field public name string Name of the proxy
---@field public tree ClusterTree downloaded etcd tree of the proxy
---@field public cluster_path string path to this tree
---@field public policy string policy of parsing etcd tree
local Proxy = {}
Proxy.__index = Proxy

function Proxy:build_proxy()
	if type(self.tree.instances) ~= 'table' then
		return false, ("%s.instances required to be table got %s"):format(self.name, type(self.tree.instances))
	end

	for instance_name, instance_info in pairs(self.tree.instances) do
		if type(instance_name) ~= 'string' then
			return false, ("key of %s.instances required to be a string got %s"):format(self.name, type(instance_name))
		end
		if type(instance_info) ~= 'table' then
			return false, ("%s.instances[%q] required to be a table got %s"):format(self.name,
				instance_name, type(instance_info))
		end
		instance_info.cluster = instance_name
		if type(instance_info.box) ~= 'table' then
			return false, ("%s.instances[%q].box required to be a table got %s"):format(self.name,
				instance_name, type(instance_info.box))
		end
		if instance_info.box.listen then
			if type(instance_info.box.listen) ~= 'string' then
				return false, ("%s.instances[%q].box.listen required to be a string got %s"):format(self.name,
					instance_name, type(instance_info.box.listen))
			end
		end
		if instance_info.box.remote_addr then
			if type(instance_info.box.remote_addr) ~= 'string' then
				return false, ("%s.instances[%q].box.remote_addr required to be a string got %s"):format(self.name,
					instance_name, type(instance_info.box.remote_addr))
			end
		end
		if not instance_info.box.remote_addr and not instance_info.box.listen then
			return false, ("%s.instances[%q]: either box.listen or box.remote_addr must be present"):format(
				self.name, instance_name
			)
		end
	end

	return self
end

---Creates new Proxy from given arguments
---@return Proxy|boolean, nil|string returns proxy or nil and error message
function Proxy:new(args)
	if type(args) ~= 'table' then
		return false, ("args required to be table got %s"):format(type(args))
	end

	if args.policy ~= 'etcd.instance.single' then
		return false, "args.policy must be 'etcd.instance.single' for Proxy"
	end

	self = setmetatable({
		name = args.name,
		tree = args.tree,
		cluster_path = args.cluster_path,
		policy = args.policy,
	}, self)

	return self:build_proxy()
end

function Proxy:shard(instance_name)
	if fun.length(self.tree.instances) == 1 then
		return self
	end

	if not self.tree.instances[instance_name] then
		panic("Proxy:shard %s.instances does not contain instance %q", self.name, instance_name)
	end

	return Proxy:new {
		name = ("%s/%s"):format(self.name, instance_name),
		policy = 'etcd.instance.single',
		tree = {
			instances = {
				[instance_name] = self.tree.instances[instance_name]
			},
		}
	}

end

function Proxy:get_shard_names()
	return fun.iter(self.tree.instances):totable()
end

function Proxy:instances()
	local result = fun.iter(self.tree.instances):map(function(name, v)
		return {
			name = name,
			listen = v.box.listen,
			endpoint = v.box.remote_addr or v.box.listen,
			remote_addr = v.box.remote_addr,
			instance_uuid = v.box.instance_uuid,
			role = 'master',
		}
	end):totable()
	table.sort(result, function(a, b) return a.name < b.name end)
	return result
end

function Proxy:endpoints()
	return fun.iter(self:instances())
		:map(function(t) return t.listen or t.remote_addr end)
		:totable()
end

function Proxy.shard_by_name(_)
	panic("Proxy does not have shards")
end

function Proxy:instance_by_uuid(instance_uuid)
	for instance_name, instance_info in pairs(self.tree.instances) do
		if instance_info.box.instance_uuid:lower() == instance_uuid:lower() then
			return self:instance(instance_name)
		end
	end
end

---@param instance_name string
---@return boolean
function Proxy:has_instance(instance_name)
	return self.tree.instances[instance_name] ~= nil
end

---@return ExtendedInstanceInfo
function Proxy:instance(instance_name)
	if self.tree.instances[instance_name] == nil then
		panic("%s.instances does not contain instance %q", self.name, instance_name)
	end

    local ret = setmetatable({ name = instance_name }, { __index = self.tree.instances[instance_name] })
	---@cast ret ExtendedInstanceInfo
	return ret
end

function Proxy:uri2name()
	return fun.iter(self.tree.instances):map(function(name, info)
		return (info.box.listen or info.box.remote_addr), name
	end):tomap()
end

---Proxy always has master
---@return boolean
function Proxy.has_master()
	return true
end

---@return ExtendedInstanceInfo,string
function Proxy:master()
	if fun.length(self.tree.instances) ~= 1 then
		panic("%q contains too many shards", self.name)
	end
	local instance_name = next(self.tree.instances)
	return self:instance(instance_name), instance_name
end

function Proxy.is_proxy()
	return true
end

return setmetatable(Proxy, { __call = Proxy.new })