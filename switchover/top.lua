local M = {}
local fun = require 'fun'
local fiber = require 'fiber'

local term = require 'switchover._plterm'
local termui = require 'switchover._termui'

local function bytes(b)
	for _, k in ipairs{{2^10, ''}, {2^20,'K'}, {2^30,'M'}, {2^40,'G'}} do
		if b < k[1] then
			local s = b/(k[1]/2^10)
			if math.floor(s) == s then return ('%d'..k[2]):format(s) end
			return ("%.1f"..k[2]):format(s)
		end
	end
	return ("%.2fT"):format(b/2^50)
end

local function concat(t, sep)
	local r = {}
	for _, v in pairs(t) do
		table.insert(r, tostring(v))
	end
	return table.concat(r, sep)
end

local function human(n)
	n = tonumber(n) or 0
	if n <= 0 then return '-' end
	if n < 1000 then return math.floor(n) end
	if n < 1e6 then return ("%.1fK"):format(n/1000) end
	return ("%.2fM"):format(n/1e6)
end

local function cpu(inst)
	if not inst.metrics_export or not next(inst.metrics_export) then return '???' end
	local me = inst.metrics_export

	if not me.tnt_cpu_user_time or not me.tnt_cpu_system_time then return '???' end

	local ret = '0%'

	local p = inst.real_instance.cpu_prev
	if p then
			local u_util = (me.tnt_cpu_user_time.value - p.tnt_cpu_user_time.value)
					/(tonumber(me.tnt_cpu_user_time.timestamp - p.tnt_cpu_user_time.timestamp)/1e6)
			local s_util = (me.tnt_cpu_system_time.value - p.tnt_cpu_system_time.value)
					/(tonumber(me.tnt_cpu_system_time.timestamp - p.tnt_cpu_system_time.timestamp)/1e6)

			ret = ('%.1f%%'):format(tonumber((u_util+s_util)*100))
	end

	inst.real_instance.cpu_prev = {
		tnt_cpu_user_time = me.tnt_cpu_user_time,
		tnt_cpu_system_time = me.tnt_cpu_system_time,
	}

	return ret
end

local general_fields = {
	{ name = 'Name',     func = 'name',                                                             description = 'Instance name'                                                                            },
	{ name = 'Endpoint', func = '(real_instance or {}).masked_endpoint',                            description = 'URI of Tarantool'                                                                         },
	{ name = 'ID',       func = 'box_info.id',                                                      description = 'box.info.id of Instance'                                                                  },
	{ name = 'PID',      func = 'box_info.pid',                                                     description = 'box.info.pid of Instance'                                                                 },
	{ name = 'Size',     func = 'bytes(slab_info.quota_size)',                                      description = 'box.slab.info().quota_size of instance'                                                   },
	{ name = 'Used',     func = 'bytes(slab_info.arena_used)',                                      description = 'box.slab.info().arena_used of instance'                                                   },
	{ name = 'RMem',     func = 'bytes(runtime_info.used)',                                         description = 'box.runtime.info().used of instance'                                                      },
	{ name = 'MEM%',     func = '("%.1f%%"):format(100*slab_info.arena_used/slab_info.quota_size)', description = 'box.slab.info().arena_used / box.slab.info().quota_size of instance'                      },
	{ name = 'CPU%',     func = 'cpu(inst)',                                                        description = 'metrics / cpu_time of instance'                                                           },
	{ name = 'LjMEM',    func = 'bytes(box_info.memory.lua)',                                       description = 'box.info.memory().lua of instance'                                                        },
	{ name = 'NFibers',  func = 'n_fibers',                                                         description = 'Amount of fibers inside of instance'                                                      },
	{ name = 'RPS',      func = 'stat.net.REQUESTS.rps',                                            description = 'box.stat.net().REQUESTS.rps - Network requests per second of instance'                    },
	{ name = 'Reqs',     func = 'stat.net.REQUESTS.current',                                        description = 'box.stat.net().REQUESTS.current - Currently processing network requests of instance'      },
	{ name = 'Conns',    func = 'stat.net.CONNECTIONS.current',                                     description = 'box.stat.net().CONNECTIONS.current - Currently established iproto-connection to instance' },
	{ name = 'S',        func = 'h(stat.SELECT.rps)',                                               description = 'box.stat.info().SELECT.rps - current select per second'                                   },
	{ name = 'I',        func = 'h(stat.INSERT.rps)',                                               description = 'box.stat.info().INSERT.rps - current insert per second'                                   },
	{ name = 'D',        func = 'h(stat.DELETE.rps)',                                               description = 'box.stat.info().DELETE.rps - current delete per second'                                   },
	{ name = 'R',        func = 'h(stat.REPLACE.rps)',                                              description = 'box.stat.info().REPLACE.rps - current replace per second'                                 },
	{ name = 'UD',       func = 'h(stat.UPDATE.rps)',                                               description = 'box.stat.info().UPDATE.rps - current update per second'                                   },
	{ name = 'US',       func = 'h(stat.UPSERT.rps)',                                               description = 'box.stat.info().UPSERT.rps - current upsert per second'                                   },
	{ name = 'E',        func = 'h(stat.ERROR.rps)',                                                description = 'box.stat.info().ERROR.rps - current error per second'                                     },
	{ name = 'Role',     func = 'role',                                                             description = 'Tarantool Master/Replica role'                                                            },
	{ name = 'Status',   func = 'box_info.status',                                                  description = 'box.info.status - current instance status'                                                },
	{ name = 'LSN',      func = 'lsn',                                                              description = 'box.info.lsn - current instance lsn'                                                      },
	{ name = 'Bkt',      func = 'bucket_count',                                                     description = 'box.space._bucket:len() - current _bucket:len()'                                          },
	{ name = 'UP',       func = 'uptime',                                                           description = 'box.info.uptime - current instance uptime'                                                },
	{ name = 'Rep',      func = 'replication_short',                                                description = 'box.info.replication - current replication status to etcd/master'                         },
}

local network_fields = {
	{ name = 'Name',     func = 'name',                                      description = 'Instance name'                                                                            },
	{ name = 'Endpoint', func = '(real_instance or {}).masked_endpoint',     description = 'URI of Tarantool'                                                                         },
	{ name = 'ID',       func = 'box_info.id',                               description = 'box.info.id of Instance'                                                                  },
	{ name = 'RPS',      func = 'stat.net.REQUESTS.rps',                     description = 'box.stat.net().REQUESTS.rps - Network requests per second of instance'                    },
	{ name = 'Reqs',     func = 'stat.net.REQUESTS.current',                 description = 'box.stat.net().REQUESTS.current - Currently processing network requests of instance'      },
	{ name = 'Conns',    func = 'stat.net.CONNECTIONS.current',              description = 'box.stat.net().CONNECTIONS.current - Currently established iproto-connection to instance' },
	{ name = 'Rx',       func = 'bytes(stat.net.RECEIVED.rps).."/s"',        description = 'box.stat.net().RECEIVED.rps - received from client bytes per second' },
	{ name = 'Tx',       func = 'bytes(stat.net.SENT.rps).."/s"',            description = 'box.stat.net().SENT.rps - sent from tarantool bytes per second' },
	{ name = 'ReConns',  func = 'stat.net.CONNECTIONS.rps',                  description = 'box.stat.net().CONNECTIONS.rps - reconnected from clients' },
	{ name = 'E',        func = 'h(stat.ERROR.rps)',                         description = 'box.stat.info().ERROR.rps - current error per second'                                     },
	{ name = 'Role',     func = 'role',                                      description = 'Tarantool Master/Replica role'                                                            },
	{ name = 'Status',   func = 'box_info.status',                           description = 'box.info.status - current instance status'                                                },
	{ name = 'LSN',      func = 'lsn',                                       description = 'box.info.lsn - current instance lsn'                                                      },
	{ name = 'Sig',      func = 'box_info.signature',                        description = 'box.info.signature - sum of all lsn in vclock'                                            },
	{ name = 'vclock',   func = '"["..concat(box_info.vclock, ",").."]"',    description = 'box.info.vclock - current instance vclock'                                                },
	{ name = 'Rep',      func = 'replication_short',                         description = 'box.info.replication - current replication status to etcd/master'                         },
}

local memory_fields = {
	{ name = 'Name',         func = 'name',                                                             description = 'Instance name'                                                                            },
	{ name = 'Endpoint',     func = '(real_instance or {}).masked_endpoint',                            description = 'URI of Tarantool'                                                                         },
	{ name = 'ID',           func = 'box_info.id',                                                      description = 'box.info.id of Instance'                                                                  },
	{ name = 'Role',         func = 'role',                                                             description = 'Tarantool Master/Replica role'                                                            },
	{ name = 'Status',       func = 'box_info.status',                                                  description = 'box.info.status - current instance status'                                                },
	{ name = 'RuntimeMem',   func = 'bytes(runtime_info.used)',                                         description = 'box.runtime.info().used of instance'                                                      },
	{ name = 'LjMEM',        func = 'bytes(box_info.memory.lua)',                                       description = 'box.info.memory().lua of instance'                                                        },
	{ name = 'QSize',        func = 'bytes(slab_info.quota_size)',                                      description = 'box.slab.info().quota_size of instance'                                                   },
	{ name = 'QUsed',        func = 'bytes(slab_info.quota_used)',                                      description = 'box.slab.info().quota_used of instance'                                                   },
	{ name = 'Quota%',       func = '("%.1f%%"):format(100*slab_info.quota_used/slab_info.quota_size)', description = 'box.slab.info().quota_used / box.slab.info().quota_size of instance'                      },
	{ name = 'ASize',        func = 'bytes(slab_info.arena_size)',                                      description = 'box.slab.info().arena_size of instance'                                                   },
	{ name = 'AUsed',        func = 'bytes(slab_info.arena_used)',                                      description = 'box.slab.info().arena_used of instance'                                                   },
	{ name = 'Arena%',       func = '("%.1f%%"):format(100*slab_info.arena_used/slab_info.arena_size)', description = 'box.slab.info().arena_used / box.slab.info().arena_size of instance'                      },
	{ name = 'Items%',       func = '("%.1f%%"):format(100*slab_info.items_used/slab_info.items_size)', description = 'box.slab.info().items_used / box.slab.info().items_size of instance'                      },
	{ name = 'MEM%',         func = '("%.1f%%"):format(100*slab_info.arena_used/slab_info.quota_size)', description = 'box.slab.info().arena_used / box.slab.info().quota_size of instance'                      },
}

local space_fields = {
	{ name = 'ID',     func = 'space.id',                        description = '' },
	{ name = 'Name',   func = 'space.name',                      description = '' },
	{ name = 'Engine', func = 'space.engine',                    description = '' },
	{ name = 'Len',    func = 'h(space.len)' ,                   description = '' },
	{ name = 'Bsize',  func = 'bytes(space.bsize)',              description = '' },
	{ name = 'Isize',  func = 'bytes(space.isize)',              description = '' },
	{ name = 'Size',   func = 'bytes(space.isize+space.bsize)',  description = '' },
	{ name = 'Avg',    func = 'bytes((space.bsize+space.isize)/max(1, space.len)).."b"', description = '' },
	{ name = 'Arena%', func = '("%.1f%%"):format((space.bsize+space.isize)/slab_info.arena_used*100)', description = '' },
}

local slab_fields = {
	{ name = 'ItemSize',  func = 'bytes(slab.item_size).."b"', description = 'Size of tuple' },
	{ name = 'ItemCount', func = 'h(slab.item_count)',    description = 'Number of tuples' },
	{ name = 'SlabCount', func = 'h(slab.slab_count)',    description = 'Number of slabs allocated' },
	{ name = 'SlabSize',  func = 'bytes(slab.slab_size).."b"', description = 'Size of each slab' },
	{ name = 'SlabCap',   func = 'h(slab.slab_size / slab.item_size)', description = "Slab capacity" },
	{ name = 'MegaSlabs', func = 'h(slab.slab_count * slab.slab_size / 16 / 2^20)', description = "Mega slabs" },
	{ name = 'MemFree',   func = 'bytes(slab.mem_free).."b"',  description = 'Allocated but not used' },
	{ name = 'MemUsed',   func = 'bytes(slab.mem_used).."b"',  description = 'Memory used for storing tuples' },
	{ name = 'MemTotal',  func = 'bytes(slab.slab_size*slab.slab_count).."b"',  description = 'Memory allocated for tuples of this size' },
	{ name = 'Usage%',    func = '("%.1f%%"):format(slab.mem_used/(slab.slab_size*slab.slab_count)*100)'  },
}

---@param discovered_info SwitchoverResolveAndDiscoveryInfo
---@param instance_name string
---@return Tarantool?
local function findInstance(discovered_info, instance_name)
	local inst
	for _, shard in pairs(discovered_info.shards) do
		if shard.shard:has_instance(instance_name) then
			local etcd_inst = shard.shard:instance(instance_name)
			inst = shard.replicaset.replicas[etcd_inst.box.instance_uuid:lower()]
			break
		end
	end
	return inst
end

---@param discovered_info SwitchoverResolveAndDiscoveryInfo
---@param instance_name string
function M.slabs(discovered_info, instance_name)
	local inst = findInstance(discovered_info, instance_name)
	if not inst then
		return {}, 'not-found'
	end

	if not inst.fetch.slabs then
		inst.fetch.slabs = true
		if inst.discovery_f then
			inst.discovery_f:notify()
		end
		return {}, 'loading'
	end

	if not inst.slab_stats then
		return {}, 'loading'
	end

	table.sort(inst.slab_stats, function(a, b)
		if a.slab_size == b.slab_size then
			return a.item_size < b.item_size
		end
		return a.slab_size < b.slab_size
	end)

	return fun.iter(inst.slab_stats)
		---@param slab table
		:map(function(slab)
			local row = {}
			for _, f in ipairs(slab_fields) do
				local func = assert(loadstring("return "..f.func))
				setfenv(func, setmetatable({ slab = slab, max=math.max, fun = fun, h = human, bytes = bytes, cpu = cpu }, {__index=slab,__newindex=slab}))
				local ok, ret = pcall(func)
				if ok then
					table.insert(row, ret)
				else
					table.insert(row, "-")
				end
			end
			return row
		end)
		:totable()
end

---@param discovered_info SwitchoverResolveAndDiscoveryInfo
function M.no_slabs(discovered_info)
	for _, shard in pairs(discovered_info.shards) do
		for _, tt in pairs(shard.replicaset.replica_list) do
			tt.fetch.slabs = false
		end
	end
end

---@param discovered_info SwitchoverResolveAndDiscoveryInfo
---@param instance_name string
function M.spaces(discovered_info, instance_name)
	---@type Tarantool?
	local inst = findInstance(discovered_info, instance_name)
	if not inst then
		return {}, 'not-found'
	end

	if not inst.fetch.spaces then
		inst.fetch.spaces = true
		if inst.discovery_f then
			inst.discovery_f:notify()
		end
		return {}, 'loading'
	end

	if not inst.spaces then
		return {}, 'loading'
	end

	---@param a table
	---@param b table
	table.sort(inst.spaces, function(a, b) return a.id < b.id end)

	return fun.iter(inst.spaces)
		---@param space table
		:map(function(space)
			local row = {}
			for _, f in ipairs(space_fields) do
				local func = assert(loadstring("return "..f.func))
				setfenv(func, setmetatable({ space = space, fun = fun, max=math.max, slab_info=inst.slab_info, h = human, bytes = bytes, cpu = cpu }, {__index=space,__newindex=space}))
				local ok, ret = pcall(func)
				if ok then
					table.insert(row, ret)
				else
					table.insert(row, "-")
				end
			end
			return row
		end)
		:totable()
end

---@param discovered_info SwitchoverResolveAndDiscoveryInfo
function M.no_spaces(discovered_info)
	for _, shard in pairs(discovered_info.shards) do
		for _, tt in pairs(shard.replicaset.replica_list) do
			tt.fetch.spaces = false
		end
	end
end

---@param discovered_info SwitchoverResolveAndDiscoveryInfo
function M.once(discovered_info)
	local instances = {}
	for _, inf in pairs(discovered_info.cluster_info) do
		for _, inst in ipairs(inf.selected_instances) do
			table.insert(instances, inst)
		end
	end
	table.sort(instances, function(a, b) return a.name < b.name end)

	return {
		general = fun.iter(instances)
			---@param inst SwitchoverAlertsShardInstaceInfo
			:grep(function(inst) return inst.is_connected end)
			---@param inst SwitchoverAlertsShardInstaceInfo
			:map(function(inst)
				local row = {}
				for _, f in ipairs(general_fields) do
					local func = assert(loadstring("return "..f.func))
					setfenv(func, setmetatable({ table = table, concat = concat, inst = inst, fun = fun, h = human, bytes = bytes, cpu = cpu }, {__index=inst,__newindex=inst}))
					local ok, ret = pcall(func)
					if ok then
						table.insert(row, ret)
					else
						table.insert(row, "-")
					end
				end
				return row
			end)
			:totable(),
		network = fun.iter(instances)
			---@param inst SwitchoverAlertsShardInstaceInfo
			:grep(function(inst) return inst.is_connected end)
			---@param inst SwitchoverAlertsShardInstaceInfo
			:map(function(inst)
				local row = {}
				for _, f in ipairs(network_fields) do
					local func = assert(loadstring("return "..f.func))
					setfenv(func, setmetatable({ table = table, concat = concat, inst = inst, fun = fun, h = human, bytes = bytes, cpu = cpu }, {__index=inst,__newindex=inst}))
					local ok, ret = pcall(func)
					if ok then
						table.insert(row, ret)
					else
						table.insert(row, "-")
					end
				end
				return row
			end)
			:totable(),
		memory = fun.iter(instances)
			---@param inst SwitchoverAlertsShardInstaceInfo
			:grep(function(inst) return inst.is_connected end)
			---@param inst SwitchoverAlertsShardInstaceInfo
			:map(function(inst)
				local row = {}
				for _, f in ipairs(memory_fields) do
					local func = assert(loadstring("return "..f.func))
					setfenv(func, setmetatable({ table = table, concat = concat, inst = inst, fun = fun, h = human, bytes = bytes, cpu = cpu }, {__index=inst,__newindex=inst}))
					local ok, ret = pcall(func)
					if ok then
						table.insert(row, ret)
					else
						table.insert(row, "-")
					end
				end
				return row
			end)
			:totable(),
	}
end


-- Verbose status of the cluster/shard, etc
function M.run(args)
	assert(args.command == "top")

	local discovered_info = require 'switchover.core.resolve_and_discovery'.cluster {
		cluster_or_shard = args.cluster_or_shard,
		discovery_timeout = args.discovery_timeout,
		need_discovery = true,
		selector = args.selector,
		fetch = { metrics = false, fiber_info = false },
	}

	args.interval = math.max(0.1, args.top_interval or 0)
	local tabs = {
		general      = 1,
		general_help = 2,
		spaces       = 3,
		spaces_help  = 4,
	}
	termui:init({ tabs = 4, debug = false })

	termui.keyboard:on({'q', 'Q'}, function()
		termui:exit(0)
	end)

	local hdr = termui:new_widget(0, 0)

	local function buildHelp(rows)
		return fun.iter(rows)
			:map(function(f) return ("%-20s %s"):format(f.name, f.description) end)
			:totable()
	end

	local helpRows = {
		general = buildHelp(general_fields),
		network = buildHelp(network_fields),
		memory = buildHelp(memory_fields),

		basic = {
			"Key bindings",
			"f6   enable sort",
			"r    reverses sort",
			"/    enable filter",
			"s    switch to another tab",
			"g    general info",
			"m    memory  info",
			"n    network info",
			"h    toggle  help",
			"q    close window",
		},
	}

	function hdr:update()
		self.left = os.getenv('HOSTNAME')
		self.title = self._title or "Switchover Top"
		self.right = os.date("%FT%T")
		self.default_style = term.colorf(term.colors.white, term.colors.bgblack, term.colors.bold)
	end

	function hdr:draw()
		self.window:print(
			self:stylef(self:align(self.right, 'right')),
			1, 1, {notrunc=true}
		)
		self.window:print(
			self:stylef(self:align(self.title, 'center')),
			1, 1, {notrunc=true}
		)
		self.window:print(
			self:stylef(self:align(self.left, 'left')),
			1, 1, {notrunc=true}
		)
	end

	local w_spaces = termui:new_table(0, 1, 0, -2)
	local hdr_spaces = hdr:clone()
	termui:add(hdr_spaces, tabs.spaces)
	termui:add(w_spaces, tabs.spaces)

	function w_spaces:on_init()
		self:parent().on_init(self)

		self.shows = {'spaces', 'slabs'}
		self.toggle = 1
		self.show = self.shows[self.toggle]
		self.pin_rows = 1
		self.padding = "  "
		self.align_cell = 'right'
		self.paused = false

		self.headers = {
			spaces = fun.iter(space_fields)
				:map(function(f) return f.name end)
				:totable(),
			slabs = fun.iter(slab_fields)
				:map(function(f) return f.name end)
				:totable(),
		}

		self.window.keyboard:on({'q', 'Q'}, function(_)
			self:unsetInstance()
			termui:tabSet(tabs.general)
		end)

		self.window.keyboard:on({'p'}, function()
			self.paused = not self.paused
			if self.paused then
				M.no_spaces(discovered_info)
				M.no_slabs(discovered_info)
				hdr_spaces._title = ("Switchover Top (%s:paused)"):format(self.instance_name)
			else
				M[self.show](discovered_info, self.instance_name)
			end
		end)

		self.window.keyboard:on({ 's' }, function()
			self.toggle = self.toggle % #self.shows + 1
			self.show = self.shows[self.toggle]
			self:error(("Switching to %s"):format(self.show))
			self.ui:redraw()

			M.no_spaces(discovered_info)
			M.no_slabs(discovered_info)
			self.paused = false

			M[self.show](discovered_info, self.instance_name)
		end)

	end

	function w_spaces:setInstance(instance_name)
		self.instance_name = instance_name
		self.paused = false
		hdr_spaces._title = "Switchover Top ("..instance_name..":loading)"
	end

	function w_spaces:unsetInstance()
		self.instance_name = nil
		M.no_spaces(discovered_info)
		M.no_slabs(discovered_info)
		self.data = { self.headers[self.show] }
		self.rows = {}
	end

	function w_spaces:update()
		if not self.instance_name then return end
		self.styles = {
			default = term.colorf(term.colors.white, term.colors.bgblack, term.colors.normal),
			[1] = term.colorf(term.colors.white, term.colors.bgblack, term.colors.bold),
			[self.current_row] = term.colorf(term.colors.white, term.colors.bgblack, term.colors.bold)
		}

		if self.paused then return end

		if not self.data or not next(self.data) then
			self.data = { self.headers[self.show] }
		end
		local func = M[self.show]
		local data, status = func(discovered_info, self.instance_name)
		if status then
			hdr_spaces._title = ("Switchover Top (%s:%s)"):format(self.instance_name, status)
		else
			hdr_spaces._title = ("Switchover Top (%s)"):format(self.instance_name)
		end
		if not next(data) then self:redraw() return end


		self.data = { self.headers[self.show] }
		for _, line in ipairs(data) do
			table.insert(self.data, line)
		end
		self:redraw()
	end

	local w_instances = termui:new_table(0, 1, 0, -2)

	function w_instances:on_init()
		self:parent().on_init(self)

		self.pin_rows = 1
		self.show = 'general'
		self.general = {
			header = fun.iter(general_fields)
				:map(function(f) return f.name end)
				:totable(),
			rows = {},
		}
		self.network = {
			header = fun.iter(network_fields)
				:map(function(f) return f.name end)
				:totable(),
			rows = {},
		}
		self.memory = {
			header = fun.iter(memory_fields)
				:map(function(f) return f.name end)
				:totable(),
			rows = {},
		}

		self.window.keyboard:on({ 'h', 'H' }, function()
			helpRows.current = helpRows[self.show]
			termui:tabSet(tabs.general_help)
		end)

		local kmap = {
			n = 'network',
			g = 'general',
			m = 'memory',
		}

		self._refresh_data = true

		self.window.keyboard:on({ 'n', 'g', 'm' }, function(key)
			if not kmap[key] then return end
			self.show = kmap[key]
			self._refresh_data = true
			self.ui:redraw()
		end)

		self.window.keyboard:on({ 's' }, function(_)
			if self.current_row == 1 then return end
			local rows = self[self.show].rows
			local info = rows[self.current_row-1]
			if not info then return end

			local instance_name = info[1]
			self:error(("Switching to spaces for %s"):format(instance_name))

			w_spaces:setInstance(instance_name)
			termui:tabSet(tabs.spaces)
		end)

		self.window.keyboard:on({ 'r' }, function(_)
			self.reverse_sort = not self.reverse_sort
		end)

		self.window.keyboard:on({ '/' }, function(_)
			self.search_text = ''
			self.window.keyboard:focus(function(k) self:search_input(k) end)
		end)

		self.window.keyboard:on({ 'kf6' }, function(_)
			self.sort_text = ''
			self.window.keyboard:focus(function(k) self:sort_input(k) end)
		end)

		self.window.keyboard:on({'esc'}, function()
			self.filter = nil
			self.search_text = nil
		end)

		self._refresh_f = fiber.create(function()
			while true do
				fiber.sleep(args.interval)
				discovered_info:refresh()

				local data = M.once(discovered_info)
				self.general.rows = data.general
				self.network.rows = data.network
				self.memory.rows = data.memory
				self._refresh_data = true
			end
		end)
	end

	---@param key string
	function w_instances:sort_input(key)
		if key == 'del' then
			if self.sort_text == '' then
				self.window.keyboard:unfocus()
				self.sort_text = nil
			else
				self.sort_text = self.sort_text:sub(1, -2)
			end
			return
		elseif key == 'esc' then
			self.window.keyboard:unfocus()
			self.sort_text = nil
			self.ui:redraw()
			return
		elseif key == "^M" then
			self.window.keyboard:unfocus()
			local sortby = self.sort_text
			local source = self[self.show]
			if type(sortby) == 'string' and #sortby > 0 then
				local field = fun.grep(sortby, source.header):nth(1)
				if not field then
					self.alert = ("No column found for %q"):format(sortby)
				else
					self.sortby = field
					self.reverse_sort = true
				end
			end
			self.sort_text = nil
			self.ui:redraw()
			return
		elseif key == '^U' then
			self.sort_text = ''
			return
		elseif #key ~= 1 then
			return
		end

		local byte = string.byte(key)
		if 32 > byte or byte > 127 then -- printable chars
			return
		end

		self.sort_text = self.sort_text .. key
	end

	---@param key string
	function w_instances:search_input(key)
		self._refresh_data = true
		if key == 'del' then
			if self.search_text == '' then
				self.window.keyboard:unfocus()
				self.search_text = nil
			else
				self.search_text = self.search_text:sub(1, -2)
			end
			return
		elseif key == 'esc' then
			self.window.keyboard:unfocus()
			self.search_text = nil
			self.ui:redraw()
			return
		elseif key == "^M" then
			self.window.keyboard:unfocus()
			self.filter = self.search_text
			self.search_text = nil
			self.ui:redraw()
			return
		elseif key == '^U' then
			self.search_text = ''
			return
		elseif #key ~= 1 then
			return
		end

		local byte = string.byte(key)
		if 32 > byte or byte > 127 then -- printable chars
			return
		end

		self.search_text = self.search_text .. key
	end

	function w_instances:update()
		self.styles = {
			default = term.colorf(term.colors.white, term.colors.bgblack, term.colors.normal),
			[1] = term.colorf(term.colors.white, term.colors.bgblack, term.colors.bold),
			[self.current_row] = term.colorf(term.colors.white, term.colors.bgblack, term.colors.bold)
		}

		if not self.rows then
			self.rows = {}
		end

		if self._refresh_data then
			hdr._title = ("Switchover Top (%s mode)"):format(self.show)
			local source = self[self.show]
			local header = table.deepcopy(source.header)
			local data = {}
			local filter = self.search_text or self.filter or ''
			for _, line in ipairs(source.rows) do
				local endpoint, addr = line[1], line[2]
				if endpoint:find(filter, 1, true) or addr:find(filter, 1, true) then
					table.insert(data, line)
				end
			end
			if self.sortby then
				local idx = fun.index_of(self.sortby, source.header)
				if idx then
					table.sort(data, function(a, b)
						if self.reverse_sort then
							return b[idx] < a[idx]
						end
						return a[idx] < b[idx]
					end)
					local symb
					if self.reverse_sort then
						symb = '▼'
					else
						symb = '▲'
					end
					header[idx] = header[idx]..symb
				end
			end
			table.insert(data, 1, header) -- prepend header
			self.data = data
			self:redraw()
			self._refresh_data = nil
		end
	end

	function w_instances:draw()
		self:parent().draw(self)

		if self.search_text then
			local txt = "Search: "..self.search_text
			self.window:clear(1, -2)
			self.window:print(txt, 1, -2)
			self.window:set_cursor(#txt+1, -4)
		elseif self.sort_text then
			local txt = "Sort: "..self.sort_text
			self.window:clear(1, -2)
			self.window:print(txt, 1, -2)
			self.window:set_cursor(#txt+1, -4)
		elseif self.alert then
			local txt = "Alert: "..self.alert
			self.window:clear(1, -2)
			self.window:print(txt, 1, -2)
			self.window:set_cursor(#txt+1, -4)
			self.alert = nil
		end
	end

	local general_help = termui:new_table(0, 1)
	termui:add(general_help, tabs.general_help)
	termui:add(hdr:clone(), tabs.general_help)

	function general_help:on_init()
		self:parent().on_init(self)

		self.window.keyboard:on({'h', 'H', 'q', 'Q'}, function()
			termui:tabSet(tabs.general)
		end)

		self.styles = {
			[1] = term.colorf(term.colors.white, term.colors.bgblack, term.colors.bold),
			default = term.colorf(term.colors.white, term.colors.bgblack, term.colors.normal),
		}
	end

	function general_help:update()
		self.rows = table.deepcopy(helpRows.current or helpRows.general)

		table.insert(self.rows, "")

		self.styles[#self.rows+1] = term.colorf(term.colors.white, term.colors.bgblack, term.colors.bold)

		for _, v in ipairs(helpRows.basic) do
			table.insert(self.rows, v)
		end
	end

	function general_help:draw()
		self:parent().draw(self)
		self.window:print("Press h to return back ...", 1, -2)
	end

	os.exit(termui:run())
end

return M
