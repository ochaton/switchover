local M = {}
local log = require 'switchover._log'
local fun = require 'fun'
local out = require 'switchover._util'.out
local term = require 'switchover._plterm'
local json = require 'json'


---comment
---@param sorted_shard_names any[]
---@param alerts SwitchoverAlert[]
---@param ignore_alerts table<string,true>
---@param is_json boolean?
local function print_alerts(sorted_shard_names, alerts, ignore_alerts, is_json)
	if not is_json then
		if log.colors() then
			print(log.ansicolors('%{red}ALERTS'))
		else
			print('ALERTS')
		end
	end
	local level2color = {
		[1] = '%{red}',
		[2] = '%{red}',
		[3] = '%{yellow}',
		[4] = '%{blue}',
	}
	local lvl2msg = {
		[1] = 'CRIT',
		[2] = 'HIGH',
		[3] = 'MED',
		[4] = 'LOW',
	}

	local output = {}
	for _, sn in ipairs(sorted_shard_names) do
		---@type SwitchoverAlert[]
		local shard_alerts = fun.grep(function(a) return a.shard == sn end, alerts)
			---@param sa SwitchoverAlert
			:grep(function(sa)
				return (lvl2msg[sa.level] and not ignore_alerts[lvl2msg[sa.level]])
					and not ignore_alerts[sa.alert_code]
			end)
			:totable()

		if is_json then
			for _, sa in ipairs(shard_alerts) do
				sa.danger = lvl2msg[sa.level]
				table.insert(output, sa)
			end
		else
			if #shard_alerts > 0 then
				print(("\t%s: CRIT:%d HIGH:%d MED:%d LOW:%d"):format(sn,
					fun.iter(shard_alerts):grep(function(sa) return sa.level == 1 end):length(),
					fun.iter(shard_alerts):grep(function(sa) return sa.level == 2 end):length(),
					fun.iter(shard_alerts):grep(function(sa) return sa.level == 3 end):length(),
					fun.iter(shard_alerts):grep(function(sa) return sa.level == 4 end):length()
				))
				table.sort(shard_alerts, function(a, b) return a.level < b.level end)
				for _, sa in ipairs(shard_alerts) do
					local message
					if log.colors() then
						message = log.ansicolors(level2color[sa.level] .. sa.message)
					else
						message = sa.message
					end
					out("\t  %s [%s]: %s", lvl2msg[sa.level], sa.alert_code, message)
				end
			end
		end
	end
	if is_json then
		out(json.encode(output))
	end
end

local function print_by_server(sorted_shard_names, result)
	local by_server = {}
	local instances = {}

	local ipv4s, fqdns = {}, {}
	local fqdn2ipv4, ipv42fqdn = {}, {}

	for _, sn in ipairs(sorted_shard_names) do
		local shard_info = result[sn]
		for _, inst in ipairs(shard_info.selected_instances) do
			table.insert(instances, inst.name)
			if not inst.srv_addr then
				goto continue
			end

			if not by_server[inst.srv_addr] then
				if inst.srv_addr_kind == "ipv4" then
					table.insert(ipv4s, inst.ipv4)
				elseif inst.srv_addr_kind == "fqdn" then
					table.insert(fqdns, inst.fqdn)
				end
			end

			if inst.fqdn and inst.ipv4 then
				fqdn2ipv4[inst.fqdn] = inst.ipv4
				ipv42fqdn[inst.ipv4] = inst.fqdn
			end

			::continue::
		end
	end

	for fqdn in pairs(fqdn2ipv4) do
		if not by_server[fqdn] then
			table.insert(fqdns, fqdn)
		end
	end
	for ipv4 in pairs(ipv42fqdn) do
		if not by_server[ipv4] then
			table.insert(ipv4s, ipv4)
		end
	end

	local sorted_fqdns = {}
	for _, fqdn in ipairs(fqdns) do
		table.insert(sorted_fqdns, fqdn)
	end
	for _, ipv4 in ipairs(ipv4s) do
		if not ipv42fqdn[ipv4] then
			table.insert(sorted_fqdns, ipv4)
		end
	end

	table.sort(sorted_fqdns)

	local n = #sorted_fqdns+1
	local fqdn2id = {}
	for i, fqdn in ipairs(sorted_fqdns) do fqdn2id[fqdn] = i+1 end

	local wtab = require 'switchover._wtable'.init()
	wtab.align_cell = 'center'
	wtab.padding = "  "
	wtab.right_border = ""

	local data = {}
	do -- Servers
		local header = {
			wtab.newcell("Servers")
				:setColor(term.colors.bold)
				:setAlign('left')
		}
		for _, fqdn in ipairs(sorted_fqdns) do
			table.insert(header,
				wtab.newcell(fqdn)
					:setColor(term.colors.bold)
					:setAlign('center')
				)
		end
		table.insert(data, header)
	end
	do -- ipv4s
		local header = {
			wtab.newcell("IP Address")
				:setColor(term.colors.bold)
				:setAlign('left')
		}
		for _, fqdn in ipairs(sorted_fqdns) do
			local ipv4 = fqdn2ipv4[fqdn] or "-"
			table.insert(header,
				wtab.newcell(ipv4)
					:setColor(term.colors.bold)
					:setAlign('center')
				)
		end
		table.insert(data, header)
	end
	for _, sn in ipairs(sorted_shard_names) do
		local shard_info = result[sn]
		local row = table.new(n, 0)
		table.insert(data, row)

		row[1] = wtab.newcell(sn)
			:setAlign('left')
			:setColor(term.colors.yellow)

		for _, inst in ipairs(shard_info.selected_instances) do
			local column
			if inst.srv_addr_kind == "fqdn" then
				column = inst.fqdn
			elseif inst.srv_addr_kind == "ipv4" then
				column = ipv42fqdn[inst.ipv4] or inst.ipv4
			end
			if not column then
				goto continue
			end

			local id = fqdn2id[column]
			if not id then
				goto continue
			end

			local name = inst.name:match('([_0-9]+)$')
			if name then
				name = name:gsub('^[^0-9]+', '')
			else
				name = inst.name
			end
			if not inst.is_connected then
				local cell = wtab.newcell(
					("%s (%s - down)"):format(
						name, inst.etcd_is_master and "M" or "R")
					)
				cell:setColor(term.colors.black, term.colors.bgblack)
				row[id] = cell
			else
				local cell = wtab.newcell(("%s (%s)"):format(name, inst.role))
				if inst.role == "M" or inst.role == "R" then
					cell:setColor(term.colors.green)
				else
					cell:setColor(term.colors.red, term.colors.bright)
				end
				row[id] = cell
			end
			::continue::
		end
		for i = 1, n do
			if row[i] == nil then
				row[i] = "X"
			end
		end
	end

	wtab:setData(data)
	wtab:draw()
	return 0
end

---comment
---@param sorted_shard_names string[]
---@param result table<string,SwitchoverAlertsShardInfo>
---@param is_json boolean?
---@return nil
local function print_status(sorted_shard_names, result, is_json)
	if is_json then
		local output = {}
		for _, sn in ipairs(sorted_shard_names) do
			for _, inst in ipairs(result[sn].selected_instances) do
				inst.real_instance = nil
				inst.print = nil
				table.insert(output, inst)
			end
		end
		return out(json.encode(output))
	end
	for _, sn in ipairs(sorted_shard_names) do
		local info = result[sn]
		print(("%s [etcd master: %s, real masters: %s]"):format(
			sn, info.etcd_master_name, table.concat(info.real_masters, ",")
		))

		for _, inst in ipairs(info.selected_instances) do
			if inst.is_connected then
				print("\t"..inst:print())
			else
				local msg = "\t"..inst:print()
				if log.colors() then
					msg = log.ansicolors('%{red}'..msg)
				end
				print(msg)
			end
		end
		print()
	end
end

-- Verbose status of the cluster/shard, etc
function M.run(args)
	assert(args.command == "status")

	local ignore_alerts = args.ignore_alerts
	if type(ignore_alerts) == 'table' then
		ignore_alerts = fun.zip(ignore_alerts, fun.ones()):tomap()
	else
		ignore_alerts = {}
	end

	local discovered_info = require 'switchover.core.resolve_and_discovery'.cluster {
		cluster_or_shard = args.cluster,
		discovery_timeout = args.discovery_timeout,
		selector = args.selector,
		fetch = { metrics = false, fiber_info = false, vshard_alerts = true },
	}

	local result = discovered_info.cluster_info
	local sorted_shard_names = fun.totable(result)
	table.sort(sorted_shard_names)

	log.verbose("Shards discovered. Preparing information")

	local alerts = discovered_info.alerts

	local do_print_status = not args.only_alert
	local do_print_alerts = not args.no_alert and #alerts > 0

	if do_print_status then
		print()
		if args.by_server then
			print_by_server(sorted_shard_names, result)
		else
			print_status(sorted_shard_names, result, args.status_output_format == "json")
		end
		if do_print_alerts then
			print()
		end
	end

	if do_print_alerts then
		print_alerts(sorted_shard_names, alerts, ignore_alerts, args.status_output_format == "json")
		print()
	end

	return 0
end

return M
