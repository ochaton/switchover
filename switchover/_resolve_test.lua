require 'jit'.off()
require 'jit'.flush()
io.stdout:setvbuf("no")
io.stderr:setvbuf("no")

local settings = {
	etcd_endpoints = (
		os.getenv("ETCD_ADVERTISE_CLIENT_URLS") or "http://etcd0:2379"
	):split(','),

	etcd_prefix = os.getenv('ETCD_PREFIX') or '/',
	etcd_timeout = tonumber(os.getenv('ETCD_TIMEOUT')) or 3,
}

local switchover = '/usr/bin/switchover --no-config -e '..settings.etcd_endpoints[1]..' '

local fio = require 'fio'
local log = require 'log'
local yaml = require 'yaml'
local popen = require 'popen'
local _etcd = require 'switchover._etcd'
local G = require 'switchover._global'
local etcd

local t = require 'luatest'
local g = t.group('simple')

local function clear_etcd()
	local v = etcd:ls('/')
	for _, p in ipairs(v) do
		log.info("Removing %s from etcd", p)
		etcd:rmrf(p)
	end
end

g.before_each(clear_etcd)
g.after_each(clear_etcd)

g.before_all(function()
	etcd = _etcd.new {
		endpoints = settings.etcd_endpoints,
		prefix = settings.etcd_prefix,
		autoconnect = true,
		timeout = settings.etcd_timeout,
		integer_auto = true,
		boolean_auto = true,
	}
	G.etcd = etcd
end)

function g.test_leader_is_defined()
	t.assert(etcd.leader, "etcd leader is not discovered")
end

function g.test_etcd_list_empty()
	local ph, stdout, err
	ph, err = popen.shell("/usr/bin/switchover --no-config -e "..settings.etcd_endpoints[1].." etcd ls /", 'r')
	t.assert(ph, 'popen handle must be created')
	assert(ph, err)

	stdout, err = ph:read()
	t.assert_type(stdout, 'string', 'stdout must be a string')
	t.assert_type(err, 'nil', 'no error must be faced')

	ph:wait()
	t.assert_equals(ph:info().status.exit_code, 0, 'exit code must be 0')

	local r = yaml.decode(stdout)
	t.assert_type(r, 'table', "must return table")
	t.assert(not next(r), "table must be empty")
end

local uuid = require 'uuid'

local e = t.group('etcd')

local structure = {
	common = {
		box = {
			log_level = 5,
			log_format = 'plain',
		},
	},
	clusters = {
		single = {
			master = 'single_001',
			replicaset_uuid = uuid.str(),
		},
	},
	instances = {
		single_001 = {
			cluster = 'single',
			box = {
				listen = 'single_001:3301',
				instance_uuid = uuid.str(),
			},
		},
		single_002 = {
			cluster = 'single',
			box = {
				listen = 'single_002:3302',
				instance_uuid = uuid.str(),
			},
		},
		single_003 = {
			cluster = 'single',
			box = {
				listen = 'single_003:3303',
				instance_uuid = uuid.str(),
			},
		},
	}
}

e.before_each(function()
	etcd = _etcd.new {
		endpoints = settings.etcd_endpoints,
		prefix = settings.etcd_prefix,
		autoconnect = true,
		timeout = settings.etcd_timeout,
		integer_auto = true,
		boolean_auto = true,
	}
	G.etcd = etcd
end)

local function load_data(data)
	local dir = fio.tempdir()
	local input_file = fio.pathjoin(dir, 'single.etcd.yaml')
	local fh = fio.open(input_file, { 'O_CREAT', 'O_WRONLY', 'O_EXCL' }, tonumber("0644", 8))
	assert(fh:write(yaml.encode(data)))
	assert(fh:close())

	local ph, err
	ph, err = popen.shell(switchover .. 'etcd load / '..input_file)

	t.assert(ph, 'popen handle must be created')
	assert(ph, err)

	ph:wait()
	t.assert_equals(ph:info().status.exit_code, 0, 'etcd load - exit code must be 0')
end

e.before_each(clear_etcd)
e.after_each(clear_etcd)

function e.test_etcd_load_single()
	load_data({ singleapp = structure })

	local ph, err = popen.shell(switchover .. 'etcd get -r /', 'r')
	t.assert(ph, 'popen handle must be created')
	assert(ph, err)

	ph:wait()
	t.assert_equals(ph:info().status.exit_code, 0, 'etcd get -r - exit code must be 0')

	local result = ph:read()
	t.assert_type(result, 'string', 'stdout must be given')

	result = yaml.decode(result)
	t.assert_equals(result, { singleapp = structure }, 'etcd must contain exact the same data as we loaded')

	ph, err = popen.shell(switchover .. 'etcd validate singleapp', 'r')
	t.assert(ph, 'popen handle must be created')
	assert(ph, err)

	ph:wait()
	t.assert_equals(ph:info().status.exit_code, 0, 'etcd validate singleapp - exit code must be 0')
end

local function test_single_cluster(cluster)
	t.assert_equals(cluster.policy, 'etcd.cluster.master', 'must be discovered with etcd.cluster.master')
	t.assert_equals(cluster.name, 'single', 'cluster name must be singleapp')

	t.assert_equals(cluster.tree.common, structure.common, 'cluster.tree must be exactly the same as we loaded ealier')

	---@cast cluster +Cluster
	t.assert(cluster:has_master(), 'single-shard cluster always has master')
	t.assert_equals(cluster:master_name(), structure.clusters.single.master, 'master_name must be extracted')
end

local function test_shard(shard)
	t.assert(shard:has_master(), 'shard from single-shard cluster must contain master')
	t.assert_equals(shard:master_name(), structure.clusters.single.master, 'shard from single-shard cluster must contain master')

	t.assert_equals(shard.name, 'single', 'shard name must be a concatenation of cluster-name and shard-name')
	t.assert_not(shard:is_proxy(), 'statefull shard never a proxy')
end

function e.test_etcd_resolve_single()
	G.etcd.prefix = '/apps'
	load_data({ apps = { single = structure } })

	local _resolve = require 'switchover._resolve'

	local is_etcd, cluster, shard, instance = _resolve({'single'}, 'cluster')
	t.assert_equals(is_etcd, 'etcd', 'resolve must performed with etcd')
	t.assert(cluster, 'cluster must be defined')

	t.assert_not(shard, 'shard must be nil')
	t.assert_not(instance, 'instance must be nil')

	test_single_cluster(cluster)

	t.assert(cluster:shard('single'), 'shard must be extraceted from single-shard cluster')

	shard = cluster:shard('single')
	---@cast shard Cluster
	test_shard(shard)
	t.assert_equals(shard:switchover_path(), 'single/clusters/single/switchover', 'switchover path must be inside /clusters/single')

	is_etcd, cluster, shard, instance = _resolve({'single/single'}, 'shard')
	t.assert_equals(is_etcd, 'etcd', 'resolve must be perfomed from etcd')
	t.assert_not(instance, 'instance must not be defined')
	t.assert(cluster, 'cluster must be resolved')
	t.assert(shard, 'shard must be resolved')


	test_single_cluster(cluster)
	test_shard(shard)
	t.assert_equals(shard:switchover_path(), 'single/clusters/single/switchover', 'switchover path must be inside /clusters/single')

	---@type string,Cluster,Cluster,ExtendedInstanceInfo
	is_etcd, cluster, shard, instance = _resolve({'single@single_001'}, 'instance')
	t.assert_equals(is_etcd, 'etcd', 'resolve must be perfomed from etcd')
	t.assert(cluster, 'cluster must be resolved')
	t.assert(shard, 'shard must be resolved')
	t.assert(instance, 'instance must be resolved')

	test_single_cluster(cluster)
	test_shard(shard)
	t.assert_equals(shard:switchover_path(), 'single/clusters/single/switchover', 'switchover path must be inside /clusters/single')

	t.assert_equals(instance.name, 'single_001', 'instance.name')
	t.assert_equals(instance.box, structure.instances.single_001.box, 'instance box from etcd')
	t.assert_equals(instance.role, 'master', 'instance role is master')

	t.assert_not(cluster:routers(), "single-shard without routers")
	t.assert_equals(select(2, shard:slice(':rw')), instance, ":rw slice returns exactly instance_001")

	t.assert_equals(cluster:instance_by_uuid(instance.instance_uuid), instance, 'find self instance in cluster')

	local instances = cluster:instances()
	t.assert_type(instances, 'table', 'cluster:instances must be a list')
	t.assert_equals(instances, shard:instances(), 'cluster:instances() == shard:instances()')

	---@type string,Cluster,Cluster,ExtendedInstanceInfo
	is_etcd, cluster, shard, instance = _resolve({'single'}, 'shard')
	t.assert_equals(is_etcd, 'etcd', 'resolve must be perfomed from etcd')
	t.assert(cluster, 'cluster must be resolved')
	t.assert(shard, 'shard must be resolved')
	t.assert_not(instance, 'instance must not be resolved')

	test_single_cluster(cluster)
	test_shard(shard)

	---@type string,Cluster,Cluster,ExtendedInstanceInfo
	is_etcd, cluster, shard, instance = _resolve({'single:master'}, 'shard')
	t.assert_equals(is_etcd, 'etcd', 'resolve must be perfomed from etcd')
	t.assert(cluster, 'cluster must be resolved')
	t.assert(shard, 'shard must be resolved')
	t.assert(instance, 'instance must be resolved')

	test_single_cluster(cluster)
	test_shard(shard)
	t.assert_equals(instance.name, 'single_001', 'instance.name is master.name')
end

local cluster_structure = {
	common = { box = { log_level = 5, log_format = 'plain' } },
	clusters = {
		storage_001 = { master = 'storage_001_01', replicaset_uuid = uuid.str() },
		storage_002 = { master = 'storage_002_01', replicaset_uuid = uuid.str() },
		storage_003 = { master = 'storage_003_01', replicaset_uuid = uuid.str() },
	},
	instances = {
		storage_001_01 = { cluster = 'storage_001', box = { instance_uuid = uuid.str(), listen = 'storage_001_01:4011' } },
		storage_001_02 = { cluster = 'storage_001', box = { instance_uuid = uuid.str(), listen = 'storage_001_02:4012' } },
		storage_001_03 = { cluster = 'storage_001', box = { instance_uuid = uuid.str(), listen = 'storage_001_03:4013' } },
		storage_002_01 = { cluster = 'storage_002', box = { instance_uuid = uuid.str(), listen = 'storage_002_01:4021' } },
		storage_002_02 = { cluster = 'storage_002', box = { instance_uuid = uuid.str(), listen = 'storage_002_02:4022' } },
		storage_002_03 = { cluster = 'storage_002', box = { instance_uuid = uuid.str(), listen = 'storage_002_03:4023' } },
		storage_003_01 = { cluster = 'storage_003', box = { instance_uuid = uuid.str(), listen = 'storage_003_01:4021' } },
		storage_003_02 = { cluster = 'storage_003', box = { instance_uuid = uuid.str(), listen = 'storage_003_02:4022' } },
		storage_003_03 = { cluster = 'storage_003', box = { instance_uuid = uuid.str(), listen = 'storage_003_03:4023' } },
		router_001     = { router = true, box = { instance_uuid = uuid.str(), listen = 'router_001:5001' } },
		router_002     = { router = true, box = { instance_uuid = uuid.str(), listen = 'router_002:5002' } },
		router_003     = { router = true, box = { instance_uuid = uuid.str(), listen = 'router_003:5003' } },
	}
}

local function test_multiple_cluster(cluster)
	t.assert_equals(cluster.policy, 'etcd.cluster.master', 'must be discovered with etcd.cluster.master')
	t.assert_equals(cluster.name, 'cluster', 'cluster name must be cluster')

	t.assert_equals(cluster.tree.common, cluster_structure.common, 'cluster.tree must be exactly the same as we loaded ealier')

	---@cast cluster +Cluster
	t.assert_not(cluster:has_master(), 'multiple-shard cluster always has no master')
end

function e.test_etcd_resolve_cluster()
	G.etcd.prefix = '/apps'
	load_data({ apps = { cluster = cluster_structure } })

	local _resolve = require 'switchover._resolve'

	---@type string, Cluster, Cluster, ExtendedInstanceInfo
	local is_etcd, cluster, shard, instance = _resolve({'cluster'}, 'cluster')
	t.assert_equals(is_etcd, 'etcd', 'resolve must performed with etcd')
	t.assert(cluster, 'cluster must be defined')

	t.assert_not(shard, 'shard must be nil')
	t.assert_not(instance, 'instance must be nil')

	test_multiple_cluster(cluster)
	for shard_name in pairs(cluster_structure.clusters) do
		local etcd_shard = assert(cluster:shard(shard_name))
		t.assert(etcd_shard:master(), "shard always has master")
		t.assert(etcd_shard:master_name(), "shard always has master_name")
		t.assert(etcd_shard:has_master(), "shard always has master_name")

		t.assert_equals(etcd_shard.name, cluster.name .. '/' .. shard_name, "shard name is always concatenation")
		t.assert_equals(etcd_shard:switchover_path(), cluster.name..'/clusters/'..shard_name..'/switchover', "switchover path")
		t.assert_equals(etcd_shard:master_name(), cluster_structure.clusters[shard_name].master, "master name of the shard is exactly the same as in config")

		local master = etcd_shard:master()
		t.assert_equals(master.box, cluster_structure.instances[cluster_structure.clusters[shard_name].master].box, "master.box is same as in instances/*/box")
	end
	t.assert_equals(cluster:get_shard_names(), {'storage_001','storage_002','storage_003'}, 'shard_names are correct')

	---@type string,Cluster,Cluster,ExtendedInstanceInfo
	is_etcd, cluster, shard, instance = _resolve({'cluster/storage_001:master'}, 'instance')
	t.assert_equals(is_etcd, 'etcd', 'resolve must performed with etcd')
	t.assert(cluster, 'cluster must be defined')

	t.assert(shard, 'shard must be defined')
	t.assert(instance, 'instance must be defined')

	t.assert_equals(shard:master(), instance, "shard:master() is same as instance")
	t.assert_equals(instance.box, cluster_structure.instances.storage_001_01.box, 'master.box is storage_001_01.box')
	t.assert_equals(instance.name, 'storage_001_01', 'instance_name is storage_001_01')
	t.assert_equals(shard.name, 'cluster/storage_001', 'shard.name is cluster/storage_001')
	t.assert_equals(shard:switchover_path(), 'cluster/clusters/storage_001/switchover', 'switchover path is correct')

	t.assert_not(cluster:is_proxy(), 'cluster is not a proxy')
	t.assert_equals(#cluster:routers():instances(), 3, 'cluster does contain 3 routers')

	t.assert_equals(cluster:get_shard_names(), {'storage_001','storage_002','storage_003'}, 'shard_names are correct')
	t.assert_equals(cluster:RCQ('storage_001'), 3, 'RCQ is correctly evaluated')

	---@type string,Cluster,Cluster,ExtendedInstanceInfo
	is_etcd, cluster, shard, instance = _resolve({'cluster@storage_001_01'}, 'instance')
	t.assert_equals(is_etcd, 'etcd', 'resolve must performed with etcd')
	t.assert(cluster, 'cluster must be defined')
	t.assert(shard, 'shard must be defined')
	t.assert(instance, 'instance must be defined')

	t.assert_equals(shard:master(), instance, "shard:master() is same as instance")
	t.assert_equals(instance.box, cluster_structure.instances.storage_001_01.box, 'master.box is storage_001_01.box')
	t.assert_equals(instance.name, 'storage_001_01', 'instance_name is storage_001_01')
	t.assert_equals(shard.name, 'cluster/storage_001', 'shard.name is cluster/storage_001')
	t.assert_equals(shard:switchover_path(), 'cluster/clusters/storage_001/switchover', 'switchover path is correct')

	---@type string,Cluster,Cluster,ExtendedInstanceInfo
	local routers
	is_etcd, routers, shard, instance = _resolve({'cluster:routers'}, 'instance')
	t.assert_equals(is_etcd, 'etcd', 'resolve must performed with etcd')
	t.assert(routers, 'routers must be defined')
	t.assert_not(shard, 'shard must be defined')
	t.assert_not(instance, 'instance must be defined')

	t.assert(routers:is_proxy(), 'routers are proxy')
	t.assert(routers:has_master(), 'routers always have master')
	t.assert(routers.name, 'cluster', 'routers.name also cluster')
end
