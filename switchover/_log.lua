local clock = require 'clock'
local log   = require 'log'
local fio   = require 'fio'
local json  = require 'json'
local fiber = require 'fiber'
local G     = require 'switchover._global'

local ansicolors = require 'switchover._ansicolors'
local colors = setmetatable({ info = 'green', warn = 'yellow', error = 'red', verbose = 'magenta' }, {__index = function() return 'white' end})
local colors_enabled = false

local function safe_call(f, ...)
	local ok, err = xpcall(f, debug.traceback, ...)
	if not ok then
		io.stderr:write(("log failed: %s\n"):format(err))
		return
	end
	return err
end

local switchover_log = true

local M
M = setmetatable({
	__log = function(level, ...)
		-- local lvls = { info = 'I', warn = 'W', error = 'E', verbose = 'V', debug = 'D' }
		local fmt, data
		if select('#', ...) == 1 then
			local x = ...
			if type(x) == 'table' then
				fmt, data = '%s', { json.encode(x) }
			elseif x == nil then
				fmt, data = '%s', { 'nil' }
			else
				fmt, data = '%s', { x }
			end
		else
			fmt = ...
			data = { select(2, ...) }
		end

		local i = 4
		local caller
		while true do
			local info = debug.getinfo(i)
			if not info then break end
			if info.source:match("switchover") then
				caller = fio.basename(info.source)
					:gsub("^@?", "@")
					..":"..info.currentline
					..':'..info.linedefined..'-'..info.lastlinedefined
				break
			end
			i = i + 1
		end

		local fiber_prefix = fiber.self().storage.log_prefix or log._prefix

		if fiber_prefix then
			fmt = fiber_prefix .. ' ' .. fmt
		end

		caller = caller or "@unknown"
		fmt = fmt .. ' (%s)'
		table.insert(data, caller)
		if switchover_log then
			local now = clock.time()
			fmt = '%s %s %s '..fmt
			table.insert(data, 1, ("%s/%s"):format(fiber.id(), fiber.name()))
			table.insert(data, 1, ("+%.1fms"):format((now-G.start_at)*1000))
			table.insert(data, 1, os.date("%FT%T", now)..("%.3f"):format(now-math.floor(now)):sub(2))
		end

		if colors_enabled then
			data = string.format(fmt, unpack(data, 1, #data))
			return log[level](ansicolors('%{'..colors[level]..'}'..data))
		else
			return log[level](fmt, unpack(data))
		end

	end,
	ansicolors = ansicolors,
	switchover_log = function(enabled) switchover_log = enabled  end,
	colors = function(switch) if switch ~= nil then colors_enabled = switch end return colors_enabled end,
	info    = function(...) return safe_call(package.loaded.log.__log, 'info', ...) end,
	error   = function(...) return safe_call(package.loaded.log.__log, 'error', ...) end,
	warn    = function(...) return safe_call(package.loaded.log.__log, 'warn', ...) end,
	verbose = function(...) return safe_call(package.loaded.log.__log, 'verbose', ...) end,
	prefix  = function(...)
		if select('#', ...) == 0 then
			return log._prefix
		else
			log._prefix = ...
		end
		return log._prefix
	end,
	set_fiber_prefix = function(prefix)
		fiber.self().storage.log_prefix = prefix
	end,
}, { __index = log })

package.loaded.log = M
return M
