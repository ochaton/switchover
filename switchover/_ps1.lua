local default_format = "{{prompt}}[{{date}} {{time}}] ({{cmd_t}}) Arena:{{p(remote.slab.arena_used/remote.slab.quota_size)}} LjMEM:{{hb(remote.mem.lua)}} \n"

local fiber = require 'fiber'
local clock = require 'clock'
local fun = require 'fun'

if debug.getregistry().console_ps1 then
	return debug.getregistry().console_ps1
end

local console_fiber = fun.iter(fiber.info())
	:map(function(f) return fiber.find(f) end)
	:grep(function(f) return f.storage.console end)
	:nth(1)

local console_mt = getmetatable(console_fiber.storage.console)
console_mt.__index.prompt = nil

local ps1 = {}
ps1.stash = setmetatable({}, console_mt)

local ps1_console_mt = { __index = ps1 }
setmetatable(console_fiber.storage.console, ps1_console_mt)

local start_remote
local stop_remote

function ps1_console_mt.__newindex(_, key, value)
	ps1.stash[key] = value
	if key == 'remote' then
		if value ~= nil then
			start_remote()
		else
			stop_remote()
		end
	end
end

local function say_prompt()
	local vars = {
		date   = os.date('%F'),
		time   = os.date('%T'),
		ms     = ("%03d"):format(math.floor(1000*select(2, math.modf(clock.time())))),
		cmd_t  = ("%.3fs"):format(ps1.stash.cmd_time or 0),
		prompt = ps1.stash.prompt and ps1.stash.prompt..' ' or '',
		remote = ps1.remote_stat,
		p = function(p)
			return ("%.02f%%"):format(p*100)
		end,
		hb = function(bytes)
			if bytes <= 1024 then
				return bytes..'B'
			elseif bytes <= 2^20 then
				return ("%.2fKB"):format(bytes/2^10)
			elseif bytes <= 2^30 then
				return ("%.2fMB"):format(bytes/2^20)
			elseif bytes <= 2^40 then
				return ("%.2fGB"):format(bytes/2^30)
			elseif bytes <= 2^50 then
				return ("%.2fTB"):format(bytes/2^40)
			end
		end,
	}

	local fmt = ps1.format or os.getenv('TT_PS1') or os.getenv('TARANTOOL_PS1') or default_format

	return (fmt:gsub('{{([^{}]+)}}', function(var)
		local code = ([[return %s]]):format(var)
		local fnc = assert(loadstring(code))
		setfenv(fnc, vars)

		local ok, val = pcall(fnc)
		if not ok then
			return ''
		end
		return tostring(val)
	end))
end

local function timeit(s, ...)
	ps1.stash.cmd_time = clock.time()-s
	return ...
end

function ps1:eval(...)
	return timeit(clock.time(), self.stash:eval(...))
end

function start_remote()
	ps1.remote_poller = fiber.create(function()
		while ps1.stash.remote ~= nil do
			pcall(function()
				local res = {}
				if ps1.on_remote_fetch then
					pcall(ps1.on_remote_fetch, res)
				end
				ps1.remote_stat = res
			end)
			fiber.sleep(1)
			fiber.testcancel()
			collectgarbage('collect')
		end
		ps1.remote_stat = nil
	end)
end

function stop_remote()
	print("stop_remote")
	if ps1.remote_poller then
		pcall(fiber.wakeup, ps1.remote_poller)
		if ps1.remote == nil and pcall(fiber.cancel, ps1.remote_poller) then
			ps1.remote_poller = nil
		end
	end
	ps1.remote_stat = nil
	print(ps1.remote_stat)
end

return setmetatable(ps1, {
	__index = function(_, key)
		if key == 'prompt' then return say_prompt() end
		return ps1.stash[key]
	end
})
