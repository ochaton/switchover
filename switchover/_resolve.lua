local resolve_from = {}
local fun = require 'fun'
local log = require 'log'
local panic = require 'switchover._error'.panic
local G = require 'switchover._global'
local Cluster = require 'switchover._cluster'
local Proxy = require 'switchover._proxy'

---@type table<string,string>
local special_instance_name = {
	[':master']  = ':rw',
	[':rw']      = ':rw',
	[':ro']      = ':ro',
	[':ros']     = ':ros',
	[':routers'] = ':routers',
}

---Resolves given cluster/shard@instance to Cluster
---@param cluster_name string|nil
---@param shard_name string|nil
---@param instance_name string|nil
---@return string,Cluster|Proxy,Cluster|Proxy|nil,ExtendedInstanceInfo|nil
local function resolve(cluster_name, shard_name, instance_name)
	assert(cluster_name, "resolve: cluster is required")

	if instance_name then
		if instance_name:find ":" and instance_name:sub(1, 1) ~= ':' then
			panic(
				"Malformed instance_name given. You probably should remove ':' from arguments."
				.." cluster:%q; shard:%q; instance:%q",
				cluster_name, shard_name, instance_name
			)
		end
		if instance_name:find ":" and not special_instance_name[instance_name] then
			panic(
				"Malformed instance_name given. Only supported %q"
				.." cluster:%q; shard:%q; instance:%q",
				table.concat(fun.totable(special_instance_name), ','),
				cluster_name, shard_name, instance_name
			)
		end
	end

	log.verbose('cluster: %s; shard: %s; instance: %s',
		cluster_name, shard_name or '', instance_name or '')

	if not G.etcd then
		panic("Attempt to resolve %s/%s@%s but ETCD is not configured",
			cluster_name, shard_name or cluster_name, instance_name or '')
	end
	local cluster_tree = G.etcd:getr(cluster_name, {quorum = true})
	if not cluster_tree then
		panic("ETCD: key %s/%s is not found", G.etcd.prefix:gsub("/$", ""), cluster_name)
	end

	---@type nil|boolean|Cluster|Proxy
	local cluster, err
	if cluster_tree.clusters then
		cluster, err = Cluster:new {
			policy = 'etcd.cluster.master',
			name = cluster_name,
			tree = cluster_tree,
		}
		if err then
			panic(err)
		end
	elseif cluster_tree.instances then
		cluster, err = Proxy:new {
			policy = 'etcd.instance.single',
			name = cluster_name,
			tree = cluster_tree,
		}
		if err then
			panic(err)
		end
	else
		panic("cannot parse tree")
	end

	---@cast cluster -nil,-boolean
	---@type nil|boolean|Cluster|Proxy
	local shard
	---@type ExtendedInstanceInfo
	local instance
	if shard_name then
		shard = cluster:shard(shard_name)
	end

	if instance_name then
		local slice = special_instance_name[instance_name]
		if instance_name == ':routers' then
			log.verbose("Resolving :routers")
			local routers = cluster:routers()
			if not routers then
				panic("Routers were not discovered in %s", cluster_name)
			end
			---@cast routers -nil
			return "etcd", routers, nil, nil
		end

		if slice then
			if not shard then
				shard = assert(cluster:slice(slice))
			end
			shard, instance = shard:slice(slice)
		else
			instance = cluster:instance(instance_name)
			if instance.cluster then
				shard = cluster:shard(instance.cluster)
			else
				assert(instance.role == 'router', 'instane must be router')
				shard = cluster:routers():shard(instance_name)
			end
		end
	end

	return "etcd", cluster, shard, instance
end

---
---@param strings string[]
---@param expect string
---@return string,Cluster|Proxy|string[],Cluster|Proxy|nil,ExtendedInstanceInfo|nil
function resolve_from.generic(strings, expect)
	assert(type(strings) == 'table', "resolve_from requires table")
	expect = expect or 'shard'

	for _, s in ipairs(strings) do
		assert(type(s) == 'string', "resolve_from: only strings supported")
	end

	local first = strings[1]
	if first:find ":[0-9]+$" then
		if not fun.all(function(s) return not not s:find ":[0-9]+$" end, strings) then
			panic("All arguments must contain <host>:<port>")
		end

		return "endpoints", strings, nil, nil
	end

	local n = #strings
	assert(n == 1, "resolve: it is allowed to specify only single shard name")

	-- <cluster>/<shard>@<instance>
	-- <cluster>/<shard>:master
	-- <shard>:master
	---@type string|nil,string|nil,string|nil
	local cluster, shard, instance = first:match("^([^/@:]*)/?([^/@:]*)@?([^@]*)$")
	if shard == "" then shard = nil end
	if cluster == "" then cluster = nil end
	if instance == "" then instance = nil end

	if not shard and not instance then
		if expect == 'instance' then
			shard, instance = nil, cluster
		elseif expect == 'shard' then
			shard, instance = cluster, nil
		elseif expect == 'cluster' then
			shard, instance = nil, nil
		end
	end

	return resolve(cluster, shard, instance)
end

return setmetatable(resolve_from, {
	__call = function(_, ...) return resolve_from.generic(...) end
})
