local fiber = require 'fiber'
local log = require 'log'
local M = {}
M.__index = M

---@class SwitchoverStatsMetricOptions
---@field type "time" | "gauge"
---@field graphite string graphite template
---@field labels string[] valid variables

---@class SwitchoverStats: table<string, SwitchoverStatsMetricOptions>
local stats = {
	autofailover_last_error = {
		type = 'time',
		graphite = 'autofailover.{{host}}.{{cluster}}.last_error',
		labels = { 'host', 'cluster' },
	},
	autofailover_last_online = {
		type = 'time',
		graphite = 'autofailover.{{host}}.{{cluster}}.last_online',
		labels = { 'host', 'cluster' },
	},

	autofailover_resolution_no_master_fire = {
		type = 'time',
		graphite = 'autofailover.{{host}}.{{cluster}}.resolution.no_master.{{shard_name}}.fire',
		labels = { 'host', 'cluster', 'shard_name' },
	},
	autofailover_resolution_no_master_resolved = {
		type = 'time',
		graphite = 'autofailover.{{host}}.{{cluster}}.resolution.no_master.{{shard_name}}.resolved',
		labels = { 'host', 'cluster', 'shard_name' },
	},
	autofailover_resolution_no_master_unresolved = {
		type = 'time',
		graphite = 'autofailover.{{host}}.{{cluster}}.resolution.no_master.{{shard_name}}.unresolved',
		labels = { 'host', 'cluster', 'shard_name' },
	},

	autofailover_resolution_master_missmatch_fire = {
		type = 'time',
		graphite = 'autofailover.{{host}}.{{cluster}}.resolution.master_missmatch.{{shard_name}}.fire',
		labels = { 'host', 'cluster', 'shard_name' },
	},
	autofailover_resolution_master_missmatch_resolved = {
		type = 'time',
		graphite = 'autofailover.{{host}}.{{cluster}}.resolution.master_missmatch.{{shard_name}}.resolved',
		labels = { 'host', 'cluster', 'shard_name' },
	},
	autofailover_resolution_master_missmatch_unresolved = {
		type = 'time',
		graphite = 'autofailover.{{host}}.{{cluster}}.resolution.master_missmatch.{{shard_name}}.unresolved',
		labels = { 'host', 'cluster', 'shard_name' },
	},

	autofailover_resolution_master_is_ro_fire = {
		type = 'time',
		graphite = 'autofailover.{{host}}.{{cluster}}.resolution.master_is_ro.{{shard_name}}.fire',
		labels = { 'host', 'cluster', 'shard_name' },
	},
	autofailover_resolution_master_is_ro_resolved = {
		type = 'time',
		graphite = 'autofailover.{{host}}.{{cluster}}.resolution.master_is_ro.{{shard_name}}.resolved',
		labels = { 'host', 'cluster', 'shard_name' },
	},
	autofailover_resolution_master_is_ro_unresolved = {
		type = 'time',
		graphite = 'autofailover.{{host}}.{{cluster}}.resolution.master_is_ro.{{shard_name}}.unresolved',
		labels = { 'host', 'cluster', 'shard_name' },
	},

	autofailover_resolution_vshard_is_broken_fire = {
		type = 'time',
		graphite = 'autofailover.{{host}}.{{cluster}}.resolution.vshard_is_broken.{{shard_name}}.fire',
		labels = { 'host', 'cluster', 'shard_name' },
	},
	autofailover_resolution_vshard_is_broken_resolved = {
		type = 'time',
		graphite = 'autofailover.{{host}}.{{cluster}}.resolution.vshard_is_broken.{{shard_name}}.resolved',
		labels = { 'host', 'cluster', 'shard_name' },
	},
	autofailover_resolution_vshard_is_broken_unresolved = {
		type = 'time',
		graphite = 'autofailover.{{host}}.{{cluster}}.resolution.vshard_is_broken.{{shard_name}}.unresolved',
		labels = { 'host', 'cluster', 'shard_name' },
	},
	autofailover_resolution_unknown_unresolved = {
		type = 'time',
		graphite = 'autofailover.{{host}}.{{cluster}}.resolution.unknown.{{shard_name}}.unresolved',
		labels = { 'host', 'cluster', 'shard_name' },
	},

	autofailover_shard_is_ok = {
		type = 'time',
		graphite = 'autofailover.{{host}}.{{cluster}}.ok.{{shard_name}}',
		labels = { 'host', 'cluster', 'shard_name' },
	},

	autofailover_instance_info_ro = {
		type = 'gauge',
		graphite = 'autofailover.{{host}}.{{cluster}}.info.{{instance_name}}.ro',
		labels = { 'host', 'cluster', 'instance_name' },
	},

	autofailover_instance_info_rw = {
		type = 'gauge',
		graphite = 'autofailover.{{host}}.{{cluster}}.info.{{instance_name}}.rw',
		labels = { 'host', 'cluster', 'instance_name' },
	},

	autofailover_instance_info_election_leader = {
		type = 'gauge',
		graphite = 'autofailover.{{host}}.{{cluster}}.info.{{instance_name}}.election_leader',
		labels = { 'host', 'cluster', 'instance_name' },
	},

	autofailover_instance_info_election_term = {
		type = 'gauge',
		graphite = 'autofailover.{{host}}.{{cluster}}.info.{{instance_name}}.election_term',
		labels = { 'host', 'cluster', 'instance_name' },
	},

	autofailover_instance_info_election_vote = {
		type = 'gauge',
		graphite = 'autofailover.{{host}}.{{cluster}}.info.{{instance_name}}.election_vote',
		labels = { 'host', 'cluster', 'instance_name' },
	},

	autofailover_instance_info_uptime = {
		type = 'gauge',
		graphite = 'autofailover.{{host}}.{{cluster}}.info.{{instance_name}}.uptime',
		labels = { 'host', 'cluster', 'instance_name' },
	},

	autofailover_instance_info_lsn = {
		type = 'gauge',
		graphite = 'autofailover.{{host}}.{{cluster}}.info.{{instance_name}}.lsn',
		labels = { 'host', 'cluster', 'instance_name' },
	},

	autofailover_instance_rtt = {
		type = 'gauge',
		graphite = 'autofailover.{{host}}.{{cluster}}.rtt.{{instance_name}}',
		labels = { 'host', 'cluster', 'instance_name' },
	},

	autofailover_instance_not_connected = {
		type = 'time',
		graphite = 'autofailover.{{host}}.{{cluster}}.not_connected.{{instance_name}}',
		labels = { 'host', 'cluster', 'instance_name' },
	},

	alert_quota_used = {
		type = 'time',
		graphite = 'autofailover.{{host}}.{{cluster}}.alert.alert_quota_used.{{shard}}.{{instance}}',
		labels = { 'host', 'cluster', 'shard', 'instance' },
		value_from = 'quota_used_ratio',
	},

	alert_shard_split_brain = {
		type = 'time',
		graphite = 'autofailover.{{host}}.{{cluster}}.alert.shard_in_split_brain.{{shard}}',
		labels = { 'host', 'cluster', 'shard' },
	},

	alert_master_is_down = {
		type = 'time',
		graphite = 'autofailover.{{host}}.{{cluster}}.alert.master_is_down.{{shard}}.{{instance}}',
		labels = { 'host', 'cluster', 'shard', 'instance' },
	},

	alert_master_is_ro = {
		type = 'time',
		graphite = 'autofailover.{{host}}.{{cluster}}.alert.master_is_ro.{{shard}}.{{instance}}',
		labels = { 'host', 'cluster', 'shard', 'instance' },
	},

	alert_replica_is_down = {
		type = 'time',
		graphite = 'autofailover.{{host}}.{{cluster}}.alert.replica_is_down.{{shard}}.{{instance}}',
		labels = { 'host', 'cluster', 'shard', 'instance' },
	},

	alert_not_connected_to_master = {
		type = 'time',
		graphite = 'autofailover.{{host}}.{{cluster}}.alert.not_connected_to_master.{{shard}}.{{instance}}',
		labels = { 'host', 'cluster', 'shard', 'instance' },
	},

	alert_noone_connected_to_master = {
		type = 'time',
		graphite = 'autofailover.{{host}}.{{cluster}}.alert.noone_connected_to_master.{{shard}}.{{instance}}',
		labels = { 'host', 'cluster', 'shard', 'instance' },
	},

	alert_wal_cleanup_is_paused = {
		type = 'time',
		graphite = 'autofailover.{{host}}.{{cluster}}.alert.wal_cleanup_is_paused.{{shard}}.{{instance}}',
		labels = { 'host', 'cluster', 'shard', 'instance' },
	},

	alert_replica_not_connected_to_replica = {
		type = 'time',
		graphite = 'autofailover.{{host}}.{{cluster}}.alert.replica_not_connected_to_replica.{{shard}}.{{instance}}',
		labels = { 'host', 'cluster', 'shard', 'instance' },
	},

	alert_ntp_problems = {
		type = 'time',
		graphite = 'autofailover.{{host}}.{{cluster}}.alert.ntp_problems.{{shard}}.{{instance}}',
		labels = { 'host', 'cluster', 'shard', 'instance' },
	},
}

---@class SwitchoverStatsShardObservation
---@field name string
---@field replicaset SwitchoverReplicasetShadowInfo
---@field etcd_shard Cluster

---@class SwitchoverStatsShardObserver
---@field observe_shard fun(SwitchoverStatsShardObservation)

---Creates stats
---@param graphite? SwitchoverGraphite
---@param vars? table<string, string> default vars
---@return SwitchoverStats|SwitchoverStatsShardObserver
function M.new(graphite, vars)
	if type(vars) ~= 'table' then
		vars = {}
	end
	local collectors = {}
	for metric, options in pairs(stats) do
		local metric_type = options.type
		local labels = options.labels
		local template = options.graphite
		local value_from = options.value_from
		collectors[metric] = function(value, args)
			local now = math.ceil(fiber.time())
			args = args or {}
			if type(args) ~= 'table' then
				return
			end

			if value_from then
				value = tonumber(args[value_from])
				if not value then
					return
				end
			end

			if not value then
				if metric_type == 'time' then
					value = fiber.time()
				else
					return
				end
			end

			local tags = {}
			for _, lbl in ipairs(labels) do
				tags[lbl] = args[lbl] or vars[lbl]
				if not tags[lbl] then
					return
				end
			end

			local metric_name = template:gsub('{{([^{}]+)}}', tags)
			if graphite then
				graphite:send(metric_name, value, now)
			end
		end
	end

	---@param observation SwitchoverStatsShardObservation
	local function observe_shard (observation)
		local shard_name = observation.name

		for _, replica in pairs(observation.replicaset.replicas) do
			local ei = observation.etcd_shard:instance_by_uuid(replica:uuid())
			local inst_vars = { shard_name = shard_name, instance_name = ei and ei.name or replica._name }
			if replica.connected then
				collectors.autofailover_instance_info_uptime(replica:info().uptime, inst_vars)
				collectors.autofailover_instance_info_lsn(replica:info().lsn, inst_vars)
				if replica.rtt then
					collectors.autofailover_instance_rtt(replica.rtt, inst_vars)
				end
				local is_ro = replica:ro() and 0 or 1
				collectors.autofailover_instance_info_rw(is_ro, inst_vars)
				local election = replica:election()
				if type(election) == 'table' then
					collectors.autofailover_instance_info_election_leader(election.leader, inst_vars)
					collectors.autofailover_instance_info_election_term(election.term, inst_vars)
					collectors.autofailover_instance_info_election_vote(election.vote, inst_vars)
				end
			end
		end

		local repl = observation.replicaset
		for _, ei in ipairs(observation.etcd_shard:instances()) do
			if not repl.replicas[ei.instance_uuid] then
				collectors.autofailover_instance_not_connected(fiber.time(),
					{ shard_name = shard_name, instance_name = ei.name })
			end
		end
	end

	---@param alerts SwitchoverAlert[]
	local function observe_cluster_alerts(alerts)
		for _, alert in ipairs(alerts) do
			local func = collectors["alert_"..(alert.alert_code or "")]
			if func then
				func(fiber.time(), alert)
			end
		end
	end

	collectors.observe_shard = function(observation)
		local ok, err = pcall(observe_shard, observation)
		if not ok then
			log.error("failed: %s", err)
		end
	end
	collectors.observe_cluster_alerts = function (alerts)
		local ok, err = pcall(observe_cluster_alerts, alerts)
		if not ok then
			log.error("failed: %s", err)
		end
	end

	return collectors
end

return M
