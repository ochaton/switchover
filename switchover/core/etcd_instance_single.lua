local v = require 'switchover._validator'

return v.object { required = { 'instances', 'common' }, allow_undefined = false }
	:fields {
		common = v.object { required = { 'box' } }
			:fields {
				box = v.object {}:fields {
					log_level = v.oneof { v.string(), v.number('+cast') },
					listen = v.oneof { v.string(), v.number('+cast') },
				},
			},
		instances = v.object { allow_undefined = false }
			:match {
				["^[a-zA-Z][a-zA-Z0-9_]*$"] = v.object {}
					:fields {
						app = v.object {},
						box = v.object {}
							:fields {
								instance_uuid = v.string('+cast'),
								listen = v.string('+cast'),
								remote_addr = v.string('+cast'),
							}
					}
					:must {
						['must not include field cluster'] = function(t)
							return type(t.cluster) == 'nil'
						end,
					}
			}
	}
