local log = require 'log'
local fun = require 'fun'
local uri = require 'uri'
local e = require 'switchover._error'
local S = require 'switchover._selector'

local function human_uptime(ts)
	local s = {}
	local orig = math.abs(ts)
	if ts > 86400 then
		local days = math.floor(ts/86400)
		ts = ts - days*86400
		table.insert(s, ("%dd"):format(days))
	end
	if ts > 3600 then
		local hours = math.floor(ts/3600)
		ts = ts - hours*3600
		table.insert(s, ("%dh"):format(hours))
	end
	if ts > 60 then
		local mins = math.floor(ts/60)
		ts = ts - mins*60
		table.insert(s, ("%dm"):format(mins))
	end
	if ts >= 1 then
		table.insert(s, ("%ds"):format(ts))
	end
	if orig < 1 then
		if orig < 0.001 then
			table.insert(s, ("%0.1fms"):format(orig*1000))
		else
			table.insert(s, ("%0.fms"):format(orig*1000))
		end
	elseif ts < 1 then -- orig >= 1
		table.insert(s, "00s")
	end

	return table.concat(s, "")
end

---@alias VShardAlertCode string
---@alias VShardAlertMessage string
---@alias VShardAlert { [1]: VShardAlertCode, [2]: VShardAlertMessage }

---@enum VShardStatus
local VShardStatus = {
	GREEN = 0,
	YELLOW = 1,
	ORANGE = 2,
	RED = 3,
}

---@type table<number, VShardStatus>
local VShardStatusStr = fun.iter(VShardStatus)
	:map(function(k, v) return v, k end)
	:tomap()

---@class SwitchoverAlertsShardInstaceInfo
---@field shard_name string Name of the shard derived from topology
---@field etcd_shard Shard ETCD information about given shard
---@field srv_addr string? ipv4 of fqdn of server addr (undefined only when endpoint is not valid uri)
---@field srv_addr_kind "ipv4"|"fqdn"? kind of srv_addr
---@field etcd_master_name string? Master name registered in ETCD if matched
---@field etcd_instance ExtendedInstanceInfo ETCD instance extended info
---@field name string Name of the instance derived from ETCD and matched
---@field endpoint string Endpoint for connection, usually instance/box/listen or instance/box/remote_addr
---@field etcd_role "master"|"replica" Role of the instance as it's set in ETCD
---@field etcd_is_master boolean is true when etcd_role == master
---@field is_router boolean flag for routers if instance/router:true is set in ETCD
---@field is_connected boolean flag if connection to Tarantool was successfully established b
---@field fqdn string? Reverse DNS record for box/cfg/listen for ipv4 or bind_address derived from endpoint
---@field masked_endpoint string masked endpoint (without password) to Tarantool
---@field real_instance Tarantool? Tarantool object
---@field role ("R"|"M"|"R/M"|"M/R")? Real role of Tarantool R - Replica, M - Master, R/M - Replica but should be master, M/R - Master but should be replica
---@field status (BoxInfoStatusRunning | BoxInfoStatusLoading | BoxInfoStatusOrphan | BoxInfoStatusHotStandBy)? just box.info.status of connected tarantool
---@field uptime string? box.info.uptime converted to human readable string
---@field id integer? box.info.id derived from Tarantool instance
---@field wall_clock number? clock.time() derived from Tarantool instance
---@field box_cfg BoxCfg? box.cfg derived from Tarantool instance
---@field box_info BoxInfo? box.info derived from Tarantool instance
---@field ipv4 string? Tarantool ipv4 if discovered
---@field is_ro boolean? true if box.info.ro is set to true
---@field is_cfg_ro boolean? true if box.cfg.read_only set to true
---@field election_mode election_mode? Election mode derived from Tarantool instance
---@field lsn number? box.info.lsn derived from Tarantool instance
---@field version string? box.info.version derived from Tarantool instance
---@field vclock table<number,number>? box.info.vclock derived from Tarantool instance
---@field election BoxInfoElection? box.info.election derived from Tarantool instance
---@field etcd_config table? config.get('etcd') derived from Tarantool instance
---@field n_upstreams number? number of running upstreams derived from Tarantool instance
---@field n_downstreams number? number of running downstreams derived from Tarantool instance
---@field slab_info table? box.slab.info() derived from Tarantool instance
---@field stat table? box.stat() and box.stat.net() derived from Tarantool instance
---@field n_fibers number? #fiber.info() derived from Tarantool instance
---@field fiber_info FiberInfo? fiber.info() derived from Tarantool instance (only when requested)
---@field metrics_export table? exported metrics values derived from Tarantool instance (only when requested)
---@field bucket_count number? box.space._buclet:len() derived from Tarantool instance
---@field quota_size number? value of box.slab.info().quota_size derived from Tarantool instance
---@field quota_used_ratio number? value of box.slab.info().quota_used_ratio derived from Tarantool instance
---@field master_upstream string? calculated status of connection to registered master in ETCD
---@field replication_short string? short replication info to Master discovered in ETCD
---@field replication_from_master boolean? true when replication is running from discovered master in ETCD
---@field lag number? replication lag to master derived from Tarantool instance
---@field idle number? replication idle to master derived from Tarantool instance
---@field quota_alert number?
---@field spaces {name: string, len: number, bsize:number}[] verbose statisics of the user spaces (id>=512) derived from Tarantool instance
---@field runtime_info { lua: number, used: number, maxalloc: number } box.runtime.info() derived from Tarantool instance
---@field vshard_alerts VShardAlert[] list of vshard alerts derived from Tarantool instance
---@field vshard_storage_status VShardStatus vshard.storage.info().status
---@field vshard_router_status VShardStatus vshard.router.info().status
---@field print(SwitchoverAlertsShardInstaceInfo): string
---@field has_enabled_raft boolean true when in replicaset RAFT is enabled

---Print status line for instances
---@param inst SwitchoverAlertsShardInstaceInfo
---@return string
local function status_line(inst)
	if not inst.is_connected then
		local msg = ("%s\t%s (%s)\tetcd:%0-7s - not connected"):format(
			inst.name, inst.endpoint, inst.fqdn, inst.etcd_is_master and 'master' or 'replica'
		)
		return msg
	end

	return ("%s  %-18s (%s)  %sv:%s  etcd:%0-7s  role:%0-7s  status:%s  uptime:%s%s  %s↓:%d ↑:%d  %s"):format(
		inst.name, inst.masked_endpoint, inst.fqdn,
		(inst.etcd_config.dc and inst.etcd_config.dc..'  ' or ''),
		inst.version, inst.etcd_role,
		inst.is_ro and 'replica' or 'master',
		inst.box_info.status,
		inst.uptime,
		(inst.box_info.gc.checkpoint_is_in_progress and "  snap:true" or ""),
		(inst.has_enabled_raft and (
			("raft:%s/%s  "):format(inst.election.state, inst.election_mode)
		) or ""),
		inst.n_upstreams, inst.n_downstreams,
		inst.master_upstream
	)
end

---Method merges information from ETCD and real Tarantool instance and returns complex Object
---@param shard_name string
---@param ei ExtendedInstanceInfo
---@param info_map SwitchoverAlertsShardInfo
---@return SwitchoverAlertsShardInstaceInfo
local function verbose_instance_info(shard_name, ei, info_map)
	local repl = info_map.repl
	local ri = repl.replicas[ei.instance_uuid]
	local instance_info = {
		shard_name = shard_name,
		etcd_shard = info_map.etcd_shard,
		etcd_master_name = info_map.etcd_master_name,
		etcd_instance = ei,
		name = ei.name,
		ipv4 = uri.parse(ei.endpoint).ipv4,
		fqdn = uri.parse(ei.endpoint).host,
		endpoint = ei.endpoint,
		etcd_role = ei.role,
		etcd_is_master = ei.role == 'master',
		is_connected = ri and ri.connected or false,
		is_router = not ei.cluster,
		has_enabled_raft = repl:has_enabled_raft(),
		print = status_line,
	}

	if instance_info.ipv4 then
		instance_info.srv_addr = instance_info.ipv4
		instance_info.srv_addr_kind = 'ipv4'
	elseif instance_info.fqdn then
		instance_info.srv_addr = instance_info.fqdn
		instance_info.srv_addr_kind = 'fqdn'
	end

	if not ri then return instance_info end

	instance_info.fqdn = ri.fqdn or instance_info.fqdn
	instance_info.ipv4 = ri.ipv4 or instance_info.ipv4
	instance_info.masked_endpoint = ri.masked_endpoint

	if instance_info.ipv4 then
		instance_info.srv_addr = instance_info.ipv4
		instance_info.srv_addr_kind = 'ipv4'
	end

	instance_info.real_instance = ri
	if ri:cfg().read_only == ri:info().ro then
		instance_info.role = ri:ro() and 'R' or 'M'
	else
		local need = ri:cfg().read_only and 'R' or 'M'
		local actl = ri:ro() and 'R' or 'M'
		instance_info.role = need..'/'..actl
	end

	instance_info.uptime = human_uptime(ri:info().uptime)
	instance_info.id = ri:id()

	instance_info.wall_clock = ri.wall_clock
	instance_info.box_cfg = ri:cfg()
	instance_info.box_info = ri:info()
	instance_info.is_ro = ri:ro()
	instance_info.is_cfg_ro = ri:cfg().read_only
	instance_info.election_mode = ri:cfg().election_mode
	instance_info.lsn = ri:info().lsn

	instance_info.version  = ri:version()
	instance_info.vclock   = ri:jvclock()
	instance_info.election = ri:election()

	instance_info.etcd_config = ri:etcd()
	instance_info.n_upstreams = #ri:followed_upstreams()
	instance_info.n_downstreams = #ri:followed_downstreams()
	instance_info.slab_info = ri.slab_info
	instance_info.stat = ri.stat_info
	instance_info.n_fibers = ri.n_fibers
	instance_info.fiber_info = ri.fiber_info ~= nil and ri.fiber_info or {}
	instance_info.metrics_export = fun.iter(ri.metrics_export ~= nil and ri.metrics_export or {})
		:map(function(n) return n.metric_name, n end)
		:tomap()
	instance_info.bucket_count = ri.bucket_count
	instance_info.vshard_alerts = {}
	if type(ri.vshard_alerts) == 'table' then
		if type(ri.vshard_alerts.storage) == 'table' then
			for _, v in ipairs(ri.vshard_alerts.storage) do
				table.insert(instance_info.vshard_alerts, v)
			end
		end
		if type(ri.vshard_alerts.router) == 'table' then
			for _, v in ipairs(ri.vshard_alerts.router) do
				table.insert(instance_info.vshard_alerts, v)
			end
		end
		if ri.vshard_alerts.storage_status then
			instance_info.vshard_storage_status = ri.vshard_alerts.storage_status
		end
		if ri.vshard_alerts.router_status then
			instance_info.vshard_router_status = ri.vshard_alerts.router_status
		end
	end
	instance_info.spaces = ri.spaces
	instance_info.runtime_info = ri.runtime_info
	instance_info.slab_stats = ri.slab_stats or {}

	instance_info.quota_size = ri.slab_info.quota_size
	instance_info.quota_used_ratio = tonumber(ri.slab_info.quota_used_ratio:gsub("%%", ""), 10)

	local mu = ri:upstream(info_map.etcd_master_uuid)
	if ri:uuid() == info_map.etcd_master_uuid then
		instance_info.master_upstream = 'self'
		instance_info.replication_short = 'S'
		instance_info.replication_from_master = true
	elseif not mu then
		instance_info.master_upstream = 'not-connected-to-master'
		instance_info.replication_short = 'not-connected'
		instance_info.replication_from_master = false
	else
		if mu.upstream.status == 'follow' then
			instance_info.lag = mu.upstream.lag
			instance_info.replication_from_master = true
		else
			instance_info.replication_from_master = false
			instance_info.lag = mu.upstream.idle
		end
		instance_info.replication_short = ('%s/%s'):format(
			mu.upstream.status == 'follow' and 'ok' or mu.upstream.status, human_uptime(instance_info.lag)
		)
		instance_info.master_upstream = ("%s/%s:%s/%.3fs%s"):format(
			mu.upstream.status == "follow" and "ok" or "fail",

			info_map.etcd_master_name, mu.upstream.status,

			(mu.upstream.status == "follow" or mu.upstream.status == "sync")
				and mu.upstream.lag or mu.upstream.idle,
			mu.upstream.status ~= 'follow' and (" err:"..(mu.upstream.message or "unknown")) or ""
		)
	end

	return instance_info
end

---@generic T
---@param lst1 T[]
---@param lst2 T[]
---@return T[]
local function append(lst1, lst2)
	for _, v in ipairs(lst2) do
		table.insert(lst1, v)
	end
	return lst1
end


local M = {}
M.__index = M

---@class SwitchoverAlertsShardInfo
---@field selected_instances SwitchoverAlertsShardInstaceInfo[]
---@field repl SwitchoverReplicaset
---@field etcd_master_name string?
---@field etcd_master_uuid string?
---@field etcd_shard Cluster
---@field real_masters string[]
---@field n_connected number
---@field instances SwitchoverAlertsShardInstaceInfo[]


---@class SwitchoverAlert
---@field shard string
---@field level number
---@field alert_code string
---@field instance? string
---@field message string

---Function build_alerts rewraps DiscoveredShard into SwitchoverAlertsShardInfo and evaluates many alerts
---@param shards table<string,DiscoveredShard>
---@param selector? fun(Iterator): Iterator
---@return table<string,SwitchoverAlertsShardInfo>, SwitchoverAlert[], SwitchoverAlertsShardInstaceInfo[]
local function build_alerts(shards, selector)
	---@type table<string,SwitchoverAlertsShardInfo>
	local smap = fun.iter(shards)
		---@param shard_name string
		---@param msg DiscoveredShard
		---@return string,SwitchoverAlertsShardInfo
		:map(function(shard_name, msg)
			local repl = msg.replicaset
			local etcd_shard = msg.shard
			local etcd_master
			local etcd_master_name
			local etcd_master_uuid
			if etcd_shard:has_master() then
				etcd_master, etcd_master_name = etcd_shard:master()
				etcd_master_uuid = etcd_master.box.instance_uuid
			else
				log.warn("shard %s does not have master in etcd", etcd_shard.name)
			end

			---@type SwitchoverAlertsShardInfo
			local rv = {
				repl = repl,
				etcd_master_name = etcd_master_name,
				etcd_master_uuid = etcd_master_uuid,
				etcd_shard = etcd_shard,
				real_masters = fun.iter(repl:masters())
					:map(function(m)
						local inst = etcd_shard:instance_by_uuid(m:uuid())
						if inst then return inst.name end
					end)
					:grep(fun.op.truth)
					:totable(),
				n_connected = #repl.replica_list,
				instances = {},
			}

			rv.instances = fun.iter(etcd_shard:instances())
				:map(function(etcd_instance)
					return verbose_instance_info(shard_name, etcd_instance, rv)
				end)
				:totable()

			if selector then
				rv.selected_instances = selector(rv.instances):totable()
			else
				rv.selected_instances = rv.instances
			end

			return shard_name, rv
		end)
		:tomap()

	local quota_quantilies = { {100, 1}, {95, 2}, {90, 3}, {80, 4} }

	local alerts = {
		no_master = fun.iter(smap)
			:grep(function(_, map)
				return #map.real_masters == 0
			end)
			:map(function(sname, _)
				return { shard = sname, level = 1, message = ("no master found in %s"):format(sname) }
			end)
			:totable(),

		shard_split_brain = fun.iter(smap)
			---@param _ string
			---@param map SwitchoverAlertsShardInfo
			---@return boolean
			:grep(function(_, map)
				return #map.real_masters > 1
			end)
			---@param sname string
			---@param map SwitchoverAlertsShardInfo
			---@return SwitchoverAlert
			:map(function(sname, map)
				return {
					shard = sname,
					level = 1,
					message = ("shard in split brain %s (should be %s) got %s"):format(
						sname,
						map.etcd_master_name,
						table.concat(map.real_masters, ",")
					)
				}
			end)
			:totable(),

		not_connected = fun.iter(smap)
			---@param _ string
			---@param map SwitchoverAlertsShardInfo
			---@return SwitchoverAlertsShardInstaceInfo[]
			:map(function(_, map)
				local rv = {}
				for _, inst in ipairs(map.instances) do
					if not inst.is_connected then
						table.insert(rv, inst)
					end
				end
				return rv
			end)
			---@param rv table
			---@param list SwitchoverAlertsShardInstaceInfo[]
			---@return SwitchoverAlert[]
			:reduce(function(rv, list)
				for _, inst in ipairs(list) do
					table.insert(rv, {
						shard = inst.shard_name,
						instance = inst.name,
						level = 3,
						message = ("connection to %s@%s:%s is not established"):format(
							inst.shard_name, inst.name, inst.etcd_role),
					})
				end
				return rv
			end, {}),

		quota_used = fun.iter(smap)
			---@param _ string
			---@param map SwitchoverAlertsShardInfo
			---@return SwitchoverAlertsShardInstaceInfo[]
			:map(function(_, map)
				return fun.iter(map.instances)
					:grep(function(inst) return inst.quota_used_ratio end)
					:grep(function(inst)
						local quota = fun.iter(quota_quantilies)
							:grep(function(q) return inst.quota_used_ratio > q[1] end)
							:nth(1)

						if quota then
							inst.quota_alert = quota
							return inst
						end
						return false
					end)
					:totable()
			end)
			---@param rv table
			---@param list SwitchoverAlertsShardInstaceInfo[]
			---@return SwitchoverAlert[]
			:reduce(function(rv, list)
				for _, inst in ipairs(list) do
					table.insert(rv, {
						shard = inst.shard_name,
						instance = inst.name,
						level = inst.quota_alert[2],
						quota_used_ratio = tostring(inst.quota_used_ratio):gsub("%%", ""),
						message = ("%s@%s Quota used is above %s%%: %s%% (%.2fGB)"):format(
							inst.shard_name, inst.name, inst.quota_alert[1], inst.quota_used_ratio,
							inst.quota_size / 2^30
						)
					})
				end
				return rv
			end, {}),

		master_is_down = fun.iter(smap)
			---@param _ string
			---@param map SwitchoverAlertsShardInfo
			---@return SwitchoverAlert[]
			:map(function(_, map)
				local rv = {}
				for _, inst in ipairs(map.instances) do
					if inst.etcd_is_master and not inst.is_router and not inst.is_connected then
						table.insert(rv, {
							shard = inst.shard_name,
							instance = inst.name,
							level = 1,
							message = ('%s@%s Master is not discovered'):format(
								inst.shard_name, inst.name
							),
						})
					end
				end
				return rv
			end)
			:reduce(append, {}),

		master_is_ro = fun.iter(smap)
			---@param _ string
			---@param map SwitchoverAlertsShardInfo
			---@return SwitchoverAlert[]
			:map(function(_, map)
				local rv = {}
				for _, inst in ipairs(map.instances) do
					if inst.etcd_is_master and inst.is_connected and inst.is_ro then
						table.insert(rv, {
							shard = inst.shard_name,
							instance = inst.name,
							level = 2,
							message = ('%s@%s Master discovered but ro (status=%s, cfg.read_only=%s, rcq=%s)'):format(
								inst.shard_name, inst.name, inst.status, inst.is_cfg_ro, inst.box_cfg.replication_connect_quorum or #(inst.box_cfg.replication or {})
							),
						})
					end
				end
				return rv
			end)
			:reduce(append, {}),

		replica_is_down = fun.iter(smap)
			---@param _ string
			---@param map SwitchoverAlertsShardInfo
			---@return SwitchoverAlert[]
			:map(function(_, map)
				local rv = {}
				for _, inst in ipairs(map.instances) do
					if not inst.etcd_is_master and not inst.is_connected then
						table.insert(rv, {
							shard = inst.shard_name,
							instance = inst.name,
							level = 3,
							message = ('%s@%s replica is not discovered'):format(
								inst.shard_name, inst.name
							),
						})
					end
				end
				return rv
			end)
			:reduce(append, {}),

		not_connected_to_master = fun.iter(smap)
			---@param _ string
			---@param map SwitchoverAlertsShardInfo
			---@return SwitchoverAlert[]
			:map(function(_, map)
				local rv = {}
				for _, inst in ipairs(map.instances) do
					if not inst.etcd_is_master and inst.is_connected and not inst.replication_from_master then
						table.insert(rv, {
							shard = inst.shard_name,
							instance = inst.name,
							level = 3,
							message = ('%s@%s replica is not connected to master %s (cfg.replication={%s}, %s)'):format(
								inst.shard_name, inst.name, inst.etcd_master_name, table.concat(inst.box_cfg.replication or {}, ','), inst.replication_short
							),
						})
					end
				end
				return rv
			end)
			:reduce(append, {}),

		noone_connected_to_master = fun.iter(smap)
			---@param _ string
			---@param map SwitchoverAlertsShardInfo
			:grep(function(_, map) return #map.real_masters == 1 and not map.etcd_shard:is_proxy() end)
			---@param sname string
			---@param map SwitchoverAlertsShardInfo
			---@return SwitchoverAlert?
			:map(function (sname, map)
				local n_live_replicas = fun.iter(map.instances)
					---@param inst SwitchoverAlertsShardInstaceInfo
					:grep(function(inst)
						return inst.is_connected
							and not inst.etcd_is_master
							and inst.replication_from_master
					end)
					:length()

				if n_live_replicas == 0 then
					return {
						shard = sname,
						level = 1,
						instance = map.etcd_master_name,
						message = ("%s does not have live replicas"):format(map.etcd_master_name)
					}
				end
			end)
			:grep(fun.op.truth)
			:totable(),

		wal_cleanup_is_paused = fun.iter(smap)
			:grep(function(_, map) return not map.etcd_shard:is_proxy() end)
			---@param sname string
			---@param map SwitchoverAlertsShardInfo
			---@return SwitchoverAlert[]
			:map(function(sname, map)
				return fun.iter(map.instances)
					---@param inst SwitchoverAlertsShardInstaceInfo
					:grep(function (inst)
						return inst.is_connected
							and inst.box_info.gc.is_paused
							and inst.box_info.uptime > (inst.box_cfg.wal_cleanup_delay or 14400)
					end)
					---@param inst SwitchoverAlertsShardInstaceInfo
					:map(function(inst)
						return {
							shard = sname,
							instance = inst.name,
							level = 3,
							message = ("%s@%s WAL GC is paused"):format(
								sname, inst.name
							)
						}
					end)
					:totable()
			end)
			:reduce(append, {}),

		wal_outdated_consumer = fun.iter(smap)
			---@param _ string
			---@param map SwitchoverAlertsShardInfo
			:grep(function(_, map) return not map.etcd_shard:is_proxy() end)
			---@param sname string
			---@param map SwitchoverAlertsShardInfo
			---@return SwitchoverAlert[]
			:map(function(sname, map)
				return fun.iter(map.instances)
					---@param inst SwitchoverAlertsShardInstaceInfo
					:grep(function(inst)
						return inst.is_connected
							and #(inst.box_info.gc.checkpoints or {}) > 1
					end)
					---@param inst SwitchoverAlertsShardInstaceInfo
					:map(function (inst)
						local gcinfo = inst.box_info.gc
						---@type integer
						local min_sig = fun.iter(gcinfo.checkpoints)
							:map(function(ch) return ch.signature end)
							:min()

						if inst.box_info.gc.signature >= min_sig then
							return
						end

						return fun.iter(gcinfo.consumers)
							:grep(function(cons) return cons.signature < min_sig end)
							:map(function(cons)
								if cons.name:find('replica', 1, true) then
									local uuid = cons.name:match('replica (.+)')
									local ei = map.etcd_shard:instance_by_uuid(uuid)
									if ei then
										return {
											shard = sname,
											instance = inst.name,
											level = 2,
											message = ("%s@%s outdated replica %s holds xlogs from removal"):format(
												sname, inst.name, ei.name
											)
										}
									else
										return {
											shard = sname,
											instance = inst.name,
											level = 2,
											message = ("%s@%s unknown outdated %s holds xlogs from removal"):format(
												sname, inst.name, cons.name
											)
										}
									end
								else
									return {
										shard = sname,
										instance = inst.name,
										level = 2,
										message = ("%s@%s unknown consumer %s holds xlogs from removal"):format(
											sname, cons.name, inst.name
										)
									}
								end
							end)
							:totable()

					end)
					:grep(fun.op.truth)
					:reduce(append, {})
			end)
			:reduce(append, {}),

		snapshot_in_progress = fun.iter(smap)
			:grep(function(_, map) return not map.etcd_shard:is_proxy() end)
			---@param sname string
			---@param map SwitchoverAlertsShardInfo
			---@return SwitchoverAlert[]
			:map(function(sname, map)
				return fun.iter(map.instances)
					---@param inst SwitchoverAlertsShardInstaceInfo
					:grep(function(inst)
						return inst.is_connected
							and inst.box_info.gc.checkpoint_is_in_progress
					end)
					---@param inst SwitchoverAlertsShardInstaceInfo
					---@return SwitchoverAlert
					:map(function(inst)
						return {
							shard = sname,
							instance = inst.name,
							level = 4,
							message = ("%s@%s is in box.snapshot()"):format(
								sname, inst.name
							)
						}
					end)
					:reduce(append, {})
			end)
			:reduce(append, {}),

		replica_not_connected_to_replica = fun.iter(smap)
			---@param _ string
			---@param map SwitchoverAlertsShardInfo
			:grep(function(_, map) return not map.etcd_shard:is_proxy() end)
			---@param sname string
			---@param map SwitchoverAlertsShardInfo
			---@return SwitchoverAlert[]
			:map(function(sname, map)
				local etcd_instances = map.etcd_shard:instances()
				return fun.iter(map.instances)
					:grep(function(inst) return inst.is_connected end)
					---@param inst SwitchoverAlertsShardInstaceInfo
					---@return SwitchoverAlert[]
					:map(function(inst)
						return fun.iter(etcd_instances)
							---@param ei ExtendedInstanceInfo
							:grep(function(ei)
								return ei.role ~= 'master'
									and not inst.real_instance:replicates_from(ei.instance_uuid)
									and ei.instance_uuid ~= inst.real_instance:uuid()
							end)
							---@param ei ExtendedInstanceInfo
							:map(function(ei)
								local up = inst.real_instance:upstream(ei.instance_uuid)
								return {
									shard = sname,
									instance = inst.name,
									level = 4,
									message = ("%s <- %s replication is down%s"):format(
										inst.name, ei.name,
										up and (" (%s %s idle:%s lag:%s)"):format(
											up.upstream.status, up.upstream.message,
											(up.upstream.idle and ("%.1fs"):format(up.upstream.idle) or "-"),
											(up.upstream.lag and ("%.1fs"):format(up.upstream.lag) or "-")
										) or ""
									)
								}
							end)
							:totable()
					end)
					:reduce(append, {})
			end)
			:reduce(append, {}),

		vshard_indicators = fun.iter(smap)
			---@param sname string
			---@param map SwitchoverAlertsShardInfo
			---@return SwitchoverAlert[]
			:map(function(sname, map)
				return fun.iter(map.instances)
					---@param inst SwitchoverAlertsShardInstaceInfo
					---@return boolean
					:grep(function(inst)
						if not inst.is_connected then return false end
						if not next(inst.vshard_alerts or {}) then return false end
						return true
					end)
					---@param inst SwitchoverAlertsShardInstaceInfo
					:grep(function (inst)
						return (inst.vshard_router_status or 0) > 0
							or (inst.vshard_storage_status or 0) > 0
					end)
					---@param inst SwitchoverAlertsShardInstaceInfo
					---@return SwitchoverAlert
					:map(function(inst)
						local msg = {}
						if inst.vshard_router_status and inst.vshard_router_status > 0 then
							table.insert(msg, ("router: %s"):format(VShardStatusStr[inst.vshard_router_status]))
						end
						if inst.vshard_storage_status and inst.vshard_storage_status > 0 then
							table.insert(msg, ("storage: %s"):format(VShardStatusStr[inst.vshard_storage_status]))
						end
						return {
							shard = sname,
							instance = inst.name,
							level = 5-math.max(inst.vshard_router_status or 0, inst.vshard_storage_status or 0),
							message = ('%s@%s vshard not green: %s (should be 0)'):format(
								inst.shard_name, inst.name, table.concat(msg, ";")
							),
						}
					end)
					:totable()
			end)
			:reduce(append, {}),

		vshard_alerts = fun.iter(smap)
			---@param sname string
			---@param map SwitchoverAlertsShardInfo
			---@return SwitchoverAlert[]
			:map(function(sname, map)
				return fun.iter(map.instances)
					---@param inst SwitchoverAlertsShardInstaceInfo
					:grep(function(inst)
						if not inst.is_connected then return false end
						if not next(inst.vshard_alerts or {}) then return false end
						return true
					end)
					---@param inst SwitchoverAlertsShardInstaceInfo
					---@return SwitchoverAlert[]
					:map(function(inst)
						return fun.iter(inst.vshard_alerts)
							---@param alrt VShardAlert
							---@return SwitchoverAlert
							:map(function(alrt)
								return {
									shard = sname,
									instance = inst.name,
									level = math.max(inst.vshard_router_status or 0, inst.vshard_storage_status or 0),
									message = ("%s@%s %s %s"):format(
										sname, inst.name,
										alrt[1], alrt[2]
									)
								}
							end)
							:totable()
					end)
					:reduce(append, {})
			end)
			:reduce(append, {}),

		vshard_rw_is_ro = fun.iter(smap)
			---@param sname string
			---@param map SwitchoverAlertsShardInfo
			---@return SwitchoverAlert[]
			:map(function(sname, map)
				return fun.iter(map.instances)
					---@param inst SwitchoverAlertsShardInstaceInfo
					---@return SwitchoverAlertsShardInstaceInfo
					:grep(function(inst)
						return inst.is_connected
							and inst.real_instance.has_vshard
							and inst.real_instance:vshard_storage_rw_but_ro()
					end)
					---@param inst SwitchoverAlertsShardInstaceInfo
					:map(function(inst)
						return {
							shard = sname,
							instance = inst.name,
							level = 1,
							message = ('%s@%s vshard.storage is missconfigured (NON_MASTER): please call switchover pr %s@%s'):format(
								inst.shard_name, inst.name, inst.etcd_shard.name, inst.name
							),
						}
					end)
					:totable()
			end)
			:reduce(append, {}),

		autofailover_no_candidate = fun.iter(smap)
			---@param map SwitchoverAlertsShardInfo
			:grep(function(_, map) return not map.etcd_shard:is_proxy() end)
			---@param sname string
			---@param map SwitchoverAlertsShardInfo
			---@return SwitchoverAlert[]
			:map(function(sname, map)
				---@type SwitchoverAlertsShardInstaceInfo[]
				local online_instances = fun.iter(map.instances)
					---@param inst SwitchoverAlertsShardInstaceInfo
					:grep(function(inst) return inst.is_connected end)
					:totable()

				local online_candidates = fun.iter(online_instances)
					---@param inst SwitchoverAlertsShardInstaceInfo
					:grep(function(inst)
						return inst.etcd_instance.autofailover.role ~= "witness"
					end)
					:totable()


				local online_witnesses = fun.iter(online_instances)
					---@param inst SwitchoverAlertsShardInstaceInfo
					:grep(function(inst)
						return inst.etcd_instance.autofailover.role == "witness"
					end)
					:totable()

				if #online_candidates > 0 then
					return
				end

				return {
					shard = sname,
					level = 2,
					message = ("no available candidates for autofailover (online: %d, witnesses: %d)"):format(
						#online_instances, #online_witnesses
					)
				}
			end)
			:grep(fun.op.truth)
			:totable(),

		ntp_problems = fun.iter(smap)
			---@param sname string
			---@param map SwitchoverAlertsShardInfo
			---@return SwitchoverAlert[]
			:map(function(sname, map)
				local rv = {}
				---@type number[]
				local wall_ts = fun.iter(map.instances)
					---@param inst SwitchoverAlertsShardInstaceInfo
					:grep(function(inst) return inst.is_connected end)
					---@param inst SwitchoverAlertsShardInstaceInfo
					---@return number
					:map(function(inst) return tonumber(inst.wall_clock) end)
					:grep(fun.op.truth)
					:totable()
				if #wall_ts == 0 then return rv end -- no alerts
				local min_ts = fun.min(wall_ts)
				local max_ts = fun.max(wall_ts)

				local threashold = 1.1

				if math.abs(max_ts - min_ts) < threashold then
					return rv -- ntp is synced
				end

				for _, inst in ipairs(map.instances) do
					local wall_clock = inst.is_connected and tonumber(inst.wall_clock)
					if wall_clock and (math.abs(wall_clock - min_ts) > threashold or math.abs(wall_clock - max_ts) > threashold) then
						local ts = wall_clock
						table.insert(rv, {
							shard = sname,
							instance = inst.name,
							level = 2,
							message = ('%s@%s server has ntp problems min:%s max:%s wall:%s/+%.2fs/-%.2fs'):format(
								inst.shard_name, inst.name,
								os.date("%FT%T", min_ts)..tostring(min_ts):gsub("^%d+", ""),
								os.date("%FT%T", max_ts)..tostring(max_ts):gsub("^%d+", ""),
								os.date("%FT%T", ts)..tostring(ts):gsub("^%d+", ""),
								ts - min_ts,
								max_ts - ts
							),
						})
					end
				end
				return rv
			end)
			:reduce(append, {}),
	}

	---@type SwitchoverAlert[]
	alerts = fun.iter(alerts)
		:map(function(alert_code, list)
			for _, item in ipairs(list) do
				item.alert_code = alert_code
			end
			return list
		end)
		:reduce(append, {})

	---@type SwitchoverAlertsShardInstaceInfo[]
	local instances = fun.iter(smap)
		:reduce(function(r, _, s)
			for _, inst in ipairs(s.selected_instances) do
				table.insert(r, inst)
			end
			return r
		end, {})

	return smap, alerts, instances
end

---@class SwitchoverResolveAndDiscoveryClusterArgs
---@field cluster_or_shard string name of the cluster or shard to discover from ETCD
---@field discovery_timeout number discovery_timeout in seconds which will be passed into Discovery mechanism
---@field fetch {fiber_info: boolean?, spaces: boolean?, slabs: boolean?, vshard_alerts: boolean?, metrics: boolean?} enablers which forces to fetch desired info from each instance
---@field selector string selector string derived from user input
---@field need_discovery boolean should discovery fiber be executed
---@field cluster boolean? (default) whether cluster_or_shard should be reduced to cluster
---@field shard boolean? whether cluster_or_shard should be reduced to shard

---Method resolves given string in ETCD as cluster and establishes connection to cluster
---Instead of DiscoveredShard it returns SwitchoverResolveAndDiscoveryInfo with alerts
---This method resolves selector and applies it to discovered shards after alerts evaluation
---@param args SwitchoverResolveAndDiscoveryClusterArgs
---@return SwitchoverResolveAndDiscoveryInfo
function M.cluster(args)
	local cluster_or_shard = assert(args.cluster_or_shard, "cluster or shard must be given")
	local what = 'cluster'
	if args.shard and not args.cluster then
		what = 'shard'
	end
	local how, cluster, shard = require 'switchover._resolve'({cluster_or_shard}, what)
	if how ~= 'etcd' then
		local repl = require 'switchover.discovery'.discovery {
			endpoints = cluster,
			discovery_timeout = args.discovery_timeout,
		}
		if repl:has_cartridge() then
			log.info("cartridge discovered. Getting cartridge topology")
			cluster = repl:cartridge_resolve()
		elseif repl:has_vshard() then
			log.info("vshard discovered. Getting vshard topology")
			cluster = repl:vshard_topology()
		else
			cluster = repl:auto_topology()
		end
	end
	if shard then
		cluster = shard
	end
	if not cluster then
		e.panic("Cluster cannot be discovered from %s", cluster_or_shard)
	end

	local shards = require 'switchover.discovery'.cluster {
		cluster = cluster,
		discovery_timeout = args.discovery_timeout,
		fetch = args.fetch,
		need_discovery = args.need_discovery,
	}

	local selector
	if args.selector then
		selector = S.parse(args.selector)
	end

	return M.refresh({
		shards = shards,
		cluster = cluster,
		fetch = args.fetch,
		selector = selector,
	})
end

M.build_alerts = build_alerts

---@class SwitchoverResolveAndDiscoveryInfo
---@field cluster_info table<string,SwitchoverAlertsShardInfo>
---@field instances SwitchoverAlertsShardInstaceInfo[]
---@field alerts SwitchoverAlert[]
---@field shards table<string,DiscoveredShard>
---@field refresh fun(SwitchoverResolveAndDiscoveryInfo): SwitchoverResolveAndDiscoveryInfo

---Recalls build_alerts on saved info.shards
---@param info { shards: table<string,DiscoveredShard>, selector?: fun(Iterator): Iterator }
---@return SwitchoverResolveAndDiscoveryInfo
function M.refresh(info)
	local cluster_map, alerts, instances = build_alerts(info.shards, info.selector)
	info.cluster_info = cluster_map
	info.instances = instances
	info.alerts = alerts

	return setmetatable(info, M)
end

---@class ResolveAndDiscoveryClusterShardOpts
---@field cluster boolean true when cluster should be discovered (prio 1)
---@field shard boolean true when shard should be discovered (prio 2)
---@field instance string name of the instance

---Resolves cluster or shard. Expects {cluster:true} or {shard:true} requested
---@param args ResolveAndDiscoveryClusterShardOpts
---@return Cluster
function M.cluster_or_shard(args)
	local what
	if args.cluster then
		what = 'cluster'
	elseif args.shard then
		what = 'shard'
	end
	local _, etcd_cluster, etcd_shard = require 'switchover._resolve'({args.instance}, what)
	if args.cluster then
		return etcd_cluster
	else
		return etcd_shard
	end
end


return M
