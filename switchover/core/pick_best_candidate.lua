local fun = require 'fun'
local log = require 'switchover._log'

local M = {}

local function diff_vclock(v1, v2)
	local diffv = {}
	local diff_sign = 0
	local maxid = 0
	for id in pairs(v1) do
		if maxid < id then maxid = id end
	end
	for id in pairs(v2) do
		if maxid < id then maxid = id end
	end
	for id = 1, maxid do
		diffv[id] = math.abs((v1[id] or 0) - (v2[id] or 0))
		diff_sign = diff_sign + diffv[id]
	end
	return diffv, diff_sign
end


---@param repl SwitchoverReplicaset
---@param tt Tarantool
---@param max_vclock table
---@param failover_priority number
---@return CandidateInfo
local function build_candidate_info(repl, tt, max_vclock, failover_priority)
	local diffv, diff_sign = diff_vclock(max_vclock, tt:vclock())

	local max_achievable_vclock = repl:max_achievable_vclock(tt)
	local _, mdsign = diff_vclock(max_vclock, max_achievable_vclock)

	return {
		replica = tt,
		n_downstreams = #tt:followed_downstreams(),
		score = repl:stability_score(tt),
		vclock = tt:vclock(),
		dsign = diff_sign,
		dvclock = diffv,
		max_achievable_vclock = max_achievable_vclock,
		mdsign = mdsign,
		failover_priority = failover_priority,
		-- reload will freeze (0 - no, 1 - yes)
		rwf_num = tt:reload_will_freeze() and 1 or 0,
	}
end

---Picks best candidate for switch
---@param inst SwitchoverAlertsShardInstaceInfo
---@param shard_info SwitchoverAlertsShardInfo
---@param ignore SwitchoverAlertsShardInstaceInfo[]
---@return SwitchoverAlertsShardInstaceInfo|false, string? error_message
function M.pick(inst, shard_info, ignore)
	if not inst.is_connected then
		-- Do not work yet when instance is not connected
		return false, "Instance is not connected"
	end

	if inst.etcd_shard.name ~= shard_info.etcd_shard.name then
		return false, ("Instance %s is in shard %s but discovered shard is %s"):format(
			inst.name, inst.etcd_shard, shard_info.etcd_shard.name)
	end

	if not shard_info.repl.replicas[inst.real_instance:uuid()] then
		return false, ("Instance %s (with uuid=%s) is not registered in connected replicaset %s"):format(
			inst.name, inst.real_instance:uuid(), shard_info.repl.uuid
		)
	end

	if #shard_info.repl:masters() > 1 then
		return false, ("Can't resign instance %s because shard %s has more than 1 master: (%s)"):format(
			inst.name, shard_info.etcd_shard.name, table.concat(
				fun.iter(shard_info.repl:masters())
					---@param tt Tarantool
					:map(function(tt) return tt:name() end)
					:totable(),
				","
			)
		)
	end

	local dwns = {}
	for _, dwn in ipairs(inst.real_instance:followed_downstreams()) do
		local tt = shard_info.repl.replicas[dwn.uuid]
		if tt and tt:replicates_from(inst.real_instance:uuid()) then
			table.insert(dwns, tt)
		end
	end

	if #dwns == 0 then
		return false, ("Noone in shard %s replicates data from %s"):format(shard_info.etcd_shard.name, inst.name)
	end

	local uuid = inst.real_instance:vclock()
	local instance_autofailover_role = "candidate"
	local instance_autofailover_priority = 1

	local max_vclock = shard_info.repl:max_vclock()

	---@type table<string,true>
	local ignore_map = {}
	for _, ignore_inst in ipairs(ignore) do
		if ignore_inst.is_connected then
			ignore_map[ignore_inst.real_instance:uuid()] = true
		else
			ignore_map[ignore_inst.etcd_instance.instance_uuid] = true
		end
		log.verbose("ignore %s as resign destination (matches selector)", ignore_inst.name)
	end

	---@type CandidateInfo[]
	local switch_order = {}
	for _, tt in ipairs(dwns) do
		local ei = shard_info.etcd_shard:instance_by_uuid(tt:uuid())
		if ei then
			instance_autofailover_role = ei.autofailover.role
			instance_autofailover_priority = ei.autofailover.priority or 1
		end

		if ({orphan=true,running=true})[tt:status()]
			and tt:uuid() ~= uuid
			and instance_autofailover_role ~= "witness"
			and not ignore_map[tt:uuid()]
		then
			table.insert(
				switch_order,
				build_candidate_info(
					shard_info.repl,
					tt,
					max_vclock,
					instance_autofailover_priority
				)
			)
		end
	end

	-- if ndws > RF then fastest
	-- otherwise take max_achievable_vclock
	table.sort(switch_order, function(a, b)
		if a.score == b.score then
			if a.dsign == b.dsign then
				-- if diff signature is equal use failover priority
				return a.failover_priority > b.failover_priority
			end
			-- choose with lowest diff signature
			return a.dsign < b.dsign
		end

		-- if reload will freeze, choose the one where it will not
		-- if possible
		if a.rwf_num ~= b.rwf_num then
			return a.rwf_num < b.rwf_num
		end
		-- choose with most downstreams
		return a.score > b.score
	end)

	if #switch_order == 0 then
		return false, ("No instance is suitable in shard %s to switch instance %s"):format(
			shard_info.etcd_shard.name, inst.name
		)
	end

	local candidate = switch_order[1]

	for _, si in ipairs(shard_info.instances) do
		if si.real_instance:uuid() == candidate.replica:uuid() then
			return si
		end
	end

	return false, ("Something bad happened: failed to fetch candidate info of %s in local ETCD copy"):format(
		candidate.replica:uuid()
	)
end

return M