local v = require 'switchover._validator'

return v.object { required = { 'clusters', 'instances', 'common' } }
	:fields {
		common = v.object { required = { 'box' } }
			:fields {
				box = v.object {}:fields {
					log_level = v.oneof { v.string(), v.number('+cast') },
					listen = v.oneof { v.string(), v.number('+cast') },
				},
			},
		clusters = v.object { allow_undefined = false }
			:match {
				["^[a-zA-Z][a-zA-Z0-9_]*$"] = v.object { required = { 'master' } }
					:fields {
						master = v.string('+cast'),
						replicaset_uuid = v.string('+cast'),
						box = v.object {},
						app = v.object {},
					}
					:must {
						---@param t table
						---@param ctx CheckContext
						---@return boolean
						['master must be defined in instances and linked to cluster'] = function(t, ctx)
							return type(t.master) == 'string'
								and type(ctx.init.instances) == 'table'
								and type(ctx.init.instances[t.master]) == 'table'
								and type(ctx.init.instances[t.master].cluster) == 'string'
								and type(ctx.init.clusters) == 'table'
								and type(ctx.init.clusters[ctx.init.instances[t.master].cluster]) == 'table'
								and t == ctx.init.clusters[ctx.init.instances[t.master].cluster]
						end,
					},
			},
		instances = v.object { allow_undefined = false }
			:match {
				["^[a-zA-Z][a-zA-Z0-9_]*$"] = v.object {}
					:fields {
						cluster = v.string('+cast'),
						router = v.boolean('+cast'),
						app = v.object {},
						box = v.object {}
							:fields {
								instance_uuid = v.string('+cast'),
								listen = v.string('+cast'),
								remote_addr = v.string('+cast'),
							}
					}
					:must {
						['instance name must contain shard name'] = function(t, ctx)
							-- skip for routers
							if type(t.cluster) == 'nil' then return true end
							-- Name of the shard must be a prefix of instance
							return ctx:key():sub(1, #t.cluster) == t.cluster
						end,

						['cluster or router must be given (mutually exclusive)'] = function(t)
							return (t.cluster and not t.router) or (t.router and not t.cluster)
						end,
						['shard must be defined for instance'] = function(t, ctx)
							-- skip for routers
							if type(t.cluster) == 'nil' then return true end

							return type(t.cluster) == 'string'
								and type(ctx.init.clusters) == 'table'
								and type(ctx.init.clusters[t.cluster]) == 'table'
						end,
					}
			}
	}
