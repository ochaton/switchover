local fiber = require 'fiber'
local termui = require 'switchover._termui'
local term = require 'switchover._plterm'
termui:init({ tabs = 2, debug = true })

local help_wdg = termui:new_widget(0, 0)
termui:add(help_wdg, 2)

function help_wdg:on_init()
	self:parent().on_init(self)
	self.window.keyboard:on({ 'q', 'Q' }, function()
		termui:tabSet(1)
	end)
end

function help_wdg:draw()
	local txt = self:stylef(
		self:align('Help screen', 'center'),
		term.colors.blue, term.colors.bgwhite, term.colors.bold
	)
	self.window:print(txt, 1, 1, {notrunc = true})

	local help = {
		{ 't', 'Next window tab' },
		{ 'q', 'Exits application' },
		{ 'n', 'Creates new row' },
		{ 'd', 'Deletes selected row' },
	}

	for y, row in ipairs(help) do
		txt = table.concat(row, (" "):rep(8))
		self.window:print(txt, 1, y+1)
	end
end

termui.keyboard:on({'t', 'T'}, function(key)
	if key:lower() ~= 't' then return end
	termui:tabNext()
	term.reset()
end)

local wt = termui:new_table(0, 1, nil, -1)

---@diagnostic disable-next-line: duplicate-set-field
function wt:on_init()
	self:parent().on_init(self)

	self.data = {
		{ "Date", "Line", "fiber.time()", "os.clock()" }
	}
	self.rows = {table.concat(self.data[1], " ")}
	self.pin_rows = 1

	self._spawner_f = fiber.create(function()
		while true do
			self:create_row()
			fiber.sleep(0.1)
		end
	end)
end

wt.window.keyboard:on({'q', 'Q'}, function(_)
	termui:exit(0)
end)
wt.window.keyboard:on({'d'}, function(_)
	wt:delete_row()
end)

wt.window.keyboard:on({'n'}, function(_)
	wt:create_row()
end)

function wt:delete_row()
	local id = self.current_row
	if #self.data >= id and id >= 1 then
		table.remove(self.data, id)
		self.current_row = math.min(self.current_row, #self.data)
		self:redraw()
	end
end

---@diagnostic disable-next-line: duplicate-set-field

function wt:create_row()
	table.insert(self.data, { os.date("%FT%T"), #self.data, fiber.time(), os.clock() })
	self:redraw()
end

function wt:update()
	self.styles = {
		default = term.colorf(term.colors.white, term.colors.bgblack, term.colors.normal),
		[self.current_row] = term.colorf(term.colors.white, term.colors.bgblack, term.colors.bold),
	}
end

local hdr = termui:new_widget(0, 0)

function hdr:draw()
	local txt = self:align("Header", 'center')
	txt = self:stylef(txt, term.colors.white, term.colors.bgblack, term.colors.bold)
	self.window:print(txt, 1, 1, {notrunc = true})
end

os.exit(termui:run())
