--- This module provides connector to ETCD
---@module 'switchover._etcd'

---@class SwitchoverETCDClient:http
---@field timeout number
---@field headers table<string, any>

---@class ETCD
---@field client SwitchoverETCDClient
---@field last_headers table<string,string>
---@field __peers table
---@field integer_auto boolean
---@field boolean_auto boolean
---@field prefix string
---@field read_only boolean (default: false)
local M = {}
local uri = require 'switchover._net_url'
local fiber = require 'fiber'
local fun = require 'fun'
local E = require 'switchover._error'

---@class SwitchoverETCDOptions
---@field leader boolean?
---@field raw boolean?

local log = require 'log'
local json = require 'json'
local base64 = {
	encode = require 'digest'.base64_encode,
	decode = require 'digest'.base64_decode,
}
local http = require 'http.client'
local clock = require 'clock'

---@enum ETCDErrorCodes
local ecode = {
	EcodeKeyNotFound  = 100,
	EcodeTestFailed   = 101,
	EcodeNotFile      = 102,
	EcodeNotDir       = 104,
	EcodeNodeExist    = 105,
	EcodeRootROnly    = 107,
	EcodeDirNotEmpty  = 108,
	EcodeUnauthorized = 110,

	EcodePrevValueRequired = 201,
	EcodeTTLNaN            = 202,
	EcodeIndexNaN          = 203,

	EcodeInvalidField       = 209,
	EcodeInvalidForm        = 210,
	EcodeRefreshValue       = 211,
	EcodeRefreshTTLRequired = 212,

	EcodeRaftInternal = 300,
	EcodeLeaderElect  = 301,

	EcodeWatcherCleared    = 400,
	EcodeEventIndexCleared = 401,
}

local function deepmerge(t1, t2, seen)
	seen = seen or {}

	if type(t2) ~= 'table' or type(t1) ~= 'table' then
		return t2 or t1
	end

	if seen[t2] then
		return seen[t2]
	elseif seen[t1] then
		return seen[t1]
	end

	local r = {}

	-- from this point t1 and t2 are both tables
	seen[t1] = r
	seen[t2] = r
	-- and we have saw them

	for k2, v2 in pairs(t2) do
		if type(t1[k2]) == 'table' then
			r[k2] = deepmerge(t1[k2], v2, seen)
		else
			r[k2] = v2
		end
	end

	for k1, v1 in pairs(t1) do if r[k1] == nil then r[k1] = v1 end end
	return r, seen
end

local function trace(...)
	log.verbose(...)
end

local function __assert(cond, err, ...)
	if not cond then
		error(err, 2)
	end
	return cond, err, ...
end

--- Instances ETCD object
---@param cfg table configuration of ETCD
---@return ETCD # etcd object
function M.new(cfg)
	assert(cfg ~= M, "Usage: etcd.new({...}) not etcd:new({...})")
	assert(type(cfg.endpoints) == 'table', ".endpoints are required to be table")

	trace("create etcd with: %s", json.encode(cfg))

	local self = setmetatable({
		prefix       = cfg.prefix or "/",
		endpoints    = {},
		__peers      = cfg.endpoints,
		timeout      = tonumber(cfg.timeout) or 1,
		http_params  = cfg.http_params or {},
		boolean_auto = cfg.boolean_auto,
		integer_auto = cfg.integer_auto,
		last_headers = {},
		read_only    = cfg.read_only,
	}, { __index = M })

	self.client = setmetatable(deepmerge({}, deepmerge(self.http_params, { timeout = self.timeout })), { __index = http })

	if cfg.login then
		self.client.headers.authorization = ("Basic %s"):format(base64.encode(("%s:%s"):format(cfg.login, cfg.password or "")))
	end

	if cfg.autoconnect then
		self:connect()
	end

	return self
end

---
---@param opts SwitchoverETCDOptions
---@return string endpoint
function M:_endpoint(opts)
	opts = opts or {}
	if not opts.leader then
		return self.endpoints[math.random(#self.endpoints)]
	end
	return self.leader
end

---Performs general request to ETCD.
---@param method string http method.
---@param path string url path.
---@param query table url query (it is better to transmit a table).
---@param options SwitchoverETCDOptions options of http request such as timeout and others. They will be directly forwarded to http client.
---@return false|ETCDResponse body, string|table|HTTPResponse, HTTPResponse?
function M:request(method, path, query, options, with_discovery)
	if with_discovery then
		self:discovery()
	end

	local endpoint = self:_endpoint(options)
	E.assert(endpoint, ("ETCD endpoint is not discovered for %s %s (q:%s)"):format(
		method, path, json.encode(query)
	))

	if not self.leader and self.read_only then
		E.assert(method == "GET", "ETCD is broken (read_only mode activated) write operations are prohibited")

		if type(query) == 'table' and query.quorum then
			log.warn("ETCD is broken (read_only mode activated), quorum is ignored")
			query.quorum = nil
		end
	end

	local url = uri.parse(endpoint .. "/" .. path)
	url:setQuery(deepmerge(url.query, query))
	url:normalize()

	local s = clock.time()
	local opts = deepmerge(self.client, options)
	trace("Requesting %s %s with %s (discovery:%s)", method, tostring(url), json.encode(opts), tostring(with_discovery))

	-- clear leader opt from options for http.client
	opts.leader = nil
	local r, err = self.client.request(method, tostring(url), "", opts)
	trace("%s %s (%s) => %s:%s h:%s (%.4f)", method, url, json.encode(opts), r and r.status, r and r.reason,
		json.encode(r.headers), clock.time() - s)

	if not r then
		return false, err or "Unexpected HTTP error", r
	end
	if not r.headers or r.status >= 500 then
		r.headers = {}
		if not with_discovery then
			return self:request(method, path, query, options, true)
		end
	end
	local body
	if r.body and r.headers["content-type"] and r.headers["content-type"]:match("^application/json") then
		local ok, data = pcall(json.decode, r.body)
		if not ok then
			return false, data, r
		end
		body = data
	end
	for _, h in ipairs { "x-etcd-cluster-id", "x-etcd-index", "x-radt-index", "x-term-raft" } do
		self.last_headers[h] = r.headers[h]
	end
	body = body or ("%s:%s: %s"):format(r.status, r.reason, r.body)
	if r.status >= 500 then
		return false, body, r
	end
	return body, r
end

---GETs etcd
---@param endpoint string
---@param path string
---@return unknown
---@return any
---@return table
function M:_raw_request(endpoint, path)
	local url = uri.parse(("%s/%s"):format(endpoint, path))
	url:normalize()

	local s = clock.time()
	local res = self.client.request("GET", tostring(url), "", self.client)

	trace("GET %s => %s:%s (%.4f)", tostring(url), res.status, res.reason, clock.time() - s)

	local ok, data
	if res.body then
		ok, data = pcall(json.decode, res.body)
		if not ok then
			trace("%s: JSON decode of '%s' failed with: %s", tostring(url), tostring(res.body), tostring(data))
			data = nil
		end
	end

	return res.status, data, res.headers or {}
end

---
---Discovers ClientURls from ETCD (updates self.endpoints)
function M:discovery()
	trace("Running discovery: %s", json.encode(self.client))

	---@type table<string,boolean>
	local endpoints = {}
	local leaders = {}
	local peers = {}
	for _, p in ipairs(self.__peers) do
		if peers[p] == nil then
			peers[#peers + 1] = p
			peers[p] = true
		end
	end
	for _, p in ipairs(self.endpoints) do
		if peers[p] == nil then
			peers[#peers + 1] = p
			peers[p] = true
		end
	end

	for _, endpoint in ipairs(peers) do
		local status, body = self:_raw_request(endpoint, "/v2/members")
		if status ~= 200 or type(body) ~= 'table' then
			goto continue
		end

		for _, member in pairs(body.members) do
			local use_client_urls
			for _, u in pairs(member.clientURLs) do
				if not peers[u] then
					table.insert(peers, u)
					peers[u] = true
				end
				if u:sub(1, #endpoint) == endpoint or endpoint:sub(1, #u) == u then
					use_client_urls = true
				end
			end
			if use_client_urls then
				for _, u in pairs(member.clientURLs) do
					if not endpoints[u] then
						endpoints[u] = true
					end
				end
			end
		end

		::continue::
	end

	local function leader_discovery(endpoint)
		local status, body = self:_raw_request(endpoint, "/v2/members/leader")
		if status ~= 200 or type(body) ~= 'table' then
			trace("%s/v2/members/leader => %s:%s", endpoint, status, type(body) == 'table' and json.encode(body) or tostring(body))
			-- discard endpoint
			-- if endpoint does not has leader, then it is unhealthy
			endpoints[endpoint] = false
			return
		end

		local leader = body.clientURLs[1]:gsub("/*$", "")
		leaders[leader] = (leaders[leader] or 0) + 1
	end

	local fs = {}
	for endpoint in pairs(endpoints) do
		local f = fiber.new(leader_discovery, endpoint)
		f:set_joinable(true)
		fs[#fs + 1] = f
	end
	for _, f in ipairs(fs) do f:join() end

	if fun.length(leaders) == 1 then
		self.leader = next(leaders)
	elseif fun.length(leaders) > 1 then
		local total = fun.iter(leaders):map(function(_, v) return v end):sum()
		local leader_candidate = fun.iter(leaders):max_by(function(_, v) return v end)

		if leaders[leader_candidate] >= math.ceil((total + 1) / 2) then
			self.leader = leader_candidate
		else
			error(("Can't choose leader from: %s. ETCD is in split brain"):format(
				table.concat(fun.totable(leaders) ",")), 0)
		end
	end

	if self.read_only and not self.leader then
		log.warn("ETCD is broken, only readonly methods allowed")
	else
		E.assert(self.leader, "No ETCD leader found (etcd is a mess?)")
		trace("Choosing leader: %s", self.leader)
	end

	local _endpoints = fun.grep(function(_, t) return t == true end, endpoints):totable()
	assert(#_endpoints > 0, "No endpoints discovered")
	self.peers = peers
	self.endpoints = _endpoints
	return self.endpoints
end

--- Connects to the ETCD cluster discovering all peers.
function M:connect()
	self:discovery()
	trace("Got endpoints: %s", table.concat(self.endpoints, ","))
end

---@class ETCDNodeBase
---@field key string
---@field modifiedIndex number
---@field createdIndex number
---@field ttl? number TTL of the node in seconds.
---@field expiration? string expiration date of expiration in the following format: '2019-11-06T10:04:02.215117744Z'

---@class ETCDNodeDir:ETCDNodeBase
---@field dir true
---@field nodes ETCDNode[]

---@class ETCDNodeKV:ETCDNodeBase
---@field value string

---@alias ETCDNode ETCDNodeDir|ETCDNodeKV

---@class ETCDResponseSuccess
---@field action "get"|"set"|"update"|"delete"|"create"
---@field node ETCDNode

---@class ETCDResponseError
---@field errorCode ETCDErrorCodes
---@field message string
---@field cause string
---@field index number

---@alias ETCDResponse ETCDResponseSuccess|ETCDResponseError

--- Converts ETCD response to Lua table
---@param root ETCDNode ETCD tree
---@param prefix string a prefix of the call
---@param keys_only boolean? flag forces to return only keys if true.
---@param flatten boolean? flag forces to return flat structure instead of subtree.
---@usage unpacked = etcd:unpack(etcd:get("/some/prefix", { recursive = true }, { raw = true }), "/some/prefix")
---@return table<string,any> unpacked tree
function M:unpack(root, prefix, keys_only, flatten)
	local r = {}
	local flat = {}

	local stack = { root }
	repeat
		local node = table.remove(stack)
		if node.key then
			if self.integer_auto then
				if tostring(tonumber(node.key)) == node.key then
					node.key = tonumber(node.key)
				end
			end
			if node.dir then
				flat[node.key] = {}
			elseif keys_only then
				flat[node.key] = true
			else
				if self.boolean_auto then
					if node.value == "true" then
						node.value = true
					elseif node.value == "false" then
						node.value = false
					end
				end
				if self.integer_auto then
					if tostring(tonumber(node.value)) == node.value then
						node.value = tonumber(node.value)
					end
				end
				flat[node.key] = node.value
			end
		end
		if node.nodes then
			for i = 1, #node.nodes do
				table.insert(stack, node.nodes[i])
			end
		end
	until #stack == 0

	if flatten then
		return flat
	end

	r[prefix:sub(#"/" + 1)] = flat[prefix]

	for key, value in pairs(flat) do
		local cur = r
		local len = 0
		for chunk in key:gmatch("([^/]+)/") do
			cur[chunk] = cur[chunk] or {}
			cur = cur[chunk]
			len = len + #"/" + #chunk
		end
		local tail = key:sub(#"/" + len + 1)
		if keys_only then
			table.insert(cur, tail)
		elseif cur[tail] == nil then
			cur[tail] = value
		end
	end

	local sub = r
	for chunk in prefix:gmatch("([^/]+)") do
		sub = sub[chunk]
	end

	return sub
end

function M:_path(path)
	return (("%s/%s"):format(self.prefix, path):gsub("/+", "/"):gsub("/$", ""))
end

--- Gets subtree from ETCD.
---@param path string path of subtree
---@param flags table flags of the request.
---@param options SwitchoverETCDOptions of request.
---@return ETCDResponseSuccess|table<string,any> subtree from ETCD
function M:get(path, flags, options)
	options = options or {}
	flags = flags or {}

	path = self:_path(path)

	local res = __assert(self:request("GET", "/v2/keys" .. path, flags, options))
	if options.raw then
		return res
	end
	if type(res) ~= 'table' then
		error(("etcd get %s => %s"):format(path, res))
	end
	if res.errorCode then
		error(("etcd get %s => %s:%s"):format(path, res.errorCode, res.message))
	end

	return self:unpack(res.node, path)
end

--- Returns whole subtree from ETCD
---@param path string path to subtree
---@param options? table options of request.
---@return table subtree from ETCD
function M:getr(path, flags, options)
	return self:get(path, deepmerge(flags, { recursive = true }), options)
end

--- Returns listing of keys from subtree
---@param path string path to subtree
---@param flags table ls flags
---@param options SwitchoverETCDOptions of http
---@return ETCDResponseSuccess|table<string,any> listing
function M:ls(path, flags, options)
	flags = flags or {}
	options = options or {}

	local res = self:get(path, flags, deepmerge(options, { raw = true }))
	if options.raw then
		return res
	end
	log.verbose(res)
	if type(res) ~= 'table' then
		error(("etcd ls %s => %s"):format(self:_path(path), res))
	end
	if res.errorCode then
		error(("etcd ls %s => %s:%s"):format(self:_path(path), res.errorCode, res.message))
	end

	return self:unpack(res.node, self:_path(path), true)
end

--- Returns recursive listing of subtree
---@param path string path to subtree
---@param options SwitchoverETCDOptions
---@return ETCDResponseSuccess|table<string,any> listing
function M:lsr(path, options)
	return self:ls(path, { recursive = true }, options)
end

--- Waits for changes of given path in ETCD
---@param path string path of subtree
---@param args table flags
---@param opts SwitchoverETCDOptions
---@return ETCDResponseSuccess
function M:wait(path, args, opts)
	args = args or {}
	opts = opts or {}
	return (__assert(self:request(
		"GET", "/v2/keys" .. self:_path(path),
		(deepmerge({ wait = true }, args)),
		(deepmerge({}, opts))
	)))
end

--- Provides 'mkdir -p' mechanism.
---@param path string to the directory. All parent directories will be created if not exist.
---@param options? table options of http request
---@return ETCDResponseSuccess
function M:mkdir(path, options)
	return (__assert(self:request("PUT", "/v2/keys" .. self:_path(path), { dir = true }, options)))
end

--- Provides 'rmdir' mechanism.
-- @param path path ro the directory to delete.
-- @param[optional] options options of http request
-- @return etcd API response
function M:rmdir(path, options)
	return __assert(self:request("DELETE", "/v2/keys" .. self:_path(path), { dir = true }, options))
end

--- Provides 'rm' mechanism.
-- @param path path to the resource to delete.
-- @tparam[optional] rmFlags flags of command.
-- @param[optional] options options of http request.
-- @return etcd API response
function M:rm(path, flags, options)
	flags = flags or {}
	return __assert(self:request("DELETE", "/v2/keys" .. self:_path(path), flags, options))
end

function M:rmrf(path, options)
	assert(path, "path is required")
	options = options or {}
	return __assert(self:request("DELETE", "/v2/keys" .. self:_path(path), { recursive = true, force = true }, options))
end

--- Sets value by path into ETCD.
---@param path string path to the key
---@param value string value of the key
---@param path_options table options of the path
---@param options SwitchoverETCDOptions of http request
---@return ETCDResponse response
function M:set(path, value, path_options, options)
	return (__assert(self:request("PUT", "/v2/keys" .. self:_path(path), deepmerge(path_options, { value = value }), options)))
end

function M:version()
	local fs = {}
	local r = {}
	for _, e in ipairs(self.endpoints) do
		local key = uri.parse(e).authority
		table.insert(fs, fiber.create(function()
			fiber.self():set_joinable(true)
			local status, data = self:_raw_request(e, "/version")
			if status == 200 then
				r[key] = data
			end
		end))
	end
	for _, f in ipairs(fs) do f:join() end
	return r
end

function M:health()
	local fs = {}
	local r = {}
	for _, e in ipairs(self.peers) do
		local key = uri.parse(e).authority
		r[key] = 'not-connected'
		table.insert(fs, fiber.create(function()
			fiber.self():set_joinable(true)
			local s = clock.time()
			local status, data = self:_raw_request(e, "/health")
			local elapsed = clock.time()-s
			if status == 200 and type(data)=='table' and data.health == "true" then
				log.info("Received healthy result from %s in %.3fs", e, elapsed)
				r[key] = 'healthy'
			elseif type(data) == 'table' and data.health ~= "true" then
				log.warn("Received non-healthy result from %s in %.3fs", e, elapsed)
				r[key] = ('not-healthy:%s'):format(json.encode(data))
			else
				log.warn("Received no result from %s in %.3fs", e, elapsed)
				r[key] = ('unknown: http=%s: %s'):format(status, data)
			end
		end))
	end
	for _, f in ipairs(fs) do f:join() end
	return r
end

local function flatten(subtree, rootpath)
	local flat = {}
	local stack = { subtree }
	local map = { [subtree] = rootpath }
	repeat
		local node = table.remove(stack)
		for key, sub in pairs(node) do
			local fullpath = (map[node] .. "/" .. key):gsub("/+", "/")
			if map[sub] then
				error(("Caught recursive subtree. Key '%s' can be reached via '%s'"):format(map[sub], fullpath), 3)
			end
			if type(sub) == 'table' then
				map[sub] = fullpath
				table.insert(stack, sub)
			else
				flat[fullpath] = tostring(sub)
			end
		end
	until #stack == 0
	return flat
end

---@class SwitchoverETCDLoadOptions:SwitchoverETCDOptions
---@field dry_run boolean
---@field no_change boolean
---@field sync_delete boolean

--- Fills up the ETCD config.
-- PUTs structure key be key using CaS prevExists = false denying clearing previous value.
---@param path string path to subtree
---@param subtree table lua table describes subtree
---@param options SwitchoverETCDLoadOptions of the request
---@return table<string,any> result
function M:fill(path, subtree, options)
	path = path:gsub("/+", "/"):gsub("/+$", "/")
	options = options or {}

	local rawlist = self:get(path, { recursive = true }, { raw = true }).node
	local current
	if rawlist then -- can be nil ;)
		current = self:unpack(rawlist, path, false, true)
	end

	local rootpath = self:_path(path)
	log.verbose("fill into %s", rootpath)

	local flat = flatten(subtree, rootpath)

	for newkey, newvalue in pairs(flat) do
		local prevValue, prevExist

		if current and current[newkey] and tostring(current[newkey]) == newvalue then
			if options.dry_run then
				log.info("Skip %s %s = %s", newkey, current[newkey], newvalue)
			else
				log.verbose("Skipping %s (same value)", newkey)
			end
			goto continue
		elseif current and current[newkey] ~= nil then
			prevValue = current[newkey]
			prevExist = true
		else
			prevExist = false
		end

		if options.no_change and prevValue ~= nil then
			if options.dry_run then
				log.info("NoChange %s %s => %s", newkey, prevValue, newvalue)
			else
				log.verbose("Skipping %s %s => %s (no change)", newkey, prevValue, newvalue)
			end
			goto continue
		end

		if options.dry_run then
			if prevExist then
				log.info("Change %s %s => %s", newkey, prevValue, newvalue)
			else
				log.info("Add %s => %s", newkey, newvalue)
			end
			goto continue
		end

		local body, headers = self:request("PUT", "/v2/keys/" .. newkey, {
			prevExist = prevExist, prevValue = prevValue, value = newvalue
		}, options)

		log.verbose("Set %s => %s : %s", newkey, newvalue, json.encode(body))
		if not (headers.status == 201 or headers.status == 200) then
			error(json.encode(body), 2)
		end
		if type(body) ~= 'table' or not body.action then
			error(json.encode(body), 2)
		end

		::continue::
	end

	if options.sync_delete then
		local rmdirs = {}
		-- Remove files
		for oldkey, oldvalue in pairs(current or {}) do
			if type(oldvalue) == 'table' then
				table.insert(rmdirs, oldkey)
				goto continue
			end
			if flat[oldkey] ~= nil then
				goto continue
			end

			if options.dry_run then
				log.info("Delete %s (%s)", oldkey, oldvalue)
				goto continue
			end

			local body, headers = self:request("DELETE", "/v2/keys/" .. oldkey, {
				prevValue = oldvalue
			}, options)

			log.verbose("delete %s => %s : %s", oldkey, oldvalue, json.encode(body))
			if headers.status ~= 200 then
				if type(body) == 'table' and body.errorCode == ecode.EcodeNotFile then
					table.insert(rmdirs, oldkey)
				else
					error(json.encode(body), 2)
				end
			end
			if type(body) ~= 'table' or not body.action then
				error(json.encode(body), 2)
			end

		    ::continue::
		end

		table.sort(rmdirs, function(a, b) return a > b end)

		for _, dirKey in ipairs(rmdirs) do
			-- if not a prefix of flat:
			for key in pairs(flat) do
				if key:sub(1, #dirKey) == dirKey then
					dirKey = nil
					break
				end
			end

			if not dirKey then
				goto continue
			end

			if options.dry_run then
				local dir = flatten(current[dirKey], dirKey)
				local keys = fun.totable(dir)
				table.sort(keys)
				for _, k in ipairs(keys) do
					log.warn("Delete %s (%s)", k, dir[k])
				end
				goto continue
			end

			local body, headers = self:request("DELETE", "/v2/keys/" .. dirKey, {
				dir = true
			}, options)
			if headers.status ~= 200 then
				error(json.encode(body), 2)
			end
			if type(body) ~= 'table' or not body.action then
				error(json.encode(body), 2)
			end

			::continue::
		end
	end

	return self:getr(path)
end

return M
