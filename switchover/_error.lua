local M = {}
local log = require 'log'

---executes assert and throws nonrecoverable panic if condition is falsy
---otherwise returns given args
---@generic T
---@param cond? T
---@param message? any
---@return T
---@return ... any
function M.assert(cond, message, ...)
	if cond then return cond, message, ... end
	return M.panic(message, ...)
end

local function exit(code)
	if type(box.cfg) == 'function' then
		os.exit(code)
	end
	error("Exit", 3)
end

---throws unrecoverable panic (exit(1))
---@param err any
---@param ... any?
function M.panic(err, ...)
	if select('#', ...) == 0 then
		log.error("Panic: %s", err)
		exit(1)
	end
	log.error("PANIC: "..err:format(...))
	log.verbose(debug.traceback(err:format(...)))
	exit(1)
end

return M