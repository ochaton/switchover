local panic = require 'switchover._error'.panic
local fun = require 'fun'
local digest = require 'digest'

local Proxy = require 'switchover._proxy'

---@class CommonInfo
---@field public box table<string,any>

---@class ShardInfo
---@field public replicaset_uuid string UUID of Replicaset
---@field public master string Name of chosen master in shard

---@class SwitchoverInstanceAutofailoverInfo
---@field role "candidate"|"witness"? by default candidate
---@field priority number? priority for candidate (by default=1)

---@class InstanceInfo
---@field public cluster string? name of the shard
---@field public router boolean? is instance declared as router
---@field public box table<string,any> KV table of the box
---@field public autofailover SwitchoverInstanceAutofailoverInfo? autofailover info

---@class ClusterTree
---@field public clusters table<string,ShardInfo>
---@field public instances table<string,InstanceInfo>
---@field public common CommonInfo

---@class Cluster
---@field public name string Name of the cluster
---@field public tree ClusterTree downloaded etcd tree of the cluster
---@field public cluster_path string path to this tree
---@field public policy string policy of parsing etcd tree
local Cluster = {}
Cluster.__index = Cluster

---@class Shard:Cluster
---@field public shard_name string Name of the Shard

local function gen_instance_uuid(instance_name)
	local k,d1,d2 = instance_name:match("^([A-Za-z_]+)_(%d+)_(%d+)$")
	if k then
		return string.format(
			"%08s-%04d-%04d-%04d-%012x",
			digest.sha1_hex(k .. "_instance"):sub(1,8),
			d1,d2,0,0
		)
	end

	k,d1 = instance_name:match("^([A-Za-z_]+)_(%d+)$")
	if k then
		return string.format(
			"%08s-%04d-%04d-%04d-%012d",
			digest.sha1_hex(k):sub(1,8),
			0,0,0,d1
		)
	end
	error("Can't generate uuid for instance "..instance_name, 2)
end

local function gen_replicaset_uuid(cluster_name)
	local k,d1 = cluster_name:match("^([A-Za-z_]+)_(%d+)$")
	if k then
		return string.format(
			"%08s-%04d-%04d-%04d-%012d",
			digest.sha1_hex(k .. "_shard"):sub(1,8),
			d1,0,0,0
		)
	end
	error("Can't generate uuid for instance "..cluster_name, 2)
end

---@return nil|Cluster,string|nil
function Cluster:build_cluster()
	if type(self.tree.clusters) ~= 'table' then
		return nil, ("%s.clusters required to be table got %s"):format(self.name, type(self.tree.clusters))
	end
	if type(self.tree.instances) ~= 'table' then
		return nil, ("%s.instances required to be table got %s"):format(self.name, type(self.tree.instances))
	end

	if type(self.tree.common) == 'table' and type(self.tree.common.box) == 'table' then
		self.common = self.tree.common
	end

	for shard_name, shard_info in pairs(self.tree.clusters) do
		if type(shard_name) ~= 'string' then
			return nil, ("key of %s.clusters required to be a string got %s"):format(self.name, type(shard_name))
		end
		if type(shard_info) ~= 'table' then
			return nil, ("%s.clusters[%q] required to be a table got %s"):format(self.name,
				shard_name, type(shard_info))
		end
		if type(shard_info.master) ~= 'string' then
			return nil, ("%s.clusters[%q].master required to be a string got %s"):format(self.name,
				shard_name, type(shard_info.master))
		end
		if type(shard_info.replicaset_uuid) ~= 'string' then
			shard_info.replicaset_uuid = gen_replicaset_uuid(shard_name)
			-- return nil, ("%s.clusters[%q].replicaset_uuid required to be a string got %s"):format(self.name,
			-- 	shard_name, type(shard_info.replicaset_uuid))
		end
		shard_info.replicaset_uuid = shard_info.replicaset_uuid:lower()
	end

	for instance_name, instance_info in pairs(self.tree.instances) do
		if type(instance_name) ~= 'string' then
			return nil, ("key of %s.instances required to be a string got %s"):format(self.name, type(instance_name))
		end
		if type(instance_info) ~= 'table' then
			return nil, ("%s.instances[%q] required to be a table got %s"):format(self.name,
				instance_name, type(instance_info))
		end
		if instance_info.router == nil and instance_info.cluster == nil then
			return nil, ("%s.instances[%q].{cluster or router} required to be present"):format(self.name,
				instance_name)
		end
		if instance_info.cluster ~= nil then
			if type(instance_info.cluster) ~= 'string' then
				return nil, ("%s.instances[%q].cluster required to be a string got %s"):format(self.name,
					instance_name, type(instance_info.cluster))
			end
			if self.tree.clusters[instance_info.cluster] == nil then
				return nil, ("%s.instances[%q].cluster must link to existing cluster, but %q is not found in tree.clusters"):format(self.name,
					instance_name, instance_info.cluster
				)
			end
		end
		if type(instance_info.box) ~= 'table' then
			return nil, ("%s.instances[%q].box required to be a table got %s"):format(self.name,
				instance_name, type(instance_info.box))
		end
		if instance_info.box.listen then
			if type(instance_info.box.listen) ~= 'string' then
				return nil, ("%s.instances[%q].box.listen required to be a string got %s"):format(self.name,
					instance_name, type(instance_info.box.listen))
			end
		end
		if instance_info.box.remote_addr then
			if type(instance_info.box.remote_addr) ~= 'string' then
				return nil, ("%s.instances[%q].box.remote_addr required to be a string got %s"):format(self.name,
					instance_name, type(instance_info.box.remote_addr))
			end
		end
		if not instance_info.box.remote_addr and not instance_info.box.listen then
			return nil, ("%s.instances[%q]: either box.listen or box.remote_addr must be present"):format(
				self.name, instance_name
			)
		end
		if type(instance_info.box.instance_uuid) ~= 'string' then
			instance_info.box.instance_uuid = gen_instance_uuid(instance_name)
			-- return nil, ("%s.instances[%q].box.instance_uuid required to be a string got %s"):format(self.name,
			-- 	instance_name, type(instance_info.box.instance_uuid))
		end
		instance_info.box.instance_uuid = instance_info.box.instance_uuid:lower()
		if type(instance_info.autofailover) == 'nil' then
			instance_info.autofailover = { priority = 1, role = "candidate" }
		end
		if type(instance_info.autofailover) ~= 'table' then
			return nil, ("%s.instances[%q]: autofailover must be a kv-map: got %s (type: %s)"):format(
				self.name, instance_name, instance_info.autofailover, type(instance_info.autofailover)
			)
		end
		if instance_info.autofailover.role then
			if instance_info.autofailover.role ~= "candidate" and instance_info.autofailover.role ~= "witness" then
				return nil, ("%s.instances[%q].autofailover.role: must be either 'candidate' (default) or 'witness' got %s"):format(
					self.name, instance_name, instance_info.autofailover.role
				)
			end
		end
		if instance_info.autofailover.priority then
			local p = tonumber(instance_info.autofailover.priority)
			if type(p) ~= 'number' then
				return nil, ("%s.instances[%q].autofailover.prioiry: must be a number (default = 1) got %s"):format(
					self.name, instance_name, instance_info.autofailover.priority
				)
			end
			if p < 0 then
				return nil, ("%s.instances[%q].autofailover.prioiry: when defined must be ≥0 got %s"):format(
					self.name, instance_name, instance_info.autofailover.priority
				)
			end
			instance_info.autofailover.priority = p
		end
	end

	return self
end

---Constructor of the Cluster
---If args.policy is set to `etcd.instance.single` then method will attempt to create Proxy
---@param args table arguments to build cluster
---@return Cluster|Proxy|nil,string|nil returns cluster or message
function Cluster:new(args)
	if type(args) ~= 'table' then
		return nil, ("args required to be table got %s"):format(type(args))
	end

	self = setmetatable({
		name = args.name,
		tree = args.tree,
		shard_name = args.shard_name,
		cluster_path = args.cluster_path,
		policy = args.policy or 'etcd.cluster.master',
	}, self)

	if self.policy == 'etcd.cluster.master' then
		return self:build_cluster()
	elseif self.policy == 'etcd.instance.single' then
		return Proxy(self)
	else
		return nil, ("Switchover does not support policy: %q"):format(self.policy)
	end
end

---returns subcluster for given `shard_name`
---@param shard_name string name of the shard
---@return Shard|Proxy|false, string? error_message
function Cluster:shard(shard_name)
	if not self.tree.clusters[shard_name] then
		panic("%s.clusters does not contain shard %q", self.name, shard_name)
	end

	if fun.length(self.tree.clusters) == 1 then
		local single_shard_name = next(self.tree.clusters)
		if single_shard_name == self.name then
			if not self.cluster_path then
				self.cluster_path = ("%s/clusters/%s"):format(self.name, shard_name)
			end
			self.shard_name = self.name
			return self
		end
	end

	return Cluster:new {
		common = self.common, -- can be nil
		name = ("%s/%s"):format(self.name, shard_name),
		shard_name = assert(shard_name),
		cluster_path = ("%s/clusters/%s"):format(self.name, shard_name),
		tree = {
			clusters = {
				[shard_name] = self.tree.clusters[shard_name],
			},
			instances = fun.iter(self.tree.instances)
				:grep(function(_, v) return v.cluster == shard_name end)
				:tomap(),
		}
	}
end

---Returns replication_connect_quorum for shard
---@param shard_name? string name of the shard
---@return number replication_connect_quorum, string|nil default_or_not
function Cluster:RCQ(shard_name)
	if self.common and self.common.box and self.common.box.replication_connect_quorum then
		return self.common.box.replication_connect_quorum, 'default'
	end
	if fun.length(self.tree.clusters) == 1 then
		return #self:instances()
	end
	return #self:shard(shard_name):instances()
end

---Returns list of shard names of the cluster
---@return string[] list of shard names
function Cluster:get_shard_names()
	local list = fun.iter(self.tree.clusters):totable()
	table.sort(list)
	return list
end

---@alias InstanceRole table<string,number>
local InstanceRole = {-- luacheck: ignore
	---Master of the shard
	['master']  = 1,
	---Replica of the shard
	['replica'] = 2,
}

---@class ExtendedInstanceInfo
---@field public name string instance name
---@field public listen string listen URI to instance
---@field public endpoint string connect URI of the instance (advertise or listen)
---@field public remote_addr string advertise URI of the instance
---@field public instance_uuid string UUID of the instance
---@field public role InstanceRole master or replica
---@field public cluster string? link to shard_name
---@field public box table info about box in ETCD
---@field public autofailover SwitchoverInstanceAutofailoverInfo autofailover info

---Extends info about instance
---@param name string instance_name
---@param v InstanceInfo
---@return ExtendedInstanceInfo
function Cluster:populate_instance_info(name, v)
	local role
	if v.router then
		role = 'router'
	else
		role = self:shard(v.cluster):master_name() == name and 'master' or 'replica'
	end
	return {
		name = name,
		listen = v.box.listen,
		endpoint = v.box.remote_addr or v.box.listen,
		remote_addr = v.box.remote_addr,
		instance_uuid = v.box.instance_uuid,
		role = role,
		cluster = v.cluster,
		autofailover = v.autofailover,
	}
end

---Returns list of the instances
---@return ExtendedInstanceInfo[]
function Cluster:instances()
	local result = fun.iter(self.tree.instances):map(function(name, v)
		return self:populate_instance_info(name, v)
	end):totable()
	table.sort(result, function(a, b) return a.name < b.name end)
	return result
end

---Returns list of routers
---@return Proxy|nil|false, string? error_message
function Cluster:routers()
	local routers = fun.iter(self.tree.instances)
		:grep(function(_, v) return v.router end)
		:tomap()

		if not next(routers) then
		return nil
	end

	return Proxy:new {
		name = self.name,
		tree = {
			common = self.common,
			instances = routers,
		},
		policy = 'etcd.instance.single',
		cluster_path = self.cluster_path,
	}
end

---Returns list of connect endpoints of all instances
---@return string[]
function Cluster:endpoints()
	return fun.iter(self:instances())
		:map(function(t) return t.listen or t.remote_addr end)
		:totable()
end

---Returns shard by instance_name
---Calls Cluster.shard
---@param instance_name string
---@return Cluster isolated to single shard which contains given instance
function Cluster:shard_by_name(instance_name)
	if self.tree.instances[instance_name] == nil then
		panic("%s.instances does not contain instance %q", self.name, instance_name)
	end

	local isolated = assert(self:shard(self.tree.instances[instance_name].cluster)) --[[@as Cluster]]
	return isolated
end

---Return verbose instance info by uuid
---@param instance_uuid string UUID of the instance
---@return ExtendedInstanceInfo|nil
function Cluster:instance_by_uuid(instance_uuid)
	for instance_name, instance_info in pairs(self.tree.instances) do
		if instance_info.box.instance_uuid:lower() == instance_uuid:lower() then
			return self:instance(instance_name)
		end
	end
end

---@param instance_name string
---@return boolean
function Cluster:has_instance(instance_name)
	return self.tree.instances[instance_name] ~= nil
end

---Returns verbose instance info by name
---@param instance_name string name of the instance
---@return ExtendedInstanceInfo
function Cluster:instance(instance_name)
	if self.tree.instances[instance_name] == nil then
		panic("%s.instances does not contain instance %q", self.name, instance_name)
	end

	local inst = self:populate_instance_info(instance_name, self.tree.instances[instance_name])
	---@cast inst +ExtendedInstanceInfo
	setmetatable(inst, { __index = self.tree.instances[instance_name] })
	return inst
end

---Interface method. Returns whether Cluster is Proxy
---@return boolean always false actually
function Cluster.is_proxy()
	return false
end

function Cluster:uri2name()
	return fun.iter(self.tree.instances):map(function(name, info)
		return (info.box.listen or info.box.remote_addr), name
	end):tomap()
end


---@return boolean
function Cluster:has_master()
	if fun.length(self.tree.clusters) ~= 1 then
		return false
	end
	local _, shard = next(self.tree.clusters)
	if shard.master and self.tree.instances[shard.master] then
		return true
	end
	return false
end

function Cluster:master_name()
	if fun.length(self.tree.clusters) ~= 1 then
		panic("%q contains too many shards", self.name)
	end
	local _, shard = next(self.tree.clusters)
	return shard.master
end

function Cluster:switchover_path()
	if self.cluster_path then
		return self.cluster_path .. '/switchover'
	else
		return self.name..'/switchover'
	end
	-- return self.name:find('/') and (self.name.."_switchover") or (self.name.."/switchover")
end

---
---@return ExtendedInstanceInfo,string master info and master_name
function Cluster:master()
	local master_name = self:master_name()
	return self:instance(master_name), master_name
end

local whitelist = fun.zip({':ro',':rw',':ros',':routers'}, fun.ones()):tomap()

---Slices response to given slice
---@param slice ":ro"|":rw"|":ros"|":routers"
function Cluster:slice(slice)
	if not whitelist[slice] then
		return self
	end

	local instance

	if slice == ':rw' then
		for instance_name, inst in pairs(self.tree.instances) do
			if not inst.cluster then
				self.tree.instances[instance_name] = nil
			elseif self.tree.clusters[inst.cluster].master ~= instance_name then
				self.tree.instances[instance_name] = nil
			end
		end
		if #self:get_shard_names() == 1 then
			instance = self:master()
		end
	elseif slice == ':ros' then
		-- returns only replicas
		for instance_name, inst in pairs(self.tree.instances) do
			if not inst.cluster then
				self.tree.instances[instance_name] = nil
			elseif self.tree.clusters[inst.cluster].master == instance_name then
				self.tree.instances[instance_name] = nil
			end
		end
	elseif slice == ':routers' then
		return self:routers()
	elseif slice == ':ro' then
		-- we need to choose single replica

		---@type table<string,string[]>
		local shard2ros = {}
		for instance_name, inst in pairs(self.tree.instances) do
			if not inst.cluster then
				self.tree.instances[instance_name] = nil
			elseif self.tree.clusters[inst.cluster].master == instance_name then
				self.tree.instances[instance_name] = nil
			else
				shard2ros[inst.cluster] = shard2ros[inst.cluster] or {}
				table.insert(shard2ros[inst.cluster], instance_name)
			end
		end

		for _, ros in pairs(shard2ros) do
			while #ros > 1 do
				local rnd = math.random(1, #ros)
				---@type string
				local inst = table.remove(ros, rnd)
				self.tree.instances[inst] = nil
			end
		end

		if #self:get_shard_names() == 1 then
			local instance_name = next(self.tree.instances)
			instance = self:instance(instance_name)
		end
	end

	-- by default return unchanged
	return self, instance
end

return setmetatable(Cluster, { __call = Cluster.new })