---@class SwitchoverGlobal
---@field etcd ETCD
---@field workdir string
---@field args table

---@type SwitchoverGlobal
local global = {}

return global
