require 'jit'.off()
require 'jit'.flush()
io.stdout:setvbuf("no")
io.stderr:setvbuf("no")

local fun = require 'fun'

local settings = {
	etcd_endpoints = (
		os.getenv("ETCD_ADVERTISE_CLIENT_URLS") or "http://etcd0:2379"
	):split(','),

	etcd_prefix = os.getenv('ETCD_PREFIX') or '/',
	etcd_timeout = tonumber(os.getenv('ETCD_TIMEOUT')) or 3,
}

local switchover = '/usr/bin/switchover --no-config -e '..settings.etcd_endpoints[1]..' '

local fio = require 'fio'
local log = require 'log'
local yaml = require 'yaml'
local popen = require 'popen'
local _etcd = require 'switchover._etcd'
local G = require 'switchover._global'
local etcd

local t = require 'luatest'
local g = t.group('etcd_load')

local function clear_etcd()
	local root = etcd:ls('/')
	for _, p in ipairs(root) do
		log.info("Removing %s from etcd", p)
		etcd:rmrf(p)
	end
end

g.before_each(clear_etcd)
g.after_each(clear_etcd)

g.before_all(function()
	etcd = _etcd.new {
		endpoints = settings.etcd_endpoints,
		prefix = settings.etcd_prefix,
		autoconnect = true,
		timeout = settings.etcd_timeout,
		integer_auto = true,
		boolean_auto = true,
	}
	G.etcd = etcd
end)

g.after_all(clear_etcd)

local function dump_to_file(data)
	local dir = fio.tempdir()
	local input_file = fio.pathjoin(dir, 'data.yaml')
	local fh = fio.open(input_file, { 'O_CREAT', 'O_WRONLY', 'O_EXCL' }, tonumber("0644", 8))
	assert(fh:write(yaml.encode(data)))
	assert(fh:close())

	return input_file
end

local structure = {
	dir = {
		nested = {
			one  = 1,
			two  = 2,
			zero = 0,
			float = 0.001,
			["true"] = true,
			["false"] = false,
			empty = "",
			string = "some_string",
			array = {
				["1"] = "first",
				["2"] = "second",
			},
		},
	},
}

local new_structure = {
	dir = {
		nested = {
			one  = 1,
			two  = 2,
			zero = 0,
			float = 0.001,
			["true"] = true,
			["false"] = false,
			empty = "",
			string = "some_string",
			array = {
				["1"] = "first",
				["2"] = "second",
			},

			new_dir = {
				value = "string",
				numeric = 0.001,
			},
		},
	}
}

local function load_from_data(data, command)
	command = command or 'etcd load /'
	local file_name = dump_to_file(data)

	local ph, err
	local cmd = switchover..' '..command..' '..file_name
	local argv = fun.iter(cmd:split(' ')):grep(function(s) return #s > 0 end):totable()
	log.info(argv)
	ph = popen.new(argv, {
		shell = false,
		env = os.environ(),
	})

	t.assert(ph, 'popen handle must be created (etcd load)')
	assert(ph, err)

	ph:wait()
	t.assert_equals(ph:info().status.exit_code, 0, cmd..' - exit code must be 0')
end

local function test_structure(struct)
	local root = etcd:getr("/")
	t.assert_items_equals(root, struct, "same root")

	local dir = etcd:getr("/dir")
	t.assert_items_equals(dir, struct.dir, "same dir")

	local nested = etcd:getr("/dir/nested")
	t.assert_items_equals(nested, struct.dir.nested, "same dir.nested")

	local one = etcd:getr("/dir/nested/one")
	t.assert_equals(one, struct.dir.nested.one, "same dir.nested.one")

	local two = etcd:getr("/dir/nested/two")
	t.assert_equals(two, struct.dir.nested.two, "same dir.nested.two")

	local float = etcd:getr("/dir/nested/float")
	t.assert_equals(float, struct.dir.nested.float, "same dir.nested.float")

	local zero = etcd:getr("/dir/nested/zero")
	t.assert_equals(zero, struct.dir.nested.zero, "same dir.nested.zero")

	local truth = etcd:getr("/dir/nested/true")
	t.assert_equals(truth, struct.dir.nested["true"], "same dir.nested.true")

	local falsy = etcd:getr("/dir/nested/false")
	t.assert_equals(falsy, struct.dir.nested["false"], "same dir.nested.false")

	local empty = etcd:getr("/dir/nested/empty")
	t.assert_equals(empty, struct.dir.nested.empty, "same dir.nested.empty")

	local str = etcd:getr("/dir/nested/string")
	t.assert_equals(str, struct.dir.nested.string, "same dir.nested.string")

	if struct.dir.nested.array then
		local array = etcd:getr("/dir/nested/array")
		t.assert_items_equals(array, struct.dir.nested.array, "same dir.nested.array")
		if struct.dir.nested.array["1"] then
			local first = etcd:getr("/dir/nested/array/1")
			t.assert_equals(first, struct.dir.nested.array["1"], "same dir.nested.array.1")
		end

		if struct.dir.nested.array["2"] then
			local second = etcd:getr("/dir/nested/array/2")
			t.assert_equals(second, struct.dir.nested.array["2"], "same dir.nested.array.2")
		end
	end
end

function g.test_etcd_load_list_get_structure()
	load_from_data(structure)

	for _, cmd in ipairs{
		"etcd get -r /", "etcd getr /",
		"etcd list /", "etcd ls /",

		"etcd get -r /dir", "etcd getr /dir",
		"etcd list /dir", "etcd ls /dir",

		"etcd get -r /dir/nested", "etcd getr /dir/nested",
		"etcd list /dir/nested", "etcd ls /dir/nested",

		"-p /dir etcd get -r /nested", "-p /dir etcd getr /nested",
		"-p /dir etcd list /nested", "-p /dir etcd ls /nested",

		"-p /dir/nested etcd get -r /", "-p /dir/nested etcd getr /",
		"-p /dir/nested etcd list /", "-p /dir/nested etcd ls /",

		"-p /dir/nested etcd get -r /array", "-p /dir/nested etcd getr /array",
		"-p /dir/nested etcd list /array", "-p /dir/nested etcd ls /array",

		"-p /dir/nested/array etcd get -r /", "-p /dir/nested/array etcd getr /",
		"-p /dir/nested/array etcd list /", "-p /dir/nested etcd ls /",

		"etcd get /dir/nested/one", "etcd get /dir/nested/two", "etcd get /dir/nested/zero",
		"etcd get /dir/nested/true", "etcd get /dir/nested/false",
		"etcd get /dir/nested/empty", "etcd get /dir/nested/string",
		"etcd get /dir/nested/array/1/", "etcd get /dir/nested/array/2/",

		"etcd get -r /dir/nested/one", "etcd get -r /dir/nested/two", "etcd get -r /dir/nested/zero",
		"etcd get -r /dir/nested/true", "etcd get -r /dir/nested/false",
		"etcd get -r /dir/nested/empty", "etcd get -r /dir/nested/string",
		"etcd get -r /dir/nested/array/1/", "etcd get -r /dir/nested/array/2/",

		"etcd getr /dir/nested/one", "etcd getr /dir/nested/two", "etcd getr /dir/nested/zero",
		"etcd getr /dir/nested/true", "etcd getr /dir/nested/false",
		"etcd getr /dir/nested/empty", "etcd getr /dir/nested/string",
		"etcd getr /dir/nested/array/1/", "etcd getr /dir/nested/array/2/",
	} do
		local ph, err = popen.shell(switchover .. cmd, 'r')
		t.assert(ph, 'popen handle must be created ('..cmd..')')
		assert(ph, err)

		ph:wait()
		t.assert_equals(ph:info().status.exit_code, 0, '('..cmd..') - exit code must be 0')
	end

	test_structure(structure)
end

function g.test_etcd_double_load()
	load_from_data(structure)
	test_structure(structure)

	load_from_data(structure)
	test_structure(structure)
end

function g.test_etcd_load_append_new_data()
	load_from_data(structure)
	t.assert_error(function()
		etcd:getr("/dir/nested/new_dir")
	end, "/dir/nested/new_dir - must be not found")

	load_from_data(new_structure)
	test_structure(new_structure)

	local new_dir = etcd:getr("/dir/nested/new_dir")
	t.assert_equals(new_dir, new_structure.dir.nested.new_dir, "loaded dir.nested.new_dir")
end

function g.test_etcd_load_overwrite_data()
	load_from_data(structure)

	local change = table.deepcopy(structure)
	change.dir.nested.one = 0
	change.dir.nested.two = 1

	load_from_data(change)
	test_structure(change)
end

function g.test_etcd_load_overwrite_data_prefix()
	load_from_data(structure.dir, "etcd load /dir")

	local change = table.deepcopy(structure)
	change.dir.nested.one = 0
	change.dir.nested.two = 1

	load_from_data(change.dir.nested, "etcd load /dir/nested")
	test_structure(change)
end

function g.test_etcd_load_overwrite_data_etcd_prefix()
	load_from_data(structure.dir, "-p /dir etcd load /")

	local change = table.deepcopy(structure)
	change.dir.nested.one = 0
	change.dir.nested.two = 1

	load_from_data(change, "-p / etcd load /")
	test_structure(change)

	clear_etcd()

	load_from_data(structure.dir.nested, "etcd load /dir/nested")
	test_structure(structure)
	clear_etcd()

	load_from_data(structure.dir.nested, "-p /dir etcd load /nested")
	test_structure(structure)
	clear_etcd()

	load_from_data(structure.dir.nested, "-p /dir/nested etcd load /")
	test_structure(structure)
	clear_etcd()
end

function g.test_etcd_load_with_delete()
	load_from_data(structure)
	test_structure(structure)

	local change = table.deepcopy(structure)
	change.dir.nested.array = {tri = "third"}

	load_from_data(change, "etcd load --delete /")
	test_structure(change)
end

function g.test_etcd_load_no_change()
	load_from_data(structure)
	test_structure(structure)

	local change = table.deepcopy(structure)
	change.dir.nested.one = 4
	change.dir.one = 1

	load_from_data(change, "etcd load --append-only /")

	change.dir.nested.one = structure.dir.nested.one
	test_structure(change)
end

function g.test_etcd_load_no_change_with_delete()
	load_from_data(structure)
	test_structure(structure)

	local change = table.deepcopy(structure)
	change.dir.nested.one = 4
	change.dir.nested.array = nil

	load_from_data(change, "etcd load --append-only --delete /")

	change.dir.nested.one = structure.dir.nested.one
	test_structure(change)
end
