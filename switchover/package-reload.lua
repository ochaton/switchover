local fiber = require 'fiber'
local log = require 'log'
local out = require 'switchover._util'.out
local err = require 'switchover._util'.err

local M = {}

function M.run(args)
	assert(args.command == "package-reload")

	local order = {}

	if args.cluster or args.shard then
		---@type SwitchoverResolveAndDiscoveryInfo
		local rndinfo = require 'switchover.core.resolve_and_discovery'.cluster {
			cluster_or_shard = args.instance,
			cluster = args.cluster,
			shard = args.shard,
			discovery_timeout = args.discovery_timeout,
			selector = args.selector,
		}

		if args.cluster and not args.yes then
			io.stdout:write("You are going to reload all instances of the cluster. Are you sure? [yes/no] ")
			if io.stdin:read("*line") ~= "yes" then
				log.error("Discarding reload")
				return 1
			end
		end

		for _, sh in pairs(rndinfo.cluster_info) do
			local etcd_shard = sh.etcd_shard
			for _, inst in pairs(sh.selected_instances) do
				if inst.is_connected
					and inst.real_instance
					-- TODO: do we really need this condition?
					and etcd_shard:instance_by_uuid(inst.real_instance:uuid())
				then
					table.insert(order, inst.real_instance)
				end
			end
		end

		table.sort(order, function(a, b)
			local aro = a:ro() and 1 or 0
			local bro = b:ro() and 1 or 0
			return aro > bro -- read_only must come first
		end)
	else
		local candidate = require 'switchover.discovery'.candidate {
			instance = args.instance,
			discovery_timeout = args.discovery_timeout,
		}

		order = { candidate }
	end

	-- check reload will freeze:
	local will_freeze = 0
	for _, inst in ipairs(order) do
		if inst:ro() == false and inst:reload_will_freeze() then
			will_freeze = will_freeze + 1
		end
		if args.dry_run then
			out("dry run package.reload() on %s/%s - ok", inst:name(), inst:role())
		end
	end

	if will_freeze > 0 then
		if not args.allow_reload_during_snapshot then
			err("Reload will freeze due to box.snapshot() on some instances, discarding reload")
			return 1
		end
	end

	do
		local workers = tonumber(args.package_reload_jobs) or 1
		local pool = require 'sync.pool'.new("pr", workers)

		local first =  true
		for _, inst in ipairs(order) do
			if not first then
				fiber.sleep(tonumber(args.package_reload_interval) or 0)
			else
				first = false
			end
			pool:send(inst.package_reload_check_freeze, {inst, args.allow_reload_during_snapshot})
		end
		pool:terminate()
		pool:wait()
	end

	require 'switchover._background'.stop_all_fibers()
	if args.cluster then
		return require 'switchover.status'.run {
			command = 'status',
			cluster = args.instance,
			discovery_timeout = args.discovery_timeout,
			selector = args.selector,
		}
	end

	return require 'switchover.discovery'.run {
		command = 'discovery',
		endpoints = {args.instance},
		discovery_timeout = args.discovery_timeout,
	}
end

return M
