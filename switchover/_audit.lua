---@module 'switchover._audit'
local M = {}
M.__index = M

---@class AuditMessage
---@field shard_name string ShardName
---@field etcd_shard Cluster
---@field replicaset SwitchoverReplicaset
---@field resolution AutofailoverResolution
---@field message string

local log = require 'log'
local uuid = require 'uuid'
local fiber = require 'fiber'
local clock = require 'clock'
local background = require 'switchover._background'

---@param chan fiber.channel
---@return fun(any, any): AuditMessage
local function range(chan)
	return function () return chan:get() end
end

local function human_uptime(ts)
	local s = {}
	local orig = math.abs(ts)
	if ts > 86400 then
		local days = math.floor(ts/86400)
		ts = ts - days*86400
		table.insert(s, ("%dd"):format(days))
	end
	if ts > 3600 then
		local hours = math.floor(ts/3600)
		ts = ts - hours*3600
		table.insert(s, ("%dh"):format(hours))
	end
	if ts > 60 then
		local mins = math.floor(ts/60)
		ts = ts - mins*60
		table.insert(s, ("%dm"):format(mins))
	end
	if ts >= 1 then
		table.insert(s, ("%ds"):format(ts))
	end
	if orig < 1 then
		if orig < 0.001 then
			table.insert(s, ("%0.1fms"):format(orig*1000))
		else
			table.insert(s, ("%0.fms"):format(orig*1000))
		end
	end

	return table.concat(s, "")
end

function M.new(opts)
	assert(type(box.cfg) ~= 'function', "tarantool must be loaded")

	local self = setmetatable({}, M)
	self.journal = box.schema.space.create('journal', { if_not_exists = true })

	self.journal:format({
		{ name = 'time',       type = 'unsigned' },
		{ name = 'shard_name', type = 'string'   },
		{ name = 'resolution', type = 'string'   },
		{ name = 'message',    type = 'string', is_nullable = true },
	})

	self.journal:create_index('primary', {
		if_not_exists = true,
		parts = {
			{ 1, self.journal:format()[1].type },
			{ 2, self.journal:format()[2].type },
		}
	})

	self.alerts = box.schema.space.create('alerts', {
		temporary = true,
		if_not_exists = true,
	})
	self.alerts:format({
		{ name = 'uuid',       type = 'string'   },
		{ name = 'created_at', type = 'number'   },
		{ name = 'updated_at', type = 'number'   },
		{ name = 'shard_name', type = 'string'   },
		{ name = 'resolution', type = 'string'   },
	})

	self.alerts:create_index('primary', {
		if_not_exists = true,
		parts = {
			{ 1, self.alerts:format()[1].type },
		}
	})

	self.alerts:create_index('shard_resolution', {
		if_not_exists = true,
		parts = {
			{ 4, self.alerts:format()[4].type }, -- shard_name
			{ 5, self.alerts:format()[5].type }, -- resolution
		}
	})

	if box.space.state then
		box.space.state:drop()
	end
	self.state = box.schema.space.create('state', {
		temporary = true,
		if_not_exists = true,
	})
	self.state:format({
		{ name = 'instance_name',  type = 'string'   },
		{ name = 'updated_at',     type = 'unsigned' },
		{ name = 'etcd_info',      type = 'map'      },
		{ name = 'tarantool_info', type = 'map'      },
	})

	self.state:create_index('primary', {
		if_not_exists = true,
		parts = {
			{ 1, self.state:format()[1].type },
		},
	})

	self._chan = fiber.channel(1000)
	self._poller_w = background {
		name = 'audit/poller',
		restart = 60,
		func = function(_)
			self:_poller_f()
		end,
		on_fail = function(_, err)
			log.error(err)
		end,
	}

	self._state_ch = fiber.channel(1000)
	self._state_w = background {
		name = 'audit/writer',
		restart = 60,
		func = function(_)
			self:_state_f()
		end,
		on_fail = function(_, err)
			log.error(err)
		end,
	}

	self._rotar = background {
		name = 'rotate/',
		restart = 60,
		run_interval = 60,
		func = function()
			local max_rows = (opts or { max_rows = 1000 }).max_rows

			local nrec = 0
			while self.journal:len() > max_rows do
				box.begin()
					nrec = nrec + self.journal:pairs()
						:take(1000)
						:map(function(t) return self.journal:delete(t[1]) end)
						:length()
				box.commit()
			end

			if nrec > 0 then
				log.warn("rotated %d records from audit", nrec)
			end
		end,
	}

	return self
end

function M:notify(msg)
	if not self._chan:put(msg, 0) then
		log.warn("Message for audit was dropped")
	end
end

function M:_poller_f()
	fiber.name('audit/poll')

	for msg in range(self._chan) do
		self._state_ch:put(msg, 0)

		if msg.resolution == 'ok' then
			self:alert_off(msg)
		else
			self:alert_on(msg)
		end
	end
end

---@param msg AuditMessage
function M:alert_on(msg)
	local alert = self.alerts.index.shard_resolution:get({ msg.shard_name, msg.resolution })
	if alert then
		self.alerts:update({ alert.uuid }, {
			{ '=', 3, fiber.time() }
		})
	else
		box.atomic(function()
			self.journal:replace({
				fiber.time64(),
				msg.shard_name or "unknown",
				msg.resolution or "unknown",
				("Firing Alert %s"):format(msg.message or msg.resolution),
			})

			self.alerts:replace({
				uuid.str(),
				fiber.time(),
				fiber.time(),
				msg.shard_name or "unknown",
				msg.resolution or "unknown",
			})
			return 1
		end)
	end
end

---@param msg AuditMessage
function M:alert_off(msg)
	local alerts = self.alerts.index.shard_resolution:pairs({ msg.shard_name }):totable()
	if not #alerts == 0 then return end

	box.atomic(function()
		for _, alert in ipairs(alerts) do
				self.alerts:delete({ alert.uuid })

				self.journal:replace({
					fiber.time64(),
					alert.shard_name or "unknown",
					alert.resolution or "unknown",
					("OK - alert was firing %s - %s (for %s)"):format(
						os.date("%FT%T", alert.created_at),
						os.date("%FT%T"),
						human_uptime(fiber.time()-alert.created_at)
					),
				})
		end
	end)

end

function M:_state_f()
	for msg in range(self._state_ch) do
		local now = clock.time64()
		local max_t = self.state.index.primary:max()
		if max_t and now < max_t[2] then
			now = max_t[2] + 1
		end

		box.atomic(function()
			for _, ei in ipairs(msg.etcd_shard:instances()) do
				local ri = msg.replicaset.replicas[ei.instance_uuid]
				self.state:replace({ ei.name, now, ei, ri })
			end
		end)
	end
end

return M
