# Name

[![Coverage Status](https://coveralls.io/repos/gitlab/ochaton/switchover/badge.svg?branch=master)](https://coveralls.io/gitlab/ochaton/switchover?branch=master)

Switchover - performs discovery and consistent Master/Replica switch with ETCD as coordinator.
Instance must be configured with https://github.com/moonlibs/config.

## Table of Contents

- [Name](#name)
  - [Table of Contents](#table-of-contents)
  - [Status](#status)
  - [Version](#version)
  - [Installation](#installation)
  - [Development](#development)
  - [Synopsis](#synopsis)
  - [Usage](#usage)
    - [discovery](#discovery)
      - [with ETCD](#with-etcd)
      - [without ETCD](#without-etcd)
      - [graphviz](#graphviz)
    - [switch](#switch)
    - [promote](#promote)
    - [heal](#heal)
    - [restart-replication (rr)](#restart-replication-rr)
    - [package-reload (pr)](#package-reload-pr)
  - [Configuration](#configuration)

## Status

Under early development

## Version

This document describes switchover 2.0.0

## Installation

You may get stable version from https://gitlab.com/ochaton/switchover/-/packages

## Development

Required packages:

- luarocks >= 2.4.0

```bash
git clone https://gitlab.com/ochaton/switchover && cd switchover

docker network create tt_net
make switchover-1
switchover help
```

## Synopsis

```bash
$ switchover help
         -c <config>,                                       Path to config (default: /etc/switchover/config.yaml)
   --config <config>
   --no-config                                              Does not load config even if it exists
           -w <work_dir>,                                   Path to workdir where switchover will keep it's files (default: .)
   --work-dir <work_dir>
   --dns-timeout <dns_timeout>                              Sets timeout for dns resolve
       -e <etcd>,                                           Address to ETCD endpoint
   --etcd <etcd>
   --no-etcd                                                disables etcd
               -t <etcd_timeout>,                           Sets timeout to ETCD
   --etcd-timeout <etcd_timeout>
         -p <prefix>,                                       Prefix to configuration of clusters in ETCD
   --prefix <prefix>
   -q, --quiet                                              Disables logs (quiet mode)
   -v, --verbose                                            Verbosity level
                    -d <discovery_timeout>,                 Discovery timeout (in seconds)
   --discovery_timeout <discovery_timeout>
                   -f <discovery_format>,                   Discovery format
   --discovery-format <discovery_format>

Commands:
   help                                                     Show help for commands.
   version                                                  Shows switchover version
   discovery, d                                             Discovers all members of the replicaset
   status, s                                                Shows status of cluster or shard
   top, t                                                   Runs cluster or shard top
   promote                                                  Promotes given instance to master
   switch, sw                                               Switches current master to given instance
   auto                                                     Runs auto failover for single shard
   watch                                                    Watch of healthiness of cluster
   heal                                                     Heals ETCD /cluster/master
   restart-replication, rr                                  Restarts replication on choosen instance|shard|cluster
   resign                                                   Switches masters from server to another server
   eval                                                     Evals given command on instance or all instances
   package-reload, pr                                       Reload replication on given instance
   connect, c                                               Opens tarantool console to given instance
   etcd, e                                                  Interface to operate with ETCD

Home: https://gitlab.com/ochaton/switchover
```

Switchover is standalone script which allows SRE to change Leader role in any Tarantool replicaset.

Switchover shows topology of replication, determines lag, status of replication, auto discovers all members of replicaset (upstream-first search).

Additionally switchover can execute hot code reload on any instance of replicaset if it supports [package.reload](https://github.com/moonlibs/package-reload).

Switchover can restart replication on given instance (helpfull when replication was stopped because of network problems).

Mainly, switchover suggests best choice for the future leader according to topology of replication. Switchover provides 2 different mechanisms to promote node to leader:

* `switch`: downgrades current master to RO and promotes given replica to RW consistently as fast as possible. <b>Refuses switch if no master is found in replicaset.</b>
* `promote`: promotes given replica to RW. <b>Refuses operation if replicaset contains node in RW state.</b>

Both `promote` and `switch` by default requires ETCD to acquire mutex. Switchover may leave replicaset in RO-state if timeouts happened.

## Usage

This section describes usage of each command of switchover

### discovery

Discovers all reachable instances of replicaset and prints to the screen. Supports discovery from ETCD by name or by ip+port of given instances.

#### with ETCD

```bash
$ switchover help discovery
Usage: switchover discovery [-h] [--ero] [-g] [-l]
       [-o {plain,json,table}] <endpoints|shard>

Some of the examples:
        switchover discovery cluster_name/shard_name     - discovers nodes inside single shard inside cluster (requires ETCD)
        switchover discovery cluster_name@instance_name  - discovers nodes inside single shard inside cluster (requires ETCD)
        switchover discovery mysingle                    - discovers nodes from the single replicaset (not cluster, required ETCD)
        switchover discovery tnt1:3301,tnt2:3301         - performs upstream-first search of the replicas (requires full-mesh, no ETCD)


Arguments:
   endpoints|shard                                          host:port to tarantool or name of replicaset

Options:
   -h, --help                                               Show this help message and exit.
   --ero                                                    Allows ReadOnly requests from ETCD if ETCD leader is not discovered
   -g, --show-graph                                         Prints topology to the console in dot format
   -l, --link-graph                                         Build url to online visualization
                -o {plain,json,table},                      Discovery output format (default: plain)
   --output-format {plain,json,table}

$ switchover discovery single
Discovered 3/3 nodes from single_001:3301,single_002:3301,single_003:3301 in 0.049s
single_001 e:replica single_001:3301 2.8.2.0 id:1 s:running v:{"1":1085077,"2":2298546,"3":359700} r:ro t:1 l:0 s:follower   m:off  q:1 qo:0 600 ok/2:follow/0.000s
single_002 e:replica single_002:3301 2.8.2.0 id:3 s:running v:{"1":1085077,"2":2298546,"3":359700} r:ro t:1 l:0 s:follower   m:off  q:1 qo:0 600 ok/2:follow/0.000s
single_003 e:master  single_003:3301 2.8.2.0 id:2 s:running v:{"1":1085077,"2":2298546,"3":359700} r:rw t:1 l:0 s:follower   m:off  q:1 qo:0 400 self
```

Discovery with ETCD allows to connect to every instance in ETCD and check replication status to each neighbour.
It prints:

```plain
single_001 - instance name in ETCD

e:{replica, master} - e stands for ETCD, is this instance registered as master or replica

single_001:3301 - host:port to the instance

id:1 - numeric ID of the node (result of box.info.id)

s:running - status of the instance (result of box.info.status)

v:{"1":1085077,"2":2298546,"3":359700} - vclock of the instance (result of box.info.vclock)

r:ro - Is instance RO or RW (result of box.info.ro). RW = master, RO = replica

t, l, s, m - are RAFT specific. t = term, l = ID of leader, s = RAFT status, m = box.cfg.election_mode

q, qo - are Synchoronus Replication specific. q = quorum of synchro queue, qo = queue owner of synchro queue.

600 - score of the instance to be the master. (Currenctly work in progress). The higher score - the higher possibility that choosing this node to be the master is good idea.

ok/2:follow/0.000s - Status of the replication to ETCD master. ok - means replication is working. 2:follow - ID of the master and it's upstream status, 0.000s - is lag or idle of the replication flow.
```

#### without ETCD

```plain
$ switchover discovery single_001:3301

Discovered 3/1 nodes from single_001:3301 in 0.046s
single_001:3301 2.8.2.0 id:1 s:running v:{"1":1085077,"2":2298546,"3":359700} r:ro t:1 l:0 s:follower   m:off  q:1 qo:0 600 ok/2:follow/0.000s
single_003:3301 2.8.2.0 id:2 s:running v:{"1":1085077,"2":2298546,"3":359700} r:rw t:1 l:0 s:follower   m:off  q:1 qo:0 400 self
single_002:3301 2.8.2.0 id:3 s:running v:{"1":1085077,"2":2298546,"3":359700} r:ro t:1 l:0 s:follower   m:off  q:1 qo:0 600 ok/2:follow/0.000s
```

Also, if you don't have ETCD, you may perform discovery using just ip:port of any instance.

<b>Note! switchover performs discovery only through instances in box.cfg.replication.</b> It may lose some instances if replicaset does not use full-mesh topology.

Example:

```plain
M (master:3301) - (replicates to) -> R1 (replica1:3301)
M (master:3301) - (replicates to) -> R2 (replica2:3301)
```

Then performing `switchover discovery master:3301` will list only master.<br>
But calling `switchover discovery replica1:3301,replica2:3301` will list all 3 instances.

If you don't use ETCD then you better organize replication with full-mesh topology.

#### graphviz

Discovery may represent topology using dot:

```bash
$ switchover discovery -g tnt1:3301
digraph G {
	node[shape="circle"]
	tnt1_3301 [label="1/replica: {'1':251630,'2':382743}\ntnt1:3301"]
	tnt3_3301 [label="3/replica: {'1':251630,'2':382748}\ntnt3:3301"]
	tnt2_3301 [label="2/master: {'1':251630,'2':382749}\ntnt2:3301"]
	tnt1_3301 -> tnt2_3301 [style=solid,label="0.000s"]
	tnt1_3301 -> tnt3_3301 [style=solid,label="0.001s"]
	tnt3_3301 -> tnt1_3301 [style=solid,label="0.003s"]
	tnt3_3301 -> tnt2_3301 [style=solid,label="0.001s"]
	tnt2_3301 -> tnt1_3301 [style=solid,label="0.004s"]
	tnt2_3301 -> tnt3_3301 [style=solid,label="0.001s"]
}
```

Or even give you a link to render it in browser:

```bash
$ switchover discovery -gl tnt1:3301
```

In case of network partitioning discovery may work too long (at least 5 seconds). You can decrease this timeout specifying `--discovery-timeout` option.

### switch

Switch performs consistent switch of RW role in replicaset.
Replicaset must have single, alive master.

Algorithm of the switch:

1. Discovers as many instances of replicaset as possible.
2. Verifies that suggested `candidate` is legitimate leader:
   1. `Candidate` has enough downstreams.
   2. `Candidate` has alive upstream to master.
   3. Replication lag of this path is less than `max_lag` (default: 1 second).
3. Monitors that modifications from the `master` reaches `candidate` in reasonable time (less than `2*max_lag`)
4. Reserves intention in ETCD inside `<etcd_prefix>/<cluster_name>/clusters/<replicaset_name>/switchover` using compare-and-swap with `TTL = 3*max_lag`
5. Performs the switch:
   1. Connects to `master`
      1. switches `master` to `RO` calling `box.cfg{ read_only = true }`
      2. `fiber.yield()` to pass the way for other running fibers to finish their modifications
      3. Starts while-loop waiting until own vclock will match `candidate` vclock using `box.info.replication[candidate.id].downstream.vclock`.
         1. If deadline happens then rollback the switch calling `box.cfg{ read_only = false }` and stopping the switch.
         2. Otherwise, returns updated master vclock via `box.info.vclock`
   2. Connects to `candidate`
      1. Waits until `box.info.vclock` reaches `master` vclock.
         1. If deadline happens then fails the switch and goes to $5.5
         2. Otherwise calling `box.cfg{ read_only = false }` finishes the switch. -> $5.3
      2. If `switchover` catches timeout to `candidate` then it rechecks `box.info.ro` of `canidate` and only if `candidate` is still in RO performs $5.5.
   3. After 5.2.1.2 releases ETCD mutex calling compare-and-delete.
   4. Runs `switchover heal` to actualize leader of replicaset.
   5. (rollback case). Executed only when `master` and `candidate` are in `RO` state and `candidate` will not come leader eventually. It does the following:
      1. Checks that `candidate` is in RO.
      2. Connects to `master` and calls `box.cfg{ read_only = false }` to restore it's RW role.
      3. Releases ETCD mutex calling compare-and-delete
6. May call `package.reload` on `candidate` if switch was successfull (option `--with-reload`).

```bash
$ switchover switch single@single_002
# single@single_002: means application `single` and instance_name `single_002`
# Discover all instances of the replicaset from ETCD
2021-10-23T15:42:22.904 +61.6ms @_log.lua:12 I> Discovered 3/3 nodes from single_001:3301,single_002:3301,single_003:3301 in 0.061s

# Checking if candidate is good
2021-10-23T15:42:22.905 +62.1ms @_log.lua:12 I> Candidate single_002:3301 2.8.2.0 id:3 s:running v:{"1":1085077,"2":2298546,"3":359700} r:ro t:1 l:0 s:follower   m:off  q:1 qo:0 can be next leader
2021-10-23T15:42:22.905 +62.4ms @_log.lua:12 I> Candidate single_002:3301 2.8.2.0 id:3 s:running v:{"1":1085077,"2":2298546,"3":359700} r:ro t:1 l:0 s:follower   m:off  q:1 qo:0 can be next leader (current: 2/single_003:3301). Running replication check (safe)

# Checking replication flow
2021-10-23T15:42:22.907 +64.5ms @_log.lua:12 W> wait_clock(2298546, 2) on 3 succeed {"1":1085077,"2":2298546,"3":359700} => 0.0000s
2021-10-23T15:42:22.907 +64.6ms @_log.lua:12 I> Replication 2/single_003:3301 -> 3/single_002:3301 is good. Lag=0.0010s

# Do the switch algorithm
2021-10-23T15:42:22.914 +70.8ms @_log.lua:12 W> Running switch for: 2/ce343d92-3640-4146-8655-abb7d3501be7 (single_003:3301 vclock:{"1":1085077,"2":2298546,"3":359700}) -> 3/20ebffd7-f40e-4c26-8263-a2382f2edafe (single_002:3301 vclock: {"1":1085077,"2":2298546,"3":359700})

# Checking that master successfully switched to Read Only mode (became replica)
2021-10-23T15:42:22.915 +72.2ms @_log.lua:12 W> Master is in RO: single_003:3301 2.8.2.0 id:2 s:running v:{"1":1085077,"2":2298546,"3":359700} r:ro t:1 l:0 s:follower   m:off  q:1 qo:0 took -3.9994s

# Checking that candidate successfully switched to Read Write mode (became master)
2021-10-23T15:42:22.918 +75.1ms @_log.lua:12 W> Candidate is in RW state took: 0.0000s

# Changing ETCD state (performing CaS)
2021-10-23T15:42:22.924 +81.2ms @_log.lua:12 W> Switch 2/single_003:3301 -> 3/single_002:3301 was successfully done
2021-10-23T15:42:22.932 +89.0ms @_log.lua:12 I> Changing master in ETCD: single_003 -> single_002
2021-10-23T15:42:22.940 +96.8ms @_log.lua:12 I> ETCD response: {"action":"compareAndSwap","node":{"createdIndex":548,"modifiedIndex":564,"key":"\/apps\/single\/clusters\/single\/master","value":"single_002"},"prevNode":{"createdIndex":548,"modifiedIndex":561,"key":"\/apps\/single\/clusters\/single\/master","value":"single_003"}}

# If etcd is okey, then call package.reload (if instance support that)
2021-10-23T15:42:22.940 +96.9ms @_log.lua:12 W> Perfoming package.reload

# First reload new master
2021-10-23T15:42:22.940 +97.0ms @_log.lua:12 I> Calling package.reload on single_002:3301 2.8.2.0 id:3 s:running v:{"1":1085077,"2":2298546,"3":359700} r:rw t:1 l:0 s:follower   m:off  q:1 qo:0

# Then reload old master
2021-10-23T15:42:23.989 +1146.4ms @_log.lua:12 I> Calling package.reload on single_003:3301 2.8.2.0 id:2 s:running v:{"1":1085077,"2":2298546,"3":359700} r:ro t:1 l:0 s:follower   m:off  q:1 qo:0

# Perform switchover discovery single_002 again, to check that replicaset is okey
2021-10-23T15:42:25.034 +2191.6ms @_log.lua:12 I> Discovered 3/3 nodes from single_001:3301,single_002:3301,single_003:3301 in 2.191s
2021-10-23T15:42:25.035 +2192.6ms @_log.lua:12 I> single_001 e:replica single_001:3301 2.8.2.0 id:1 s:running v:{"1":1085077,"2":2298546,"3":359700} r:ro t:1 l:0 s:follower   m:off  q:1 qo:0 600 ok/3:follow/0.000s
2021-10-23T15:42:25.036 +2193.4ms @_log.lua:12 I> single_002 e:master  single_002:3301 2.8.2.0 id:3 s:running v:{"1":1085077,"2":2298546,"3":359700} r:rw t:1 l:0 s:follower   m:off  q:1 qo:0 400 self
2021-10-23T15:42:25.037 +2193.8ms @_log.lua:12 I> single_003 e:replica single_003:3301 2.8.2.0 id:2 s:running v:{"1":1085077,"2":2298546,"3":359700} r:ro t:1 l:0 s:follower   m:off  q:1 qo:0 600 ok/3:follow/0.000s
```

### promote

Promote may promote any suitable node to RW state. Promote can be used when all nodes in replicaset are in RO state.

Algorithm of promote:

1. Discovers as many instances of replicaset as possible.
2. Verifies that suggested `candidate` can be promoted to leader:
   1. `Candidate` has enough downstreams.
   2. Replication lag of this path is less than `max_lag` (default: 1 second).
3. Reserves intention in ETCD `<etcd_prefix>/<cluster_name>/clusters/<replicaset_name>/switchover` using compare-and-swap with `TTL = 3*max_lag`
4. Connects to `candidate` and calls `box.cfg{ read_only = false }`
5. Releases ETCD mutex calling compare-and-delete
6. Updates leader of replicaset in ETCD
7. May call `package.reload` on `candidate` if switch was successfull (option `--with-reload`).

Let's SIGSTOP master and promote live replica to RW

```bash
$ switchover discovery single
Took to long to discovery all instances. Continuing with instances which were discovered
Discovered 2/3 nodes from single_001:3301,single_002:3301,single_003:3301 in 3.043s
Replicaset has 0 masters
single_001 e:replica single_001:3301 2.8.2.0 id:1 s:running v:{"1":1085077,"2":2298546,"3":359700} r:ro t:1 l:0 s:follower   m:off  q:1 qo:0 200 fail/3:connect/7.299s err:timed out
single_002 e:master  single_002:3301 - not connected
single_003 e:replica single_003:3301 2.8.2.0 id:2 s:running v:{"1":1085077,"2":2298546,"3":359700} r:ro t:1 l:0 s:follower   m:off  q:1 qo:0 200 fail/3:connect/7.101s err:timed out
```

As we can see, switchover failed to connect to `single_002` and this instance is ETCD master.
We can't use `switch` to upgrade any live replica to RW. Instead we use `promote`.

```bash
$ switchover promote single@single_003
# First switchover takes --discovery-timeout (default: 3 seconds) to connect to each instance of replicaset
Took to long to discovery all instances. Continuing with instances which were discovered
Discovered 2/3 nodes from single_001:3301,single_002:3301,single_003:3301 in 3.045s

# After that switchover checks that candidate `single_003` is okey to be the master
Candidate single_003:3301 2.8.2.0 id:2 s:running v:{"1":1085077,"2":2298546,"3":359700} r:ro t:1 l:0 s:follower   m:off  q:1 qo:0 can be next leader

# switchover promotes candidate to RW
Candidate 2/single_003:3301 was promoted. Performing discovery

# And changes ETCD state of it
Changing master in ETCD: single_002 -> single_003
ETCD response: {"action":"compareAndSwap","node":{"createdIndex":548,"modifiedIndex":566,"key":"\/apps\/single\/clusters\/single\/master","value":"single_003"},"prevNode":{"createdIndex":548,"modifiedIndex":564,"key":"\/apps\/single\/clusters\/single\/master","value":"single_002"}}

# Finally switchover runs package.reload on promoted instance
Calling package.reload on single_003:3301 2.8.2.0 id:2 s:running v:{"1":1085077,"2":2298546,"3":359700} r:rw t:1 l:0 s:follower   m:off  q:1 qo:0

# And rediscovers state of the replicaset
Took to long to discovery all instances. Continuing with instances which were discovered
Discovered 2/3 nodes from single_001:3301,single_002:3301,single_003:3301 in 7.124s

# Now we see, that replication of `single_001` is ok to the `single_003`. But single_002 is still not connected.
# If you SIGCONT old master `single_002` there is chance that you will have split brain. Almost always you'll have to rebootstrap old master.
single_001 e:replica single_001:3301 2.8.2.0 id:1 s:running v:{"1":1085077,"2":2298546,"3":359700} r:ro t:1 l:0 s:follower   m:off  q:1 qo:0 400 ok/2:follow/0.000s
single_002 e:replica single_002:3301 - not connected
single_003 e:master  single_003:3301 2.8.2.0 id:2 s:running v:{"1":1085077,"2":2298546,"3":359700} r:rw t:1 l:0 s:follower   m:off  q:1 qo:0 200 self

```

Both `promote` and `switch` can be executed without ETCD mutex but that is strongly discouraged.

### heal

Handy command which actualizes ETCD configuration according to real cluster topology. Updates only `<etcd_prefix>/<cluster_name>/clusters/<replicaset_name>/master`. May refuse update if topology does not reach quorum.

Sometimes you failed to upgrade ETCD data, and get to situation when master in ETCD is different from real master in replicaset.

```bash
switchover discovery single
Discovered 3/3 nodes from single_001:3301,single_002:3301,single_003:3301 in 0.043s
single_001 e:replica single_001:3301 2.8.2.0 id:1 s:running v:{"1":1085077,"2":2298546,"3":359700} r:ro t:1 l:0 s:follower   m:off  q:1 qo:0 600 ok/2:follow/0.000s
single_002 e:replica single_002:3301 2.8.2.0 id:3 s:running v:{"1":1085077,"2":2298546,"3":359700} r:rw t:1 l:0 s:follower   m:off  q:1 qo:0 400 ok/2:follow/0.000s - etcd_not_synced
single_003 e:master  single_003:3301 2.8.2.0 id:2 s:running v:{"1":1085077,"2":2298546,"3":359700} r:ro t:1 l:0 s:follower   m:off  q:1 qo:0 600 self - etcd_not_synced
```

Then discovery will show you something like that. Sometimes you may use `switchover heal` command to actualize ETCD `master` flag.

```bash
$ switchover.lua heal single

Discovered 3/3 nodes from single_001:3301,single_002:3301,single_003:3301 in 0.046s

Changing master in ETCD: single_003 -> single_002

ETCD response: {"action":"compareAndSwap","node":{"createdIndex":548,"modifiedIndex":568,"key":"\/apps\/single\/clusters\/single\/master","value":"single_002"},"prevNode":{"createdIndex":548,"modifiedIndex":566,"key":"\/apps\/single\/clusters\/single\/master","value":"single_003"}}
```

### restart-replication (rr)

Sometimes replication may stop in case of M-M conflicts or missing xlogs or anything else. Some of this cases can be fixed just restarting replication:

```bash
$ switchover rr single@single_001
Discovered 3/3 nodes from single_001:3301,single_002:3301,single_003:3301 in 0.110s
single_001 e:replica single_001:3301 2.8.2.0 id:1 s:running v:{"1":1085077,"2":2298546,"3":359700} r:ro t:1 l:0 s:follower   m:off  q:1 qo:0 600 ok/3:follow/0.002s
single_002 e:master  single_002:3301 2.8.2.0 id:3 s:running v:{"1":1085077,"2":2298546,"3":359700} r:rw t:1 l:0 s:follower   m:off  q:1 qo:0 400 self
single_003 e:replica single_003:3301 2.8.2.0 id:2 s:running v:{"1":1085077,"2":2298546,"3":359700} r:ro t:1 l:0 s:follower   m:off  q:1 qo:0 600 ok/3:follow/0.000s
```

Switchover executes following script on given instance:

```lua
repl = box.cfg.replication box.cfg{replication={}} box.cfg{replication = repl}
```

### package-reload (pr)

Executes `package.reload()` on replicaset or given instance. Order of execution is undefined.

## Configuration

Mostly all options of switchover can be configured in file.

| Command line          | Config file        |
|-----------------------|--------------------|
| `--etcd`              | `etcd.endpoints`   |
| `--prefix`            | `etcd.prefix`      |
| `--verbose`           | `verbose`          |
| `--max-lag`           | `max_lag`          |
| `--with-reload`       | `with_reload`      |
| `--discovery-timeout` | `discovery_timeout`|
| `--color`             | `color`            |
| `--switch-timeout`    | `switch_timeout`   |

Example of working configuration:

```yaml
---
etcd:
  prefix: /cloud
  timeout: 3
  endpoints:
    - http://etcd0:2379
    - http://etcd1:2379
    - http://etcd2:2379
max_lag: 3
with_reload: true
color: true
discovery_timeout: 3
switch_timeout: 5
```
