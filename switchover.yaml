---
# configuration of ETCD endpoints
etcd:
  # root prefix inside ETCD (can't be overwritten)
  prefix: /
  # timeout for each request to ETCD
  timeout: 3
  # list of endpoints of ETCD (you should use clientURLs here)
  endpoints:
    - http://etcd0:2379
  ## login - (Basic Auth)
  # login: <username>
  ## password - (Basic Auth)
  # password: <password>
  ## http_params - additional http params for each request to ETCD
  # http_params:
  #   headers:
  #     X-Source: my-server-name

# max_lag - is maxmimum allowed Replication Lag between
# master and replicas for commands switch/promote/resign
max_lag: 3
# with_reload - (default: false) enables automatic package.reload()
# when instance support module tarantool/package-reload
with_reload: true
# color - (default: false) enables colored logs
color: true
# discovery_timeout - (default: 3, in seconds)
# default discovery timeout for replicas in the shard. Affects all commands
discovery_timeout: 3
# switch_timeout - (default: 2*max_lag, in seconds) - timeout for switch command
switch_timeout: 5

# dns_timeout - (default: 1.5, in seconds) specifies resolve timeout for fqdn listen-uris of tarantool instances
dns_timeout: 1.5

# top_interval - (default: 1, in seconds) set refresh interval of switchover top command
# same as switchover top --interval 1
top_interval: 1

## no_reload: disables package.reload for switch/promote/auto operations. (same as --no-reload)
# no_reload: false

## no_reload_routers: disables package.reload for switch/promote/auto operations. (same as --no-reload-routers)
# no_reload_routers: false

## allow_reload_during_snapshot: skips check of box.snapshot() for package-reload command for Tarantools ≤ 2.3.1
## does not affect Tarantool ≥ 2.3.1
# allow_reload_during_snapshot: false

## no_check_downstreams: allows switch/promote/auto when replica have no alive downstreams (another replica-fetchers)
# no_check_downstreams: false

## no_check_downstreams: allows switch/promote/auto when replica have no alive downstreams (another replica-fetchers)
# no_check_downstreams: false

## ignore_not_connected - (default: false) affects switchover resign
# when true resign ignores instances which are matched selector rule, but offline for switchover
# ignore_not_connected: false

## Autofailover options

# autofailover_timeout - (default: 5, in seconds) timeout for autofailover operation per shard.
# affects switchover auto. Same as flag switchover auto --timeout 5
autofailover_timeout: 5

## no_orphan_fix - enforces to not change repliacation_connect_quorum during autofailover
# no_orphan_fix: false

# with_auto_switch - (default: false) affects only autofailover.
# enables automatic switch when master is not discovered in replicaset
with_auto_switch: true
# with_auto_heal - (default: false) affects only autofailover.
# executes heal command when master is discovered but ETCD has another master registered
with_auto_heal: true
# with_auto_promote - (default: false) affects only autofailover.
# promotes discovered master when it is ReadOnly (anti-fencing)
with_auto_promote: true
# with_vshard_storage_fix - (default: false) affects only autofailover.
# calls package.reload on vshard.storage when it has outdated topology configuration for it's shard
with_vshard_storage_fix: true

# autofailover configuration
autofailover:
  # background option (should it be daemonized, default: false)
  background: false
  # memtx_memory for autofailover
  memtx_memory: 67108864
  # log configuration of autofailover (same as tarantool)
  log: 'pipe: tee -a /var/log/tarantool/switchover.log'
  # graphite configuration of autofailover
  graphite:
    host: 127.0.0.1
    port: 2003
    prefix: ""
  # configuration of autofailover audit
  audit:
    # maximum length of journal with records
    # each switch print 2 records
    # when cluster is ok, no records added
    # increase memtx_memory when you increase max_rows
    max_rows: 1000

# allow_vshard - (default: false) affects switch/auto/promote/resign.
# enables switch/auto/promote operations on vshard.storage
allow_vshard: true

# workdir - (default: .) affects switchover etcd load.
# path to directory where switchover will keep yaml backups of ETCD tree
workdir: data/

# example of short discovery format. affects only switchover discovery
short_discovery_format: >-
  {{endpoint}}
  role:{{info.ro and (cfg.read_only and 'replica' or 'replica/master') or 'master '}}
  status:{{info.status}}
  uptime:{{uptime}}
  ups:{{n_upstreams}}
  dws:{{n_downstreams}}

# example of verbose discovery format. affects only switchover discovery
verbose_discovery_format: >-
  {{info.id}}/{{endpoint}}
  role:{{info.ro and (cfg.read_only and 'replica' or 'replica/master') or 'master '}}
  status:{{info.status}}
  uptime:{{uptime}}
  v:{{info.vclock}}
  ups:{{n_upstreams}}
  dws:{{n_downstreams}}
  quota:{{("%s/%.2fGB"):format(slab.quota_used_ratio, slab.quota_size/2^30)}}
  {{stat.INSERT.rps > 0 and "I:"..stat.INSERT.rps or ''}}{{stat.REPLACE.rps > 0 and "R:"..stat.REPLACE.rps or ''}}{{stat.UPDATE.rps > 0 and "U:"..stat.UPDATE.rps or ''}}{{stat.DELETE.rps > 0 and "D:"..stat.DELETE.rps or ''}}

# example of default discovery format. affects only switchover discovery
discovery_format: >-
  id:{{info.id}}
  {{fqdn}}
  dc:{{etcd.dc or 'no'}}
  role:{{info.ro and (cfg.read_only and 'replica' or 'replica/master') or 'master '}}
  status:{{info.status}}
  uptime:{{uptime}}
  ups:{{n_upstreams}}
  dws:{{n_downstreams}}
  q:{{("%s/%.2fGB"):format(slab.quota_used_ratio, slab.quota_size/2^30)}}
  v:{{vclock}}
