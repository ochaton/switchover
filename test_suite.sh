#!/bin/sh

set -eu;

wait_etcd(){
	echo "Waiting ETCD";
	while true; do
		if curl -s --fail --connect-timeout 10 http://etcd0:2379/v2/members/leader; then
			break;
		fi;
		sleep 1;
	done;
	echo "ETCD is ready";
}

wait_vshard_online(){
	echo "Waiting vshard";
	switchover etcd wait --recursive --index 1 --timeout 3 /vshard;
	switchover etcd get -r /vshard;

	echo "Checking that etcd is healthy";
	switchover etcd health;
	num=$(switchover etcd version | grep -c "3.5.3");
	if [ "$num" != "3" ]; then
		echo "ETCD version must return 3 version of 3.5.3";
		exit 1;
	fi;

	while true; do
		if switchover -q eval vshard@storage_001_01 'return "storage_001_01"' \
			&& switchover -q eval vshard@storage_001_02 'return "storage_001_02"' \
			&& switchover -q eval vshard@storage_002_01 'return "storage_002_01"' \
			&& switchover -q eval vshard@storage_002_02 'return "storage_002_02"' \
			&& switchover -q eval vshard@router_001 'return "router_001"' \
			&& switchover -q eval vshard@router_002 'return "router_002"' \
			&& switchover -q eval vshard@router_003 'return "router_003"'
		then
			break;
		fi;
		sleep 1;
	done;

	echo "VShard - online";
}

wait_single_online(){
	echo "Waiting single";
	switchover etcd wait --recursive --index 1 --timeout 3 /single;
	switchover etcd get -r /single;

	echo "Checking that etcd is healthy";
	switchover etcd health;
	num=$(switchover etcd version | grep -c "3.5.3");
	if [ "$num" != "3" ]; then
		echo "ETCD version must return 3 version of 3.5.3";
		exit 1;
	fi;

	# wait until cluster will be alive
	while true; do
		if switchover -q eval single:master 'return 1' \
			&& switchover -q eval single@single_002 'return 1' \
			&& switchover -q eval single@single_003 'return 1'; then
			break;
		fi;
		sleep 1;
	done;

	echo "Single - online";
}

wait_proxy_online(){
	echo "Waiting proxy";
	switchover etcd wait --recursive --index 1 --timeout 3 /proxy;
	switchover etcd get -r /proxy;

	echo "Checking that etcd is healthy";
	switchover etcd health;
	num=$(switchover etcd version | grep -c "3.5.3");
	if [ "$num" != "3" ]; then
		echo "ETCD version must return 3 version of 3.5.3";
		exit 1;
	fi;

	# wait until cluster will be alive
	# wait until cluster will be alive
	while true; do
		if switchover -q eval proxy@proxy_001 'return 1' \
			&& switchover -q eval proxy@proxy_002 'return 1' \
			&& switchover -q eval proxy@proxy_003 'return 1'; then
			break;
		fi;
		sleep 1;
	done;

	echo "Proxy - online";
}

wait_cluster_online(){
	echo "Waiting cluster";
	switchover etcd wait --recursive --index 1 --timeout 3 /cluster;
	switchover etcd get -r /cluster;

	echo "Checking that etcd is healthy";
	switchover etcd health;
	num=$(switchover etcd version | grep -c "3.5.3");
	if [ "$num" != "3" ]; then
		echo "ETCD version must return 3 version of 3.5.3";
		exit 1;
	fi;

	# wait until cluster will be alive
	while true; do
		if switchover -q eval cluster@shard_001_01 'return "shard_001_01"' \
			&& switchover -q eval cluster@shard_001_02 'return "shard_001_02"' \
			&& switchover -q eval cluster@shard_002_01 'return "shard_002_01"' \
			&& switchover -q eval cluster@shard_002_02 'return "shard_002_02"'
		then
			break;
		fi;
		sleep 1;
	done;
	echo "Cluster - online"
}

vshard(){
	wait_etcd;
	wait_vshard_online;

	switchover status --alert-only vshard || exit 1;
	switchover status vshard || exit 1;

	echo "check that storage_001_01 is master in storage_001";
	sleep 0.5; switchover eval vshard/storage_001:master 'assert(config.get("sys.instance_name") == "storage_001_01" and not box.info.ro)';
	echo "check that storage_002_01 is master in storage_002";
	sleep 0.5; switchover eval vshard/storage_002:master 'assert(config.get("sys.instance_name") == "storage_002_01" and not box.info.ro)';

	echo "check that storage_001_02 is ro in storage_001";
	sleep 0.5; switchover eval vshard@storage_001_02 'assert(config.get("sys.instance_name") == "storage_001_02" and box.info.ro)';
	echo "check that storage_002_02 is ro in storage_002";
	sleep 0.5; switchover eval vshard@storage_002_02 'assert(config.get("sys.instance_name") == "storage_002_02" and box.info.ro)';

	echo "check that router_001 is rw";
	sleep 0.5; switchover eval vshard@router_001 'assert(config.get("sys.instance_name") == "router_001" and not box.info.ro)';
	echo "check that router_002 is rw";
	sleep 0.5; switchover eval vshard@router_002 'assert(config.get("sys.instance_name") == "router_002" and not box.info.ro)';
	echo "check that router_003 is rw";
	sleep 0.5; switchover eval vshard@router_003 'assert(config.get("sys.instance_name") == "router_003" and not box.info.ro)';

	echo "sleep 3 seconds before switching";
	sleep 3;

	echo "switch storage_001_01 -> storage_001_02";
	sleep 0.5; switchover switch vshard@storage_001_02;

	echo "check that storage_001_01 is ro";
	sleep 0.5; switchover eval vshard@storage_001_01 'assert(config.get("sys.instance_name") == "storage_001_01" and box.info.ro)';
	echo "check that storage_001_02 is rw";
	sleep 0.5; switchover eval vshard@storage_001_02 'assert(config.get("sys.instance_name") == "storage_001_02" and not box.info.ro)';

	echo "check that router_001 routes rw of storage_001 to storage_001_02";
	sleep 0.5; switchover eval vshard@router_001 'assert(vshard.router.route(1).master.name == "storage_001_02")';

	echo "check that router_001 routes rw of storage_001 to storage_001_02";
	sleep 0.5; switchover eval vshard@router_001 'assert(vshard.router.callrw(1, "dostring", {"return config.get([[sys.instance_name]])"}) == "storage_001_02")';

	echo "switch storage_001_02 => storage_001_01";
	sleep 0.5; switchover switch vshard@storage_001_01;

	echo "check that router_001 routes rw of storage_001 to storage_001_01";
	sleep 0.5; switchover eval vshard@router_001 'assert(vshard.router.route(1).master.name == "storage_001_01")';

	echo "check that router_001 routes rw of storage_001 to storage_001_01";
	sleep 0.5; switchover eval vshard@router_001 'assert(vshard.router.callrw(1, "dostring", {"return config.get([[sys.instance_name]])"}) == "storage_001_01")';

	echo "switch storage_001: storage_001_01 -> storage_001_02 without reload";
	sleep 0.5; switchover switch vshard@storage_001_02 --no-reload;

	echo "check that alert NON_MASTER is on";
	sleep 0.5; switchover -q status --only-alert vshard | grep NON_MASTER;

	echo "Getting last reloaded";
	last_reloaded=$(switchover -q eval vshard@storage_001_02 'return package.reload.reloaded' | grep '\- ');

	echo "last_reloaded at $last_reloaded";

	sleep 0.5; switchover watch start vshard &

	test_passed="false"

	for _ in $(seq 1 10); do
		sleep 0.5;
		reloaded=$(switchover -q eval vshard@storage_001_02 'package.reload.reloaded' | grep '\- ');

		echo "$reloaded";

		if [ "$reloaded" != "$last_reloaded" ]; then
			if ! switchover -q status --only-alert vshard | grep NON_MASTER; then
				test_passed="true";
				break;
			fi;
		fi;
	done;

	switchover watch stop vshard;

	if [ "$test_passed" = "false" ]; then
		echo "autofailover failed to fix vshard topology";
		exit 1;
	fi;

	echo "watch succeeded to fix vshard topology issue";
}

single(){
	wait_etcd;
	wait_single_online;

	switchover status --alert-only single || exit 1;
	switchover status single || exit 1;

	echo "check that single_001 is online and is master"
	sleep 0.5; switchover eval single:master 'assert(config.get("sys.instance_name") == "single_001")';
	echo "check that single_002 is online"
	sleep 0.5; switchover eval single@single_002 'assert(config.get("sys.instance_name") == "single_002")'
	echo "check that single_003 is online"
	sleep 0.5; switchover eval single@single_003 'assert(config.get("sys.instance_name") == "single_003")'

	echo "sleep 3 seconds before testing";
	sleep 3;

	echo "switch single_001->single_002"
	sleep 0.5; switchover switch single@single_002

	echo "check that etcd master is single_002"
	sleep 0.5; switchover eval single:master 'assert(config.get("sys.instance_name") == "single_002")'
	echo "check that single_001 is ro"
	sleep 0.5; switchover eval single@single_001 'assert(box.info.ro)'
	echo "check that single_003 is ro"
	sleep 0.5; switchover eval single@single_003 'assert(box.info.ro)'
	echo "check that single_002 is rw"
	sleep 0.5; switchover eval single@single_002 'assert(not box.info.ro)'

	# Check that reload works
	echo "Cheking pr of single instance"
	sleep 0.5; switchover pr single@single_001

	echo "Cheking pr of :rw"
	sleep 0.5; switchover pr single:rw

	echo "Cheking pr of shard"
	sleep 0.5; switchover pr --shard single

	echo "Cheking pr of cluster"
	sleep 0.5; switchover pr --cluster --yes single

	echo "Cheking pr of shard with interval"
	sleep 0.5; switchover pr --shard --interval 1 single

	echo "Cheking pr of shard with jobs (3)"
	sleep 0.5; switchover pr --shard --jobs 3 single

	echo "Cheking pr of shard with jobs (2)"
	sleep 0.5; switchover pr --shard --jobs 2 single

	echo "Checking dry-run pr of shard"
	sleep 0.5; switchover pr --dry-run --shard single

	echo "Checking dry-run pr of shard (jobs=2)"
	sleep 0.5; switchover pr --dry-run --jobs 2 --shard single

	echo "Checking rr instance"
	sleep 0.5; switchover rr single@single_001

	echo "Checking rr shard"
	sleep 0.5; switchover rr --shard single

	echo "Checking rr shard jobs=3"
	sleep 0.5; switchover rr --shard --jobs 3 single

	echo "Checking rr shard jobs=2"
	sleep 0.5; switchover rr --shard --jobs 2 single

	echo "Checking rr cluster"
	sleep 0.5; switchover rr --cluster --yes single

	echo "Checking rr cluster (dry-run)"
	sleep 0.5; switchover rr --cluster --dry-run --yes single

	# Malform ETCD
	echo "set etcd master to single_001 with force"
	sleep 0.5; switchover etcd set /single/clusters/single/master "single_001";
	echo "check that single:master now points to single_001"
	sleep 0.5; switchover eval single:master 'assert(config.get("sys.instance_name") == "single_001")'
	echo "check that single:master is still ro (changing master in etcd does nothing)"
	sleep 0.5; switchover eval single:master 'assert(box.info.ro)'
	echo "check that single_002 is still rw"
	sleep 0.5; switchover eval single@single_002 'assert(not box.info.ro)'

	# Fix ETCD
	echo "heal etcd"
	sleep 0.5; switchover heal single

	# Check that fixed:
	echo "check that etcd healed and master is single_002 now"
	sleep 0.5; switchover eval single:master 'assert(config.get("sys.instance_name") == "single_002")'

	# Ruin master
	echo "ro single_002 with force inside tarantool"
	sleep 0.5; switchover eval single:master 'box.cfg{read_only=true}'

	echo "check that switch refuses to work"
	if switchover switch single@single_001; then
		echo "switch must fail if no master is discovered in replicaset";
		exit 1;
	fi;

	echo "check that heal refuses to work"
	if switchover heal single; then
		echo "switchover heal must fail if no master is discovered in replicaset";
		exit 1;
	fi;

	# promote master back
	echo "promote single:master back"
	sleep 0.5; switchover promote single:master;
	echo "check that etcd master does not changed and it is single_002"
	sleep 0.5; switchover eval single:master 'assert(config.get("sys.instance_name") == "single_002")'
	echo "check single_002 is rw"
	sleep 0.5; switchover eval single:master 'assert(not box.info.ro)'

	echo "check that heal exits with 0 when ETCD and Replicaset master are synchronized";
	sleep 0.5; switchover heal single

	echo "check that heal refuses to work without etcd";
	if switchover --no-config heal single_001:3301; then
		echo "heal must not work without etcd";
		exit 1;
	fi;

	# Ruin master again
	echo "ro master (single_002) again"
	sleep 0.5; switchover eval single:master 'box.cfg{read_only=true}';

	# check that master became ro
	echo "check that master (single_002) is ro"
	sleep 0.5; switchover eval single:master 'assert(box.info.ro)';

	# check that auto does not promote master
	sleep 0.5;
	echo "check that auto refuses to change master state"
	if switchover auto single; then
		echo "auto must not promote master even it is exists";
		exit 1;
	fi;

	# check that master still ro
	echo "check that master (single_002) still ro after auto calls";
	sleep 0.5; switchover eval single:master 'assert(box.info.ro)';

	# promote master back
	echo "promote master (single_002) back";
	sleep 0.5; switchover promote single:master;

	echo "start switching (single_002 -> single_003)";
	sleep 0.5; switchover switch single@single_003;
	echo "start switching (single_003 -> single_002)";
	sleep 0.5; switchover switch single@single_002;
	echo "start switching (single_002 -> single_001)";
	sleep 0.5; switchover switch single@single_001;

	echo "exit single_003";
	sleep 0.5; switchover eval single@single_003 'os.exit(0)' || true;

	echo "check that discovery works but slow";
	sleep 0.5; switchover discovery single;

	echo "check that status works but slow";
	sleep 0.5; switchover status single;

	echo "check that switch works even if replica down (single_001 -> single_002)";
	sleep 0.5; switchover switch single@single_002

	echo "fence master to check that promote works";
	sleep 0.5; switchover eval single:master 'box.cfg{read_only=true}'

	echo "check that master (single_002) is ro";
	sleep 0.5; switchover eval single:master 'assert(box.info.ro)'
	sleep 0.5; switchover eval single:master 'assert(config.get("sys.instance_name") == "single_002")'

	echo "check that we allowed to promote single_001 when single_003 is down";
	sleep 0.5; switchover promote single@single_001;

	# return single_003 back
	sleep 0.5; docker start single_003

	echo "Wait until single_003 is up"
	while true; do
		if switchover -q eval single@single_003 'return 1'; then
			break;
		fi;
		sleep 1;
	done;

	echo "Check that single_001 is rw";
	sleep 0.5; switchover eval single:master 'assert(config.get("sys.instance_name") == "single_001")'

	echo "Check that single_003 is ro";
	sleep 0.5; switchover eval single@single_003 'assert(box.info.ro)'

	echo "Run selector to get master";
	sleep 0.5; switchover status --selector '!is_ro' single | grep single_001

	echo "Run selector to check that single_003 is up"
	sleep 0.5; switchover status --selector 'is_ro,fqdn=single_003' single | grep -v 'not connected' | grep single_003

	echo "Resign master from single_001";
	sleep 0.5; echo "yes" | switchover resign --selector 'fqdn=single_001' single | grep -v single_001

	echo "Check that single_001 was resigned"
	sleep 0.5; switchover eval single@single_001 'assert(box.info.ro)'

	echo "Check that another instance took role of the master"
	sleep 0.5; switchover eval single:rw 'assert(not box.info.ro and config.get("sys.instance_name") ~= "single_001")'

	echo "Manual step down current master"
	sleep 0.5; switchover eval single:rw 'box.cfg{read_only=true}'

	echo "Check that resign refuses to work"
	sleep 0.5;
	if switchover resign --ignore-not-connected --yes --selector '!is_ro' single; then
		echo "Resign should be exited with errno=1";
		exit 1;
	fi;

	echo "Autopromote master"
	sleep 0.5; switchover promote single:rw;

	echo "Check that now master can be resigned";
	sleep 0.5; switchover resign --yes --selector '!is_ro' single 2>&1
}

test_discovery(){
	echo "Testing discovery";
	for key in single single@single_001 single@single_002 single@single_003 single/single; do
		single_res=$(switchover discovery "$key");
		echo "$single_res" | grep single_001;
		echo "$single_res" | grep single_002;
		echo "$single_res" | grep single_003;

		switchover discovery -gl "$key";

		out_json=$(switchover discovery -o json "$key");
		names=$(echo "$out_json" | jq -r '.[].instance_name')

		echo "switchover discovery $key => $names";
		echo "$names" | grep single_001;
		echo "$names" | grep single_002;
		echo "$names" | grep single_003;
		if [ 3 != "$(echo "$out_json" | jq -r '.[].connected' | grep -c "true")" ]; then
			echo "switchover discovery -o json $key retuned not 3 connected";
			exit 1;
		fi;
		if [ 2 != "$(echo "$out_json" | jq -r '.[].info.ro' | grep -c "true")" ]; then
			echo "switchover discovery -o json $key retuned not 2 info.ro";
			exit 1;
		fi;
	done;

	for key in vshard@storage_001_01 vshard@storage_001_02 vshard/storage_001; do
		res=$(switchover discovery "$key");
		echo "$res" | grep storage_001_01;
		echo "$res" | grep storage_001_02;

		switchover discovery -gl "$key";

		out_json=$(switchover discovery -o json "$key");
		names=$(echo "$out_json" | jq -r '.[].instance_name')

		echo "switchover discovery $key => $names";
		echo "$names" | grep storage_001_01;
		echo "$names" | grep storage_001_02;
		if [ 2 != "$(echo "$out_json" | jq -r '.[].connected' | grep -c "true")" ]; then
			echo "switchover discovery -o json $key retuned not 2 connected";
			exit 1;
		fi;
		if [ 1 != "$(echo "$out_json" | jq -r '.[].info.ro' | grep -c "true")" ]; then
			echo "switchover discovery -o json $key retuned not 1 info.ro";
			exit 1;
		fi;
	done;

	for key in vshard@router_001; do
		res=$(switchover discovery "$key");
		echo "$res" | grep router_001;

		switchover discovery -gl "$key";

		out_json=$(switchover discovery -o json "$key");
		names=$(echo "$out_json" | jq -r '.[].instance_name')

		echo "switchover discovery $key => $names";
		echo "$names" | grep router_001;
		if [ 1 != "$(echo "$out_json" | jq -r '.[].connected' | grep -c "true")" ]; then
			echo "switchover discovery -o json $key retuned not 2 connected";
			exit 1;
		fi;
		if [ 0 != "$(echo "$out_json" | jq -r '.[].info.ro' | grep -c "true")" ]; then
			echo "switchover discovery -o json $key retuned not 0 info.ro";
			exit 1;
		fi;
		if [ 1 != "$(echo "$out_json" | jq -r '.[].info.ro' | grep -c "false")" ]; then
			echo "switchover discovery -o json $key retuned not 1 !info.ro";
			exit 1;
		fi;
	done;

	for key in cluster@shard_002_01 cluster@shard_002_02 cluster/shard_002; do
		res=$(switchover discovery "$key");
		echo "$res" | grep shard_002_01;
		echo "$res" | grep shard_002_02;

		switchover discovery -gl "$key";

		out_json=$(switchover discovery -o json "$key");
		names=$(echo "$out_json" | jq -r '.[].instance_name')

		echo "switchover discovery $key => $names";
		echo "$names" | grep shard_002_01;
		echo "$names" | grep shard_002_02;
		if [ 2 != "$(echo "$out_json" | jq -r '.[].connected' | grep -c "true")" ]; then
			echo "switchover discovery -o json $key retuned not 2 connected";
			exit 1;
		fi;
		if [ 1 != "$(echo "$out_json" | jq -r '.[].info.ro' | grep -c "true")" ]; then
			echo "switchover discovery -o json $key retuned not 1 info.ro";
			exit 1;
		fi;
	done;

	echo "discovery - ok";
}

test_switch() {
	echo "Testing switch";

	# single_001 is rw
	echo "Switching single";
	for key in single_002 single_003 single_001; do
		switchover switch "single@$key";
		sleep 0.5;

		switchover eval single:rw 'assert(config.get("sys.instance_name") == "'"$key"'")'
	done;

	echo "Switching cluster";
	for key in shard_001_02 shard_001_01 shard_002_02 shard_002_01; do
		switchover switch "cluster@$key";
		sleep 0.5;

		shard_name=$(echo "$key" | sed -e 's/_0[12]$//g');
		switchover eval "cluster/$shard_name:rw" 'assert(config.get("sys.instance_name") == "'"$key"'")'
	done;

	echo "Switching vshard (no-reload-routers)";
	for key in storage_001_02 storage_001_01 storage_002_02 storage_002_01; do
		switchover switch --allow-vshard --no-reload-routers "vshard@$key";
		sleep 0.5;

		shard_name=$(echo "$key" | sed -e 's/_0[12]$//g');
		switchover eval "vshard/$shard_name:rw" 'assert(config.get("sys.instance_name") == "'"$key"'")'
	done;

	echo "Switching vshard";
	for key in storage_001_02 storage_001_01 storage_002_02 storage_002_01; do
		switchover switch --allow-vshard "vshard@$key";
		sleep 0.5;

		shard_name=$(echo "$key" | sed -e 's/_0[12]$//g');
		switchover eval "vshard/$shard_name:rw" 'assert(config.get("sys.instance_name") == "'"$key"'")'
	done;

	echo "switch - ok";
}

release(){
	apk add -u jq;

	wait_etcd;

	wait_vshard_online;
	wait_single_online;
	wait_cluster_online;
	wait_proxy_online;

	# Success cases
	echo "Tesing success cases";
	switchover version;

	test_discovery;
	test_switch;

	# test_promote
	# test_heal
	# test_auto
	# test_status
	# test_etcd
}

for arg in "$@"; do
	case "$arg" in
		"single")
			single;;
		"vshard")
			vshard;;
		"release")
			release;;
	esac;
done;
