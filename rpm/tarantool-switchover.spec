Name: tarantool-switchover
Version: 1.0.1
Release: 1%{?dist}
Summary: Handy script to perform consistent switch of Master role in Tarantool replicaset
Group: Applications/Databases
License: BSD
URL: https://gitlab.com/ochaton/switchover
Source0:https://gitlab.com/ochaton/switchover/archive/%{version}/%{name}-%{version}.tar.gz
BuildArch: noarch
BuildRequires: tarantool >= 1.10
BuildRequires: tarantool-devel >= 1.10
Requires: tarantool >= 1.10
%description
Handy script to perform consistent switch of Master role in Tarantool replicaset

%prep
%setup -q -n %{name}-%{version}

%build

pwd
LUAROCKS_CONFIG=%{_builddir}/.rocks/config-5.1.lua tarantoolctl rocks --server=https://moonlibs.org make switchover-scm-1.rockspec
echo %{_builddir}
find .

%define luapkgdir %{_datadir}/tarantool/

%install

# Create /usr/bin/ and binary there
mkdir -p %{buildroot}%{_bindir}
install -m 0755 .rocks/bin/switchover %{buildroot}%{_bindir}/switchover

# Recursive copy source code to /usr/share/tarantool/switchover
cp -vaR .rocks/share %{buildroot}%{_prefix}/

# remove /usr/share/tarantool/switchover.lua
rm -vf %{buildroot}%{luapkgdir}/switchover.lua

# remove /usr/share/tarantool/rocks
rm -rvf %{buildroot}%{luapkgdir}/rocks

# Set version to /usr/bin/switchover
sed -i 's/_VERSION_/%{version}/g' %{buildroot}%{_bindir}/switchover

# install /etc/switchover
install -dm 0755 %{buildroot}/etc/switchover

# copy default configuration to /etc/switchover
install -pm 0644 switchover.yaml %{buildroot}/etc/switchover/config.yaml.example
install -pm 0644 autofailover.yaml %{buildroot}/etc/switchover/autofailover.yaml
install -pm 0644 autofailover.lua %{buildroot}/etc/switchover/autofailover.lua

%files
%{_bindir}/switchover
%dir %{luapkgdir}
%{luapkgdir}/switchover/*.lua
%{luapkgdir}/switchover/core/*.lua
%{luapkgdir}/sync.lua
%{luapkgdir}/lulpeg.lua
%{luapkgdir}/sync/*.lua
%config(noreplace) /etc/switchover/config.yaml.example
%config(noreplace) /etc/switchover/autofailover.yaml
%config(noreplace) /etc/switchover/autofailover.lua
%doc README.md
%doc Changelog.md
%{!?_licensedir:%global license %doc}
%license LICENSE

%changelog
* Wed Dec 30 2020 Vladislav Grubov <v.grubov@corp.mail.ru> 1.0.0-1
- Initial version of the RPM spec
