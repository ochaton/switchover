#!/usr/bin/env tarantool
local fio = require 'fio'

local paths = package.path:split ";"
table.insert(paths, "/usr/local/opt/luarocks/share/lua/5.3/?.lua")
table.insert(paths, "/usr/local/opt/luarocks/share/lua/5.3/?/init.lua")
table.insert(paths, "/usr/local/opt/luarocks/share/lua/5.1/?.lua")
table.insert(paths, "/usr/local/opt/luarocks/share/lua/5.1/?/init.lua")

table.insert(paths, "/usr/local/share/lua/5.1/?.lua")
table.insert(paths, "/usr/local/share/lua/5.1/?/init.lua")
table.insert(paths, "/usr/local/share/lua/5.3/?.lua")
table.insert(paths, "/usr/local/share/lua/5.3/?/init.lua")

table.insert(paths, "/usr/share/lua/5.1/?.lua")
table.insert(paths, "/usr/share/lua/5.1/?/init.lua")
table.insert(paths, "/usr/share/lua/5.3/?.lua")
table.insert(paths, "/usr/share/lua/5.3/?/init.lua")

package.path = table.concat(paths, ";")

local function recurse(root)
	root:gsub("/+$", "")
	local nodes = { root }

	local map = {}
	while #nodes > 0 do
		local node = table.remove(nodes)

		local items = assert(fio.listdir(node))
		for _, it in ipairs(items) do
			local path = node .. '/' .. it

			if fio.path.is_dir(path) then
				table.insert(nodes, path)
			elseif not path:match("_test%.lua$") and not path:match("_example%.lua$") then
				local key = path:gsub("/", "."):gsub("%.lua$", ""):gsub("%.init$", "")
				map[key] = path
			end
		end
	end

	return map
end

local rockspec_file = 'switchover-scm-1.rockspec'
local modules = recurse "switchover"
modules.switchover = 'switchover.lua'

local persist = require 'luarocks.persist'
local type_rockspec = require 'luarocks.type.rockspec'

local rockspec = assert(persist.load_into_table(rockspec_file))

rockspec.build.modules = modules
assert(persist.save_from_table(rockspec_file, rockspec, type_rockspec.order))
