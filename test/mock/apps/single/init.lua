#!/usr/bin/env tarantool
require 'strict'.on()
local fio = require 'fio'
print(fio.cwd(), fio.abspath(fio.cwd()))
print(fio.pathjoin(fio.abspath(fio.cwd()), ".rocks/share/lua/5.1/"))
package.path = fio.pathjoin(
	fio.abspath(fio.cwd()), ".rocks/share/lua/5.1/").."/?.lua"..";"..package.path

local fiber = require 'fiber'
local log   = require 'log'
local http  = require 'http.client'

local to = 0.01
repeat
	log.info("Waiting for etcd0:2379")
	fiber.sleep(to)
	to = math.min(to*2, 5)
until http.get('http://etcd0:2379/v2/keys/'..os.getenv "TT_APP_ETCD_PREFIX", {timeout = 3}).status == 200

require 'package.reload'
require 'config' {
	master_selection_policy = 'etcd.cluster.master',
	file          = 'conf.lua',
	mkdir         = true,
	instance_name = os.getenv('TT_INSTANCE_NAME'),
	on_load       = function(_,cfg)
		if cfg.box.election_mode then
			cfg.box.read_only = false
			cfg.box.replication_synchro_quorum = "N/2+1"
			cfg.box.replication_connect_quorum = 2
		end
	end;
	on_after_cfg = function()
		if not box.info.ro then
			log.info("Granting super to guest")
			box.schema.user.grant('guest', 'super', nil, nil, { if_not_exists = true })

			log.info("Creating space T")
			box.schema.space.create('T', { if_not_exists = true })
			log.info("Creating index T/I")
			box.space.T:create_index('I', { if_not_exists = true })
		end
	end,
}

fiber.create(function(gen)
	fiber.name('pusher/'..gen)
	while package.reload.count == gen and box.info.ro and not pcall(box.ctl.wait_rw, 1) do
		fiber.testcancel()
	end
	if package.reload.count ~= gen then log.info("not my gen") return end

	log.info("starting load")

	for _ = 1, 10 do
		fiber.create(function()
			while package.reload.count == gen do
				box.begin()
					for _ = 1, 50 do
						box.space.T:insert{box.space.T:len(), fiber.time(), box.info.id, box.info.vclock}
					end
				box.commit()
				fiber.sleep(0.1)
			end
			log.info("Leaving fiber")
		end)
	end
end, package.reload.count)