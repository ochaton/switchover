#!/bin/sh

tarantoolctl rocks install --server=https://moonlibs.org --global config 0.6.2
tarantoolctl rocks install --server=https://moonlibs.github.io/rocks --global package-reload scm-1

exec "$@";
