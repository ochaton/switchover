#!/usr/bin/env tarantool
require 'strict'.on()
local fio = require 'fio'
print(fio.cwd(), fio.abspath(fio.cwd()))
print(fio.pathjoin(fio.abspath(fio.cwd()), ".rocks/share/lua/5.1/"))
package.path = fio.pathjoin(
	fio.abspath(fio.cwd()), ".rocks/share/lua/5.1/").."/?.lua"..";"..package.path

local fiber = require 'fiber'
local log   = require 'log'
local http  = require 'http.client'

repeat
	log.info("Waiting for etcd0:2379")
	fiber.sleep(1)
until http.get('http://etcd0:2379/v2/keys/'..os.getenv "TT_APP_ETCD_PREFIX", {timeout = 3}).status == 200

require 'package.reload'
require 'config' {
	master_selection_policy = 'etcd.cluster.raft',
	file          = 'conf.lua',
	mkdir         = true,
	instance_name = os.getenv('TT_INSTANCE_NAME'),
}

log.info("After boxcfg")

box.once('grant:v1', function()
	box.schema.user.grant('guest', 'super', nil, nil, { if_not_exists = true })
end)

box.once('schema:v1', function()
	box.schema.space.create('T', { if_not_exists = true })
	box.space.T:create_index('I', { if_not_exists = true })
end)

fiber.create(function(gen)
	fiber.name('pusher/'..gen)
	while package.reload.count == gen and box.info.ro and not pcall(box.ctl.wait_rw, 1) do
		fiber.testcancel()
	end
	if package.reload.count ~= gen then log.info("not my gen") return end

	log.info("starting load")
	while package.reload.count == gen do
		for _ = 1, 10 do
			box.space.T:insert{box.space.T:len(), fiber.time(), box.info.id, box.info.vclock}
		end
		fiber.sleep(0.01)
	end
	log.info("Leaving fiber")
end, package.reload.count)