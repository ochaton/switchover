etcd = {
	endpoints = {
		"http://etcd0:2379",
		"http://etcd1:2379",
		"http://etcd2:2379",
	},
	prefix = os.getenv("TT_APP_ETCD_PREFIX"),
	timeout = 30,
	instance_name = assert(os.getenv("TT_INSTANCE_NAME"), "instance_name is required"),
	boolean_auto = true,
	integer_auto = true,
}

box = {
	background = false,
	vinyl_memory = 0,
}
