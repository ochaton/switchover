#!/usr/bin/env tarantool
require 'strict'.on()
local fio = require 'fio'
print(fio.cwd(), fio.abspath(fio.cwd()))
print(fio.pathjoin(fio.abspath(fio.cwd()), ".rocks/share/lua/5.1/"))
package.path = fio.pathjoin(
	fio.abspath(fio.cwd()), ".rocks/share/lua/5.1/").."/?.lua"..";"..package.path

local fiber = require 'fiber'
local log   = require 'log'
local http  = require 'http.client'
local clock = require 'clock'
require'metrics'.enable_default_metrics()

repeat
	log.info("Waiting for etcd0:2379")
	fiber.sleep(1)
until http.get('http://etcd0:2379/v2/keys/'..os.getenv "TT_APP_ETCD_PREFIX", {timeout = 30}).status == 200

require 'package.reload'
require 'config' {
	master_selection_policy = 'etcd.cluster.master',
	file          = 'conf.lua',
	mkdir         = true,
	instance_name = os.getenv('TT_INSTANCE_NAME'),
	on_before_cfg = function(_, cfg)
		if pcall(function() return box.info end) then
			return
		end
		if cfg.app.slow_load then
			fiber.create(function()
				fiber.name("while-true-loop")
				local gen = package.reload.count
				local to = tonumber(cfg.app.slow_timeout) or 0.5
				local sleep = tonumber(cfg.app.slow_sleep) or 0

				while not pcall(function() return box.info end) do
					fiber.yield()
				end
				log.info("box.info.status: %s {%s:%s}", box.info.status, to, sleep)

				while gen == package.reload.count and box.info.status == "loading" do
					local deadline = clock.time()+to
					repeat until clock.time()>deadline
					fiber.sleep(sleep)
					log.info("Executing slow loop for %s:%s", to, sleep)
				end
				log.info("Exiting heavy cpu load: %s", box.info.status)
			end)
		end
	end,
	on_load       = function(_,cfg)
		log.info(cfg)
		if cfg.box.election_mode then
			cfg.box.read_only = false
			cfg.box.replication_synchro_quorum = "N/2+1"
			cfg.box.replication_connect_quorum = 2
		end
	end;
	on_after_cfg = function()
		if not box.info.ro then
			log.info("Granting super to guest")
			box.schema.user.grant('guest', 'super', nil, nil, { if_not_exists = true })

			log.info("Creating space T")
			box.schema.space.create('T', { if_not_exists = true })
			log.info("Creating index T/I")
			box.space.T:create_index('I', { if_not_exists = true })
			log.info("Creating space W")
			box.schema.space.create('W', { if_not_exists = true })
			log.info("Creating index W/I")
			box.space.W:format({{name='id', type='string'}})
			box.space.W:create_index('I', { if_not_exists = true })
		end
	end,
}

fiber.create(function(gen)
	fiber.name('abuser/'..gen)
	while package.reload.count == gen and box.info.ro and not pcall(box.ctl.wait_rw, 1) do
		fiber.testcancel()
	end
	if package.reload.count ~= gen then log.info("not my gen") return end

	for _ = 1, 20 do
		fiber.create(function()
			while package.reload.count == gen do
				box.begin()
					for _ = 1, 100 do
						box.space.W:replace{tostring(math.random(1, 4e6)), fiber.time(), box.info.id, box.info.vclock}
					end
				box.commit()
				fiber.sleep(0.1)
			end
			log.info("Leaving fiber")
		end)
	end
end, package.reload.count)

fiber.create(function(gen)
	fiber.name('pusher/'..gen)
	while package.reload.count == gen and box.info.ro and not pcall(box.ctl.wait_rw, 1) do
		fiber.testcancel()
	end
	if package.reload.count ~= gen then log.info("not my gen") return end

	log.info("starting load")

	for _ = 1, 10 do
		fiber.create(function()
			while package.reload.count == gen do
				box.begin()
					for _ = 1, 10 do
						box.space.T:insert{box.space.T:len(), fiber.time(), box.info.id, box.info.vclock}
					end
				box.commit()
				fiber.sleep(0.1)
			end
			log.info("Leaving fiber")
		end)
	end
end, package.reload.count)