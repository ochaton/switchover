etcd = {
	endpoints = {
		"http://etcd0:2379",
		"http://etcd1:2379",
		"http://etcd2:2379",
	},
	prefix = os.getenv("TT_APP_ETCD_PREFIX"),
	timeout = 30,
	instance_name = assert(os.getenv("TT_INSTANCE_NAME"), "instance_name is required"),
	print_config = true,
	boolean_auto = true,
}

box = {
	background = false,
	vinyl_memory = 0,
	checkpoint_interval = 365*86400,
	wal_dir = '/var/lib/tarantool/xlogs/'..os.getenv("TT_INSTANCE_NAME"),
	memtx_dir = '/var/lib/tarantool/snaps/'..os.getenv("TT_INSTANCE_NAME"),
}
