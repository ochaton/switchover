#!/bin/sh

tarantoolctl rocks install --server=https://moonlibs.github.io/rocks --global config scm-5
tarantoolctl rocks install --server=https://moonlibs.github.io/rocks --global package-reload scm-1
tarantoolctl rocks install metrics scm-1

exec "$@";