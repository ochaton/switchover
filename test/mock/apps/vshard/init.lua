#!/usr/bin/env tarantool
require 'strict'.on()
local fio = require 'fio'
print(fio.cwd(), fio.abspath(fio.cwd()))
print(fio.pathjoin(fio.abspath(fio.cwd()), ".rocks/share/lua/5.1/"))
package.path = fio.pathjoin(
	fio.abspath(fio.cwd()), ".rocks/share/lua/5.1/").."/?.lua"..";"..package.path

local is_under_tarantoolctl = not not os.getenv('TARANTOOLCTL')

local fiber = require 'fiber'
local log   = require 'log'
local fun   = require 'fun'
local http  = require 'http.client'

repeat
	log.info("Waiting for etcd0:2379")
	fiber.sleep(1)
until http.get('http://etcd0:2379/v2/keys/'..os.getenv "TT_APP_ETCD_PREFIX", {timeout = 3}).status == 200

require 'package.reload'
_G.vshard = require 'vshard'

require 'config' {
	master_selection_policy = 'etcd.cluster.vshard',
	file          = 'conf.lua',
	mkdir         = true,
	instance_name = os.getenv('TT_INSTANCE_NAME'),
	on_load       = function(_,cfg)
		if cfg.box.background ~= nil and not cfg.box.background and is_under_tarantoolctl then
			cfg.box.background = true
		end
		if not cfg.box.read_only then
			-- RCQ=1 for master
			cfg.box.replication_connect_quorum = 1
		end
		cfg.box.slab_alloc_factor = 1.01
	end;
	on_before_cfg = function(conf, cfg)
		local all_cfg = conf.etcd:get_all()

		cfg.sharding = fun.iter(all_cfg.clusters)
			:map(function(shard_name, shard_info)
				return shard_info.replicaset_uuid, {
					replicas = fun.iter(all_cfg.instances)
						:grep(function(instance_name, instance_info)
							return instance_info.cluster == shard_name
						end)
						:map(function(iname, iinfo)
							return iinfo.box.instance_uuid, {
								name   = iname,
								uri    = 'guest:@'..iinfo.box.listen,
								master = iname == shard_info.master,
							}
						end)
						:tomap()
				}
			end)
			:tomap()
	end,
	on_after_cfg = function(_, cfg)
		log.info("on_after_cfg:")
		log.info(cfg)

		local consts = require 'vshard.consts'
		for name, value in pairs(config.get('vshard.consts', {})) do
			if consts[name] ~= nil then
				consts[name] = value
				log.info("Override vshard.consts.%s: %s -> %s", name, consts[name], value)
			else
				error(("Unknown vshard constant %s=%s"):format(name, value))
			end
		end

		if cfg.router then
			vshard.router.cfg {
				sharding = cfg.sharding,
				bucket_count = cfg.vshard.bucket_count,
			}
		elseif cfg.cluster then
			vshard.storage.cfg({
				sharding = cfg.sharding,
				bucket_count = cfg.bucket_count,
				replication_connect_quorum = box.cfg.replication_connect_quorum
			}, box.info.uuid)
		end

		log.info("vshard configured")
	end,
}

box.schema.user.grant('guest', 'super', nil, nil, { if_not_exists = true })

if config.get('cluster') then
	box.schema.space.create('T', {if_not_exists = true})
	box.space.T:create_index('I', {if_not_exists = true})

	fiber.create(function(gen)
		do return end
		fiber.name('storage/'..gen)
		while package.reload.count == gen and box.info.ro and not pcall(box.ctl.wait_rw, 1) do
			fiber.testcancel()
		end
		if package.reload.count ~= gen then log.info("not my gen") return end

		log.info("starting load")
		while package.reload.count == gen do
			for _ = 1, 10 do
				box.space.T:insert{box.space.T:len(), fiber.time(), box.info.id, box.info.vclock}
			end
			fiber.sleep(0.1)
		end
		log.info("Leaving fiber")
	end, package.reload.count)
else
	fiber.create(function(gen)
		fiber.name('router/'..gen)

		repeat
			fiber.sleep(0.1)
		until vshard.router.info().status == 1

		log.info("ready to bootstrap cluster")

		if not vshard.router.route(1) then
			fiber.sleep(math.random(1, 10))
			if not vshard.router.route(1) then
				log.info("Running bootstrap")
				vshard.router.bootstrap{timeout=60}
			end
		end

		log.info("starting load")
		while package.reload.count == gen do
			vshard.router.callrw(math.random(1, vshard.router.bucket_count()), 'dostring', {
				[[box.space.T:insert{
					box.space.T:len(),
					require"fiber".time(),
					box.info.id,
					box.info.vclock,
					("rnd"):rep(math.random(1, 128)),
				}]]
			}, { timeout = 3 })
			fiber.sleep(0.01)
		end

		log.info("Leaving fiber")
	end, package.reload.count)
end
