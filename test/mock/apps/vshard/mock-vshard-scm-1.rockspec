rockspec_format = "1.1"
package = "vshard"
version = "dev-1"
source = {
   url = "git+ssh://git@gitlab.com/ochaton/switchover.git"
}
description = {
   homepage = "*** please enter a project homepage ***",
   license = "*** please specify a license ***"
}
dependencies = {
   "vshard 0.1.19",
   "config scm-5",
   "package-reload scm-1",
}
build = {
   type = "builtin",
   modules = {
      conf = "conf.lua",
      init = "init.lua",
   }
}
