etcd = {
	endpoints = {
		"http://etcd0:2379",
		"http://etcd1:2379",
		"http://etcd2:2379",
	},
	prefix = os.getenv("TT_APP_ETCD_PREFIX"),
	timeout = 30,
	instance_name = assert(os.getenv("TT_INSTANCE_NAME"), "instance_name is required"),
}

box = {
	background = false,
	vinyl_memory = 0,
	wal_dir = '/var/run/tarantool/xlogs/'..os.getenv("TT_INSTANCE_NAME"),
	memtx_dir = '/var/run/tarantool/snaps/'..os.getenv("TT_INSTANCE_NAME"),
}
