#!/usr/bin/env tarantool
require "strict".on()
local fio = require "fio"
print(fio.cwd(), fio.abspath(fio.cwd()))
print(fio.pathjoin(fio.abspath(fio.cwd()), ".rocks/share/lua/5.1/"))
package.path = fio.pathjoin(fio.abspath(fio.cwd()), ".rocks/share/lua/5.1/") .. "/?.lua" .. ";" .. package.path

local fiber = require "fiber"
local log = require "log"
local fun = require "fun"
local yaml = require "yaml"
local http = require "http.client"

local vshard = require 'vshard'
rawset(_G, 'vshard', vshard)

repeat
    log.info("Waiting for etcd0:2379")
    fiber.sleep(1)
until http.get("http://etcd0:2379/v2/keys/" .. os.getenv "TT_APP_ETCD_PREFIX", {timeout = 3}).status == 200

require "package.reload"
require "config" {
    master_selection_policy = "etcd.cluster.master",
    file = "conf.lua",
    mkdir = true,
    instance_name = os.getenv("TT_INSTANCE_NAME"),
    on_load = function(_, cfg)
        if cfg.box.election_mode then
            cfg.box.read_only = false
            cfg.box.replication_synchro_quorum = "N/2+1"
            cfg.box.replication_connect_quorum = 2
		elseif cfg.box.read_only == false then
			cfg.box.replication_connect_quorum = 1
		end
		if cfg.box.replicaset_uuid then
			cfg.box.replicaset_uuid = cfg.box.replicaset_uuid:lower()
		end
		if cfg.box.instance_uuid then
			cfg.box.instance_uuid = cfg.box.instance_uuid:lower()
		end
    end,
	on_before_cfg = function(conf, cfg)
		local etcd_instances = conf.etcd:get_instances()
		local etcd_clusters  = conf.etcd:get_clusters()

		cfg.bucket_count = 3000
		cfg.sharding = fun.iter(etcd_instances)
			:map(function(instance_name, inst)
				return
					etcd_clusters[inst.cluster].replicaset_uuid,
					inst.box.instance_uuid:lower(),
					{
						uri = 'guest:@'.. (inst.box.remote_addr or inst.box.listen),
						name = instance_name,
						master = etcd_clusters[inst.cluster].master == instance_name,
					}
			end)
			:reduce(function(s, r_uuid, i_uuid, inst)
				s[r_uuid] = s[r_uuid] or { replicas = {} }
				s[r_uuid].replicas[i_uuid] = inst
				return s
			end, {})
		log.info(yaml.encode(cfg.sharding))
	end,
    on_after_cfg = function(_, cfg)
		vshard.storage.cfg({sharding = cfg.sharding, bucket_count = cfg.bucket_count}, cfg.box.instance_uuid:lower())
    end
}
box.schema.user.grant("guest", "super", nil, nil, {if_not_exists = true})

box.schema.space.create("T", {if_not_exists = true})
box.space.T:create_index("I", {if_not_exists = true})

fiber.create(
    function(gen)
        fiber.name("pusher/" .. gen)
        while package.reload.count == gen and box.info.ro and not pcall(box.ctl.wait_rw, 1) do
            fiber.testcancel()
        end
        if package.reload.count ~= gen then
            log.info("not my gen")
            return
        end

        log.info("starting load")
        while package.reload.count == gen do
            for _ = 1, 10 do
                box.space.T:insert {box.space.T:len(), fiber.time(), box.info.id, box.info.vclock}
            end
            fiber.sleep(0.01)
        end
        log.info("Leaving fiber")
    end,
    package.reload.count
)
