rockspec_format = "1.1"
package = "switchover"
version = "scm-1"
source = {
   url = "git+https://gitlab.com/ochaton/switchover",
   branch = "master"
}
description = {
   homepage = "http://gitlab.com/ochaton/switchover",
   license = "WTFPL"
}
dependencies = {
   "sync 0.11.0",
   "lulpeg scm-3",
}
build = {
   type = "builtin",
   modules = {
      switchover = "switchover.lua",
      ["switchover._ansicolors"] = "switchover/_ansicolors.lua",
      ["switchover._argparse"] = "switchover/_argparse.lua",
      ["switchover._audit"] = "switchover/_audit.lua",
      ["switchover._background"] = "switchover/_background.lua",
      ["switchover._cluster"] = "switchover/_cluster.lua",
      ["switchover._error"] = "switchover/_error.lua",
      ["switchover._etcd"] = "switchover/_etcd.lua",
      ["switchover._global"] = "switchover/_global.lua",
      ["switchover._graphite"] = "switchover/_graphite.lua",
      ["switchover._hostname"] = "switchover/_hostname.lua",
      ["switchover._log"] = "switchover/_log.lua",
      ["switchover._mutex"] = "switchover/_mutex.lua",
      ["switchover._net_url"] = "switchover/_net_url.lua",
      ["switchover._plterm"] = "switchover/_plterm.lua",
      ["switchover._proxy"] = "switchover/_proxy.lua",
      ["switchover._ps1"] = "switchover/_ps1.lua",
      ["switchover._replicaset"] = "switchover/_replicaset.lua",
      ["switchover._resolve"] = "switchover/_resolve.lua",
      ["switchover._selector"] = "switchover/_selector.lua",
      ["switchover._selector_grammar"] = "switchover/_selector_grammar.lua",
      ["switchover._semver"] = "switchover/_semver.lua",
      ["switchover._stats"] = "switchover/_stats.lua",
      ["switchover._switch_promote_common"] = "switchover/_switch_promote_common.lua",
      ["switchover._tarantool"] = "switchover/_tarantool.lua",
      ["switchover._termui"] = "switchover/_termui.lua",
      ["switchover._util"] = "switchover/_util.lua",
      ["switchover._validator"] = "switchover/_validator.lua",
      ["switchover._wtable"] = "switchover/_wtable.lua",
      ["switchover.auto"] = "switchover/auto.lua",
      ["switchover.connect"] = "switchover/connect.lua",
      ["switchover.core.etcd_cluster_master"] = "switchover/core/etcd_cluster_master.lua",
      ["switchover.core.etcd_instance_single"] = "switchover/core/etcd_instance_single.lua",
      ["switchover.core.pick_best_candidate"] = "switchover/core/pick_best_candidate.lua",
      ["switchover.core.resolve_and_discovery"] = "switchover/core/resolve_and_discovery.lua",
      ["switchover.discovery"] = "switchover/discovery.lua",
      ["switchover.etcd"] = "switchover/etcd.lua",
      ["switchover.eval"] = "switchover/eval.lua",
      ["switchover.heal"] = "switchover/heal.lua",
      ["switchover.package-reload"] = "switchover/package-reload.lua",
      ["switchover.promote"] = "switchover/promote.lua",
      ["switchover.resign"] = "switchover/resign.lua",
      ["switchover.restart-replication"] = "switchover/restart-replication.lua",
      ["switchover.status"] = "switchover/status.lua",
      ["switchover.switch"] = "switchover/switch.lua",
      ["switchover.top"] = "switchover/top.lua",
      ["switchover.watch"] = "switchover/watch.lua"
   },
   install = {
      bin = {
         switchover = "switchover.lua"
      }
   }
}
deploy = {
   wrap_bin_scripts = false
}
