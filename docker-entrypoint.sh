#!/bin/sh

set -eu;

VERSION=${VERSION:-_VERSION_};
export PATH="/root/.rocks/bin:$PATH"

if [ -f /source/switchover.lua ]; then
  sed -i "s/_VERSION_/${VERSION}/g" /source/switchover.lua;
  ln -s /source/switchover.docker.yaml /etc/switchover/config.yaml;

  chmod a+x /source/switchover.lua && \
    ln -s /source/switchover.lua /usr/bin/switchover && \
    ln -s /source/switchover.lua /usr/bin/swr;
fi;

if [ -f "/source/.rocks/config-5.1.lua" ]; then
  dir=$(pwd);
  cd /source;
    tarantoolctl rocks make switchover-scm-1.rockspec;
  cd "$dir";
fi;

if [ "$#" -gt 0 ]; then
  "$@";
else
  exec /bin/sh;
fi;
