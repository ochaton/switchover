# Changelog

## 2.7.0 - 2023-06-26

### Added

- `switchover resign` command added. It allows to decomission of RW instances for given `--selector` rule. Main use is to prepare server or vm for shutdown.
- `--selector` option is added for commands `pr/rr/eval/top/status/resign` to filter instances by given rule.
- `switchover pr/rr` commands now accepts `-j / --jobs` option to parallelize execution by instances.
- `switchover pr/rr/eval` commands now accepts `-i / --interval` to sleep between executions. Can be used with `-j / --jobs`
- Each fqdn is resolved to ipv4 address for each tarantool
- `switchover pr/rr` now respects `--timeout` as timeout for operation
- `switchover status` now supports flag `--ignore` to ignore alert names and alert levels
- `switchover` now implements simple dns cache for `dns_timeout` (default 60s) which can be specified in config
- `switchover status` now aliases `--only-alerts` and `--alerts-only` to `--only-alert`
- `switchover status` now has flag `--by-server` (experimental) which groups instances by servers
- `switchover status` now has flag `-o / --output-format = json`
- `switchover` now has global flag `-t / --etcd-timeout` which sets timeout for each etcd operation.
- `switchover etcd load` now has flag `-n / --dry-run` which prints to log etcd load actions but
does not change ETCD
- `switchover etcd load` now has flag `--append-only` which protects ETCD from change but allows
to add new keys and values. Does not suppress deletions
- `switchover etcd load` now has flag `--delete` allows to delete keys those do not exist in
the source but found in ETCD (disabled by default).
- `switchover etcd load --append-only --delete` is legitimate command existing keys in the source won't be changed in ETCD, but missing keys will be deleted. Use dry-run to check what will be done
- `switchover watch / autofailover` now send new metrics `vshard_is_broken.{fire,resolved,unresolved}` for resolution `vshard_is_broken`
- `switchover watch / autofailover` now supports `with_auto_promote` and `with_vshard_fix`
- `switchover status` now reports new alert `vshard_rw_is_ro` when `vshard.storage` does not update it's configuration (NON_MASTER issue)
- `switchover top` now allows sort by columns with key `f6` and reverse sort with key `r`.
- `switchover watch stop <cluster-name>` command now stops local autofailover.

### Changed

- `switchover` now defaults log_level to `warn`, so many logs has been hidden.
- `switchover status` now reports alerts AFTER instances.
- `switchover watch / autofailover` now allows to parallelize switching via fiber.pool through option `autofailover/maximum_pool_size`
- `sync/pool` was updated to version 0.11.0 (suppreses noisy logs)
- `switchover/_mutex` now fetches first mutex from ETCD with enforced quorum
- `switchover auto` replica candidates now choosen by minimizing differencebetween maximum achiveable vclock for candidate and potential maximum vclock in the cluster (slightly reduces data loss)

### Fixed

- `switchover/audit` now successfully saves partial messages when resolutions are unknown.
- `switchover/audit` each operation of `alarm_on/alarm_off` is atomic.
- `switchover etcd health` fixed
- `switchover status / switchover watch` now does not crash when servers with tarantools has NTP issue and some of instances are disconnected.
- `switchover etcd wait` fixed `--index`.
- `switchover etcd health` now exits with exitcode 1 when etcd is not healthy.
- `switchover eval` now accepts script with `do`
- `switchover switch` now reports correct timing of master switching.
- `switchover top` now does not crash or freeze while evaluating complex lua code.
- `switchover status/top` now correctly aligns uptime
- `switchover` now ignores etcd endpoints which does not respond with 200 on /member/list (fixes etcd-in-split-brain)
- `switchover watch` now respect `ignore_tarantool_quorum` option in configuration

### Removed

- `switchover auto/etcd wait/switch/` now does not accept short flag of `-t`. Flag `-t` is now used globally as `--etcd-timeout`.

## 2.6.0 - 2023-03-23

### Added

- `switchover watch` now respects `/instances/<instance_name>/autofailover/` configuration. User may set priorities for autofailover or disable promote of specific instances setting `/instances/<instance_name>/autofailover/role = "witness"`.
- `switchover watch` now exports telemetry and alerts into graphite if `autofailover.graphite` is configured
- switchover now mesures RTT for each connection and can report it via `switchover watch` telemetry
- `switchover pr / rr / eva` now supports flag `-i / --interval` to sleep between subsequent calls. (Default value is 0s as it was before)

### Fixed

- `Panic` now does not exit in `switchover watch`
- info logs of switchover now uses `green` color instead of `blue` color

### Removed

- `switchover` now does not build for DEB

## 2.5.2 - 2023-02-06

### Added

- `--ero` flag (meaning etcd is readonly) is added to ReadOnly commands to support degraded operations during ETCD partial outage
- `switchover watch` now supports audit messages (configured via config/autofailover/audit)
- `switchover auto` now supports autopromote of ETCD leader if it was stepped down to RO (autopromote).
- `switchover auto` now supports autoheal of ETCD information if ETCD has malformed information about shard (autoheal).
- `switchover auto` now supports flag `ignore_tarantool_quorum` to ignore tarantool quorum during autopromote.

### Removed

- nothing was removed

## 2.5.1 - 2023-01-13

### Added

- `switchover status` now shows vshard alerts and WAL alerts

### Fixed

- `switchover top` fixed search cursor position

### Removed

- Nothing was removed

## 2.5.0 - 2023-01-10

### Added

- `switchover etcd health` now requests /health endpoint from etcd
- `switchover etcd version` now requests /version endpoint from etcd
- `switchover etcd getr` added as an alias to `switchover etcd get -r`
- `switchover connect --ps1` flag added to modify tarantool console prompt (experimental)
- `switchover top` now runs in interactive mode
- `switchover --no-etcd` flag is added to disable etcd

### Fixed
- `switchover auto` now fences master before changing /<cluster>/master in ETCD
- `switchover --completion` fixed
- `switchover switch <shard>:ro` now supported to switch master to any replica
- `switchover promote <shard>:rw` now supported to promote to etcd defined master
- rockspec_format is changed to 1.1
- etcd version in tests was upgraded to 3.5.3
- switchover directories was changed: /usr/share/lua/5.1 -> /usr/share/tarantool
- `switchover top` replication lag was fixed

## 2.4.0 - 2022-12-16

### Added

- `switchover etcd create` added command to generate ETCD configuration for new application
- `switchover discovery` now supports json-output
- All switchover commands now supports slicing `:rw`, `:ro`, `:ros`
- `switchover etcd wait` now accepts `--index` and `--recursive` flags
- Implemented auto tests for single shard and vshard applications
- `switchover eval/pr/rr` now accepts `--dry-run` flag. Command is compiled and instances where command should be executed are resolved and printed out onto console.

### Fixed

- `switchover switch` and `switchover promote` now releases ETCD mutex only after changing master in ETCD.
- `switchover switch/promote/auto/watch` now always saves mutex inside `<prefix>/clusters/<shard-name>/switchover`
- `switchover eval` now does not require `return` keyword in command


### Removed

- Nothing was removed

## 2.3.2 - 2022-11-10

### Added

- `switchover etcd validate` added to validate configuration for Tarantool application
- `switchover status` now shows tarantool version
- `switchover eval --shard <shard-name> <shell>` and `switchover eval --cluster <cluster-name> <shell>` was added to run same command on replicaset or cluster.

### Fixed

- `switchover {switch,promote}` and others now properly log after-switch discovery info (as it was before 2.3.0)

### Removed

- `switchover eval --all` now removed. You should use `--shard` and/or `--cluster` instead.

## 2.3.1 - 2022-09-05

### Added

- switchover top: now accepts input as `switchover top cluster_name/shard_name` and reduces output to statistics and alerts of shard (not entire cluster)
- switchover status: now accepts input as `switchover status cluster_name/shard_name` and reduces output to statistics and alerts of shard (not entire cluster)


### Fixed

- Background fibers now closed explicitely using background.registry for `package-reload`, `restart-replication` and `switch` commands. This patch removes annoying logs.

## 2.3.0 - 2022-09-02

### Added

- switchover top: added `Conns` column which stands for `Established connections to tarantool (taken from box.stat.net().CONNECTIONS.current`
- switchover top: `Cur` was renamed to `Reqs` (stands for network requests per second).

### Changed

- `github.com/ochaton/background` was imported to switchover. All meaningfull background fibers were rewritten using background library
- `switchover restart-replication (rr)` now behaves in the same way as `package-reload (pr)`. `rr` also supports `--cluster` and `--shard` flags.

### Fixed

- Fixed fiber leaks and eval flood during `switchover watch`.
- Sockets to unconnected tarantools now closed after discovery timeout exhausted.

### Removed

- Nothing was removed

### Security

- No security changes were introduced.
