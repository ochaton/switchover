lib_modules_path = "/lib/tarantool"
lua_interpreter = "tarantool"
lua_modules_path = "/share/tarantool"
lua_version = "5.1"
rocks_servers = {
   "https://rocks.ochaton.me",
   "http://moonlibs.github.io/rocks",
   "http://rocks.tarantool.org/",
   "https://moonlibs.org",
   "http://luarocks.org/repositories/rocks"
}
rocks_subdir = "/share/tarantool/rocks"