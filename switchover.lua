#!/usr/bin/env tarantool
require 'strict'.on()
require 'jit'.off()
local VERSION = "_VERSION_"
local log = require 'switchover._log'
local fio = require 'fio'
local yaml = require 'yaml'
local json = require 'json'
local fiber = require 'fiber'
json.cfg { encode_use_tostring = true, encode_invalid_string = true }
yaml.cfg { encode_use_tostring = true, encode_invalid_string = true }

local pwd = os.getenv('PWD') or fio.cwd()
local bwd = fio.dirname(fio.abspath(debug.getinfo(1, "S").source:sub(2)))

if os.getenv('LUACOV_ENABLE') then
	require 'luacov.runner'.init()
end

local read_password

local G = require 'switchover._global'
G.pwd = pwd
G.bwd = bwd
G.start_at = fiber.time()
G.log_level = 4 -- base log_level (warn)

local config_paths = {
	fio.pathjoin(pwd, 'switchover.yaml'),
	fio.pathjoin(bwd, 'switchover.yaml'),
	fio.pathjoin(os.getenv('HOME'), 'switchover.yaml'),
	"/etc/switchover/config.yaml",
}

local function main(args)
	if args.quiet then
		log.level(1)
		G.log_level = math.min(1, G.log_level)
	elseif args.verbose then
		G.log_level = G.log_level + args.verbose
		log.level(G.log_level)
	end
	log.verbose("argv: %s", json.encode(args))

	if args.link_graph then
		args.show_graph = true
	end

	log.verbose('PWD: %s; Binary dir: %s', pwd, bwd)

	if args.ask_pass then
		args.pass = read_password()
	end

	if not args.no_config then
		if args.config then
			table.insert(config_paths, 1, args.config)
		end
		log.verbose("Searching for config in %s", table.concat(config_paths, ";"))
		local cfg_path
		for _, path in ipairs(config_paths) do
			log.verbose("Checking config in %s", path)
			if fio.stat(path) then
				cfg_path = path
				break
			end
		end
		if cfg_path then
			log.verbose("Loading config from %s", cfg_path)
			for k, v in pairs(yaml.decode(assert(io.open(cfg_path, "r")):read("*all"))) do
				log.verbose("Setting %s to %s", k, json.encode(v))
				if args[k] == nil then
					args[k] = v
				end
			end
		end
	end

	if tostring(args.color) ~= "false" and args.color then
		log.colors(true)
	end
	if args.no_color then
		log.colors(false)
	end

	if args.etcd and not args.no_etcd then
		local etcd_prefix
		if type(args.etcd) == 'string' then
			args.etcd = {
				prefix = args.etcd_prefix,
				timeout = args.etcd_timeout,
				endpoints = args.etcd:split(","),
			}
		else
			etcd_prefix = ("%s/%s"):format(args.etcd.prefix, args.etcd_prefix and args.etcd_prefix or '')
		end
		G.etcd = require 'switchover._etcd'.new {
			endpoints    = args.etcd.endpoints,
			prefix       = etcd_prefix or args.etcd.prefix,
			timeout      = args.etcd_timeout or args.etcd.timeout,
			http_params  = args.etcd.http_params or nil, -- in case false
			login        = args.etcd.login,
			password     = args.etcd.password,
			boolean_auto = true,
			integer_auto = true,
			autoconnect  = true,
			read_only    = args.etcd_read_only,
		}
	end

	if args.discovery_format then
		G.discovery_format = args.discovery_format
	end

	G.args = table.deepcopy(args)
	G.workdir = args.workdir or pwd
	log.verbose("Global: %s", json.encode(G))

	local retcode = require('switchover.' .. args.command).run(args)
	if args.command == 'watch' then return end
	os.exit(retcode or 0)
end

local function comma_split(s) return s:split "," end

local function comma_split_map(s) local m = {} for _, v in ipairs(s:split ",") do m[v] = true end return m end

local switchover
do
	switchover = require 'switchover._argparse' ()
		:name("switchover")
		:command_target "command"
		:description ("Tarantool master <-> replica switchover (version: "..VERSION..")")
		:epilog "Home: https://gitlab.com/ochaton/switchover"
		:add_help_command()
		:add_complete()
		:help_vertical_space(0)
		:help_description_margin(60)

	switchover:flag "--ask-pass"
		:target "ask_pass"
		:description "Asks to pass password from stdin"

	switchover:option "-u" "--user" "--login"
		:args "1"
		:target "user"
		:description "User to connect with"

	switchover:option "-P" "--pass" "--password" "--cluster-cookie"
		:target "pass"
		:args "1"
		:description "Password to tarantool"

	switchover:flag "-C" "--color"
		:target "color"
		:description "enables colored logs"

	switchover:flag "--no-color"
		:target "no_color"
		:description "disables colored logs"

	switchover:mutex(
		switchover:option "-c" "--config"
			:target "config"
			:default "/etc/switchover/config.yaml"
			:description "Path to config"
		,
		switchover:flag "--no-config"
			:target "no_config"
			:description "Does not load config even if it exists"
	)

	switchover:option "-w" "--work-dir"
		:target "workdir"
		:default "."
		:description "Path to workdir where switchover will keep it's files"

	switchover:option "--dns-timeout"
		:target "dns_timeout"
		:default(1.5)
		:description "Sets timeout for dns resolve"

	switchover:mutex(
		switchover:option "-e" "--etcd"
			:target "etcd"
			:description "Address to ETCD endpoint"
		,
		switchover:flag "--no-etcd"
			:target "no_etcd"
			:description "disables etcd"
	)

	switchover:option "-t" "--etcd-timeout"
		:target "etcd_timeout"
		:description "Sets timeout to ETCD"

	switchover:option "-p" "--prefix"
		:target "etcd_prefix"
		:description "Prefix to configuration of clusters in ETCD"

	switchover:mutex(
		switchover:flag "-q" "--quiet"
			:description "Disables logs (quiet mode)"
		,
		switchover:flag "-v" "--verbose"
			:description "Verbosity level"
			:count "0-2"
			:target "verbose"
	)

	switchover:option "-d" "--discovery_timeout"
		:description "Discovery timeout (in seconds)"
		:target "discovery_timeout"
		:default(3)

	switchover:option "-f" "--discovery-format"
		:description "Discovery format"
		:target "discovery_format"

	switchover:command "version"
		:description "Shows switchover version"
		:action(function()
			print(VERSION)
			os.exit(0)
		end)
end

local discovery
do
	discovery = switchover:command "discovery" "d"
		:summary "Discovers all members of the replicaset"
		:description("Some of the examples:\n"
			..
			"\tswitchover discovery cluster_name/shard_name     - discovers nodes inside single shard inside cluster (requires ETCD)\n"
			..
			"\tswitchover discovery cluster_name@instance_name  - discovers nodes inside single shard inside cluster (requires ETCD)\n"
			..
			"\tswitchover discovery mysingle                    - discovers nodes from the single replicaset (not cluster, required ETCD)\n"
			..
			"\tswitchover discovery tnt1:3301,tnt2:3301         - performs upstream-first search of the replicas (requires full-mesh, no ETCD)\n")

	discovery:flag "--ero"
		:target "etcd_read_only"
		:description "Allows ReadOnly requests from ETCD if ETCD leader is not discovered"
		:default(false)

	discovery:flag "-g" "--show-graph"
		:target "show_graph"
		:description "Prints topology to the console in dot format"

	discovery:flag "-l" "--link-graph"
		:target "link_graph"
		:description "Build url to online visualization"

	discovery:option "-o" "--output-format"
		:target "discovery_output_format"
		:choices { "plain", "json", "table" }
		:default "plain"
		:description "Discovery output format"

	discovery:argument "endpoints|shard"
		:target "endpoints"
		:args "1"
		:description "host:port to tarantool or name of replicaset"
		:convert(comma_split)
end

local status
do
	status = switchover:command "status" "s"
		:summary "Shows status of cluster or shard"
		:description("Some of the examples:\n"
			.. "\tswitchover status cluster_name                - shows status of cluster (requires ETCD)\n"
			.. "\tswitchover status cluster_name/shard_name     - shows status of shard (requires ETCD)\n"
			.. "\tswitchover status cluster_name:routers        - shows status of routers of cluster (requires ETCD)\n"
		)

	status:flag "--ero"
		:target "etcd_read_only"
		:description "Allows ReadOnly requests from ETCD if ETCD leader is not discovered"
		:default(false)

	status:flag "--no-alert"
		:description "Hides alert of the cluster"
		:target "no_alert"

	status:flag "--only-alert" "--alert-only" "--only-alerts" "--alerts-only"
		:description "Shows only alerts"
		:target "only_alert"

	status:option "--selector" "-s"
		:target "selector"
		:description "Selector rule"

	status:flag "--by-server"
		:target "by_server"
		:description "Groups instances by servers"

	status:option "--ignore"
		:target "ignore_alerts"
		:description "Ignores specified alerts (multiple separated with ','. ex. quota_used,not_connected)"
		:convert(function(t) return t:split(",") end)

	status:option "-o" "--output-format"
		:target "status_output_format"
		:choices{"json"}
		:description "Changes output format of instances info"

	status:argument "cluster_name|shard_name"
		:target "cluster"
		:args "1"
		:description "Name of the cluster (application)"
end

local top
do
	top = switchover:command "top" "t"
		:summary "Runs cluster or shard top"
		:description("Some of the examples:\n"
			.. "\tswitchover top cluster_name                - shows top of cluster (requires ETCD)\n"
			.. "\tswitchover top cluster_name/shard_name     - shows top of shard (requires ETCD)\n"
			.. "\tswitchover top cluster_name:routers        - shows top of routers of cluster (requires ETCD)\n"
		)

	top:flag "--ero"
		:target "etcd_read_only"
		:description "Allows ReadOnly requests from ETCD if ETCD leader is not discovered"
		:default(false)

	top:option "-i" "--interval"
		:target "top_interval"
		:description "Refresh interval in seconds (must be ≥ 0.1)"
		:default(1)
		:convert(tonumber)

	top:option "--selector" "-s"
		:target "selector"
		:description "Selector rule"

	top:argument "cluster_name|shard_name"
		:target "cluster_or_shard"
		:args "1"
		:description "Name of the cluster (application)"
end

local promote
do
	promote = switchover:command "promote"
		:summary "Promotes given instance to master"
		:description("Promote fails when master exists in replicaset. Supports only tarantool >= 1.10.0\nExamples:"
			..
			"\n\tswitchover promote mycluster/mycluster_002@mycluster_002_01 - Makes mycluster_002_01 master of mycluster_002"
			.. "\n\tswitchover promote mysingle@mysingle_001                    - Makes mysingle_001 master of mysingle"
			.. "\n\tswitchover promote --no-etcd tnt1:3301                      - Makes tnt1:3301 master of it's replicaset"
		)

	promote:argument "instance"
		:args(1)
		:description "Choose instance to become master"

	promote:option "--max-lag"
		:target "max_lag"
		:description "Maximum allowed Replication lag of the future master (in seconds)"
		:show_default(true)
		:default(1)

	promote:flag "--no-reload"
		:target "no_reload"
		:description "Disables execution of package.reload on candidate and old master (default: enabled)"

	promote:flag "--no-etcd-lock"
		:target "no_etcd_lock"
		:description "Disables ETCD mutexes and discovery"

	promote:flag "--no-fence-master"
		:target "no_fence_master"
		:description "Does not force master to RO before promote candidate"

	promote:option "--valid-status"
		:target "valid_status"
		:description "List of extra statuses (except running) valid for candidate (ex: orphan,loading)"
		:convert(comma_split_map)

	promote:flag "--allow-vshard"
		:target "allow_vshard"
		:description "Allows promote for vshard cluster (Use for your own risk)"
		:show_default(true)

	promote:flag "--no-reload-routers"
		:target "no_reload_routers"
		:description "Disable routers reload if were discovered via etcd.cluster.vshard"

	promote:flag "--allow-reload-during-snapshot"
		:target "allow_reload_during_snapshot"
		:description "Calls package.reload on candidate even if candidate is in box.snapshot()"
end

local switch
do
	switch = switchover:command "switch" "sw"
		:summary "Switches current master to given instance"
		:description("Switch fails when no master found in replicaset. Supports only tarantool >= 1.10.0\nExamples:"
			.. "\n\tswitchover switch mycluster/mycluster_002@mycluster_002_01 - Makes mycluster_002_01 master of mycluster_002"
			.. "\n\tswitchover switch mysingle@mysingle_001                    - Makes mysingle_001 master of mysingle"
			.. "\n\tswitchover switch --no-etcd tnt1:3301                      - Makes tnt1:3301 master of it's replicaset"
		)

	switch:argument "instance"
		:args(1)
		:description "Instance to become master"

	switch:option "--max-lag"
		:target "max_lag"
		:description "Maximum allowed Replication lag of the future master (in seconds)"
		:show_default(true)
		:default(1)

	switch:option "--valid-status"
		:target "valid_status"
		:description "List of extra statuses (except running) valid for candidate (ex: orphan,loading)"
		:convert(comma_split_map)

	switch:flag "--no-etcd-lock"
		:target "no_etcd_lock"
		:description "Disables ETCD mutexes and discovery"
		:default(false)
		:show_default(true)

	switch:option "--timeout"
		:target "switch_timeout"
		:description "Configures timeout of the switch (default: 2*max_lag)"

	switch:flag "--no-reload"
		:target "no_reload"
		:description "Disables execution of package.reload on candidate and old master (default: enabled)"

	switch:flag "--allow-vshard"
		:target "allow_vshard"
		:description "Allows switch for vshard cluster (Use for your own risk)"

	switch:flag "--allow-reload-during-snapshot"
		:target "allow_reload_during_snapshot"
		:description "Calls package.reload on candidate even if candidate is in box.snapshot()"

	switch:flag "--no-reload-routers"
		:target "no_reload_routers"
		:description "Disable routers reload if were discovered via etcd.cluster.vshard"

	switch:flag "--no-check-downstreams"
		:target "no_check_downstreams"
		:description "Disable having live downstreams for candidate"
end

local auto
do
	auto = switchover:command "auto"
		:summary "Runs auto failover for single shard"
		:description "Evaluates heuristics and promote replica to master if it is necessary"

	auto:option "--timeout"
		:target "autofailover_timeout"
		:args(1)
		:default(5)
		:description "Autofailover timeout (ETCD lock TTL)"

	auto:flag "--no-orphan-fix"
		:target "no_orphan_fix"
		:description "Does not change replication_connect_quorum"

	auto:flag "--promote"
		:target "auto_with_promote"
		:hidden(true)
		:description "Enforces auto promote of ETCD master"

	auto:argument "shard"
		:args(1)
		:description "Name of the shard"

	auto:flag "--no-reload-routers"
		:target "no_reload_routers"
		:description "Disable routers reload if were discovered via etcd.cluster.vshard"
end

local watch
do
	watch = switchover:command "watch"
		:description "Watch of healthiness of cluster"
		:target "command_watch"

	watch:option "--watch-check-timeout"
		:default(1)
		:description "Period of recheck master in cluster"

	watch:flag "--with-auto-switch"
		:target "with_auto_switch"
		:description "Auto switches if master fail"

	watch:flag "--with-auto-promote"
		:target "with_auto_promote"
		:description "Auto promotes when leader became RO"

	watch:flag "--with-auto-heal"
		:target "with_auto_heal"
		:description "Auto heals ETCD when another leader is ok"

	local watch_start
	do
		watch_start = watch:command "start"
			:summary "Starts watch for cluster"
			:target "watch_start"

		watch_start:argument "cluster"
			:target "watch_cluster"
			:description "name of the cluster to watch"
	end

	local watch_status = watch:command "status"
		:summary "Prints status of watch"
		:target "watch_status"

	watch_status:flag "--ero"
		:target "etcd_read_only"
		:description "Allows ReadOnly requests from ETCD if ETCD leader is not discovered"
		:default(false)

	watch_status:argument "cluster"
		:target "watch_cluster"
		:description "name of the cluster to wath"

	local watch_stop
	do
		watch_stop = watch:command "stop"
			:summary "Stops watch for cluster"
			:target "watch_stop"

		watch_stop:flag "--all"
			:target "watch_stop_all"
			:description "Stops all watches"

		watch_stop:argument "cluster"
			:target "watch_cluster"
			:args "?"
			:description "name of cluster or nothing"
	end
end

local heal
do
	heal = switchover:command "heal"
		:summary "Heals ETCD /cluster/master"
		:description "Sets current master of replicaset into ETCD if replication is good"

	heal:argument "shard"
		:args(1)
		:description "Name of the replicaset"
end

local restart_replication
do
	restart_replication = switchover:command "restart-replication" "rr"
		:summary "Restarts replication on choosen instance|shard|cluster"

	restart_replication:argument "instance|shard|cluster"
		:target "instance"
		:args(1)
		:description "Name or address of the instance"

	restart_replication:flag "--ero"
		:target "etcd_read_only"
		:description "Allows ReadOnly requests from ETCD if ETCD leader is not discovered"
		:default(false)

	restart_replication:flag "--shard"
		:description "Discovers all instances of the shard and restarts replication on every node"

	restart_replication:flag "-y" "--yes"
		:hidden(true)
		:description "Accept restart replication on entire cluster"

	restart_replication:flag "-n" "--dry-run"
		:target "dry_run"
		:description "Performs ETCD resolve, connection to Tarantools but do nothing"

	restart_replication:option "-j" "--jobs"
		:target "restart_replication_jobs"
		:default(1)
		:convert(tonumber)
		:description "Parallels restart-replication"

	restart_replication:flag "--cluster"
		:description "Discovers all instances of cluster and calls restart replication on every node"

	restart_replication:option "--timeout"
		:target "restart_replication_timeout"
		:description "Sets restart-replication timeout (in seconds, minimal: 3)"
		:convert(tonumber)
		:default(30)

	restart_replication:option "--selector" "-s"
		:target "selector"
		:description "Selector rule"

	restart_replication:option "-i" "--interval"
		:target "restart_replication_interval"
		:default(0)
		:convert(tonumber)
		:description "Sleeps interval between subsequent executions"
end

local resign
do
	resign = switchover:command "resign"
		:summary "Switches masters from server to another server"
		:description(
			"Example:"
			.."\n\tswitchover resign --selector 'fqdn=userdb.i' userdb"
			.."\n\tswitchover resign --selector 'ipv4=10.0.0.1' userdb"
		)

	resign:option "--selector" "-s"
		:target "selector"
		:description "Selector rule"

	resign:flag "--ignore-not-connected"
		:target "ignore_not_connected"
		:description "Ignores not connected instances"

	resign:flag "--yes"
		:target "yes"
		:hidden(true)

	resign:option "-i" "--interval"
		:target "resign_interval"
		:default(0)
		:convert(tonumber)
		:description "Sleeps interval between subsequent executions"

	resign:argument "cluster-name"
		:target "cluster"
		:args(1)
		:description "Name of the cluster"
end

local eval
do
	eval = switchover:command "eval"
		:summary "Evals given command on instance or all instances"

	eval:argument "instance"
		:args(1)
		:description "Name or address of the instance"

	eval:argument "shell"
		:args(1)
		:description "Shell command"

	eval:flag "--ero"
		:target "etcd_read_only"
		:description "Allows ReadOnly requests from ETCD if ETCD leader is not discovered"
		:default(false)

	eval:flag "-a" "--all"
		:description "Discovers all instances and restarts replication on every node"
		:hidden(true)

	eval:flag "--no-fail"
		:target "no_fail"
		:description "Skip failed executions (will attempt to eval every connection)"

	eval:option "-o" "--output"
		:target "output"
		:choices { "plain", "json", "yaml" }
		:default("yaml")

	eval:flag "-y" "--yes"
		:hidden(true)
		:description "Accept evaluation of command on entire cluster"

	eval:flag "-n" "--dry-run"
		:target "dry_run"
		:description "Performs ETCD resolve, connection to Tarantools but do nothing"

	eval:flag "--shard"
		:description "Discovers all instances of shard and evaluates given command"

	eval:flag "--cluster"
		:description "Discovers all instances of cluster and evaluates given command"

	eval:option "--selector" "-s"
		:target "selector"
		:description "Selector rule"

	eval:option "-i" "--interval"
		:description "Sleep interval between execution"
		:target "eval_interval"
		:default(0)
		:convert(tonumber)
end

local package_reload
do
	package_reload = switchover:command "package-reload" "pr"
		:summary "Reload replication on given instance"

	package_reload:argument "instance|shard|cluster"
		:target "instance"
		:description "Name or address of the instance"

	package_reload:flag "--ero"
		:target "etcd_read_only"
		:description "Allows ReadOnly requests from ETCD if ETCD leader is not discovered"
		:default(false)

	package_reload:flag "--shard"
		:description "Discovers all instances of the shard and calls package.reload on every node"

	package_reload:flag "-y" "--yes"
		:hidden(true)
		:description "Accept reload of entire cluster"

	package_reload:flag "-n" "--dry-run"
		:target "dry_run"
		:description "Performs ETCD resolve, connection to Tarantools but do nothing"

	package_reload:option "-i" "--interval"
		:target "package_reload_interval"
		:default(0)
		:convert(tonumber)
		:description "Sleeps interval between subsequent executions"

	package_reload:option "-j" "--jobs"
		:target "package_reload_jobs"
		:default(1)
		:convert(tonumber)
		:description "Parallels package.reload()"

	package_reload:flag "--cluster"
		:description "Discovers all instances of cluster and calls package.reload on every node"

	package_reload:option "--selector" "-s"
		:target "selector"
		:description "Selector rule"

	package_reload:flag "--allow-reload-during-snapshot"
		:target "allow_reload_during_snapshot"
		:description "Calls package.reload on instance even if instance is in box.snapshot()"
end

local connect
do
	connect = switchover:command "connect" "c"
		:summary "Opens tarantool console to given instance"

	connect:flag "--ps1"
		:description "Enables console formatting"
		:target "use_ps1"

	connect:flag "--ero"
		:target "etcd_read_only"
		:description "Allows ReadOnly requests from ETCD if ETCD leader is not discovered"
		:default(false)

	connect:argument "instance"
		:args "1"
		:description "Name or address of the instance"
end

local etcd
do
	etcd = switchover:command "etcd" "e"
		:description "Interface to operate with ETCD"
		:target "command_etcd"

	etcd:option "--output-format"
		:target "etcd_output_format"
		:choices { "json", "yaml" }
		:default "yaml"

	local etcd_list
	do
		etcd_list = etcd:command "list" "ls"
			:description "calls ls over etcd path"
			:target "etcd_list"

		etcd_list:flag "--ero"
			:target "etcd_read_only"
			:description "Allows ReadOnly requests from ETCD if ETCD leader is not discovered"
			:default(false)

		etcd_list:flag "-r" "--recursive"
			:description "Runs recursive listing of subpath"

		etcd_list:argument "path"
			:target "etcd_path"
			:args "1"
	end

	local etcd_get
	do
		etcd_get = etcd:command "get"
			:description "Gets specified value"
			:target "etcd_get"

		etcd_get:flag "--ero"
			:target "etcd_read_only"
			:description "Allows ReadOnly requests from ETCD if ETCD leader is not discovered"
			:default(false)

		etcd_get:flag "-r" "--recursive"
			:description "Gets recursive values of subpath"

		etcd_get:argument "path"
			:target "etcd_path"
			:args "1"

		etcd:command "getr"
			:description "Gets recursive tree"
			:target "etcd_get"
			:action(function(args) args.recursive = true end)
			:argument "path"
			:target "etcd_path"
			:args "1"

	end

	local etcd_set
	do
		etcd_set = etcd:command "set"
			:description "Sets specified value to the path"
			:target "etcd_set"

		etcd_set:argument "path"
			:target "etcd_path"
			:args "1"

		etcd_set:argument "value"
			:target "etcd_value"
			:args "1"
	end

	local etcd_load
	do
		etcd_load = etcd:command "load" "fill"
			:description "Loads subtree from file"
			:target "etcd_load"

		etcd_load:flag "--delete"
			:description "etcd load will delete keys in ETCD those do not exist in the source"
			:target "etcd_load_delete"

		etcd_load:flag "--append-only"
			:description "etcd load only adds new keys and values but does not change existing (can be used with --delete)"
			:target "etcd_load_append_only"

		etcd_load:flag "--dry-run" "-n"
			:description "Prints each step but does not change ETCD"
			:target "etcd_load_dry_run"

		etcd_load:argument "path"
			:description "Path of ETCD subtree"
			:target "etcd_path"
			:args "1"

		etcd_load:argument "input"
			:target "etcd_input"
			:description "Path to input file or stdin"
			:convert(function(f)
				if f == "@-" then
					return io.stdin
				end
				return assert(io.open(f))
			end)
			:args "1"
			:default "@-"

		etcd_load:option "-f" "--input-format"
			:target "etcd_load_input_format"
			:description "Format of the input file"
			:choices { "json", "yaml" }
			:default "yaml"
	end

	local etcd_rm
	do
		etcd_rm = etcd:command "rm"
			:description "Removes given path"
			:target "etcd_rm"

		etcd_rm:flag "-r" "--recursive"
			:target "etcd_recursive"
			:description "Recursively removes directory"

		etcd_rm:argument "path"
			:target "etcd_path"
			:args "1"
	end

	local etcd_rmdir
	do
		etcd_rmdir = etcd:command "rmdir"
			:description "Removes directory if it is empty"
			:target "etcd_rmdir"

		etcd_rmdir:argument "path"
			:target "etcd_path"
			:args "1"
	end

	local etcd_wait
	do
		etcd_wait = etcd:command "wait"
			:description "Waits for change in subpath"
			:target "etcd_wait"

		etcd_wait:argument "path"
			:target "etcd_path"
			:args "1"

		etcd_wait:option "--timeout"
			:description "Timeout of wait"
			:default "30"
			:convert(tonumber)

		etcd_wait:flag "-r" "--recursive"
			:target "etcd_recursive"
			:description "Recursive etcd wait"

		etcd_wait:option "-i" "--index"
			:target "etcd_index"
			:description "etcd-index to watch from"
			:convert(tonumber)
	end

	local etcd_validate
	do
		etcd_validate = etcd:command "validate"
			:description "Validates config stored in ETCD"
			:target "etcd_validate"

		etcd_validate:argument "path"
			:target "etcd_path"
			:args "1"

		etcd_validate:flag "--ero"
			:target "etcd_read_only"
			:description "Allows ReadOnly requests from ETCD if ETCD leader is not discovered"
			:default(false)

		etcd_validate:option "--policy"
			:target "etcd_policy"
			:description "Parsing policy"
			:choices { "etcd.cluster.master", "etcd.cluster.vshard", "etcd.cluster.raft", "etcd.instance.single" }
			:default "etcd.cluster.master"
	end

	local etcd_create
	do
		etcd_create = etcd:command "create"
			:description "Creates new ETCD config for application"
			:target "etcd_create"

		etcd_create:argument "name"
			:target "etcd_create_name"
			:description "Application name"
			:args "1"

		etcd_create:option "--policy"
			:target "etcd_policy"
			:description "Parsing policy"
			:choices { "etcd.cluster.master", "etcd.cluster.vshard", "etcd.cluster.raft", "etcd.instance.single" }
			:default "etcd.cluster.master"
	end

	local etcd_version = etcd:command "version"
		:description "Returns ETCD version"
		:target "etcd_version"

	etcd_version:flag "--ero"
		:target "etcd_read_only"
		:description "Allows ReadOnly requests from ETCD if ETCD leader is not discovered"
		:default(false)

	local etcd_health = etcd:command "health"
		:description "Checks is ETCD is healthy"
		:target "etcd_health"

	etcd_health:flag "--ero"
		:target "etcd_read_only"
		:description "Allows ReadOnly requests from ETCD if ETCD leader is not discovered"
		:default(false)
end


function read_password(prompt_message)
	local asterisk_char = '*'
	local max_length = 64
	-- returns password string
	-- "Enter" key finishes the password
	-- "Backspace" key undoes last entered character
	local backspace_key = '\127'
	io.write(prompt_message or 'Enter password: ')
	io.flush()
	os.execute 'stty -echo raw'

	local passwd = ''
	repeat
		local c = io.read(1)
		if c == backspace_key then
			if #passwd > 0 then
				io.write '\b \b'
				passwd = passwd:sub(1, -2)
			end
		elseif c ~= '\r' and #passwd < max_length then
			io.write(asterisk_char)
			passwd = passwd .. c
		end
		io.flush()
	until c == '\r'
	os.execute 'stty sane'

	io.write '\n'
	io.flush()
	return passwd
end

main(switchover:parse())
