#!/bin/sh

while true; do
	if curl -s --fail --connect-timeout 10 http://etcd0:2379/v2/members/leader; then
		break;
	fi;
	sleep 1;
done;

while ! switchover --no-config --etcd http://etcd0:2379 --prefix / etcd ls /; do
	sleep 1;
done;

luatest --coverage -c -v switchover/ || exit $?;

exit 0;
